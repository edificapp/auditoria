let user = document.head.querySelector('meta[name="user"]');
let teams = document.head.querySelector('meta[name="teams"]');

module.exports = {
    computed: {
        user(){
            return JSON.parse(user.content);
        },
        check(){
            return !! user.content;
        },
        /*
        teams(){
            return JSON.parse(teams.content);
        },
        checkTeam(){
            return teams.length > 0 ? true :false;
        }
        */
    }
};