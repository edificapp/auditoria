
const store = {
    state :{
        terceros: [],
        terceros_proyecto: [],
        tercero_proyecto_select:null,
        prefix:'/api-terceros'
    },
    getters:{
        getTerceros(state){
            return state.terceros;
        },
        getTercerosProyecto(state){
            return state.terceros_proyecto
        },
        getTerceroProyectoSelect(state){
            return state.tercero_proyecto_select;
        },
        getTerceroInstituciones(state){
            let array = ['sin institucion educativa'];
            let sedes = state.tercero_proyecto_select.sedes.filter(i => i.tipo == 'instituto').forEach(e => array.push(e.nombre));
            return array;
        }
    },
    mutations:{
        setTerceros(state,terceros){
            state.terceros = terceros
        },
        setTercerosProyecto(state,terceros_proyecto){
            state.terceros_proyecto = terceros_proyecto
        },
        setTerceroProyectoSelect(state,tercero_proyecto){
            state.tercero_proyecto_select = tercero_proyecto
        },
        setTercerosParametro(state, parametro){
            let index = state.terceros_proyecto.findIndex(e => e.id == state.tercero_proyecto_select.id);
            state.terceros_proyecto[index].parametros.push(parametro);
        },
        setUpdateTerceroDelProyecto(state, tercero){
            state.terceros_proyecto.forEach(function(item, key){
                if(item.id == tercero.id){
                    state.terceros_proyecto[key] = tercero;
                }
            });
        }
    },
    actions:{
        async AllTerceros({commit, state}){
            let res = await axios
            .get(state.prefix)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setTerceros', res);
        },
        async listarTercerosProyecto({commit,state}){
            let res = await axios
            .get(`${state.prefix}/proyecto/${this.getters.getProyecto.id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setTercerosProyecto', res);
        },
        async agregarTerceroProyecto({commit, state}, data){
            let res = await axios 
            .post(`${state.prefix}/proyecto/relacionar/${this.getters.getProyecto.id}`, data)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })              
            commit('setTercerosProyecto', res);
        },

        async guardarSedes({commit,state,getters}, sedes){
            return await axios
            .post(`${state.prefix}/sedes/${getters.getTerceroProyectoSelect.id}`, {sedes:sedes})
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
        },
        async guardarInstitutos({commit,state,getters}, sedes){
            return await axios
            .post(`${state.prefix}/institutos/${getters.getTerceroProyectoSelect.id}`, {sedes:sedes})
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
        },
        async actualizarSedes({commit,state,getters}, sedes){
            return await axios
            .patch(`${state.prefix}/sedes-u/${getters.getTerceroProyectoSelect.id}`, {sedes:sedes})
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
        },
        async actualizarInstituciones({commit,state,getters}, sedes){
            return await axios
            .patch(`${state.prefix}/instituciones-u/${getters.getTerceroProyectoSelect.id}`, {sedes:sedes})
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
        },
        async agregarParametroTercero({commit,state}, data){
            let res = await axios
            .post(`${state.prefix}/parametros/${state.tercero_proyecto_select.id}`, data)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setTercerosParametro', res);
        },
        async actualizarParametroTercero({commit,state}, data){
            let res = await axios
            .patch(`${state.prefix}/parametros/${data.parametro_id}`, data.datos)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
        },
        async eliminarParametroTercero({commit,state}, parametro_id){
            let res = await axios
            .delete(`${state.prefix}/parametros/${parametro_id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
        }
    }
}

export default store
