
const store = {
    state :{
        teams:[]
    },
    getters:{
        getTeams(state){
            return state.teams;
        }
    },
    mutations:{
        setTeams(state,teams){
            state.teams = teams
        },
        setUpdateTeam(state, res){
            let team = state.teams.each(i => {
               if( i.id = res.id)
               {
                   i = res;
               }
            })
        },
        setNewTeam(state, res){
            state.teams.push(res);
        },
        setDestroyTeam(state, index){
            state.teams.splice( index, 1 );
        },
        setTeamUpdateCountReadMessage(state, id){
            state.teams.forEach(function(e){
                if(e.id == id){
                    e.count_is_read = parseInt(e.count_is_read)+1;
                }
            });
        }
    },
    actions:{
        async listTeams({commit}){
            let res = await axios 
            .get(`/equipos`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setTeams', res);
        },
        async destroyMemberTeam({commit}, data){
            let res = await axios 
            .delete(`/equipos/members/${data.team_id}/${data.user_id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setUpdateTeam', res);
        },
        async enviarInvitacion({commit}, data){
            let res = await axios 
            .post(`/equipos/members/${data.team_id}`, {email:data.email})
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setUpdateTeam', res);
        },
        async storeTeam({commit}, data){
            let res = await axios 
            .post(`/equipos/create`, data)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setNewTeam', res);
        },
        async updateTeam({commit}, datos){
            let res = await axios 
            .put(`/equipos/edit/${datos.team_id}`, datos.data)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setNewTeam', res);
        },
        async destroyTeam({},id){
            await axios 
            .delete(`/equipos/destroy/${id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            }) 
        }
    }
}

export default store
