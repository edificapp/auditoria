import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(Vuex)
Vue.use(VueAxios, axios)

import moduloProyecto from './proyecto'
import moduloTercero from './tercero'
import moduloNotificacion from './notificacion'
import modulopersonalProyecto from './personalProyecto'
import moduloVisita from './visita'
import moduloTeams from './teams'
import moduloUsers from './users'
import moduloChat from './chat'

export default new Vuex.Store({
    modules:[
        moduloProyecto,
        moduloNotificacion,
        moduloTercero,
        modulopersonalProyecto,
        moduloVisita,
        moduloTeams,
        moduloUsers,
        moduloChat
    ]
})
