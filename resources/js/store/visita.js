
const store = {
    state :{
        visitas:[],
        prefix:'/api-visitas'
    },
    getters:{
        getVisitas(state){
            return state.visitas;
        }
    },
    mutations:{
        setVisitas(state,visitas){
            state.visitas = visitas
        },
        setAgregarVisita(state,visita){
            state.visitas.push(visita);
        },
        setActualizarVisita(state,visita){
            let visitaIndex = state.visitas.findIndex(e => e.id == visita.id);
            if(visitaIndex !== -1 ) {
                state.visitas[visitaIndex] = visita;
            }
        },
        setEliminarVisita(state, id){
           let visitaIndex = state.visitas.findIndex( e => e.id == id);
            if ( visitaIndex !== -1 ) {
                 state.visitas.splice( visitaIndex, 1 );
            }
        }
    },
    actions:{
        async visitasProyecto({commit, state}){
            let res = await axios 
            .get(`${state.prefix}/proyecto/${this.getters.getProyecto.id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setVisitas', res);
        },
        async generarVisita({commit, state}, data){
            let res = await axios 
            .post(`${state.prefix}/proyecto/${this.getters.getProyecto.id}`, data)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setAgregarVisita', res);
        },
        async actualizarVisita({commit, state}, datos){
            let res = await axios 
            .patch(`${state.prefix}/edit/${datos.visita_id}`, datos.data)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setActualizarVisita', res);
        },
        async eliminarVisita({commit, state}, id){
            let res = await axios 
            .delete(`${state.prefix}/destroy/${id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            if(res){
                commit('setEliminarVisita', id);
            }
        }
    }
}

export default store
