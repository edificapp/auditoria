
const store = {
    state :{
        coordinadores:[],
        inspectores:[],
        supervisores:[]
    },
    getters:{
        getCoordinadores(state){
            return state.coordinadores;
        },
        getInspectores(state){
            return state.inspectores;
        },
        getSupervisores(state){
            return state.supervisores;
        },
    },
    mutations:{
        setCoordinadores(state, item){
            return state.coordinadores = item;
        },
        setInspectores(state, item){
            return state.inspectores = item;
        },
        setSupervisores(state, item){
            return state.supervisores = item;
        },
    },
    actions:{
        async personalProyecto({commit}){
            let res = await axios
            .get(`/proyecto/${this.getters.getProyecto.id}/personal`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setCoordinadores', res.coordinadores);
            commit('setInspectores', res.inspectores);
            commit('setSupervisores', res.supervisores);
        }
    }
}

export default store
