
const store = {
    state :{
        proyecto: null,
        personal_proyecto:[],
        grupos_formularios:[]
    },
    getters:{
        getProyecto(state){
            return state.proyecto
        },
        getPersonalProyecto(state){
            return state.personal_proyecto
        },
        getGrupoFormulariosProyecto(state){
            return state.grupos_formularios;
        }
    },
    mutations:{
        setProyecto(state,proyecto){
            state.proyecto = proyecto
        },
        setProyectoFieldUpdate(state, data){
            state.proyecto[data.field] = data.value
        },
        setPersonalProyecto(state, data){
            state.personal_proyecto = data;
        },
        setGrupoFormulariosProyecto(state, data){
            state.grupos_formularios = data;
        }
    },
    actions:{
        async updateFieldProyecto({commit}, data){
            let res = await axios
            .post(data.url, data.datos)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })

            commit('setProyecto', res.proyecto);
            return res.message
        },
        async getPersonalProyecto({commit}, ){
            let res = await axios
            .get(`/proyecto/${this.getters.getProyecto.id}/personal`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })

            commit('setPersonalProyecto', res);
            return res
        },
        async getGrupoFormulariosProyecto({commit}, ){
            let res = await axios
            .get(`/api-gformulario/${this.getters.getProyecto.id}`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })

            commit('setGrupoFormulariosProyecto', res);
            return res
        }
    }
}

export default store
