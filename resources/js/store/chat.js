
const store = {
    state :{
        active_chat:null,
        tab_chat:'users'
    },
    getters:{
        getActiveChat(state){
            return state.active_chat;
        },
        getTabChat(state){
            return state.tab_chat;
        }
    },
    mutations:{
        setActiveChat(state,chat){
            state.active_chat = chat
        },
        setTabChat(state,type){
            state.tab_chat = type
        },
    },
    actions:{
        /*
        async getUsers({commit, state}){
            let res = await axios 
            .get(`/users`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setUsers', res);
        }
        */
    }
}

export default store
