
const store = {
    state :{
    },
    getters:{
    },
    mutations:{
    },
    actions:{
        notificationSuccess({},data){
            $.toast({
                heading:data.heading,
                text: data.message,
                showHideTransition: 'slide',
                icon: 'success',
                position : 'top-right',
                bgColor : 'green', 
                textColor : '#eee', 
            })
        },
        notificationDanger({},data){
            $.toast({
                heading:data.heading,
                text: data.message,
                showHideTransition: 'slide',
                icon: 'success',
                position : 'top-right',
                bgColor : 'red', 
                textColor : '#eee', 
            })
        },
        notificationInfo({},data){
            $.toast({
                heading:data.heading,
                text: data.message,
                showHideTransition: 'slide',
                icon: 'success',
                position : 'top-right',
                bgColor : 'blue', 
                textColor : '#eee', 
            })
        },
    }
}

export default store