
const store = {
    state :{
        users:[]
    },
    getters:{
        getUsers(state){
            return state.users;
        }
    },
    mutations:{
        setUsers(state,users){
            state.users = users
        },
        setUserUpdateCountReadMessage(state, id){
            state.users.forEach(function(e){
                if(e.id == id){
                    e.count_is_read = parseInt(e.count_is_read)+1;
                }
            });
        },
        setUserResetCountChat(state, id){
            state.users.forEach(function(e){
                if(e.id == id){
                    e.count_is_read = 0;
                }
            });
        }
    },
    actions:{
        async listUsers({commit, state}){
            let res = await axios 
            .get(`/users`)
            .then(r => r.data)
            .catch(err =>{
                console.error(err)
            })  
            commit('setUsers', res);
        }
    }
}

export default store
