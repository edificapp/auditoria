require("./bootstrap");

// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';


//import Vue from "vue";
window.Vue = require('vue');
import store from "./store";
import Vuetify from 'vuetify'
Vue.use(Vuetify)
import 'vuetify/dist/vuetify.min.css'

import "vue-select/dist/vue-select.css";
import auth from "./mixins/auth";

import CKEditor from '@ckeditor/ckeditor5-vue2';
Vue.use( CKEditor );

Vue.config.productionTip = false
//import notifications from "./mixins/notifications";

window.EventBus = new Vue();

Vue.mixin(auth);

const VueUploadComponent = require('vue-upload-component')
Vue.component('file-upload', VueUploadComponent)
Vue.component('PrivateChat', require('./components/Messenger/PrivateChat.vue').default);

Vue.component("v-select-custom", require("./components/Vselect.vue").default);
Vue.component("v-select-multi", require("./components/Vselectmulti.vue").default);
Vue.component("informacion-adicional", require("./components/InformacionAdicional/InformacionAdicional.vue").default);
//Vue.component("ejecucion", require("./components/ProyectoEjecucion/Ejecucion.vue").default);
Vue.component("proyecto-ejecucion", require("./components/ProyectoEjecucion/Ejecucion.vue").default);

const app = new Vue({
    el: "#app",
    store
});



   

