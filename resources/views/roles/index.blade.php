@extends('layouts.app')

@section('titulo')
Listar Roles
@endsection

@section('contenido')
<div class="row ">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><b>Roles</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            @can('crear-roles')
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('roles.create') }}';" 
                class="btn-create-form">Nuevo Rol</button>
            </div>
            @endcan
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 stretch-card">
        <div class="card">
            <div class="card-body borde ">
                <p class="card-title">Listado de Roles</p>
                <div class="table-responsive">
                    <table class="display" id="table" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Rol</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (auth()->user()->empresa->roles as $index => $rol)
                                <tr class="body-rows">
                                    <td style="width:5%">{!! $index+1 !!}</td>
                                    <td>{{strtolower($rol->name)}}</td>
                                    <td style="width:5%">
                                        <div class="btn-group">
                                            @can('editar-roles')
                                                <button class="btn-edit-form" type="button" 
                                                    onclick="location.href='{{ route('roles.edit',$rol->id) }}';" 
                                                    title="editar rol"
                                                >
                                                </button>
                                            @endcan
                                            <form method="post" action="{{route('roles.destroy', $rol->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                    <button class="btn-delete-form " type="submit" title="eliminar rol"></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
