@extends('layouts.app')

@section('titulo')
Crear Rol
@endsection

@section('styles')
    <style type="text/css">
        .permisos{
            margin-left: 15px;
        }

        hr { 
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }
    </style>
@stop

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><b>Crear Rol</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body borde">
                    <h4 class="card-title">Agregar Nuevo Rol</h4>
                    <form method="POST" action="{{ route('roles.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group my-3">
                                    <label for="rol" class="input-group-text bg-light col-5-sm">Nombre del Rol</label>
                                    <input type="text" class="form-control col-sm-8 @error('rol') is-invalid @enderror" 
                                        name="rol" value="{{ old('rol') }}" 
                                        placeholder="Nombre del Rol"
                                        id="rol">
                                    @error('rol')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <center><p class="h3">Permisos</p></center>
                        <hr>
                            <div id="accordion">
                                <div class="row">
                                    @foreach ($modulos->chunk($modulos->count()/2) as $chunk)
                                    <div class='col-6'>
                                        @foreach($chunk as $key => $modulo)
                                            <div class="mt-3" id="heading">
                                                <h5 class="mb-0">
                                                    <span class="collapsed pd-2 my-1" data-toggle="collapse" data-target="#acordion{{$key}}" aria-expanded="false" aria-controls="{{$modulo}}">
                                                        <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>{{$modulo}}
                                                        <input type="checkbox" name="modulo[]" value="{{$key}}" class="form-check-input modulo ml-2" id="{{Str::slug($modulo, '-')}}">
                                                    </span>
                                                </h5>
                                            </div>
                                            
                                            <div id="acordion{{$key}}" class="collapse" aria-labelledby="heading" data-parent="#accordion">
                                                <div class="card-body">
                                                    @foreach($permission->filter(function($item) use ($modulo){ return $item->modulo == $modulo; }) as $permiso)
                                                        <div class="form-check">
                                                            <label class="form-check-label" for="">{{$permiso->alias}}
                                                                <input type="checkbox" name="permission[]" value="{{$permiso->id}}" class="form-check-input  {{Str::slug($modulo, '-')}}">
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                </div>    
                            </div>
                        <hr>
                        <div class="row justify-content-md-center">
                            <div class="form-group">
                                <button type="button" onclick="location.href='{{ route('roles.index') }}';" class="btn btn-secondary">Regresar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
         $('.modulo').on('change', function(){
            $( "."+this.id ).prop('checked', $(this).prop("checked"));
        });
    </script>
@stop
