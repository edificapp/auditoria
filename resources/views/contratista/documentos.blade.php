@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home breadcrumbs hover-cursor">&nbsp;/&nbsp;</i></a>
                    <p class="breadcrumbs mb-0 hover-cursor">Inicio</p>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Mis Documentos del projecto {{$directorio->proyecto->nombre}}</p>
             </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-center">Mis Documentos del projecto {{$directorio->proyecto->nombre}}</p>
             </div>
        </div>
    </div>
</div>
<div class="row justify-content-md-center">
    <div class="col-md-10 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form   action="{{ route('documentos.store', $directorio->id)}}" class="form" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        {{--
                        <input type="file" name="documentos[]" class="form-control" required multiple/>
                            --}}
                        <input type="file" id="certificado_basico" name="documentos[]" class="file-upload-default" required multiple/>
                        <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled="" placeholder="Cargar Archivos">
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-info" type="button">Cargar Archivos</button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </form>
             </div>
        </div>
    </div>
</div>
<div class="row justify-content-md-center">
    <div class="col-md-10 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive text-center">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Fecha</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($directorio->radicados as $radicado)
                                @foreach ($radicado->documentos as $documento)
                                <tr>
                                   <td>
                                       <a class="nav-link" href="{{ $documento->ruta_storage }}" target="_blank">
                                            <i class="mdi {{$documento->tipo($documento->name_document['type'])}} "></i>
                                            <span class="menu-title">{{$documento->name_document['name']}}</span>
                                        </a>
                                    </td>

                                    <td>
                                        {{ $documento->created_at}}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary " href="{{ route('documento.borrado', $documento->id) }}" >
                                             <i class="mdi mdi-close "></i>

                                         </a>
                                     </td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
             </div>
        </div>
    </div>
</div>

           
@endsection

@section('scripts')
<script>

    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
