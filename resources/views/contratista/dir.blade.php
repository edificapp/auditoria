@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home breadcrumbs hover-cursor">&nbsp;/&nbsp;</i></a>
                    <p class="breadcrumbs mb-0 hover-cursor">Inicio</p>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card p-3">
            <div class="card-heard text-center">
                Proyectos.
            </div>
        </div>
    </div>
</div>
@foreach($directories as $dir)
<div class="row">
    <a href="{{Auth::user()->rol->nombre == 'Contratista' ? route('documentos.contratista', $dir->id) : route('documentos.perfil.contratistas', $dir->proyecto->id)}}">
        <div class="col-xs-12 col-sm-6 col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body ">
                    <img src="{{asset('img/folder.png')}}" class="img-fluid">
                     <b>
                        <small>
                            {{$dir->proyecto->nombre}}
                        </small>
                    </b>
                </div>
            </div>
        </div>
    </a>
</div>
@endforeach
@endsection

@section('scripts')
<script>

    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
