<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form method="post" action="{{route('import.store', $tipo)}}" enctype="multipart/form-data">
		@csrf
		<input type="file" name="file">
		@if($errors->has('file'))
		    <div class="error">{{ $errors->first('file') }}</div>
		@endif
		<input type="submit" value="enviar">
	</form>
</body>
</html>