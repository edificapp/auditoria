<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title></title>
	<style type="text/css">
		body { 
			margin: 4px;
			font-size: 10px;
		 }

		 .amarillo{
		 	border: 1px solid yellow;
		 }
		 .azul{
		 	border: 1px solid blue;
		 }

		 .rojo{
		 	border: 1px solid red;
		 }

		 .s7{width: 7%; display: inline-block;}
		 .s17{width: 17%; display: inline-block;}
		 .s57{width: 57%; display: inline-block; bottom-top: 10px; bottom:10px;}

		 .s57 p{font-size: 12px; }
		 .br-black-1 p{font-size: 15px; }

		 .hrFecha { 
		 	border-style: double;
		  
		} 

		.hr0margin{
			margin-bottom: 0px;
			margin-bottom: 0px;
		}

		.br-black-1{
			border: 1px solid black;
		}
	</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<p>
					<h2>
						{{$user->empresa->nombre}}
					</h2>
				</p>
			</div>
		</div>
		<div class="row">
			<h1 class="text-center">Radicado</h1>
		</div>
		<br><br><br>
		<div class="row">
			<p class="text-justify" style="font-size: 16px;">
				Se Radica a las {{Carbon\Carbon::now()->format("H:i")}} el dia {{Carbon\Carbon::now()->isoFormat("LL")}} a {{$nombre}} el radicado #{{$radicadoConteo}}. <br><br>
				Se enlista los archivos subidos en este radicado.
			</p>
		</div>
		<br><br>
		<div class="row">
			<ul style="font-size: 14px;">
				@foreach($radicado->documentos as $item)
				<li>
					{{$item->name_document['name']}}
				</li>
				@endforeach
			</ul>
		</div>
	</div>

</body>
</html>