<!DOCTYPE html>
<html lang="es">

<head>
    <title>GAP - @yield('titulo')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('admin/plugins/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/base/vendor.bundle.base.css') }}">        
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.css') }}">        
    {{-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}

    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
     <link rel="shortcut icon" href="{{ asset('img/corporativo/G.png') }}" />
    
    {{-- @yield('styles') --}}
</head>
<body>
    <div class="container-fluid">
        <div class="content-wrapper">
            @yield('contenido')
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{ asset('admin/plugins/base/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('admin/js/off-canvas.js') }}"></script>
    <script src="{{ asset('admin/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('admin/js/template.js') }}"></script>
    
    <script src="{{ asset('admin/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/js/dataTables.bootstrap4.js') }}"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    
    @include('layouts.nswal')

    @yield('scripts')
</body>

</html>

