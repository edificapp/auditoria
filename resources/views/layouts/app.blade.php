<!DOCTYPE html>
<html lang="es">

<head>
    <title>GAP - @yield('titulo')</title>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="user" content="{{Auth::user()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('admin/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/base/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700"/>

    {{-- hoja de estilos para los botones y breadcrumbs--}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('admin/plugins/toast/jquery.toast.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('img/corporativo/G.png') }}" />
    <link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet">
    <style>
        /* .fa-stack-1x{
            font-size: 10px !important;
        } */
        .icon-letter{
            font-size: 30px !important;
        }

        .text-notify{
            color:white;
            font-size:10px;
            font-weight:800;
            
        }

        .notify{
            color:red;
            position:absolute;
            margin-top:1px;
            margin-left:-10px;
            background-color: #FF0000 ;
            /* border-radius: 10px;
            width:25px; */

            width:15px;
            height:15px;
            
            padding:1px;

            border-radius: 200px 200px 200px 200px;
            -moz-border-radius: 200px 200px 200px 200px;
            -webkit-border-radius: 200px 200px 200px 200px;
            border:none;
        } 
    </style>
    @yield('styles')
</head>
<body>
    <div class="container-scroller">
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="navbar-brand-wrapper d-flex justify-content-center">
                <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
                    <a class="navbar-brand brand-logo-mini" href="{{ route('home') }}">
                        <img src="{{Auth::user()->empresa ? Auth::user()->empresa->logo_movil : url('img/corporativo/G.png')}}" alt="logo"/> 
                    </a>
                    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                        <span class="mdi mdi-sort-variant"></span>
                    </button>
                </div>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <ul class="navbar-nav mr-lg-4 w-100">
                  <li class="nav-item nav-search d-none d-lg-block w-100">
                    @yield('breadcrumbs')
                  </li>
                </ul>
                <span class="fa-stack ">
                    <i class="fa fa-email fa-stack-2x"></i>
                    <strong class="fa-stack-1x fa-stack-text fa-inverse">5</strong>
                  </span>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link count-indicator d-flex align-items-center justify-content-center btn  btn-rounded btn-icon" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                            <span class="mx-center position-relative">
                                <i class="mdi mdi-email-outline icon-letter" style="color:#1976D2; "></i>
                                <span class="notify">
                                    <strong class="text-notify"  >
                                        {{Auth::user()->unreadnotifications->count()}}
                                    </strong>
                                </span>
                                
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                          <p class="mb-0 font-weight-normal float-left dropdown-header text-primary col-12 text-center">Notificaciones</p>
                          <div class="dropdown-divider"></div>
                          @if(Auth::user()->unreadnotifications->count() > 0)
                              @foreach(Auth::user()->unreadnotifications->take(5) as $key => $item)
                              <a class="dropdown-item preview-item {{$key % 2 ==0 ? 'bg-gradient-light' : 'bg-gradient-secondary'}}" href="{{route('notificacion.link', $item->id)}}" title="{{$item->remitente->curriculum && $item->remitente->curriculum->nombres != NULL ? $item->remitente->curriculum->full_name : $item->remitente->nombre}}">
                                <div class="preview-thumbnail mr-2">
                                    <img src="{{$item->remitente->avatar_user}}" alt="perfil Remitente" class="profile-pic rounded-circle" width="40" height="40">
                                </div>
                                <div class="preview-item-content flex-grow">
                                  <h6 class="preview-subject ellipsis font-weight-normal">
                                        {{$item->fecha_human}}
                                  </h6>
                                  <p class="font-weight-light small-text text-muted mb-0">
                                    {{$item->tipo}}
                                  </p>
                                </div>
                              </a>
                              @endforeach
                            @else
                                <p class="col-12 text-center"><small >No tienes Notificaciones</small></p>
                            @endif
                          <div class="dropdown-divider"></div>
                          @if(Auth::user()->unreadnotifications->count() > 0)
                              <a class="dropdown-item text-center" href="{{route('notificaciones')}}">
                                    Mas Notificaciones ...
                              </a>
                          @endif
                        </div>
                    </li>

                    @if(session()->has('old_personificado_id'))
                        <a href="{{route('personificacion-borrar')}}" class="btn btn-sm btn-inverse-danger" title="volver a mi cuenta">
                            <i class="mdi mdi-account-off"></i>
                        </a>
                    @endif
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <span class="nav-profile-name">{!! Auth::user()->nombre !!}</span>
                            <img 
                            src="{{Auth::user()->curriculum ? Auth::user()->curriculum->avatar : asset('admin/images/usuario/usuario.jpg')}}"
                            alt="{!! Auth::user()->email !!}"/>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            @if(Auth::user()->inv_activa == 'no')
                                <a class="dropdown-item" href="{{ route('perfil') }}">
                                    <i class="mdi mdi-account-box text-primary"></i>
                                    Mi perfil
                                </a>
                                <a class="dropdown-item" href="{{route('curriculum.index')}}">
                                    <i class="mdi mdi-account-box text-primary"></i>
                                    Hoja de vida
                                </a>
                            @endif
                            <a href="{{ route('salir') }}" class="dropdown-item">
                                <i class="mdi mdi-logout text-primary"></i>
                                Cerrar sesión
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <div class="container-fluid page-body-wrapper">
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a  href="{{ route('home') }}">
                            <img src="{{Auth::user()->empresa ? Auth::user()->empresa->logo_web : url('img/corporativo/logo-abreviado-fergon-sinLetras.png')}}" alt="logo" class="img-fluid"/> 
                        </a>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link">
                            <i class="mdi mdi-magnify menu-icon"></i>
                            <span class="menu-title">
                                <input type="text" class="form-control" placeholder="Buscar"  aria-describedby="search">
                            </span>
                        </span>
                    </li>
                @if(Auth::user()->inv_activa == 'no')
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('home') }}">
                                <i class="mdi mdi-home menu-icon"></i>
                                <span class="menu-title">Inicio</span>
                            </a>
                        </li>
                        @if(Auth::user()->is_admin)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('empresas.index') }}">
                                <i class="mdi mdi-desktop-tower menu-icon"></i>
                                <span class="menu-title">Empresas</span>
                            </a>
                        </li>
                        @endif
                        @can('ver-agenda')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('eventos.index') }}">
                                <i class="mdi mdi-calendar-range menu-icon"></i>
                                <span class="menu-title">Agenda</span>
                            </a>
                        </li>
                        @endcan
                        @can('ver-informacion-empresa')
                        <li class="nav-item" title="{{Auth::user()->empresa->nombre}}">
                            <a class="nav-link" href="{{ route('empresa.informacion') }}">
                                <i class="mdi mdi-factory menu-icon"></i>
                                <span class="menu-title">{{Auth::user()->empresa->siglas == '' ? Str::limit(Auth::user()->empresa->nombre, 24) : Auth::user()->empresa->siglas}}</span>
                            </a>
                        </li>
                        @endcan
                        @can('listar-empleados')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('usuarios.index') }}">
                                <i class="mdi mdi-account-card-details menu-icon"></i>
                                <span class="menu-title">Empleados</span>
                            </a>
                        </li>
                        @endcan
                        @can('listar-proveedores')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('contactos.listar', 'proveedor') }}">
                                <i class="mdi mdi mdi-city menu-icon"></i>
                                <span class="menu-title">Entidades</span>
                            </a>
                        </li>
                        @endcan
                        @can('listar-directorio')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('directorio.index') }}">
                                <i class="mdi mdi-account-multiple-outline menu-icon"></i>
                                <span class="menu-title">Directorio</span>
                            </a>
                        </li>
                        @endcan
                        @can('crm')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('crm.index') }}">
                                <i class="mdi mdi-account-multiple-outline menu-icon"></i>
                                <span class="menu-title">CRM</span>
                            </a>
                        </li>
                        @endcan
                        @can('listar-propuestas')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('propuestas.index') }}">
                                <i class="mdi mdi-bullseye menu-icon"></i>
                                <span class="menu-title">Propuestas</span>
                            </a>
                        </li>
                        @endcan
                        @can('listar-proyectos')
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('proyectos.index') }}">
                                <i class="mdi mdi-printer menu-icon"></i>
                                <span class="menu-title">Proyectos</span>
                            </a>
                        </li>
                        @endcan
                        <li class="nav-item">
                            <a href="{{route('correspondencia.index')}}" class="nav-link">
                                <i class="mdi mdi-email-open-outline menu-icon"></i>
                                <span class="menu-title">Correspondencia</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('archivo.index')}}" class="nav-link">
                                <i class="mdi mdi-archive menu-icon"></i>
                                <span class="menu-title">Archivo</span>
                            </a>
                        </li>
                        @can('listar-roles')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('roles.index') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Configuración</span>
                            </a>
                        </li>
                        @endcan
                @endif
                </ul>
            </nav>

            <div class="main-panel" id="main-panel">
                <div class="content-wrapper">
                    @yield('contenido')
                </div>
                <footer class="footer" style="padding: 10px !important; ">
                    <div class="col-md-12">
                        <center>
                            <img src="{{asset('img/corporativo/logo-abreviado-fergon-sinLetras.png')}}" height="30" id="img-gap-footer">
                        </center>
                    </div>
                </footer>
            </div>
        </div>
    </div>

    {{--
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        --}}

    <script src="{{ asset('js/app.js') }}"></script>
 
    <script src="{{ asset('admin/plugins/base/vendor.bundle.base.js') }}"></script>
    
    <script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('admin/js/off-canvas.js') }}"></script>
    <script src="{{ asset('admin/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('admin/js/template.js') }}"></script>
    {{--
        <script src="{{ asset('admin/js/file-upload.js') }}"></script>
        
    --}}

    <script src="https://cdn.datatables.net/buttons/1.5.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.0/js/buttons.html5.min.js"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    {{-- file input--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/themes/fa/theme.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/locales/es.js"></script>
    <script src="{{ asset('admin/plugins/toast/jquery.toast.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/translations/es.js"></script>
    <script src="{{ asset('js/config.js') }}"></script>
    @include('layouts.nswal')
    @yield('scripts')

</body>

</html>

