@extends('layouts.app')

@section('titulo')
Notificaciones
@endsection

@section('styles')
    
@stop


@section('breadcrumbs')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Notificaciones</li>
      </ol>
    </nav>
@stop

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Notificaciones</h2>
                    <div class="d-flex">
                        <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Notificaciones&nbsp;/&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-md-center">
    <div class="col-md-8 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover table-sm">
                    <thead class="thead-dark">
                        <td>Imagen</td>
                        <td>Titulo</td>
                        <td>Fecha</td>
                    </thead>
                    <tbody>
                    @foreach(Auth::user()->unreadnotifications as $item)
                    <tr data-href="{{route('notificacion.link', $item->id)}}">
                        <td>
                            <img src="{{$item->remitente->curriculum ? $item->remitente->curriculum->avatar : asset('admin/images/usuario/usuario.jpg')}}" alt="perfil Remitente" class="profile-pic rounded-circle" width="40" height="40">
                        </td>
                        <td>{{$item->tipo}}</td>
                        <td>{{$item->fecha_human}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
       $(document).ready( function () {
            $('.table').DataTable();
            $('.table').on( 'click', 'tbody tr', function () {
              window.location.href = $(this).data('href');
            });
        } );
    </script>
@stop
