@extends('layouts.app')

@section('titulo')
Crear formulario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('formularios.index') }}">Formularios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Crear formulario</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <form id="form_forms" method="POST" action="{{ route('formularios.store') }}">
                    @csrf
                    <div class="row my-3">
                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="nombre" class="input-group-text bg-light">{{ __('Name') }}</label>
                                <input id="nombre" type="text" class="form-control col-12 @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="estado" class="input-group-text bg-light">Estado</label>
                                <select id="estado" type="text" class="form-control col-12 @error('activo') is-invalid @enderror" name="activo" required autofocus>
                                    <option value="no">Inactivo</option>
                                    <option value="si" selected>Activo</option>
                                </select>

                                @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>
                    <span class="invalid-feedback" role="alert" id="alert-error-formulario">
                        </span>
                    <div id="fb-editor"></div>
                    <hr>

                    <div class="row m-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" onclick="location.href='{{ route('formularios.index') }}';"> Regresar </button>
                            <button type="button" class="btn btn-primary submit_class">Crear</button>
                        </div>
                    </div>

                    <input type="hidden" name="arreglos_campos" id="arreglos_campos">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
<script>

    jQuery(function($) {
        let options = {
            disableFields: [
                'autocomplete','button','hidden','file'
            ],
            showActionButtons: false,
        };

        let fb_ed = $(document.getElementById('fb-editor')).formBuilder(options);
        @if($errors->any())
            setTimeout(function() {
                fb_ed.actions.setData(JSON.parse('{!! old('arreglos_campos') !!}'));
            }, 200);
        @endif
        
        $('.submit_class').on('click', function(event) {
            let data  = fb_ed.actions.getData();
            if(data.length){
                $("#arreglos_campos").val(JSON.stringify(fb_ed.actions.getData()));
                setTimeout(function() {
                    $("#form_forms").submit();
                }, 200);
            }else{
                $('#alert-error-formulario').empty().html('<strong>El formulario no puede ir vacio</strong>').show();
            }
        });
    });

</script>
@endsection
