@extends('layouts.app')

@section('titulo')
Crear formulario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('formularios.index') }}">Formularios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Ver formulario</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group my-3">
                            <label for="nombre" class="input-group-text col-sm-auto bg-light">{{ __('Name') }}</label>
                            <input id="nombre" readonly type="text" class="form-control col-12" name="nombre" value="{!! $formulario->nombre !!}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-group my-3">
                            <label for="estado" class="input-group-text col-sm-auto bg-light">Estado</label>
                            <input id="nombre" readonly type="text" class="form-control col-sm-12" name="nombre" value="{!! ($formulario->estado == "0") ? 'Inactivo' : 'Activo' !!}">
                        </div>
                    </div>


                    <div class="col-12">
                        <hr>
                        <center>
                            <h4>Formulario construido</h4>
                        </center>
                        <hr>
                        
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-8" style="border: 1px dotted black !important; padding: 10px !important;">
                                <div style="display: none;" id="fb-editor"></div>
                            </div>
                            <div class="col-2"></div>
                        </div>

                    </div>
                </div>
                <hr>

                <div class="row my-3 justify-content-md-center">
                    <div class="form-group ">
                        <button type="button" onclick="location.href='{{ route('formularios.index') }}';" class="btn btn-secondary">Regresar</button>
                        <button type="button" onclick="location.href='{{ route('formularios.clone',$formulario->id) }}';" class="btn btn-primary">Clonar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
<script>
    let options = {
        showActionButtons: false,
        disableFields: [
            'autocomplete','button','hidden','file'
        ]
    };

    let fb_ed = $(document.getElementById('fb-editor')).formBuilder(options);

    setTimeout(function() {
        fb_ed.actions.setData(JSON.parse('{!! $formulario->formulario !!}'));

        setTimeout(function() {
            let xml_data = fb_ed.actions.getData('xml');

            var formRenderOpts = {
                dataType: 'xml',
                formData: xml_data
            };

            var renderedForm = $('<div>');
            renderedForm.formRender(formRenderOpts);

            $("#fb-editor").html(renderedForm.html());
            $("#fb-editor").show();
        }, 100);
    }, 200);
</script>
@endsection
