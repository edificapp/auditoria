@extends('layouts.app')

@section('titulo')
Formularios
@endsection


@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Formularios</b></li>
                        </ol>
                    </nav>
                </div >
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('formularios.create') }}';" class="btn-create-form">Crear formulario </button>
            </div>
        </div>
    </div>
</div>
<div >
    @include('proyecto.components.tabs', ['select' => 'formularios']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <p class="card-title">Listado de formularios</p>
                <div class="table-responsive">
                    <table id="table" class="display" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Proyectos</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                @foreach($user->ownerFormularios as $formulario)
                                <tr class="body-rows">
                                    <td>{{Str::ucfirst(Str::lower($formulario->nombre))}}</td>
                                    <td>
                                        @foreach($formulario->grupo_formularios as $item)
                                            <span>- {{$item->proyecto->nombre}} ({{$item->name}})</span><br><br>
                                        @endforeach
                                    </td>
                                    <td>{{$formulario->created_at}}</td>
                                    <td>
                                        <div class="btn-group col-sm">
                                            <button class="btn-copy-form" onclick="location.href='{{ route('formularios.clone', $formulario->id) }}'" title="clonar formulario"></button>
                                            <button class="btn-show-form" onclick="location.href='{{ route('formularios.show', $formulario->id) }}'" title="ver formulario"></button>

                                            @if (Auth::user()->id == $formulario->creador_id)
                                            {{----}}<button class="btn-edit-form" onclick="location.href='{{ route('formularios.edit',$formulario->id) }}';" title="Editar formulario"></button>
                                            @endif

                                            {{----}}<button class="btn btn-sm btn-inverse-dark btn-rounded" onclick="location.href='{{ route('formulario_refresh',$formulario->id) }}';" title="Estado anterior del formulario">
                                                <span class="fa fa-refresh"></span>
                                            </button>
                        
                                            {{----}}<button class="{{$formulario->activo == 'no' ? 'btn-inactivo-form': 'btn-activo-form'}}" onclick="location.href='{{ route('formulario_estado',$formulario->id) }}';" title="Habilitar|Desabilitar formulario"></button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style type="text/css">
    td.details-control {
        background: url('../resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../resources/details_close.png') no-repeat center center;
    }
</style>
@stop


@section('scripts')
<script>
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
                '<td>Full name:</td>'+
                '<td>'+d.name+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extension number:</td>'+
                '<td>'+d.extn+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extra info:</td>'+
                '<td>And any further details here (images etc)...</td>'+
            '</tr>'+
        '</table>';
    }
     
    

    // $('#recent-purchases-listing tbody').on('click', 'td.details-control', function () {
    //     var tr = $(this).closest('tr');
    //     var row = table.row( tr );
 
    //     if ( row.child.isShown() ) {
    //         // This row is already open - close it
    //         row.child.hide();
    //         tr.removeClass('shown');
    //     }
    //     else {
    //         // Open this row
    //         row.child( format(row.data()) ).show();
    //         tr.addClass('shown');
    //     }
    // } );
</script>
@endsection
