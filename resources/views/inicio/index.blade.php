@extends('layouts.app')

@section('titulo')
Inicio
@endsection
@section('contenido')
  <div id="app">
    <Private-chat :activities="{{json_encode(auth()->user()->actividades_log)}}"></Private-chat>
  </div>
@endsection

