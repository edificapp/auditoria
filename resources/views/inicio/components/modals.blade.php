<div id="showProperties" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row justify-content-md-center" id="result">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
</div>

  <div id="formTeamModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="formTitle"></h5>
          <button class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row justify-content-md-center">
            <form class="form-horizontal col-md-12" method="post" id="formTeam">
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="PUT" id="method"/>

                <div class="input-group my-2 {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="input-group-text bg-light">Nombre del Equipo</label>
                      <input type="text" placeholder="Nombre del Equipo de Trabajo" class="form-control" id="name" name="name" value="{{ old('name') }}">
                      @if ($errors->has('name'))
                          <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                      @endif
                </div>

                <div class="input-group my-2{{ $errors->has('email') ? ' has-error' : '' }}" id="groupUpdate">
                  <label class="input-group-text bg-light">Correo Electronico</label>
                    <select name="owner_id" class="form-control">
                      @foreach(Auth::user()->empresa->usuarios as $item)
                        <option value="{{$item->id}}" {{$item->id == old('owner_id') || $item->id == Auth::id() ? 'selected': ''}}>{{$item->name}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('owner_id'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('owner_id') }}</strong>
                      </span>
                    @endif
              </div>

              <div class="row my-2 justify-content-md-center">
                <div class="form-group">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="teamMembersModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="my-modal-title">Detalles</h5>
          <button class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row justify-content-md-center">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="card" >
                  <div class="card-body borde">
                    <h4 class="text-center" id="titleTeamMembers"></h4>
                      <div class="my-3">
                            <table class="tableTeam" id="tabla_miembros" width="65%" >
                                <thead>
                                  <tr>
                                      <th style="width:25%">Nombre</th>
                                      <th style="width: 5%">Opciones</th>
                                  </tr>
                                </thead>
                                <tbody id="teamUsers">
                                </tbody>
                            </table>
                      </div>  
                  </div>
              </div>
            </div>
          </div>
          
          <div class="row justify-content-md-center mt-4">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card" >
                    <div class="card-body borde">
                      <h4 class="text-center" id="titleInvitacionesPendientes"></h4>
                        <div class="my-3">
                            <table class="tableTeam" id="tabla_miembros" width="65%">
                                <thead>
                                <tr>
                                    <th>Correo</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody id="teamInvites"></tbody>
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
          </div>

          <div class="row justify-content-md-center mt-4">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card" >
                    <div class="card-body borde">
                        <div class="my-3">
                            <h4 class="text-center" id="titleInviteForm"></h4>
                            <form class="form-horizontal" method="post" id="formInviteMembers">
                                {!! csrf_field() !!}
                                <div class="input-group my-3{{ $errors->has('email') ? ' has-error' : '' }}">
                                  <label class="input-group-text">Correo Elecronico</label>
                                    <select name="email" class="form-control col-7">
                                      @foreach(Auth::user()->empresa->usuarios->filter(function($item){ return $item->id != Auth::id(); }) as $item)
                                        <option value="{{$item->email}}" {{$item->email == old('email') ? 'selected': ''}}>{{$item->name}} | {{$item->email}}</option>
                                      @endforeach
                                    </select>
                                    @if ($errors->has('email'))
                                      <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="row justify-content-md-center">
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-primary">
                                      <i class="fa fa-btn fa-envelope-o"></i> Invitar al Equipo
                                    </button>
                                  </div>
                                </div>
                            </form>
                        </div>  
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <div id="conversationModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <div class="row" id="message-received"></div>
          <button type="button" style="float: left" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="message-wrapper">
              <ul class="messages" id="messages">
              </ul>
            </div>
            <div class="input-text">
              <input type="text" name="message" id="inputMessage" class="submit" placeholder="escriba aqui su mensaje...">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>