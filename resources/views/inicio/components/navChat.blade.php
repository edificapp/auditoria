 <div>
    <ul class="list-group">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <a class="btn btn-primary btn-block" href="{{route('eventos.index')}}">Evento <i class="mdi mdi-plus"></i></a>
      </li>
      
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <div class="btn-group-vertical btn-block">
          <button class="btn btn-primary" id="btn_create">Grupos <i class="mdi mdi-plus"></i></button>
          @foreach(Auth::user()->teams as $item)
            @if($item->owner_id == Auth::id())
              <div class="btn-group" role="group">
                <button 
                    id="btnGroupDrop{{$item->id}}" 
                    type="button" 
                    class="btn dropdown-toggle {{$item->id == Auth::user()->current_team_id ? 'btn-inverse-secondary':'btn-inverse-primary'}}" 
                    data-toggle="dropdown" 
                    aria-haspopup="true" 
                    aria-expanded="false" 
                    title="{{$item->name}}"
                >
                  {!! Str::limit($item->name, 18) !!}
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop{{$item->id}}">
                  @if(is_null(auth()->user()->currentTeam) || auth()->user()->currentTeam->getKey() !== $item->getKey())
                      <a href="{{route('teams.switch', $item)}}" class="dropdown-item">
                          <i class="mdi mdi-account-convert text-secondary"></i> Cambiar
                      </a>
                  @endif
                  <button class="dropdown-item" onclick="btn_miembros('{{route('teams.members.show', $item)}}')"><i class="fa fa-users text-primary"></i> Miembros</button>
                  <button class="dropdown-item " onclick="btn_editar({{$item->id}})"><i class="mdi mdi-pencil-box-outline text-info"></i> Editar</button>
                  <button class="dropdown-item" onclick="btn_borrar('delete_{{$item->id}}')"><i class="mdi mdi-delete text-danger"></i> Borrar</button>
                </div>
              </div>
              <form style="display: inline-block;" action="{{route('teams.destroy', $item)}}" method="post" id="delete_{{$item->id}}">
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="DELETE" />
              </form>
            @else
              <a 
                class="btn btn-inverse-primary {{$item->id == Auth::user()->current_team_id ? 'disabled' :''}}" 
                href="{{route('teams.switch', $item->id)}}"
              >
                {!! Str::limit($item->name, 20) !!}
              </a>
            @endif 
          @endforeach
          <a class="btn btn-inverse-primary {{ is_null(Auth::user()->currentTeam) ? 'disabled' :''}}" href="{{route('teams.switch', 0)}}">Todos mis grupos</a>
        </div>
      </li>
    </ul>
  </div>
  <div class="mt-3">
    <ul class="nav nav-pills nav-pills-custom" id="pills-tab-custom" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#chat_usuarios" role="tab" aria-controls="chat_usuarios" aria-selected="true">Usuarios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#chat_equipos" role="tab" aria-controls="chat_equipos" aria-selected="false">Grupos</a>
      </li>
      <li class="nav-item mb-1">
        <input type="text" class="form-control" placeholder="Buscar">
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade active show" id="chat_usuarios" role="tabpanel" aria-labelledby="chat-usuarios-tab">
          <div class="user-wrapper">
              <ul class="users list-group">
                @foreach(Auth::user()->empresa->usuarios->filter(function($item){ return $item->id != Auth::id(); }) as $user)
                <li class="user list-group-item " id="{{$user->id}}" title="{{$user->name}}">
                  @if($user->messages->filter(function($item){ return $item->is_read == 0; })->count() > 0)
                      <span class="pending">{{ $user->messages->filter(function($item){ return $item->is_read == 0; })->count() }}</span>
                  @endif
                  <div class="media">
                      <div class="media-left">
                          <img 
                          src="{{$user->curriculum ? $user->curriculum->avatar : asset('admin/images/usuario/usuario.jpg')}}"
                          alt="{{$user->name}}" 
                          class="media-object"
                          />
                      </div>
                      <div class="media-body">
                          <p class="name">{!! Str::limit($user->name, 18) !!}</p>
                      </div>
                  </div>
                </li>
                @endforeach
              </ul>
          </div> 
      </div>
      <div class="tab-pane fade" id="chat_equipos" role="tabpanel" aria-labelledby="chat-equipos-tab">
          <div class="user-wrapper">
              <ul class="users list-group">
                @foreach(Auth::user()->teams as $item)
                <li class="user list-group-item" id="{{$item->id}}" title="{{$item->name}}">
                  <div class="media">
                      <div class="media-left">
                          <img 
                          src="{{$item->avatar}}" alt="{{$item->name}}"
                          class="media-object"
                          />
                      </div>
                      <div class="media-body">
                          <p class="name">{!! Str::limit($item->name, 18) !!}</p>
                      </div>
                  </div>
                </li>
                @endforeach
              </ul>
          </div> 
      </div>
    </div>
  </div>
  
