
<div class="card mb-2 pt-2 pb-2 pl-2 pr-2 ">
    {{-- <div class="card-body pt-2 pb-2 pl-2 pr-2 bg-transparent"> --}}
        <ul class="nav nav-tabs">
            <li class="nav-item {!! Auth::user()->teams->count() == 0 ? 'disable' : '' !!}">
                <a class="nav-link active" data-toggle="tab" href="#tab-estados">Muro</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-actividades">Actividades Recientes</a>
            </li>
        </ul>
    {{-- </div> --}}
</div>

<div class="card ">
    <div class="card-body borde rounded pt-2">
        <div class="tab-content" id="app">
            <div class="tab-pane fade active show"   id="tab-estados" role="tabpanel" aria-labelledby="estados-tab">
                <estado-form></estado-form>
                <estado-list></estado-list>
            </div>
            <br><br>
            <div class="tab-pane fade px-3" id="tab-actividades" role="tabpanel" aria-labelledby="actividades-tab">
                <div class="table-responsive">
                    <table class="display" id="tabla_actividades" width="100%">
                        <thead>
                            <th>Id</th>
                            <th>Actividades recientes</th>
                            <th>Fecha</th>
                        </thead>
                        <tbody>
                            @foreach(Auth::user()->actividades_log as $item)
                            <tr class="body-rows">
                                <td>{{$item->id}}</td>
                                <td title="{{$item->time}}">
                                        <small class="{{Auth::id() == $item->causer_id ? 'text-primary' :'' }}">{{ucwords($item->causer->nombre)}}</small> - 
                                        <span>{{$item->description}}</span>
                                </td>
                                <td>| {{$item->created_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>    
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="card">
    <div class="tab-content" id="app">
        <div class="tab-pane fade active show" style="background:#f1f1f1;"  id="tab-estados" role="tabpanel" aria-labelledby="estados-tab">
            <estado-list></estado-list>
        </div>
    </div>
</div> --}}

