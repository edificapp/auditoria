@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('styles')
  <style>
    .disable{
      pointer-events: none !important;
    }
    ul{
      margin: 0;
      padding:0;
    }
    
    li{
      list-style: none;
    }

    .user-wrapper, .message-wrapper{
      border: 1px solid #dddddd;
      overflow-y:auto;
    }

    .user-wrapper{
      height: 400px !important;
    }
    .user {
      cursor: pointer;
      padding: 5px 0;
      position: relative;
    }

    .user:hover{
      background: #eee;
    }

    .user:last-child {
      margin-bottom: 0;
    }

    .pending{
      position: absolute;
      left: 13px;
      top: 9px;
      background: #b600ff;
      margin: 0;
      border-radius: 50%;
      width: 18px;
      height: 18px;
      line-height: 18px;
      padding-left: 5px;
      color: #fff;
      font-size: 12px;
    }

    .media-left{
      margin: 0 10px;
    }

    .media-left img{
      width: 40px;
      height: 40px;
      border-radius: 64px;
    }

    .media-body p{
      margin: 6px 0; 
    }

    .message-wrapper{
      padding:10px;
      height: 336px;
      background: #eee;
    }

    .messages .message{
      margin-bottom: 15px;
    }

    .messages .message:last-child{
      margin-bottom: 0;
    }

    .received, .sent{
      width: 51%;
      padding: 3px 10px;
      border-radius: 10px;
    }

    .received{
      background: #cec7c7ab;
      float: left;
      text-align: left;
    }

    .sent{
      background: #3bebff;
      float: right;
      text-align: right;
    }

    .message p{
      margin: 5px 0;
    }

    .date{
      color:#777;
      font-size: 12px;
    }

    #inputMessage{
      width: 100%;
      padding:12px 20px;
      margin: 15px 0 0 0;
      display: inline-block;
      border-radius: 4px;
      box-sizing: border-box;
      outline: none;
      border: 1px solid #ccc;
    }

    #inputMessage:focus{
      border: 1px solid #aaa;
      background: #cec7c7ab;
    }

    body.modal-open {margin-right: 0px}


    /* TABLA */
    th { white-space:nowrap; }
        
    table.dataTable.tableTeam thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
    }

    table.dataTable.tableTeam tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
        background-color:#FDFEFE !important;
        border-style: none;
        border-bottom: 0.5px solid #E5E7E9;
    }

  </style>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200&family=Play:wght@700&display=swap" rel="stylesheet">

@endsection
@section('contenido')
<div>
  <div class="row justify-content-md-center">
    <div class="col-9">
      @include('inicio.components.tableros')
    </div>
    <div class="col-3">
      @include('inicio.components.navChat') 
    </div>
    
  </div>
</div>
@include('inicio.components.modals')
@endsection

@section('scripts')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
      //activity logs
       $(document).ready(function(){
       
        var titulo = [];
        titulo['old'] = 'Datos viejos';
        titulo['attribute'] = 'Datos nuevos';
        var table = $('#tabla_actividades').dataTable({
          "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
          },
          responsive: true,
          order:[[2,"desc"]],
          "columnDefs": [
              {
                  "targets": [ 1 ],
                  "visible": true,
                  "searchable": true
              },
              {
                  "targets": [ 0,2 ],
                  "visible": false,
                  "searchable": true
              }
          ]
        });

            $('#tabla_actividades').on('click', 'tbody tr', function () {
                var data = table.api().row(this).data();
                $.get( "/activity-properties/"+data[0], function( resp ) {
                        $('#showProperties').modal();
                        $( "#result" ).html('').append(resp);
                  });
            } );
          });

        $('#btn_create').on('click', function(){
          $('#formTeamModal').modal();
          $('#formTeam').attr('action', '{{route('teams.store')}}');
          $('#formTitle').text('Creación de Equipo de Trabajo');
          $('#groupUpdate').css('display', 'none');
          $('#method').val('post');
          $('#name').val('');
        });

        function btn_editar(id){
          $('#formTeamModal').modal();
          $.get( "/equipos/edit/"+id, function( resp ) {
            $('#groupUpdate').show();
            $('#formTeamcreate').modal();
            $('#formTeam').attr('action', resp.update);
            $('#formTitle').text('Actualización del equipo '+resp.name);
            $('#method').val('put');
            $('#name').val(resp.name);
          });
        }

        function btn_borrar(form_id){
          $('#'+form_id).submit();
        }

        function btn_miembros(url){
          $.get(url, function( resp ) {
            $('#teamMembersModal').modal();
            $('#teamUsers').empty().html(resp.members);
            $('#teamInvites').empty().html(resp.invites);

            $('#formInviteMembers').attr('action', resp.store);
            $('#titleTeamMembers').text('Miembros del equipo '+resp.team.name);
            $('#titleInvitacionesPendientes').text('Invitaciones Pendientes del equipo '+resp.team.name);
            $('#titleInviteForm').text('Invitación al equipo '+resp.team.name);
          });
        }
    </script>
    <script>
      //chats messenger
      var received_id = '';
      var my_id = '{{Auth::id()}}';
      $(document).ready(function(){
          var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {
            cluster: '{{env('PUSHER_APP_CLUSTER')}}'
          });

          Echo.channel(`chat`).listen('MensajeChat', ({data}) =>{
            if (my_id == data.from) {
            } else if (my_id == data.to) {
                if (received_id == data.from) {
                  $( ".message:last-child").after(data.message);
                  //$( "#messages" ).after(data.message);
                } else {
                    // if receiver is not seleted, add notification for that user
                    var pending = parseInt($('#' + data.from).find('.pending').html());
                    if (pending) {
                        $('#' + data.from).find('.pending').html(pending + 1);
                    } else {
                        $('#' + data.from).append('<span class="pending">1</span>');
                    }
                }
            }
          });

        $('.user').on('click', function(){
          $('.user').removeClass('active');
          $(this).addClass('active');
          received_id = $(this).attr('id');
          $('#' + received_id +' .pending' ).remove();
          $.get('/mensajes/'+received_id, function(resp){
            $('#messages').empty().html(resp.messages);
            $('#message-received').empty().html(resp.received);
            scrollToBottomFunc();
            $('#conversationModal').modal();
          })
        });

        $(document).on('keyup', '#inputMessage', function(e){
          var message = $(this).val();
          if(e.keyCode == 13 && received_id != '' && message != '' ){
            $(this).val('');
            var datastr='_token={{ csrf_token() }}&to='+received_id+'&message='+message;
            $.post('{{route("getMessage")}}', datastr, function(resp){
              if ($('.message').length) {
                $( ".message:last-child").after(resp);
              } else {
                $( "#messages").append(resp);
              }
              
              scrollToBottomFunc();
            });
          }
        });


        $("#conversationModal").on('hidden.bs.modal', function () {
          $('.user').removeClass('active');
        });
      });
       // make a function to scroll down auto
      function scrollToBottomFunc() {
          $('.message-wrapper').animate({
              scrollTop: $('.message-wrapper').get(0).scrollHeight
          }, 'fast');
      }

      $('#tabla_miembros').DataTable ({
          "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
          },
          ordering: false,
          paging:false, info: false,"searching": false
      });
    </script>
@endsection
