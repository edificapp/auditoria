@extends('layouts.app')

@section('titulo')
Equipos de trabajo
@endsection

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('teams.index')}}">Equipos de Trabajo</a></li>
            <li class="breadcrumb-item active" aria-current="page">Miembros del equipo {{$team->name}}</li>
      </ol>
    </nav>
@stop

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <a href="{{route('teams.index')}}" class="btn btn-sm btn-default pull-right">
                    <i class="fa fa-arrow-left"></i> Atras
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card" >
            <div class="card-body">
                <div class="my-3">
                    <h2 class="text-center">Miembros del equipo {{$team->name}}</h2>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($team->users AS $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>
                                    @if(auth()->user()->isOwnerOfTeam($team))
                                        @if(auth()->user()->getKey() !== $user->getKey())
                                            <form style="display: inline-block;" action="{{route('teams.members.destroy', [$team, $user])}}" method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button class="btn-delete-form"></button>
                                            </form>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>


<div class="row mt-4">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card" >
            <div class="card-body">
                <div class="my-3">
                    <h2 class="text-center">Invitaciones Pendientes del equipo {{$team->name}}</h2>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Correo</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        @foreach($team->invites AS $invite)
                            <tr>
                                <td>{{$invite->email}}</td>
                                <td>
                                    <a href="{{route('teams.members.resend_invite', $invite)}}" class="btn btn-sm btn-default">
                                        <i class="fa fa-envelope-o"></i> Re-enviar Invitación
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card" >
            <div class="card-body">
                <div class="my-3">
                    <h3 class="text-center">Invitación al equipo "{{$team->name}}"</h3>
                    <form class="form-horizontal" method="post" action="{{route('teams.members.invite', $team)}}">
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Correo Elecronico</label>
                            <div class="col-md-6">
                                <select name="email" class="form-control">
                                    @foreach(Auth::user()->empresa->usuarios->filter(function($item) use ($team){ return $item->id != $team->owner_id; }) as $item)
                                        <option value="{{$item->email}}" {{$item->email == old('email') ? 'selected': ''}}>{{$item->name}} | {{$item->email}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('email'))
                                    <span class="help-block text-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope-o"></i> Invitar al Equipo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

