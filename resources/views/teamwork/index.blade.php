@extends('layouts.app')

@section('titulo')
Equipos de trabajo
@endsection

{{-- @section('breadcrumbs')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Equipos</li>
      </ol>
    </nav>
@stop --}}

@section('contenido')

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex align-items-end flex-wrap">
            <div class="d-flex">
                <a href="{{ route('home') }}"><i class="mdi mdi-home breadcrumbs hover-cursor">&nbsp;/&nbsp;</i></a>
                <p class="breadcrumbs mb-0 hover-cursor">Equipos de trabajo</p>
            </div>
        </div>
        <div class="d-flex justify-content-end flex-wrap">
            <div class="justify-content-end">
                <a class="btn-create-form"  href="{{route('teams.create')}}">Nuevo Equipo de Trabajo </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card" >
            <div class="card-body borde">
                <h5 class="card-title">Equipos de trabajo</h5>
                <div class="my-3">
                    <table class=" display" id="table-teams">
                        <thead>
                            <tr>
                                <th>Nombre del Equipo</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teams as $team)
                                <tr class="body-rows">
                                    <td>
                                        {{$team->name}}  
                                        @if( !(is_null(auth()->user()->currentTeam) || auth()->user()->currentTeam->getKey() !== $team->getKey()))
                                            <span class="label"> <b>(Equipo Actual)</b></span>
                                        @endif
                                    </td>
                                    <td>
                                        @if(auth()->user()->isOwnerOfTeam($team))
                                            <span class="label label-success">Creador</span>
                                        @else
                                            <span class="label label-primary">Miembro</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if(is_null(auth()->user()->currentTeam) || auth()->user()->currentTeam->getKey() !== $team->getKey())
                                            <a href="{{route('teams.switch', $team)}}" class="btn btn-sm btn-inverse-secondary btn-rounded" title="cambiar de equipo de trabajo">
                                                <i class="mdi mdi-account-convert"></i>
                                            </a>
                                        @endif
                                        <a href="{{route('teams.members.show', $team)}}" class="btn btn-sm btn-inverse-primary btn-rounded">
                                            <i class="fa fa-users"></i>
                                        </a>

                                        @if(auth()->user()->isOwnerOfTeam($team))

                                            <a href="{{route('teams.edit', $team)}}" class="btn-edit-form"></a>

                                            <form style="display: inline-block;" action="{{route('teams.destroy', $team)}}" method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button class="btn-delete-form"></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#table-teams').DataTable ({
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    },
    paging:false, 
    info: false, 
    "searching": false,
    ordering: false
});
</script>
    
@stop
