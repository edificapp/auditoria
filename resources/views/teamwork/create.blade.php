@extends('layouts.app')

@section('titulo')
Crear Equipo de trabajo
@endsection

{{-- @section('breadcrumbs')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('teams.index')}}">Equipos de Trabajo</a></li>
            <li class="breadcrumb-item active" aria-current="page">Creación de Equipo</li>
      </ol>
    </nav>
@stop --}}

@section('contenido')

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex align-items-end flex-wrap">
            <div class="d-flex">
                <a href="{{ route('home') }}"><i class="mdi mdi-home breadcrumbs hover-cursor">&nbsp;/&nbsp;</i></a>
                <a href="{{route('teams.index')}}" class="breadcrumbs mb-0 hover-cursor">Equipos de trabajo</a>&nbsp;/&nbsp;
                <p class="breadcrumbs">Crear Equipo de trabajo</p>
            </div>
        </div>
    </div>
</div>
{{-- 
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Creando Equipo de Trabajo</h2>
                </div>

            </div>
        </div>
    </div>
</div> --}}
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card" >
            <div class="card-body">
                <h5 class="card-title"> Creacion del equipo de trabajo</h5>
                <div class="my-3"> 
                    <form class="form-horizontal" method="post" action="{{route('teams.store')}}">
                        {!! csrf_field() !!}

                        <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="input-group-text bg-light">Nombre del Equipo</label>
                            <input type="text" class="form-control col-sm-4" name="name" value="{{ old('name') }}" placeholder="nombre del equipo">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                            <button type="submit" class="btn btn-primary ml-2">
                                <i class="fa fa-btn fa-save"></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection


