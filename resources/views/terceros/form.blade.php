@extends('layouts.auth')

@section('titulo')
Crear usuario
@endsection

@section('contenido')
<div class="col-lg-5 mx-auto">
    <div class="auth-form-light text-left py-5 px-4 px-sm-5">
        <div class="col-md-12">
                    <form method="POST" action="{{ route('tercero.store') }}" enctype="multipart/form-data">
                        @csrf

                        <h4 class="card-title text-center">Información Importante</h4>
                        <p class="card-description">
                            Se completa esta información para subir los archivos al proyecto correspondiente.
                        </p>
                        <br><br><br>

                        <div class="row">
                            <div class="col-md-12">
                                <label>Escoge un proyecto</label>
                                <select name="proyecto_id" class="form-control form-control-lg input-redondeado">
                                    @foreach($proyectos as $item)
                                        <option value="{{$item->id}}" {{old('proyecto_id') != $item->id ?:'selected'}}>{{$item->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br><br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="num_dc">Nit | Identificación</label>
                                    <input type="text" id="num_dc" class="form-control form-control-lg input-redondeado @error('num_dc') is-invalid @enderror" name="num_dc" value="{!! old('num_dc') !!}" required placeholder="Nit o Identificación">

                                    @error('num_dc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="razon_social">Razón Social</label>
                                    <input type="text" id="razon_social" class="form-control form-control-lg input-redondeado @error('razon_social') is-invalid @enderror" name="razon_social" value="{!! old('razon_social') !!}" placeholder="Razon Social o Nombre">
                                    @error('razon_social')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="num_dc">Escoge los archivos</label>
                                    <input type="file" class="form-control" name="documentos[]"  required multiple>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group">
                            <input type="submit" value="Enviar" class="btn btn-primary">
                        </div>
                    </form>

        </div>
    </div>
</div>
@endsection

@section('css')
    <style type="text/css">
    .input-redondeado{
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        border-radius: 50px !important;
        border: 2px solid #49505766;
        padding: 0 4px 0 4px;
     }
     .btn-redondeado{
        border-radius: 30px !important;
        background: #5c5c5d;
        color: white;
     }
     .auth-form-light{
        border-radios:10px;
        border:1px solid #5c5c5d;
        -webkit-box-shadow: 3px 3px 5px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 3px 3px 5px 0px rgba(0,0,0,0.75);
        box-shadow: 3px 3px 5px 0px rgba(0,0,0,0.75);
     }
    </style>
@stop

@section('js')
    <script type="text/javascript">
        $('#num_dc').on('change', function(){
            verificarTercero($(this).val());
        });

        function verificarTercero(num){
            $.get('/terceros/validar/'+num, function( data ) {
               $('#razon_social').val(data.razon_social);
            });
        }
    </script>
@endsection