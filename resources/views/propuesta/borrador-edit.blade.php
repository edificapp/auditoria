@extends('layouts.app')

@section('titulo')
Propuestas
@endsection

@section('contenido')
<div class="container-fluid" id="app">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb" >
                            <ol class="breadcrumb p-2" >
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('propuestas.index')}}">Propuestas</a></li>
                                <li class="breadcrumb-item "><a href="{{route('propuestas.borrador')}}">Borrador</a></li>
                                <li class="breadcrumb-item active"  aria-current="page"><b>Editar</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div >
        @include('propuesta.components.tabs', ['select' => 'borrador'])  
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-12 text-right mt-2">
                            <label for=""><b> Fecha de entrega actual:</b> </label> 
                            <small>{{$propuesta->fecha_entrega}}</small>
                        </div>
                    </div>
                    <hr>
                    <div class="card">
                        <div class="card-body p-3 border-0" style="background-color:#F4F6F6;">
                            <div class="header-detail-propuesta">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                        <span class="font-weight-bold"> Nombre:</span>
                                        </small> {{$propuesta->nombre}}</small>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label for=""> 
                                        <span class="font-weight-bold"> Nº Proceso:</span> 
                                        <small>{{$propuesta->numero_proceso}}</small>
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label for="">
                                        <span class="font-weight-bold">Fecha de creación de propuesta : </span>
                                        <small>{{$propuesta->created_at}}</small>
                                        </label>
                                    </div>
                                            
                                </div>
                                <div class="row mt-2">
                                    <div class="col-4">
                                        <label for="">
                                        <span class="font-weight-bold">Entidad contratante: </span>
                                        <small>{{$propuesta->entidad->nombre}}</small>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label for="">
                                        <span class="font-weight-bold">Contacto: </span>
                                        <small>{{$propuesta->contact->nombres}}</small>
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label for="">
                                        <span class="font-weight-bold">Fechar inicial de entrega: </span> 
                                        <small>{{$propuesta->fecha_entrega}}</small>
                                        </label>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div id="accordion" class="mt-4">
                        <div class="" id="headingOne">
                             <h5 class="mb-0">
                                 <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                     <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Documentos
                                 </span>
                             </h5>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <button type="button" class="btn-add-form mb-2" id="create"></button>

                                <div class="row mb-3 mt-2">
                                    <div class="col-3">
                                        <label for="" class="font-weight-bold"> Fecha</label>
                                    </div>
                                    <div class="col-3">
                                        <label for="" class="font-weight-bold"> Tamaño</label>
                                    </div>
                                </div>
                                @foreach ($propuesta->documentos_propuestas as $documento)
                                    <div class="row mt-2">
                                        <div class="col-3">
                                            <label for=""> 
                                              <a href="{{$documento->url_public}}" target="_blank">
                                                <small>{{$documento->nombre}}</small>                                      
                                              </a> 
                                            </label>     
                                        </div>
                                        <div class="col-3">
                                            <label for=""> <small>{{$documento->created_at}}</small></label>
                                        </div>
                                        <div class="col-3">
                                            <label for=""> <small>{{$documento->size}}</small></label>
                                        </div>
                                        
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="mt-3" id="headingTwo">
                            <h5 class="mb-0">
                                <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Codigo UNSPSC  
                                </span>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form" type="button" id="input_unspscs" title="agregar">
                                </button>
                                <div class="row my-3">
                                    <div class="col-6">
                                        @foreach ($propuesta->code_uns as $code)
                                            <div class="row mt-2">
                                                <div class="col-6">
                                                    <label for=""> {{$code->code_uns->code}} - {{$code->code_uns->name}} </label>     
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3" id="headingThree">
                            <h5 class="mb-0">
                                <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Experiencia mínima de la empresa relacionada con el objeto cotractual  
                                </span>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                @if(count($propuesta->experiencia) == 0)
                                    <button class="btn-add-form" type="button" id="experiencia" title="agregar"></button>
                                @endif
                                <div class="row my-3">
                                    <div class="col-6">
                                        <label for="" class="font-weight-bold">  </label> 
                                        @foreach ($propuesta->experiencia as $experiencia)
                                        <div class="row mt-2">
                                            <div class="col">
                                                <label for="" class="font-weight-light"> Tienes una experiencia minima agregada  <i class="mdi mdi-eye" onclick="ver({{$experiencia}})"  style="color:black;font-weight:bold"></i></label>     
                                            </div>
                                        </div>    
                                        @endforeach 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3" id="headingFour">
                            <h5 class="mb-0">
                                <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Talento Humano 
                                </span>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form" type="button" id="talento_humano" title="agregar"></button>
                            </div>
                        </div>

                        <div class="row my-3 ml-2">
                            <div class="col-12">
                                @foreach ($propuesta->talento_propuesta as $key=> $talento)
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-weight-light"> <small>Perfil  {{$key+1}}( {{$talento->cargo}} - {{$talento->profesion->nombre}} - Cantidad de contratos: {{$talento->cantidad_contratos}} - Experiencia : {{$talento->experiencia_general_años}} ) </small>
                                        </p>     
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<div id="modalCode" class="modal fade" role="dialog" style="overflow:hidden;" aria-labelledby="modalCde" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11">Codigos Unspsc</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('propuesta-code.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <v-select-custom name="unspsc"></v-select-custom>
                    <br><br>
                    <div class="btn-group-vertical">
                    <input type="hidden" name="propuesta" value="{{$propuesta->id}}">
                    </div>
                    <div class="row m-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>
</div>



{{-- modal documentos --}}
<div id="modalFormCreate" class="modal fade" role="dialog" aria-labelledby="modalCreateForm" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="modalCreateForm">Agregar Documentos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('propuesta-documentos.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-3 bg-light">Nombre de Documento: </label>
                            <input 
                                type="text" 
                                name="name" 
                                class="form-control input-personal col-9 @error('name') is-invalid @enderror"  
                                placeholder="Nombre de Documento" 
                                value="{{old('name')}}"
                            >
                            <br>
                            @error('name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3 justify-content-md-center">
                        <div class="col-md-8">
                           <input class="file_certificado" name="documento" type="file">
                        </div>
                    </div>
                   
                   
                    <input type="hidden" name="propuesta_id" value="{{$propuesta->id}}">
                  
                    <div class="row justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" wire:click="save">Cargar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>


<div class="modal fade" id="experienciaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Experiencia minima de la empresa relacionada con el objecto contractual</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <form method="POST" action="{{ route('propuestas.experiencia') }}">
                @csrf
                <div class="row my-3">
                          <div class="input-group col-12">
                              <label class="input-group-text col-3 bg-light">Cantidad de Contratos: </label>
                              <input 
                                  type="text" 
                                  name="contratos_cantidad" 
                                  class="form-control col-9 @error('nombre') is-invalid @enderror"  
                                  placeholder="Cantidad de Contratos" 
                                  value="{{old('nombre')}}"
                              >
                              <br>
                              @error('nombre')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                </div>
                <div class="row my-3">
                          <div class="input-group col-6">
                              <label class="input-group-text col-6 bg-light">Tiempo en Años: </label>
                              <input 
                                  type="text" 
                                  name="años" 
                                  class="form-control col-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="Tiempo en Años" 
                                  value="{{old('entidad_contratante')}}"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <div class="input-group col-6">
                              <label class="input-group-text col-6 bg-light">Tiempo en Meses: </label>
                              <input 
                                  type="text" 
                                  name="meses" 
                                  class="form-control col-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="Tiempo en Meses" 
                                  value="{{old('entidad_contratante')}}"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                </div>
                <div class="row my-3">
                          <div class="input-group col-6">
                              <label class="input-group-text col-6 bg-light">Monto en SMLV </label>
                              <input 
                                  type="text" 
                                  name="monto" 
                                  class="form-control col-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="monto" 
                                  value="{{old('entidad_contratante')}}"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
    
                </div>
                <input type="hidden" name="propuesta_id" value="{{$propuesta->id}}">
                <div class="row justify-content-md-center">
                    <div class="form-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="experienciaDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Experiencia minima de la empresa relacionada con el objecto contractual</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <form method="POST" action="{{url('propuestas-experiencia-update')}}" id="form_id">
                @csrf
                <div class="row my-3">
                          <div class="input-group col-12">
                              <label class="input-group-text col-3 bg-light">Cantidad de Contratos: </label>
                              <input 
                                  type="text" 
                                  name="contratos_cantidad" 
                                  class="form-control col-9 @error('nombre') is-invalid @enderror"  
                                  placeholder="Cantidad de Contratos" 
                                  value=""
                                  id="cantidad_contratos"
                              >
                              <br>
                              @error('nombre')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                </div>
                <div class="row my-3">
                          <div class="input-group col-6">
                              <label class="input-group-text col-6 bg-light">Tiempo en Años: </label>
                              <input 
                                  type="text" 
                                  name="años" 
                                  class="form-control col-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="Tiempo en Años" 
                                  value=""
                                  id="tiempo_anos"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <div class="input-group col-6">
                              <label class="input-group-text col-6 bg-light">Tiempo en Meses: </label>
                              <input 
                                  type="text" 
                                  name="meses" 
                                  class="form-control col-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="Tiempo en Meses" 
                                  value="{{old('entidad_contratante')}}"
                                  id="tiempo_meses"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                </div>
                <div class="row my-3">
                          <div class="input-group col-6">
                              <label class="input-group-text col-6 bg-light">Monto en SMLV </label>
                              <input 
                                  type="text" 
                                  name="monto" 
                                  class="form-control col-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="monto" 
                                  value=""
                                  id="monto_smlv"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
    
                </div>
                <input type="hidden" name="propuesta_id"  value="{{$propuesta->id}}">
                <input type="hidden" name="experiencia_id" id="experien" value="">
                      
                <div class="row justify-content-md-center">
                    <div class="form-groupp">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="talentoHumano" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Personal Requerido</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <form method="POST" action="{{ route('propuestas.talento') }}">
              @csrf
              <input type="hidden" name="propuesta_id" value="{{$propuesta->id}}">
              <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-4 bg-light">Cargo: </label>
                    <input 
                        type="text" 
                        name="cargo"
                        class="form-control col-7 @error('cargo') is-invalid @enderror"  
                        placeholder="Cargo" 
                        value="{{old('cargo')}}"
                    >
                    <br>
                    @error('cargo')
                        <span  class="invalid-feedback" role="alert">{{ $message }}</span> 
                    @enderror
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group  col-12">
                    <label class="input-group-text col-4 bg-light">Tipo de profesión: </label>
                    <select  id="tipo" class="form-control col-6" name="tipo">
                        @foreach($tipo_profesion as $tipo)
                            <option value="{{$tipo->id}}" {{old('tipo') == $tipo->id ? 'selected' : ''}}>{{$tipo->nombre}}</option>
                        @endforeach
                    </select>
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group  col-12">
                    <label class="input-group-text col-4 bg-light">Profesión: </label>
                    <select  id="profesion" class="form-control col-7" name="profesion">
                    </select>
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Dedicación %: </label>
                    <input 
                        type="text" 
                        name="dedicacion"
                        class="form-control col-5 @error('dedicacion') is-invalid @enderror"  
                        placeholder="Dedicación %" 
                        value="{{old('dedicacion')}}"
                    >
                    <br>
                    @error('dedicacion')
                        <span  class="invalid-feedback" role="alert">{{ $message }}</span> 
                    @enderror
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-7 bg-light">Experiencia especifica en este Tipo de Contratos en años y meses</label>
                    <input 
                        type="text" 
                        name="experiencia_especifica"
                        class="form-control col-5 @error('experiencia_especifica') is-invalid @enderror"  
                        placeholder="Experiencia" 
                        value="{{old('experiencia_especifica')}}"
                    >
                    <br>
                    @error('experiencia_especifica')
                        <span  class="invalid-feedback" role="alert">{{ $message }}</span> 
                    @enderror
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-9 bg-light">Cantidad de Contratos que sorpotan la experiencia especificar en este tipo de contratos</label>
                    <input 
                        type="text" 
                        name="cantidad_contratos"
                        class="form-control col-6 @error('cantidad_contratos') is-invalid @enderror"  
                        placeholder="Cantidad de Contratos" 
                        value="{{old('cantidad_contratos')}}"
                    >
                    <br>
                    @error('cantidad_contratos')
                        <span  class="invalid-feedback" role="alert">{{ $message }}</span> 
                    @enderror
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-6 bg-light">Experiencia en general en años</label>
                    <input 
                        type="text" 
                        name="experiencia_años"
                        class="form-control col-6 @error('experiencia_años') is-invalid @enderror"  
                        placeholder="Experiencia en años" 
                        value="{{old('experiencia_años')}}"
                    >
                    <br>
                    @error('experiencia_años')
                        <span  class="invalid-feedback" role="alert">{{ $message }}</span> 
                    @enderror
                </div>
              </div>
              <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-6 bg-light">Cantidad</label>
                    <input 
                        type="text" 
                        name="cantidad"
                        class="form-control col-6 @error('cantidad') is-invalid @enderror"  
                        placeholder="Cantidad" 
                        value="{{old('cantidad')}}"
                    >
                    <br>
                    @error('cantidad')
                        <span  class="invalid-feedback" role="alert">{{ $message }}</span> 
                    @enderror
                </div>
              </div>
                <div class="row my-3">
                    <div class="input-group col-12">
                    <label  class="input-group-text col-5 bg-light">Profesional en la salud </label>
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="radio" id="si" name="salud" value="si">
                                <label for="salud">Si</label>
                            </div>
                            <div class="input-group-text">
                                <input type="radio" id="no" name="salud" value="no">
                                <label for="salud">No</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="form-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>


@endsection



@section('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
         $('#create').on('click', function(){
           $('#modalFormCreate').modal('show'); 
         });
         $('#experiencia').on('click', function(){
           $('#experienciaModal').modal('show'); 
         });

        $('#input_unspscs').on('click', function(){
            $('#modalCode').modal('show');
        });
        
        $('#talento_humano').on('click', function(){
            $('#talentoHumano').modal('show');
        });

        cargarProfesiones();
    });

    $('#tipo').on('change', function(){
      cargarProfesiones();
    });

    function cargarProfesiones(){
      let value = $('#tipo').val();
      $.get(`/tipo-profesion/${value}`, function( data ) {
        $('#profesion').empty();
        $.each(data.profesiones, function( key, value ) {
          $('#profesion').append(`<option value="${value.id}">${value.nombre}</option>`);
        });
      });
    }

    function ver(experiencia){
        console.log(experiencia)
        $('#experienciaDetailModal').modal('show'); 
        $('#cantidad_contratos').val(experiencia.contrato_cantidad)
        $('#tiempo_anos').val(experiencia.tiempo_anos)
        $('#tiempo_meses').val(experiencia.tiempo_meses)
        $('#monto_smlv').val(experiencia.monto_smlv)
        $('#experien').val(experiencia.id)

    }
    </script>
@stop
