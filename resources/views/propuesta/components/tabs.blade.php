{{-- <div class="mx-auto"> --}}
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a href="{{route('propuestas.borrador')}}" class="nav-link {{$select != 'borrador' ?:'active'}}">
                Borrador
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('propuestas.preparacion')}}" class="nav-link {{$select != 'preparacion' ?:'active'}}">
                Preparación
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link {{$select != 'presentacion' ?:'active'}}">
                Presentación
            </a>
        </li>
        {{-- <li class="nav-item">
            <a href="{{route('propuestas.index')}}" class="nav-link {{$select != 'formularios' ?:'active'}}">
                Adjudicada
            </a>
        </li> --}}
    </ul>
    <hr style="margin-top: 0;margin-bottom: 1px; border:none;">
{{-- </div> --}}
