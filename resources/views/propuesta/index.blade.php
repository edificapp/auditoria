@extends('layouts.app')

@section('titulo')
Propuestas
@endsection


@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><b>Propuestas</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <button type="button" class="btn-create-form" data-toggle="modal" data-target="#modalcreate">
                        Agregar Propuesta
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div >
        @include('propuesta.components.tabs', ['select' => '']) 
    </div>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body borde">
                    <p class="card-title">Listado de Propuestas</p>
                    <div class="table-responsive">
                        <table id="table" class="display" width="100%">
                            <thead>
                                <tr>
                                    <th>Nombre Propuesta</th>
                                    <th>Entidad contratante</th>
                                    <th>Nº proceso</th>
                                    <th>Estado </th>
                                </tr>
                            </thead>
                            <tbody>
                             @foreach ($propuestas as $propuesta)
                                <tr class="body-rows">
                                    <td>{{$propuesta->nombre}}</td>
                                    <td>{{$propuesta->entidad->nombre}}</td>
                                    <td>{{$propuesta->numero_proceso}}</td>
                                    <td>{{ucfirst($propuesta->estado)}}</td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalcreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Nueva Propuesta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <form method="POST" action="{{ route('propuestas.store') }}">
                      @csrf
                      <div class="row my-3">
                          <div class="input-group col-12">
                              <label for="nombre" class="input-group-text col-sm-3 bg-light">Nombre de Propuesta: </label>
                              <input
                                  id="nombre"
                                  type="text" 
                                  name="nombre" 
                                  class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                                  placeholder="Nombre del Proyecto" 
                                  value="{{old('nombre')}}"
                              >
                              <br>
                              @error('nombre')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>
                      <div class="row my-3">
                          <div class="input-group col-6">
                              <label for="numero_proceso" class="input-group-text col-sm-6 bg-light">Numero del Proceso: </label>
                              <input
                                  id="numero_proceso"   
                                  type="text" 
                                  name="numero_proceso" 
                                  class="form-control col-sm-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="Entidad Contratante" 
                                  value="{{old('entidad_contratante')}}"
                              >
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <div class="input-group col-6">
                              <label for="entidad_contratante" class="input-group-text col-sms-6 bg-light">Entidad Contratante: </label>
                              {{-- <input 
                                  type="text" 
                                  name="entidad_contratante" 
                                  class="form-control col-sm-6 @error('entidad_contratante') is-invalid @enderror"  
                                  placeholder="Entidad Contratante" 
                                  value="{{old('entidad_contratante')}}"
                              > --}}
                                <select id="entidad_contratante"  name="entidad_contratante" required autofocus class="form-control">
                                    @foreach ($terceros as $tercero)
                                        <option value="{{$tercero->id}}">{{$tercero->nombre}}</option>
                                    @endforeach
                                </select>
                              <br>
                              @error('entidad_contratante')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>
                      <div class="row my-3">
                          <div class="input-group col-6">
                              <label for="fecha_entrega" class="input-group-text col-sm-6 bg-light">Fecha de Entrega: </label>
                              <input
                                    id="fecha_entrega" 
                                  type="date" 
                                  name="fecha_entrega" 
                                  class="form-control col-sm-6 @error('fecha_entrega') is-invalid @enderror"  
                                  value="{{old('fecha_entrega')}}"
                              >
                              <br>
                              @error('fecha_entrega')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <div class="input-group col-6">
                              <label for="lugar_entrega" class="input-group-text col-sm-6 bg-light">Lugar de Entrega: </label>
                              <input
                                    id="lugar_entrega" 
                                  type="text" 
                                  name="lugar_entrega" 
                                  class="form-control col-sm-6 @error('n_proceso') is-invalid @enderror"  
                                  placeholder="Número de proceso" 
                                  value="{{old('n_proceso')}}"
                              >
                              <br>
                              @error('n_proceso')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>
                      <div class="row my-3">
                          <div class="input-group col-6">
                              <label for="time" class="input-group-text col-sm-6 bg-light">Hora de Entrega: </label>
                              <input 
                                    id="time"
                                  type="time" 
                                  name="hora_entrega" 
                                  class="form-control col-sm-6 @error('n_proceso') is-invalid @enderror"  
                                  placeholder="Número de proceso" 
                                  value="{{old('n_proceso')}}"
                              >
                              <br>
                              @error('n_proceso')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <div class="input-group col-6">
                              <label for="contacto" class="input-group-text col-sm-6 bg-light">Contacto: </label>
                              {{-- <input 
                                  type="text" 
                                  name="contacto" 
                                  class="form-control col-sm-6 @error('n_proceso') is-invalid @enderror"  
                                  placeholder="Número de proceso" 
                                  value="{{old('n_proceso')}}"
                              > --}}
                              <select id="contacto" name="contacto" required autofocus class="form-control">
                                @foreach ($contactos as $contactos)
                                    <option value="{{$contactos->id}}">{{$contactos->nombres}}</option>
                                @endforeach
                            </select>
                              <br>
                              @error('n_proceso')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>
                      {{-- <div class="row my-3">
                          <div class="input-group col-12">
                              <label class="input-group-text col-sm-3 bg-light">Contacto: </label>
                              <select 
                                      class="form-control col-sm-7" 
                                      name="contacto_id"
                              >
                                  {{-- @foreach($contactos  as $contacto)
                                      <option value="{{$contacto->id}}" {{old('contacto_id') == $contacto->id ? 'selected' : ''}}>{{$contacto->nombre}}</option>
                                  @endforeach --}}
                              {{-- </select>
                              <br>
                          </div> --}}
                      {{-- </div>˜ --}}
  
                      <hr>
  
                      <div class="row my-3 justify-content-md-center">  
                              <div class="form-group">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Cerrar </button>
                                <button type="submit" class="btn btn-primary"> Guardar </button>
                              </div>
                        </div>
                  </form>
        </div>
      </div>
    </div>
</div>


@endsection


