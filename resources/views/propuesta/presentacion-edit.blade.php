@extends('layouts.app')

@section('titulo')
    Propuestas
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('propuestas.index')}}">Propuestas</a></li>
                                <li class="breadcrumb-item"><a href="#">Presentacion</a></li>
								<li class="breadcrumb-item active" aria-current="page"><b>Editar</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('propuesta.components.tabs', ['select' => 'presentacion']) 
    </div>
	<div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
                    
					<div class="row">
                        <div class="col-12 text-right mt-2">
                            <label for=""><b> Fecha creacion propuesta</b></label> 
                            <small>######</small>
                        </div>
                    </div>
                    <hr>

					<div class="card">
                        <div class="card-body p-3 border-0" style="background-color:#F4F6F6;">
                            <div class="header-detail-propuesta">
                                <div class="row">
                                    <div class="col-4">
                                        <label class="text-muted">
                                          <b>Interventoria tecnica, administrativa y financ PAE</b>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label for=""> 
                                          <span class="font-weight-bold"> Nº Proceso:</span> 
                                          <small>230-20</small>
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label for="">
                                          <span class="font-weight-bold">Fecha de creación de propuesta : </span>
                                          <small>22/06/20</small>
                                        </label>
                                    </div>
                                            
                                </div>
                                <div class="row mt-2">
                                    <div class="col-4">
                                        <label for="">
                                          <span class="font-weight-bold">Entidad contratante: </span>
                                          <small>Municipio de Yumbo</small>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label for="">
                                          <span class="font-weight-bold">Contacto: </span>
                                           <small>Susana Quintero</small>
                                         </label>
                                    </div>
                                    <div class="col-5">
                                        <label for="">
                                          <span class="font-weight-bold">Fechar inicial de entrega: </span> 
                                           <small>24/06/20</small>
                                         </label>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>


					{{--  --}}
					<div id="accordion" class="mt-3">
						{{-- DOCUMENTOS ADICIONALES --}}
                        <div class="" id="headingOne">
                             <h5 class="mb-0">
                                 <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                     <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Documentos Adicionales
                                 </span>
                             </h5>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <button type="button" class="btn-add-form mb-2" data-toggle="modal" data-target="#modalCreateForm"></button>

								@for ($i = 0; $i < 2; $i++)
									<div class="row mt-2">
										<div class="col-3">
											<label for=""> 
												Poliza de seriedad y muchas mas
											</label>     
										</div>
										<div class="col-1">
											<label for="">210 kb</label>
										</div>
										<div class="col-2">
											<button class="btn-show-form" type="button"></button>
											<button class="btn-delete-form" type="button"></button>
										</div>
									</div>
								@endfor
                                
							</div>
                        </div>

						{{-- GENERACION PROPUESTAS --}}
						<div class="mt-3" id="headingTwo">
							<h5 class="mb-0">
								<span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									<i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Generacion Propuesta
								</span>
							</h5>
					   </div>
					   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
						   <div class="card-body">
							   <button type="button" class="btn-add-form mb-2" data-target="#cantidadDocumentos" data-toggle="modal"></button>

							   @for ($i = 0; $i < 2; $i++)
									<div class="row mt-2">
										<div class="col-3">
											<label for=""> 
												Propuesta tecnica
											</label>     
										</div>
										<div class="col-2">
											<button class="btn-show-form" type="button"></button>
											<button class="btn-delete-form" type="button"></button>
										</div>
									</div>
								@endfor
						   </div>
					   </div>

						{{-- TABLA DE CONTENIDO Y NUMERACION --}}
						<div class="mt-3" id="headingThree">
							<h5 class="mb-0">
								<span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
									<i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Tabla de contenido y numeracion
								</span>
							</h5>
					   </div>
					   <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
						   <div class="card-body">
							   <button type="button" class="btn-add-form mb-2" data-toggle="modal" data-target="#modalAsignar"></button>
						   </div>
					   </div>
					</div>
					{{--  --}}
                </div>
            </div>
        </div>
    </div>


	{{-- MODAL DOCUMENTOS ADICIONALES --}}
	<div id="modalCreateForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Documen adicionales</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" enctype="multipart/form-data">
						
						<div class="row my-3">
							<div class="input-group col-12">
								<label class="input-group-text col-3 bg-light">Nombre de Documento: </label>
								<input 
									type="text" 
									name="name" 
									class="form-control "  
									placeholder="Nombre de Documento" 
								>
							</div>
						</div>
						<div class="row my-3 justify-content-md-center">
							<div class="col-md-8">
							<input class="file_certificado" name="documento" type="file">
							</div>
						</div>

						<div class="row justify-content-md-center">
							<div class="form-group">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" wire:click="save">Cargar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	{{-- MODAL CANTIDAD DOCUMENTOS --}}
	<div id="cantidadDocumentos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Cantidad de documentos</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-5">Cantidad de archivos</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						@for ($i = 1; $i <= 3; $i++)
							<div class="row my-3">
								<div class="input-group col-md-10">
									<label for="" class="input-group-text bg-light col-sm-5">Nombre archivo {{ $i }}</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
						@endfor

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>

	{{-- MODAL ASIGNAR DOCUMENTOS --}}
	<div id="modalAsignar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Asignar documentos</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mt-2">Asignar documentos en el orden que desea generar la propuesta</p>
					<br>
					<form action="">
						<div class="row my-3">
							<div class="col-md-6">
								<div class="input-group">
									<label for="" class="col-sm-12  text-center" >Propuesta tecnica</label>
									<textarea name="" id="" cols="10" rows="40" class="form-control"></textarea>
								</div>
							</div>
							<div style="margin-top:250px" >
								<i class="mdi mdi-arrow-left-bold" style="color:#229954;width:150px"></i>
								<br>
								<i class="mdi mdi-arrow-right-bold" style="color:#2E86C1;width:150px"></i>
							</div>
							<div class="col-md-5">
								<div class="input-group">
									<label for="" class="col-sm-12 text-center" >Todos los documentos de la propuesta</label>
									<textarea name="" id="" cols="10" rows="40" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">cancelar</button>
								<button type="submit" class="btn btn-primary">Cargar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection