@extends('layouts.app')

@section('titulo')
    Propuestas
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('propuestas.index')}}">Propuestas</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><b>Presentacion</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        @include('propuesta.components.tabs', ['select' => 'presentacion']) 
    </div>
	<div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
                   
                    <div class="col-sm-12">
                        <div class="table-responsive">   
                            <table id="table" class="display" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre Propuesta</th>
                                        <th>Entidad contratante</th>
                                        <th>Nº proceso</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="body-rows">
                                        <td class="text-capitalize">interventoria tecnica</td>
                                        <td>Gobernacion del valle del cauca</td>
                                        <td>5420-20</td>
                                        <td>
                                            <div class="btn-group col-sm">
                                                <a href="" class="btn-edit-form"></a>
                                                <a href="" class="btn btn-inverse-dark"><span class="mdi mdi-arrow-right"></span></a>
                                            </div>
                                        </td>
                                    </tr>
								</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection