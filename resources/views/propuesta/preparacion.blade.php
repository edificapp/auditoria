@extends('layouts.app')

@section('titulo')
Propuestas
@endsection

{{-- @section('styles')
    <style> 
        th { white-space:nowrap; }

        table.dataTable.display thead th{
            font-size: 15px;
            padding: 18px;
            /* padding-left:10px;
            padding-top: 10px; */
            text-align: left;
            /* border-style: none;
            border-bottom: 1px solid #808B96; */
        }

        table.dataTable.display tbody td {
            padding: 5px;
            padding-left:10px;
            font-size: 13px;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            /* border-style: none;
            border-bottom: 1px solid #D5DBDB; */
        }
        table.dataTable.display tbody tr.group td{
            /* text-align:; */
            font-size: 14px;
            font-weight: bold;
            padding: 3px; 
        }
        .body-rows{
        background-color:#FDFEFE !important;
        }

    </style>
@stop --}}

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('propuestas.index')}}">Propuestas</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><b>Preparacion</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                {{-- <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <button type="button" class="btn btn-secondary mt-2 mt-xl-0" data-toggle="modal" data-target="#modalcreate">
                    <span class="mdi mdi-folder-plus"></span>
                    </button>
                </div> --}}
            </div>
        </div>
    </div>
    <div>
        @include('propuesta.components.tabs', ['select' => 'preparacion']) 
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body borde">
                    <p class="card-title">Listado de propuestas</p>
                    <div class="col-sm-12">
                        <div class="table-responsive">   
                            <table id="table" class="display" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre Propuesta</th>
                                        <th>Entidad contratante</th>
                                        <th>Nº proceso</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($propuestas as $propuesta)
                                    <tr class="body-rows">
                                        <td>{{$propuesta->nombre}}</td>
                                        <td>{{$propuesta->entidad->nombre}}</td>
                                        <td>{{$propuesta->numero_proceso}}</td>
                                        <td>{{ucfirst($propuesta->estado)}}</td>
                                        <td>
                                            <div class="btn-group col-sm">
                                                <a href="{{route('propuestas.preparacion.edit', $propuesta->id)}}" class="btn-edit-form"></a>
                                                @if(false)
                                                    <a href="{{route('propuestas.preparacion.edit', $propuesta->id)}}" class="btn btn-inverse-dark"><span class="mdi mdi-arrow-right"></span></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


