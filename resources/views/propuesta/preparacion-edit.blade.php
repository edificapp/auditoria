@extends('layouts.app')

@section('titulo')
Propuestas
@endsection

@section('contenido')

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('propuestas.index')}}">Propuestas</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('propuestas.preparacion') }}">Preparacion</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><b>Editar</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        @include('propuesta.components.tabs', ['select' => 'preparacion']) 
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body borde">
                    <div class="row">
                        <div class="col-12 text-right mt-2">
                            <label for=""><b> Fecha de entrega actual:</b> </label> 
                            <small>{{$propuesta->fecha_entrega}}</small>
                        </div>
                    </div>
                    <hr>

                    <div class="card">
                        <div class="card-body p-3 border-0" style="background-color:#F4F6F6;">
                            <div class="header-detail-propuesta">
                                <div class="row">
                                    <div class="col-4">
                                        <label class="text-muted">
                                          <b> {{$propuesta->nombre}} </b>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label for=""> 
                                          <span class="font-weight-bold"> Nº Proceso:</span> 
                                          <small>{{$propuesta->numero_proceso}}</small>
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label for="">
                                          <span class="font-weight-bold">Fecha de creación de propuesta : </span>
                                          <small>{{$propuesta->created_at}}</small>
                                        </label>
                                    </div>
                                            
                                </div>
                                <div class="row mt-2">
                                    <div class="col-4">
                                        <label for="">
                                          <span class="font-weight-bold">Entidad contratante: </span>
                                          <small>{{$propuesta->entidad->nombre}}</small>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label for="">
                                          <span class="font-weight-bold">Contacto: </span>
                                           <small>{{$propuesta->contact->nombres}}</small>
                                         </label>
                                    </div>
                                    <div class="col-5">
                                        <label for="">
                                          <span class="font-weight-bold">Fechar inicial de entrega: </span> 
                                           <small>{{$propuesta->fecha_entrega}}</small>
                                         </label>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-3">
                        <label for="" class="font-weight-bold"> Requerimientos Empresa </label>     
                    </div>

                    <div id="accordion">
                        <div class="" id="headingOne">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Contratos Vinculados a la Persona
                                </span>
                            </h5>
                       </div>
                  
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form mb-3" type="button" title="añadir contratos" id="contratos" > 
                                   
                                </button>

                                <div class="row mt-2">
                                    <div class="col-3">
                                        <label for=""> 
                                            Nombre del contrato
                                        </label>     
                                    </div>
                                </div>
                                @foreach ($propuesta->contratos as $key => $documento)
                                <div class="row mt-2">
                                    <div class="col-sm-4">
                                        <label for=""> 
                                            {{$key+1}}. {{$documento->name}}
                                        </label>
                                         
                                        <button class="btn-show-form btn-xs ml-2" type="button"></button> 
                                        <button class="btn-delete-form btn-xs ml-2" type="button"></button> 
                                        
                                    </div>
                                    @foreach ($documento->code_uns as $code)
                                        <div class="col-2">
                                            <small><label for=""> {{$code->code_uns->code}} - {{$code->code_uns->name}} </label> </small>    
                                        </div>
                                    @endforeach    
                                    <div class="col-2">
                                        <label for=""> <small>{{$documento->tiempo_anos}} años y {{$documento->tiempo_meses}} meses</small></label>
                                    </div>
                                    <div class="col-2">
                                        <label for=""> <small>{{$documento->monto}} SMLV</small></label>
                                    </div>
                                    
                                </div>
                                @endforeach  

                                <div class="row mt-4">
                                    <div class="col-3" style="margin-left: 5px">
                                        <h5 >Codigo Unspsc</h5>       
                                    </div>
                                    @foreach ($propuesta->code_uns as $code)
                                        <div class="col-4">
                                            <small>{{$code->code_uns->code}} - {{$code->code_uns->name}}</small>
                                        </div>
                                    @endforeach     
                                    <div class="col-4" style="display: flex;justify-content: flex-end;">
                                        @if($code_status)
                                            <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                        @else   
                                        <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                        @endif
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <div class="col-6" style="border-right: 1px dashed #80808059">
                                        <div class="row mt-2">
                                            <div class="col-6" style="margin-left: 5px">
                                                <span >Cantidad de Contratos</span>       
                                            </div>
                                            @foreach ($propuesta->experiencia as $experiencia)
                                            <div class="col">
                                                {{$experiencia->contrato_cantidad}}
                                            </div>
                                            @endforeach     
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-6" style="margin-left: 5px">
                                                <span>Tiempo de Ejecucion</span>       
                                            </div>
                                            @foreach ($propuesta->experiencia as $experiencia)
                                            <div class="col">
                                                {{$experiencia->tiempo_anos}} años y {{$experiencia->tiempo_meses}} meses
                                            </div>
                                            @endforeach         
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-6" style="margin-left: 5px">
                                                <span>Monto Acumulado</span>       
                                            </div>
                                            @foreach ($propuesta->experiencia as $experiencia)
                                            <div class="col">
                                                {{$experiencia->monto_smlv}} MSLV
                                            </div>
                                        </div>
                                        @endforeach     
                                    </div>
                                    {{-- comparacion --}}
                                    <div class="col-4" style="margin-left:20px">
                                        <div class="row mt-2">
                                            <div class="col">
                                                {{count($propuesta->contratos)}}
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col">
                                                @php
                                                    $anos = 0;
                                                    $meses = 0;
                                                @endphp
                                                @foreach ($propuesta->contratos as $documento)
                                                    @php
                                                        $anos = $anos + $documento->tiempo_anos; 
                                                        $meses = $meses + $documento->tiempo_meses; 
                                                    @endphp
                                                @endforeach         
                                            {{$anos}} Años y {{$meses}} meses
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col">
                                                @php
                                                $monto = 0;
                                                @endphp
                                                @foreach ($propuesta->contratos as $documento)
                                                @php
                                                    $monto = $monto + $documento->monto;  
                                                @endphp
                                                @endforeach         
                                            {{$monto}} SMLV
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1" style="margin-left:20px">
                                        <div class="row mt-2">
                                            <div class="col">
                                                @if($cantidad_status)
                                                <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                                @else   
                                                <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col">
                                            @if($tiempo_status)
                                                <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                            @else   
                                            <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                            @endif
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col">
                                            @if($monto_status)
                                                <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                            @else   
                                            <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3" id="headingTwo">
                            <h5 class="mb-0">
                                <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>
                                    Talento Humano    
                                </span>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form mb-2" type="button" id="abrirModalTalentoHumano" title="agregar talento humano">
                                    
                                </button>
                                <table id="tabla" class="display table-responsive" width="100%">
                                    <thead>
                                        <th>Nombre</th>
                                        <th>Cargo</th>
                                        <th>Profesion</th>
                                        <th>Dedicación</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        @foreach($propuesta->talentos_finalizados as $item)
                                            <tr class="body-rows">
                                                <td>{{$item->competente->curriculum->full_name}}</td>
                                                <td>{{$item->cargo}}</td>
                                                <td>{{$item->profesion->nombre}}</td>
                                                <td class="text-center">{{$item->dedicacion}} %</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn-show-form" type="button"></button>
                                                        <button class="btn-delete-form" type="button"></button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="mt-3" id="headingThree">
                            <h5 class="mb-0">
                                <div  class="collapsed pd-2"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Propuestas Economicas                              
                                </div>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                
                                <div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <h5>Factor prestacional 1,65
                                                <a data-toggle="modal" data-target="#modal">
                                                    <i class="mdi mdi-pencil text-muted"></i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div class="col-sm-1" style="display: flex;justify-content: flex-end;">
                                            <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                            <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="row mt-3 mb-3 ">
                                        <div class="col-6">
                                            Costos directos
                                            <a data-toggle="modal" data-target="#modalCostos">
                                                <i class="mdi mdi-plus" style="color: red"></i>
                                            </a>
                                        </div>
                                    </div>

                                    @for ($i = 0; $i < 3; $i++)
                                        <div class="row mt-1">
                                            <div class="col-2 ">
                                                <label for=""> 
                                                    Nombre del contrato 
                                                </label>    
                                            </div>
                                            <div class="col-2">
                                                <label > 
                                                    $ $$$$$$$$$$
                                                </label>  
                                            </div>
                                            <div class="col-3">
                                                <button class="btn-show-form" type="button"></button>
                                                <button class="btn-delete-form" type="button"></button>
                                            </div>
                                        </div>
                                    @endfor
                                
                                    <div class="row">
                                        <div class="col-2">
                                            <label class="ml-5" > 
                                                <b>Subtotal costos directos</b>
                                            </label>  
                                        </div>
                                        <div class="col-2 text-left">
                                            <label > 
                                                <b>$ $$$$$$$$$$</b> 
                                            </label>  
                                        </div>
                                        <div class="col-sm-2" style="display: flex;justify-content: flex-end;">
                                            <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                            <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}

                                <div class="mt-4">
                                    <div class="row mt-3 mb-3 ">
                                        <div class="col-6">
                                            Costos indirectos
                                            <a data-toggle="modal" data-target="#modalCostos">
                                                <i class="mdi mdi-plus" style="color: red"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-sm-2 ">
                                            <label for=""> 
                                                Concepto 1 
                                            </label>    
                                        </div>
                                        <div class="col-sm-2">
                                            <label > 
                                                $ $$$$$$$$$$
                                            </label>  
                                        </div>
                                        <div class="col-sm-3">
                                            <button class="btn-show-form" type="button"></button>
                                            <button class="btn-delete-form" type="button"></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label class="ml-4" > 
                                                <b>Subtotal costos indirectos</b>
                                            </label>  
                                        </div>
                                        <div class="col-sm-2 text-left">
                                            <label > 
                                                <b>$ $$$$$$$$$$</b> 
                                            </label>  
                                        </div>
                                        <div class="col-sm-2" style="display: flex;justify-content: flex-end;">
                                            <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                            <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                        </div>
                                    </div>
                                </div>
                                
                                {{--  --}}
                                <div class="row mt-3">
                                    <div class="col-sm-2">
                                        <label> Costos directos + Costos indirectos <label>  
                                    </div>
                                    <div class="col-sm-2 text-left">
                                        <label > $ $$$$$$$$$$ </label>  
                                    </div>
                                </div>

                                <div class="mt-4">

                                    <div class="row mt-3 mb-3 ">
                                        <div class="col-6">
                                            Impuestos
                                            <a data-toggle="modal" data-target="#modalImpuestos">
                                                <i class="mdi mdi-plus" style="color: red"></i>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-2">
                                            <label> IVA (19%) <label>  
                                        </div>
                                        <div class="col-sm-2 text-left">
                                            <label > $ $$$$$$$$$$ </label>  
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label class="ml-4" > 
                                                <b>Subtotal impuestos</b>
                                            </label>  
                                        </div>
                                        <div class="col-sm-2 text-left">
                                            <label > 
                                                <b>$ $$$$$$$$$$</b> 
                                            </label>  
                                        </div>
                                        <div class="col-sm-2" style="display: flex;justify-content: flex-end;">
                                            <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                            <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-sm-2">
                                        <label class="ml-4" > 
                                            <b>Total propuesta economica </b>
                                        </label>  
                                    </div>
                                    <div class="col-sm-2 text-left">
                                        <label > 
                                            <b>$ $$$$$$$$$$</b> 
                                        </label>  
                                    </div>
                                    
                                    <div class="col-sm-2" style="display: flex;justify-content: flex-end;">
                                        <i class="mdi mdi-check" style="color:green;font-weight:bold"></i>
                                        <i class="mdi mdi-close" style="color:red;font-weight:bold"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    {{-- MODAL AÑADIR CONTRATOS --}}
    <div class="modal fade" id="contratoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir Contratos que soporten la experiencia del proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('propuestas.preparacion.contrato') }}">
                        @csrf
                        <div class="row my-3">
                            <div class="input-group col-12">
                                <label for="contrato" class="input-group-text col-sm-3 bg-light">Contrato: </label>
                                <input  
                                    id="contrato"
                                    type="text" 
                                    name="name" 
                                    class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                                    placeholder="Nombre Contrato" 
                                    value="{{old('nombre')}}"
                                >
                                <br>
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12" style="position: relative;
                            display: flex;
                            flex-wrap: initial;
                            align-items: stretch;
                            width: 100%;"
                            >
                                <label for="time_anos" class="input-group-text col-sm-4 pl-2 bg-light">Tiempo de ejecucion: </label>
                                <div class="col-sm-4" style="position: relative;display: flex;flex-wrap: initial;align-items: stretch;width: 100%;">
                                <input  
                                    id="time_anos"
                                    type="text" 
                                    name="time_anos" 
                                    class="form-control col-sm-8"  
                                    placeholder="##" 
                                    value="{{old('entidad_contratante')}}"
                                >                                    
                                <div class="input-group">
                                    <span class="input-group-text pl-2 " id="basic-addon2">Años</span>
                                </div>
                                <input
                                    for="time_meses"  
                                    type="text" 
                                    name="time_meses" 
                                    class="form-control col-sm-6 "  
                                    placeholder="###" 
                                    value="{{old('entidad_contratante')}}"
                                    > 
                                    <div class="input-group">
                                    <span for="time_meses" class="input-group-text col-sm" id="basic-addon2">Meses</span>
                                    </div>
                                </div>
                                <br>
                                @error('entidad_contratante')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-12">
                                <label for="monto" class="input-group-text col-sm-4 bg-light">Monto: </label>
                                <input  
                                    id="monto"
                                    type="text" 
                                    name="monto" 
                                    class="form-control col-sm-6 @error('entidad_contratante') is-invalid @enderror"  
                                    placeholder="Monto SMLV" 
                                    value="{{old('entidad_contratante')}}"
                                >
                                <br>
                                @error('entidad_contratante')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-12">
                                <label class="input-group-text col-sm-4 bg-light">Codigos UNSPSC: </label>
                                <input type="text" id="input_unspscs" class="form-control col-sm-10">
                                <br>
                                @error('entidad_contratante')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <input type="hidden" name="propuesta_id" value="{{$propuesta->id}}">
                        <hr>
                        <div class="row my-3 justify-content-md-center">
                            <div class="form-group">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary"> Guardar </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalTalentoHumano" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Asignar personal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" id="formAddCompetentes">
                        @csrf
                        <input type="hidden" name="_method" value="patch">
                        <div class="row my-3">
                            <div class="input-group col-12">
                                <label class="input-group-text col-sm-2 bg-light pl-2">Cargo:</label>
                                <select class="form-control" name="talento" id="cargoTalentoHumano">
                                    @foreach($propuesta->talento_propuesta as $item)
                                        <option value="{{$item->id}}">{{$item->cargo}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-12">
                                <label class="input-group-text col-sm-4 bg-light pl-2">Persona Competente:</label>
                                <select class="form-control" name="competente_id" id="competentes">
                                </select>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p class="card-text">Seleccionar experiencia que aplica para esta propuesta</p>
                        </div>
                        <div class="row ml-auto">
                            <div class="card-body">
                                @foreach(auth()->user()->empresa->experiencias as $item)
                                    <div class="form-check ml-2">
                                        <input type="checkbox" class="form-check-input ml-1 mr-2" name="proyectos[]" value="{{$item->id}}" id="contrato{{$item->id}}">
                                        <label class="form-check-label p-0" for="contrato{{$item->id}}">
                                            {{$item->nombre_contrato}} 
                                            <button class="btn-show-form ml-2" type="button"></button>
                                        </label>    
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        
                        <hr>
                        <div class="row my-2 justify-content-md-center justify-content-sm-center">
                            <div class="form-group">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="button" class="btn btn-primary" onclick="agregarCompetente()">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL EDITAR TALENTO HUMANO --}}
     <div class="modal fade" id="modalEditarTalentoHumano" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Personal</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <form method="POST" id="formAddCompetentes">
                  @csrf
                  <input type="hidden" name="_method" value="patch">
                  <div class="row my-3">
                      <div class="input-group col-12">
                          <label class="input-group-text col-sm-3 bg-light">Cargo: </label>
                          <select class="form-control" name="talento" id="cargoTalentoHumano">
                              @foreach($propuesta->talento_propuesta as $item)
                                <option value="{{$item->id}}">{{$item->cargo}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="row my-3">
                      <div class="input-group col-12">
                          <label class="input-group-text col-sm-3 bg-light">Personal Competente: </label>
                          <select class="form-control" name="competente_id" id="competentes">
                          </select>
                      </div>
                  </div>
                  <div class="row my-3 ml-4">
                    @foreach(auth()->user()->empresa->experiencias as $item)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="proyectos[]" value="{{$item->id}}" id="contrato{{$item->id}}">
                            <label class="form-check-label" for="contrato{{$item->id}}">{{$item->nombre_contrato}}</label>
                        </div>
                    @endforeach
                  </div>
                  
                  <hr>
                  <div class="row my-3 justify-content-md-center">
                    <div class="form-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="agregarCompetente()">Guardar</button>
                    </div>
                  </div>
                </form>
            </div>
          </div>
        </div>
    </div>

    {{-- MODAL CODIGOS UNSPSC  --}}
    <div id="modalCode" class="modal fade" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog modal-sm modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header white">
                    <h4 class="modal-title white text-center" id="myModalLabel11">Codigos Unspsc</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" id="buscar_code" class="form-control">
                    <br><br>
                    <div class="btn-group-vertical">
                    </div>
                    <div class="row m-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>

    {{-- MODAL COSTOS DIRECTOS --}}
    <div id="modalCostos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Agregar costos directos</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="row my-3">
                            <div class="input-group col-md-12">
                                <label for="" class="input-group-text bg-light col-sm-3">Concepto</label>
                                <select name="" id="" class="form-control col-sm-9">
                                    <option value=""></option>
                                    <option value="">Costo de personal</option>
                                    <option value="">Laboratorio</option>
                                    <option value="">Transporte</option>
                                    <option value="">Otro</option>
                                </select>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-md-7">
                                <label for="" class="input-group-text bg-light col-sm-4">Grupos</label>
                                <input type="text" class="form-control col-sm-9" >
                            </div>
                        </div>
                        
                        <div class="row my-3">
                            <div class="input-group col-md-10">
                                <label for="" class="input-group-text bg-light col-sm-5">Nombre grupo</label>
                                <input type="text" class="form-control col-sm-9">
                             </div>
                        </div>

                        <div class="row justify-content-md-center">
                            <div class="form-group">
                                <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- MODAL IMPUESTOS --}}
    <div id="modalImpuestos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Agregar Impuestos</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">

                        <div class="row my-3">
                            <div class="input-group col-md-12">
                                <label for="" class="input-group-text bg-light col-sm-3">Concepto</label>
                                <select name="" id="" class="form-control col-sm-9">
                                    <option value="">IVA 19%</option>
                                </select>
                            </div>
                        </div>

                        <div class="row my-3">
                            <div class="input-group col-md-9">
                                <label for="" class="input-group-text bg-left col-sm-4">Porcentaje</label>
                                <input class="form-control" type="text" name="">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-light">%</span>
                                  </div>
                            </div>
                        </div>

                        <div class="row my-2 justify-content-md-center">
                            <div class="form-goup">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript">
   
    var codes_select = [];
    const talentos_finalizados = @json($propuesta->talentos_finalizados) 
        $(document).ready(function(){
            
            $('#tabla').DataTable ({
                paging:false, info:false, searching:false, ordering:false
            });

        $('nav-link').removeClass('active');
         $('#create').on('click', function(){
           $('#modalFormCreate').modal('show'); 
         });
         $('#contratos').on('click', function(){
           $('#contratoModal').modal('show'); 
         });
        
        $('#talento_humano').on('click', function(){
            $('#talentoHumano').modal('show');
        });

        // -----
        $('#select-unspsc').select2({
            multiple: true,
            allowClear: true,
        });
        $('#select-unspsc').val(null).trigger('change');
        // -------

        cargarCompetentes();
        console.log('finalizados', talentos_finalizados);
    });

    $('#input_unspscs').on('click', function(){
            $('#modalCode').modal('show');
        });

        // ------
        $('#buscar_code').on('keyup', function(){
            var val = $(this).val();
            if(val.length > 3){
                $.get( "/code-unspsc/"+val, function( data ) {
                    var datos = data.filter(function(item){
                        return codes_select.includes(item.id) == false;
                    })
                    $('.btn-group-vertical').empty();

                    $.each(datos, function( index, value ) {
                        $('.btn-group-vertical').append('<button onclick="button_code('+value.id+','+value.code+')" class="btn btn-block btn-secondary" id="button_code-'+value.code+'">'+value.code+' - '+value.name+'</button>');
                    });
                });
            }
        })
        // ----

    $('#abrirModalTalentoHumano').on('click', function(){
        $('#modalTalentoHumano').modal();
    })

    $('#cargoTalentoHumano').on('change', function(){
        cargarCompetentes();        
    });

    function cargarCompetentes(){
        let talento = $('#cargoTalentoHumano').val();
        $.get(`/profesion-competentes/${talento}`, function(data, status){
            $('#competentes').empty();
            console.log('dd', data);
            $.each(data, function( index, value ) {
                $('#competentes').append(`<option value="${value.user_id}">${value.nombre}</option>`);
            });
        });
    }

    function agregarCompetente(){
        let talento = $('#cargoTalentoHumano').val();
        $('#formAddCompetentes').attr('action', `/profesion-competentes/${talento}`).submit();
    }

    function ver(experiencia){
        console.log(experiencia)
        $('#experienciaDetailModal').modal('show'); 
        $('#cantidad_contratos').val(experiencia.contrato_cantidad)
        $('#tiempo_anos').val(experiencia.tiempo_anos)
        $('#tiempo_meses').val(experiencia.tiempo_meses)
        $('#monto_smlv').val(experiencia.monto_smlv)
        $('#experien').val(experiencia.id)
    }

    // ------

    function button_code(id, code){
            codes_select.push(id);
            var val_input = $('#input_unspscs').val();
            $('#div_codigos_unspsc').append('<input type="hidden" name="codigos_unspsc[]" value="'+id+'"/>');
            if(val_input == ''){
                $('#input_unspscs').val(code);
            }else{
                $('#input_unspscs').val(val_input+' | '+code);
            }
            $('#button_code-'+code).addClass('invisible');
        }

        $( '#modalCode' ).on( 'hidden.bs.modal' , function() {
            $( 'body' ).addClass( 'modal-open' );
        } );

    // -------
    </script>
@stop
