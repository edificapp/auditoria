<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{route ('getServicioCliente')}}" class="nav-link {{$select != 'cliente' ?:'active'}}">Cliente</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('getServicioTecnico')}}" class="nav-link {{$select != 'servicio' ?:'active'}}">Servicio Tecnico</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('getServicioDevolucion')}}" class="nav-link {{$select != 'devoluciones' ?: 'active'}}">Devoluciones</a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link {{$select != 'reclamaciones' ?: 'active'}}">Reclamaciones</a>
    </li>
</ul>
