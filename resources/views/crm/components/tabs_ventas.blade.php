<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{route('ventas.gestion')}}" class="nav-link {{ $select != 'gestion' ?:'active'}}">Gestion</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('ventas.cotizacion')}}" class="nav-link {{$select != 'cotizaciones' ?: 'active'}}">Cotizaciones</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('ventas.ventas')}}" class="nav-link {{$select != 'ventas' ?: 'active'}}">Ventas</a>
    </li>
</ul>