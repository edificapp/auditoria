<ul class="nav nav-pills bg-transparent">
    <li class="nav-item">
        <a class="nav-link {{$select != 'servicios' ?:'active'}}" href="{{route('getServicioCliente')}}">Servicios</a>
    </li>
    <li class="nav-item">
        <a href="{{route('ventas.index')}}" class="nav-link {{$select != 'ventas' ?:'active'}}">Ventas</a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link {{$select != 'marketing' ?:'active'}}">Marketing</a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link {{$select != 'secop' ?:'active'}}">SECOP II</a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link {{$select != 'informes' ?:'active'}}">Informes</a>
    </li>
</ul>

<hr style="margin-top: 0;margin-bottom:2px; border: 0.5px solid #D7DBDD ;">