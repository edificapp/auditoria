@extends('layouts.app')

@section('titulo')
CRM
@endsection
@section('styles')
	<style>
		.circulo{
			width: 250px;
			height:250px;
            color: #FDFEFE;
			-moz-border-radius: 50%;
			-webkit-border-radius: 50%;
			border-radius: 50%;
		}
		.text-p{
			margin-top: 60px;
			text-align: center;
			font-size: 25px;
			line-height:30px;
		}
	</style>
@endsection


@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>CRM</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@include('crm.components.tabs',['select'=>''])
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body borde">

                <div class="row row-cols-md-4">
                    <div class="col">
                        <div class="card text-whit mb-2 circulo" style="background-color:#6cca51 ">
                            <div class="card-body">
                                <p class="text-p">CLIENTES ACTIVOS <br> 32 </p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card text-white  mb-2 circulo" style="background-color:#d3d34d">
                            <div class="card-body">
                                <p class="text-p">CLIENTES INACTIVOS <br> 15 </p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card text-white mb-2 circulo" style="background-color: #46cfb8">
                            <div class="card-body">
                                <p class="text-p">CLIENTES POTENCIALES <br> 40 </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    
@stop