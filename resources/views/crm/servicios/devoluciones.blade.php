@extends('layouts.app')

@section('titulo')
CRM
@endsection


@section('styles')
	<style>
		table.dataTable.tabla thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
        border-bottom: 1px solid #808B96;
    }

    table.dataTable.tabla tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
    }
	</style>
@endsection

@section('contenido')
	@php
        $devoluciones = array(	
        
            array(
                'nombre' => "Fergon Outsourcing",
                'fecha'  => "28-01-2021",
                'codigo'  => "58745825",
                'nombrep' => 'Rodillo difusor',
                'motivo' => 'no es la referencia solicitada'
            ),
			array(
                'nombre' => "Fundapadua",
                'fecha'  => "12-01-2021",
                'codigo'  => "55523669",
                'nombrep' => 'ua fusoras',
                'motivo' => 'es muy grande'
            ),
			array(
                'nombre' => "Liceo Frances",
                'fecha'  => "30-12-2020",
                'codigo'  => "45222002",
                'nombrep' => 'toner 2800',
                'motivo' => 'es para una kyocera 2801'
            ),
        );
    @endphp
	
    <div class="row">
        <div class="col-md-12 grid-margin">
			<div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb p-2">
							<li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
							<li class="breadcrumb-item"><a href="{{route('crm.index')}}">CRM</a></li>
							<li class="breadcrumb-item"><a href="{{route('getServicioCliente')}}">Servicios</a></li>
							<li class="breadcrumb-item active" aria-current="page">Devoluciones</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
    </div>
    <div>
        @include('crm.components.tabs',['select'=>'servicios'])
    </div>
    <div>
        @include('crm.components.tabs_servicio',['select'=>'devoluciones'])
    </div>
	<div class="row">
		<div class="col-md-12 grid-margin"> 
			<div class="card">
				<div class="card-body borde">
					<p class="h5">
						Devoluciones 
						<button 
							class="btn btn-primary btn-sm" 
							type="button" data-toggle="modal" 
							data-target="#modalDevoluciones" 
							title="crear devolucion"
						><i class="mdi mdi-library-plus"></i>
						</button>
					</p>


					<p class="h5 my-3">Historial devoluciones</p>
					<div class="table-responsive">
						<table class="tabla" id="tablaDevolucion" width="100%">
							<thead class="thead-light">
								<tr>
									<th>Nombre</th>
									<th>Fecha devolucion</th>
									<th>Codigo producto</th>
									<th>Nombre producto</th>
									<th>Motivo devolucion</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($devoluciones as $devolucion => $item)
									<tr>
										<td>{{ $item['nombre']}}</td>
										<td>{{ $item['fecha']}}</td>
										<td>{{ $item['codigo']}}</td>
										<td>{{ $item['nombrep']}}</td>
										<td>{{ $item['motivo']}}</td>
										<th><button class="btn-show-form" title="ver devolucion"></button></th>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modalDevoluciones" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="devolucion" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header white">
					<h5 class="modal-title" id="devolucion">Devolucion</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row my-3">
						<div class="input-group col-sm-12"> 
							<label for="n_factura" class="input-group-text bg-light col-sm-3">No. Factura</label>
							<button type="button" class="btn btn-primary ml-1 mr-1 btn-sm"><i class="mdi mdi-magnify"></i></button>
							<input type="text" name="n_factura" id="n_factura" class="form-control col-sm-9">
						</div>
					</div>
					<form action="">
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="nombre" class="input-group-text bg-light col-sm-3">Nombre</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-6">
								<label for="tipoD" class="input-group-text bg-light col-sm-5">Tipo de documento</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
							<div class="input-group col-sm-6">
								<label for="n_documento" class="input-group-text bg-light col-sm-5">No. Documento</label>
								<input type="text" id="n_docuemnto" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-6">
								<label for="correo" class="input-group-text bg-light col-sm-5">Correo Electronico</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
							<div class="input-group col-sm-6">
								<label for="celular" class="input-group-text bg-light col-sm-5">No. Celular</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="direccion" class="input-group-text bg-light col-sm-3">Direccion</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-6">
								<label for="" class="input-group-text bg-light col-sm-5">Fecha compra</label>
								<input type="date"  id="" class="form-control col-sm-9" disabled>
							</div>
							<div class="input-group col-sm-6">
								<label for="" class="input-group-text col-sm-5 bg-light">Fecha Devolucion</label>
								<input type="date"  id="" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-6">
								<label for="" class="input-group-text bg-light col-sm-5">Producto a devolver</label>
								<select class="select-multiple form-control" name="" multiple="multiple">
									<option value="1">Rodillo difusor</option>
									<option value="2">Rodillo difusor</option>
									<option value="3">Rodillo difusor</option>
								</select>
							</div>
							<div class="input-group col-sm-6">
								<label for="" class="input-group-text col-sm-5 bg-light">Codigo producto</label>
								<input type="text" name="" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="" class="input-group-text bg-light col-sm-3">Motivo devolucion</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="" class="input-group-text bg-light col-sm-3">Observaciones</label>
								<textarea name="" id="" rows="2" class="form-control col-sm-9"></textarea>
							</div>
						</div>
						<div class="row my-2 justify-content-md-center">
							<div class="form-group">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Aceptar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script>
	$(document).ready(function() {
		$('.select-multiple').select2();
		$('#tablaDevolucion').DataTable({
			info:false,searching:false, paging: false,ordering: false,

		});
	} );
</script>
@endsection