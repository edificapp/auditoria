
@extends('layouts.app')

@section('titulo')
CRM
@endsection

@section('styles')
	<style>
		table.dataTable.tablaConsultas thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
        border-bottom: 1px solid #808B96;
    }

    table.dataTable.tablaConsultas tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
    }
	</style>
@endsection

@Section ('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('crm.index')}}">CRM</a></li>
                            <li class="breadcrumb-item"><a href="{{route('getServicioCliente') }}">Servicios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Cliente</b></li>
                        </ol>
                    </nav>
                </div>
				<div class="d-flex justify-content-around align-items-end flex-wrap">
					<a href="#" class="text-secondary" ><i class="mdi mdi-subdirectory-arrow-left"></i></a>
				</div>
        </div>
    </div>
</div>
<div>
    @include('crm.components.tabs', ['select' => 'servicios'])
</div>
<div>
    @include('crm.components.tabs_servicio', ['select' =>'cliente'])
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body borde">

				<form method="" action="">
					<div class="row">
						<div class="input-group my-3 col-sm-6">
							<label class="input-group-text  bg-light col-sm-3 p-2"> Buscar por </label>
							<select class="select-multiple form-control col-sm-6" name="" multiple="multiple">
								<option value="1">Nombre</option>
								<option value="2">cedula</option>
								<option value="3">Nit</option>  
							</select>
						</div>
						<div class="input-group my-3 col-sm-6">
							<button class="btn btn-primary p-1 mr-1" type="button"><i class="mdi mdi-magnify"></i></button>
							<select class="select-multiple form-control col-sm-9" name="" multiple="multiple">
								<option value="1">Fergon Outsourcing SAS</option>
								<option value="2">PE&S</option>
							</select>
						</div>
					</div>
					<div class="row mt-3 justify-content-md-center">
						<div class="form-group">
							<button type="submit" class="btn btn-primary" >Aceptar</button>
							<button type="button" class="btn btn-secondary">Cancelar</button>
						</div>
					</div>
				</form>
				
                <div class="dropdown">
                    <button id="my-dropdown" class="btn button-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filtrar</button>
                    <div class="dropdown-menu" aria-labelledby="my-dropdown">
                        <a class="dropdown-item" href="#">Cotizacion</a>
						<a class="dropdown-item" href="#">Propuestas</a>
						<a class="dropdown-item" href="#">Ventas</a>
						<a class="dropdown-item" href="#">Servicio Tecnico</a>
						<a class="dropdown-item" href="#">Devoluciones</a>
                    </div>
                </div>
				<div class="d-flex justify-content-end">
					<p class="h5">
						Exportar <button class="btn btn-primary btn-xs" type="button"><i class="mdi mdi-download"></i></button>
					</p>
				</div>
				

				<div class="table-responsive">
					<table class="display" id="tablaConsultas" width="100%">
						<thead>
							<tr>
								<th class="h4">Cotizacion</th>
								<th class="h4">Valor</th>
								<th class="h4">Fecha</th>
								<th class="h4">Estado</th>
								<th class="h4">Vendedor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Digitalizacion</td>
								<td>$50.000.000</td>
								<td>28-10-2020</td>
								<td>Aprobada</td>
								<td>Katherine Vazquez</td>
							</tr>
							<tr>
								<td>Diagnostico de archivo</td>
								<td>$20.000.000</td>
								<td>15-10-2020</td>
								<td>Pte aprobacion</td>
								<td>Katherine Vazquez</td>
							</tr>
                            <tr>
								<td>Foliacion de documentos</td>
								<td>$1.065.000.000</td>
								<td>01-10-2020</td>
								<td>Rechazada</td>
								<td>Katherine Vazquez</td>
							</tr>
						</tbody>
					</table>
					<table class="display c" id="" width="100%">
						<thead>
							<tr>
								<th class="h4">Propuestas</th>
								<th class="h4">Valor</th>
								<th class="h4">Fecha</th>
								<th class="h4">Estado</th>
								<th class="h4">Vendedor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Propuesta</td>
								<td>$50.000.000</td>
								<td>28-10-2020</td>
								<td>Aprobada</td>
								<td>Katherine Vazquez</td>
							</tr>
						</tbody>
					</table>
					<table class="display c" id="" width="100%">
						<thead>
							<tr>
								<th class="h4">Ventas</th>
								<th class="h4">Valor</th>
								<th class="h4">Fecha</th>
								<th class="h4">Estado</th>
								<th class="h4">Vendedor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>TonerRicoh</td>
								<td>$150.000.00</td>
								<td>28-10-2020</td>
								<td>Aprobada</td>
								<td>Katherine Vazquez</td>
							</tr>
						</tbody>
					</table>
					<table class="display c" id="" width="100%">
						<thead>
							<tr>
								<th class="h4">Servicio Tecnico</th>
								<th class="h4">Valor</th>
								<th class="h4">Fecha</th>
								<th class="h4">Estado</th>
								<th class="h4">Vendedor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Mantenimiento</td>
								<td>$20.000.000</td>
								<td>28-10-2020</td>
								<td>Aprobada</td>
								<td>Katherine Vazquez</td>
							</tr>
						</tbody>
					</table>
				</div>

            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
    $(document).ready(function() {
        $('.select-multiple').select2();
        $('#tablaConsultas, .c').DataTable({
            info:false,searching:false, paging: false,ordering: false,
        });
    } );
</script>
@endsection