@extends('layouts.app')

@section('titulo')
CRM
@endsection

@section('styles')
	<style>
		table.dataTable.tabla thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
        border-bottom: 1px solid #808B96;
    }

    table.dataTable.tabla tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
    }
	</style>
@endsection

@section('contenido')


    @php
        $servicios = array(	
        
            array(
                'nombre' => "fergon outsourcing",
                'fecha'  => "28-01-2021",
                'hora'  => "8:30 am",
                'servicio' => 'Preventivo',
                'producto' => 'Rodillo',
                'tecnico' => 'Diego Vivas'
            ),
            array(
                'nombre' => "Fundapadua",
                'fecha'  => "25-01-2021",
                'hora'  => "11:00 am",
                'servicio' => 'Preventivo',
                'producto' => '-',
                'tecnico' => 'Leandro montero'
            ),
            array(
                'nombre' => "Liceo frances",
                'fecha'  => "30-12-2020",
                'hora'  => "5:00 pm",
                'servicio' => 'Correctivo',
                'producto' => '-',
                'tecnico' => 'Leandro montero'
            ),
            array(
                'nombre' => "fergon outsourcing",
                'fecha'  => "27-12-2020",
                'hora'  => "2:00 pm",
                'servicio' => 'Preventivo',
                'producto' => ' Cauchos Rodillo',
                'tecnico' => 'Dagoberto ramos'
            ),
            array(
                'nombre' => "Fundapadua",
                'fecha'  => "15-12-2021",
                'hora'  => "8:00 am",
                'servicio' => 'Correctivo',
                'producto' => 'Unas fusoras',
                'tecnico' => 'Diego vivas'
            ),
        );
    @endphp

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('crm.index')}}">CRM</a></li>
							<li class="breadcrumb-item"><a href="{{route('getServicioCliente')}}">Servicios</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Servicio tecnico</li>
                        </ol>
                    </nav>

                </div>
            </div>
        </div>
    </div>
	<div>
		@include('crm.components.tabs', ['select'=>'servicios'])
	</div>
	<div>
		@include('crm.components.tabs_servicio', ['select'=>'servicio'])
	</div>
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					<div>
						<p class="h5">
							Programacion servicio tecnico 
							<button class="btn-create-form btn-sm" 
									type="button" data-toggle="modal" 
									data-target="#modalServicio">
							</button>
						</p>
					</div>

					<p class="h5 my-3">Servicios Programados</p>

					<div class="table-responsive">
						<table class="tabla" id="tablaServicios" width="100%">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Fecha</th>
									<th>Hora</th>
									<th>Servicio</th>
									<th>Producto</th>
									<th>Tecnico de servicio</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($servicios as $servicio => $item)
									<tr>
										<td>{{ $item['nombre'] }}</td>
										<td>{{ $item['fecha'] }}</td>
										<td>{{ $item['hora'] }}</td>
										<td>{{ $item['servicio'] }}</td>
										<td>{{ $item['producto'] }}</td>
										<td>{{ $item['tecnico'] }}</td>
										<td><button class="btn-show-form"></button></td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modalServicio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header white">
					<h5 class="modal-title" id="my-modal-title">Programar Servicio</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="nombre" class="input-group-text col-sm-3 bg-light">Nombre de la empresa </label>
								<button class="btn btn-primary ml-1 p-2 mr-1" type="button"><i class="mdi mdi-magnify"></i></button>
								<input
									id="nombre"
									type="text" 
									name="nombre" 
									class="form-control col-sm-9"
									placeholder="Nombre de la empresa"
								>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-6">
								<label for="fecha" class="input-group-text bg-light bg-light col-sm-3">Fecha</label>
								<input type="date" name="fecha" id="fecha" class="form-control col-sm-9">
							</div>
							<div class="input-group col-sm-6">
								<label for="hora" class="input-group-text bg-light bg-light col-sm-3">Hora</label>
								<input type="time" name="fecha" id="hora" class="form-control col-sm-9">
							</div>
						</div>
                        <div class="row my-3">
                            <div class="input-group col-sm-6">
                                <label for="servicio" class="input-group-text bg-light col-sm-4">Servicio</label>
                                <select id="my-select" class="form-control col-sm-9"  name="">
                                    <option></option>
                                </select>
                            </div>
                            <div class="input-group col-sm-6">
                                <label for="producto" class="input-group-text bg-light col-sm-4">Producto</label>
                                <select id="my-select" class="form-control" name="">
                                    <option></option>
                                </select>
                            </div>
                        </div>
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="tecnico" class="input-group-text bg-light col-sm-3">Tecnico de servicio</label>
								<input type="text" name="" class="form-control col-sm-9" id="tecnico">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-sm-12">
								<label for="observaciones" class="input-group-text bg-light col-sm-3">Observaciones</label>
								<textarea name="" id="observaciones" cols="3" class="form-control col-sm-9" ></textarea>
							</div>
						</div>
						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Aceptar</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function() {
			$('#tablaServicios').DataTable({
				info:false,searching:false, paging: false,ordering: false,

			});
		} );
	</script>
@endsection