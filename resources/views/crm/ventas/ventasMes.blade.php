@extends('layouts.app')

@section('titulo')
    CRM
@endsection
@section('styles')
	<style>
		table.dataTable.tablaConsultas thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
        border-bottom: 1px solid #808B96;
    }

    table.dataTable.tablaConsultas tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
    }
	</style>
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">CRM</a></li>
                            <li class="breadcrumb-item"><a href="#">Ventas</a></li>
                            <li class="breadcrumb-item"><a href="#">Ventas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Resumen mes</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('crm.components.tabs',['select'=>'ventas'])
    </div>
    <div>
        @include('crm.components.tabs_ventas',['select' => 'ventas'])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
                   <h4 class="text-bold">Ventas mes (Facturado)</h4>
				   <div class="table-responsive">
					<table class="display" id="tablaConsultas" width="100%">
						<thead>
							<tr>
								<th>No. Factura</th>
								<th>Empresa</th>
								<th>Servicio</th>
								<th>Valor($)</th>
								<th>Estado</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>569856</td>
								<td>Fundapadua</td>
								<td>Alquiler impresora</td>
								<td>$300.000</td>
								<td>Pagada</td>
								<td><button class="btn-show-form" title="ver factura"></button></td>
							</tr>
							<tr>
								<td>874323</td>
								<td>Sukimoto</td>
								<td>Digitalizacion</td>
								<td>$1.500.000</td>
								<td>Pagada</td>
								<td><button class="btn-show-form" title="ver factura"></button></td>
							</tr>
							<tr>
								<td>522310</td>
								<td>Prosegur</td>
								<td>Alquiler impresora</td>
								<td>$480.000</td>
								<td>Pendiente</td>
								<td><button class="btn-show-form" title="ver factura"></button></td>
							</tr>
						</tbody>
					</table>
				</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			$('#tablaConsultas,table').DataTable({
				info:false,searching:false, paging: false,ordering: false,
			});
		});
		
	</script>
@stop