@extends('layouts.app')

@section('titulo')
    CRM
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">CRM</a></li>
                            <li class="breadcrumb-item"><a href="#">Ventas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ventas</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('crm.components.tabs',['select'=>'ventas'])
    </div>
    <div>
        @include('crm.components.tabs_ventas',['select' => 'ventas'])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
					<div class="table-responsive">
						<table class="table table-light">
							<thead>
								<tr>
									<th class="text-right">Item</th>
									<th >Valor</th>
									<th >Cantidad facturas</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-right"><a href="{{route('ventas.mes')}}">Ventas (mes facturado)</a></td>
									<td>$ 3.346.000</td>
									<td>6</td>
								</tr>
								<tr>
									<td class="text-right"><a href="{{route('ventas.ano')}}">Ventas acumulados año</a></td>
									<td>$ 15.000.000</td>
									<td>13</td>
								</tr>
								<tr>
									<td class="text-right"><a href="#">Facturas pagadas</a></td>
									<td>$5.000.000</td>
									<td>5</td>
								</tr>
								<tr>
									<td class="text-right"><a href="{{route('ventas.proyecto')}}">Ventas proyectos</a></td>
									<td>$150.000.000</td>
									<td>10</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="row mt-3">
						<p class="h4 ml-3"> Resumen </p>
						<canvas id="resumenChart" width="100" height="20"></canvas>
					</div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
	<script>
		$(document).ready(function(){
			
			var Canvas = document.getElementById("resumenChart");

			Chart.defaults.global.defaultFontFamily = "Roboto";
			Chart.defaults.global.defaultFontSize = 17;

			var Data = {
				labels: [
					"Facturas no pagadas %",
					"Facturas pagadas %"
				],
				datasets: [
					{
						data: [80, 30],
						backgroundColor: [
							"#7FB3D5",
							"#73C6B6"
						]
					}]
			};

			var pieChart = new Chart(Canvas, {
			type: 'pie',
			data: Data
			});
		});
		
	</script>
@stop