
@extends('layouts.app')

@section('titulo')
    CRM
@endsection

@section('styles')
	<style>
		table.dataTable.tablaConsultas thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
        border-bottom: 1px solid #808B96;
    }

    table.dataTable.tablaConsultas tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
    }
	</style>
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('crm.index')}}">CRM</a></li>
                            <li class="breadcrumb-item"><a href="{{route('ventas.index')}}">Ventas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Gestion</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('crm.components.tabs',['select'=>'ventas'])
    </div>
    <div>
        @include('crm.components.tabs_ventas',['select'=>'gestion'])
    </div>
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					<p class="h5">
						Clientes<button class="btn btn-primary btn-xs ml-1" type="button" data-toggle="modal" data-target="#modalClientes" ><i class="mdi mdi-library-plus"></i></button>
					</p>
					<p class="h5">
						Actividad comercial <button class="btn btn-primary btn-xs ml-1" type="button" data-toggle="modal" data-target="#modalActividad" ><i class="mdi mdi-library-plus"></i></button>
					</p>

					<div class="table-responsive">
						<table class="display" id="tablaConsultas" width="100%">
							<thead>
								<tr>
									<th class="h4">Tipo</th>
									<th class="h4">Fecha</th>
									<th class="h4">Hora</th>
									<th class="h4">Observaciones</th>
									<th class="h4">Vendedor</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Llamada</td>
									<td>15-09-2020</td>
									<td>08:30 am</td>
									<td>No fue posible comunicarse con la persona encargada</td>
									<td>Leandro Moreno</td>
								</tr>
								<tr>
									<td>Visita</td>
									<td>17-09-2020</td>
									<td>09:00 am</td>
									<td>Se realizo visita al archivo de la empresa</td>
									<td>Katherine Vazquez</td>
								</tr>
								<tr>
									<td>Correo Electronico</td>
									<td>18-09-2020</td>
									<td>08:00 am</td>
									<td>Se envio correo a la entidad con la cotizacion del servicio</td>
									<td>Diego Vivas</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		{{-- modal clientes --}}
	<div id="modalClientes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="clientes" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="clientes">Clientes</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group my-3">
									<label  class="input-group-text bg-light col-sm-4">Buscar por</label>
									<select class=" form-control" name="" >
										<option value="1">Nombre</option>
										<option value="2">cedula</option>
										<option value="3">Nit</option>  
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group my-3">
									<button class="btn btn-primary btn-sm mr-1" type="button"><i class="mdi mdi-magnify"></i></button>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
						</div>
					</form>
					<div class="row">
						<div class="col-6">
							<b>Nombre: </b><small> Fergon Outsourcing S.A.S</small>
						</div>   
						<div class="col-6">
							<b> Documento de identidad: </b> <small> 800.077.569</small>
						</div>                       
					</div>
					
					<div class="table-responsive mt-3">	
						<table class="display table" id="tablaConsultas" width="90%">
								<thead>
									<tr>
										<th>Cotizacion P/S</th>
										<th>Valor</th>
										<th>Fecha</th>
										<th>Estado</th>
										<th>Vendedor</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Digitalizacion</td>
										<td>$50.000.000</td>
										<td>28-10-2020</td>
										<td>Aprobada</td>
										<td>Katherine Vazquez</td>
									</tr>
									<tr>
										<td>Diagnostico de archivo</td>
										<td>$20.000.000</td>
										<td>15-10-2020</td>
										<td>Pte aprobacion</td>
										<td>Katherine Vazquez</td>
									</tr>
									<tr>
										<td>Foliacion de documentos</td>
										<td>$1.065.000.000</td>
										<td>01-10-2020</td>
										<td>Rechazada</td>
										<td>Katherine Vazquez</td>
									</tr>
								</tbody>
						</table>
					</div>
					<div class="row mt-3">
						<p class="h4 ml-3"> Resumen </p class="h4 ml-3">
						<canvas id="resumenChart" width="150" height="50"></canvas>
					</div>

					<div class="row mt-3 justify-content-md-center">
						<div class="form-group">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary">Crear</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	{{-- modal actividad comercial --}}
	<div id="modalActividad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Actividad comercial</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="col-md-4">
								<div class="input-group">
									<label for="my-select" class="input-group-text bg-light col-sm-3 pl-2">Tipo</label>
									<select id="my-select" class="form-control col-sm-12 " name="">
										<option value=""></option>
										<option value="">Llamada</option>
										<option value="">Visita</option>
										<option value="">Correo Electronico</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group ">
									<label for="" class="input-group-text bg-light col-sm-3 pl-2">Fecha</label>
									<input type="date" class="form-control col-sm-11" name="" id="">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<label for="" class="input-group-text bg-light col-sm-4 pl-2">Hora</label>
									<input type="time" class="form-control col-sm-12" name="" id="">
								</div>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12">
								<div class="input-group">
									<label for="" class="input-group-text bg-light col-sm-2">Observacion</label>
									<textarea name="" id="" rows="7" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12">
								<div class="input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Vendedor</label>
									<select id="my-select" class="form-control col-sm-9" name="">
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-7">
								<div class="input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Cliente</label>
									<select id="my-select" class="form-control col-sm-9" name="">
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="row mt-2 justify-content-md-center">
							<div class="form-group">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Crear</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			$('.select-multiple').select2();
			$('#tablaConsultas,table').DataTable({
				info:false,searching:false, paging: false,ordering: false,
			});

			var Canvas = document.getElementById("resumenChart");

			Chart.defaults.global.defaultFontFamily = "Roboto";
			Chart.defaults.global.defaultFontSize = 17;

			var Data = {
				labels: [
					"Aprobado %",
					"Pte. aprobacion %",
					"Rechazado %"
				],
				datasets: [
					{
						data: [60, 30, 10],
						backgroundColor: [
							"#7FB3D5",
							"#73C6B6",
							"#F0B27A"
						]
					}]
			};

			var pieChart = new Chart(Canvas, {
			type: 'pie',
			data: Data
			});
		});
		
	</script>
@stop