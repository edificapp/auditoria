@extends('layouts.app')

@section('titulo')
    CRM
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('crm.index')}}">CRM</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ventas</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
	<div>
		@include('crm.components.tabs',['select'=>'ventas'])
	</div>
	<div>
		@include('crm.components.tabs_ventas',['select' => ''])
	</div>
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					<table class="table table-light">
						<thead>
							<tr>
								<th></th>
								<th>Valor</th>
								<th>Cantidad</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-right">Ventas Mes (Facturado)</td>
								<td>$$$$$$$</td>
								<td>6</td>
							</tr>
							<tr>
								<td class="text-right">Ventas acumulados año</td>
								<td>$$$$$$$</td>
								<td>9</td>
							</tr>
							<tr>
								<td class="text-right">Cotizaciones enviadas</td>
								<td>$$$$$$$</td>
								<td>25</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection