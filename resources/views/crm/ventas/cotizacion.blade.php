@extends('layouts.app')
@section('titulo')
    CRM
@endsection
@section('styles')
	<style>
		table.dataTable.tablaConsultas thead th{
        font-size: 15px;
        width:12px;
        padding: 12px;
        padding-left:10px;
        padding-top: 10px;
        text-align: left;
        border-style: none;
        border-bottom: 1px solid #808B96;
    }

    table.dataTable.tablaConsultas tbody td {
        padding: 4px !important;
        padding-left:10px !important;
        font-family: 'Roboto', sans-serif !important;
        color: #030000 !important;
        font-size: 15px;
    }
	</style>
@endsection
@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-md-between flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('crm.index')}}">CRM</a></li>
                            <li class="breadcrumb-item"><a href="{{route('ventas.index')}}">Ventas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cotizaciones</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('crm.components.tabs',['select' => 'ventas'])
    </div>
    <div>
        @include('crm.components.tabs_ventas',['select' => 'cotizaciones'])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
					<div id="accordion">
						<div class="" id="headingOne">
							<h5 class="mb-0">
								<span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Propuestas<i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> 
								</span>
							</h5>
					   </div>
					   <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body">
								Crear Propuestas<button class="btn-add-form ml-1 mb-2" type="button" data-toggle="modal" data-target="#modalPropuesta" title="crear propuesta"></button>
								
								<div class="" id="heading">
									<span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse">
										<b>Propuestas creadas</b><i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>
									</span>
								</div>
								<div id="collapse" class="collapse" aria-labelledby="heading" data-parent="#collapseOne">
									
									<div class="dropdown mt-3">
										<button id="my-dropdown" class="btn button-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filtrar</button>
										<div class="dropdown-menu" aria-labelledby="my-dropdown">
											<a class="dropdown-item" href="#">Nombre</a>
											<a class="dropdown-item" href="#">Asunto</a>
											<a class="dropdown-item" href="#">Valor</a>
											<a class="dropdown-item" href="#">Proyecto</a> 
										</div>
									</div>

									<div class="table-responsive mt-3">
										<table class="display" id="tablaConsultas" width="100%">
											<thead>
												<tr>
													<th class="h4">Nombre entidad</th>
													<th class="h4">Asunto</th>
													<th class="h4">Valor propuesta</th>
													<th class="h4">Proyecto</th>
													<th class="h4"></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>ESAP</td>
													<td>Digitalizacion archivo inactivo</td>
													<td>60.000.000</td>
													<td>Katherine Vazquez</td>
													<td><button type="button" class="btn btn-inverse-secondary"><i class="mdi mdi-arrow-right"></i></button></td>
												</tr>
												<tr>
													<td>Visita</td>
													<td>17-09-2020</td>
													<td>09:00 am</td>
													<td>Katherine Vazquez</td>
													<td><button type="button" class="btn btn-inverse-secondary"><i class="mdi mdi-arrow-right"></i></button></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							
							</div>
					   </div>
					   
					   <div class=" mt-3" id="headingTwo">
							<h5 class="mb-0">
								<span class="ps-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									Cotizaciones<i class="mdi mdi-menu-right" style="color: black;font-weight:bold"></i>
								</span>
							</h5>
					   </div>
					   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							<div class="card-body">
								Crear Cotizaciones<button class="btn-add-form ml-1 mb-2" type="button" data-toggle="modal" data-target="#modalCotizacion" title="crear propuesta"></button>
								
								<div class="" id="headingT">
									<span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse">
										<b>Cotizaciones creadas</b><i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>
									</span>
								</div>
								<div id="collapse" class="collapse" aria-labelledby="headingT" data-parent="#collapseTwo">
									<div class="table-responsive mt-3">
										<table class="display" id="tablaConsultas" width="100%">
											<thead>
												<tr>
													<th class="h4">Nombre entidad</th>
													<th class="h4">Asunto</th>
													<th class="h4">Valor propuesta</th>
													<th class="h4">Proyecto</th>
													<th class="h4"></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>ESAP</td>
													<td>Digitalizacion archivo inactivo</td>
													<td>60.000.000</td>
													<td>Katherine Vazquez</td>
													<td><button type="button" class="btn btn-inverse-secondary"><i class="mdi mdi-arrow-right"></i></button></td>
												</tr>
												<tr>
													<td>Visita</td>
													<td>17-09-2020</td>
													<td>09:00 am</td>
													<td>Katherine Vazquez</td>
													<td><button type="button" class="btn btn-inverse-secondary"><i class="mdi mdi-arrow-right"></i></button></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

	{{-- MODAL PROPUESTA --}}

	<div id="modalPropuesta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Crear Propuesta</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h6 class="text-bold">Informacion a quien se dirige</h6>
					<form action="">
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Contacto</label>
								<select name="" id="" class="form-control col-sm-10">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="row my-2">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Entidad</label>
								<select name="" id="" class="form-control col-sm-10">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-3">Cargo</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<br>
						<h6 class="text-bold">Informacion de propuesta</h6>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-2">Asunto</label>
								<textarea name="" id="" rows="2" class="form-control col-sm-12"></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-4">Codigo de Producto o servicio</label>
								<select name="" id="" class="form-control col-sm-10">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Descripcion del servicio</label>
								<textarea name="" id="" rows="4" class="form-control col-sm-12"></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-2">Actividades</label>
								<textarea name="" id="" rows="6" class="form-control col-sm-12"></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-2">Entregable</label>
								<textarea name="" id="" rows="6" class="form-control col-sm-12"></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-2 mt-5 text-center"><h6>Valor de la Propuesta</h6></div>
							
							<div class="row my-3 col-md-10">
								<div class="input-group mt-2 col-md-7">
									<label for="" class="input-group-text bg-light col-sm-4  pl-2">Descripcion</label>
									<input class="form-control col-sm-12" type="text" name="">
								</div>
								<div class="input-group mt-2 col-md-5">
									<label for="" class="input-group-text bg-light col-sm-4 pl-2">Cantidad</label>
									<input class="form-control col-sm-12" type="text" name="">
								</div>
								<div class="input-group mt-2 col-md-5">
									<label for="" class="input-group-text bg-light col-sm-5  pl-2">Valor Unitario</label>
									<input class="form-control col-sm-12" type="text" name="">
								</div>
								<div class="input-group mt-2 col-md-7">
									<label for="" class="input-group-text bg-light col-sm-5 pl-2">Valor antes e IVA</label>
									<input class="form-control col-sm-12" type="text" name="">
								</div>
								<div class="input-group mt-2 col-md-5">
									<label for="" class="input-group-text bg-light col-sm-5  pl-2">IVA 19%</label>
									<input class="form-control col-sm-12" type="text" name="">
								</div>
								<div class="input-group mt-2 col-md-7">
									<label for="" class="input-group-text bg-light col-sm-5 pl-2">Valor total propuesta</label>
									<input class="form-control col-sm-12" type="text" name="">
								</div>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-7">
								<label for="" class="input-group-text bg-light col-sm-5">Tiempo de ejecucion</label>
								<input type="text" class="form-control col-sm-9">
								<span class="input-group-text">Meses</span>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-7">
								<label for="" class="input-group-text bg-light col-sm-3 p-2">Firma </label>

								<div class="input-group-prepend">
									<button class="btn btn-primary p-2 ml-2" type="button"><i class="mdi mdi-paperclip"></i></button>
								</div>
								{{-- <div class="custom-file">
									<input type="file" class="custom-file-input" id="inputGroupFile03">
									<label class="custom-file-label" for="inputGroupFile03">Seleccionar archivo</label>
								</div> --}}
							</div>
						</div>
						<div class="row justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Crear</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	{{-- MODAL COTIZACION --}}
	<div id="modalCotizacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cotizaciones" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cotizaciones">Crear Cotizacion</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h6 class="text-bold">Informacion empresa</h6>
					<form action="">
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="Contacto" class="input-group-text bg-light col-sm-3">Contacto</label>
								<select name="" id="" class="form-control col-sm-9">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4" >Direccion</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Telefono</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-3">Correo electronico</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<h6 class="text-bold mt-3">Items cotizados</h6>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-3">Codigo</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-3">Cantidad</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Descripcion</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-5">Valor unitario</label>
								<input type="text" class="form-control col-sm-9">
							</div>
							<div class="col-md-6 input-group">
								<label for="" class="input-group-text bg-light col-sm-5">Valor total</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3 justify-content-md-center">
							<p class="h5">
								<button class="btn btn-primary btn-sm mr-2" type="button"><i class="mdi mdi-library-plus"></i></button>Agregar producto
							</p>
						</div>
						<h6 class="text-bold mt-3">Valor cotizacion</h6>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Total bruto</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Descuento</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Subtotal</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">IVA 19%</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Neto a pagar</label>
								<input type="text" class="form-control col-sm-9" disabled>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-2">Valor en letras</label>
								<textarea name="" id="" rows="1" class="form-control col-sm-9" disabled></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<h6 class="col-sm-12">Terminos y condiciones</h6>
								<textarea name="" id="" rows="4" class="form-control col-sm-12"></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<h6 class="col-sm-12">Observaciones</h6>
								<textarea name="" id="" rows="4" class="form-control col-sm-12"></textarea>
							</div>
						</div>
						<div class="row justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Crear</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$('#tablaConsultas,table').DataTable({
				info:false,searching:false, paging: false,ordering: false,
			});
	</script>
@endsection