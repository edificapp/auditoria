<html lang="en"><head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Majestic Admin Pro</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('admin/plugins/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin/plugins/base/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
  <link rel="shortcut icon" href="{{ asset('img/corporativo/G.png') }}" />
<link rel="stylesheet" type="text/css" href="chrome-extension://bbpokcagpggnekcmamgdieebhpkjmljm/css/fonts.css">
<style>
  .card{
    background: rgba(255,255,255,.5);
    -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
  }
</style>
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth lock-full-bg">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
           <div class="card">
                <div class="card-body text-center">
                  <div>
                    <img src="{{$evento->owner->curriculum ? $evento->owner->curriculum->avatar : asset('admin/images/usuario/usuario.jpg')}}" class="img-lg rounded-circle mb-2" alt="profile image">
                    <h4>{{$evento->owner->curriculum->nombres != NULL ? $evento->owner->curriculum->full_name : $evento->owner->nombre}}</h4>
                    {{--<p class="text-muted mb-0">Developer</p>--}}
                  </div>
                  <p class="mt-2 card-text">
                      Te han invitado a la reunión {{$evento->title}}, inicia a las  {{$evento->hora.':00'}} el dia {{$evento->fecha}}.
                  </p>
                  {{--
                  <button class="btn btn-info btn-sm mt-3 mb-4">Follow</button>
                    --}}
                  <div class="border-top pt-3">
                    <div class="row">
                      <div class="col-4">
                        <h6>Fecha</h6>
                        <p>{{$evento->fecha}}</p>
                      </div>
                      <div class="col-4">
                        <h6>Hora y Duración</h6>
                        <p>{{$evento->hora}} ({{$evento->duracion_real}})</p>
                      </div>
                      <div class="col-4">
                        <h6>Lugar</h6>
                        <p>{{$evento->lugar}}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{asset('admin/plugins/base/vendor.bundle.base.j')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{asset('admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/js/template.js')}}"></script>
  <!-- endinject -->



<iframe style="display: none;"></iframe><hv-copy-modal></hv-copy-modal></body></html>