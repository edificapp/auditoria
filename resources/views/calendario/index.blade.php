@extends('layouts.app')

@section('titulo')
Agenda
@endsection

@section('styles')
    <link href='{{asset('admin/plugins/fullcalendar-5.3.2/lib/main.min.css')}}' rel='stylesheet' />
    <style type="text/css">
        #calendar {
            margin: 40px auto;
        }
        .fc-toolbar-title, .fc-event-title{
          text-transform:lowercase !important;
        }

        .fc-toolbar-title:first-letter, .fc-event-title:first-letter{
          text-transform:uppercase !important;
        }

        .fc-view-container {
          width: auto;
        }
        .fc-view-container .fc-view {
          overflow-x: scroll;
        }
    </style>
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-2">
                      <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                      <li class="breadcrumb-item active" aria-current="page">Agenda</li>
                    </ol>
                  </nav>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" class="btn btn-primary mt-2 mt-xl-0" id="openModalEvento"><span class="mdi mdi-calendar-plus"></span> Nueva Reunión</button>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-md-center">
    <div class="col-md-10 grid-margin stretch-card">
        <div class="card">
            <div class="card-body dashboard-tabs p-4 text-capitalize" id="div_calendario">
               <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEvento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center">Nuevo Evento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal"  action="{{route('eventos.store')}}" method="post">
      <div class="modal-body">
        @csrf
        <input type="hidden" name="_modal" value="#modalEvento">
          <div class="row my-3">
              <div class="input-group col-6">
                  <label class="input-group-text col-sm-5 bg-light">Nombre del Evento:</label>
                  <input 
                      type="text" 
                      name="title" 
                      id="title" 
                      class="form-control col-sm-7 @error('title') is-invalid @enderror"  
                      placeholder="Nombre del Evento" 
                      value="{{old('title')}}"
                      required
                  >
                  <br>
                  @error('title')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
              <div class="input-group col-6">
                  <label class="input-group-text col-sm-5 bg-light">Lugar del Evento: </label>
                  <input 
                      type="text" 
                      name="lugar" 
                      id="lugar" 
                      class="form-control col-sm-7 @error('lugar') is-invalid @enderror"  
                      placeholder="Lugar del Evento" 
                      value="{{old('lugar')}}"
                      required
                  >
                  <br>
                  @error('lugar')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
          </div>
          <div class="row my-3">
              <div class="input-group col-3">
                  <label class="input-group-text col-sm-4 bg-light">Fecha: </label>
                  <input 
                      type="date"
                      id="fecha" 
                      value="{{date("Y-m-d")}}"
                      name="fecha" 
                      class="form-control col-sm-9 @error('fecha') is-invalid @enderror"  
                      placeholder="Fecha" 
                      value="{{old('fecha')}}"
                  >
                  <br>
                  @error('fecha')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
              <div class="input-group col-3">
                  <label class="col-sm-6 bg-light input-group-text">Hora</label>
                  <select name="hora" class="form-control col-sm-6" id="hora">
                    @foreach($horas as $hora)
                      <option value="{{$hora}}" @if($hora == '08:00') selected="selected" @endif>{{$hora}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="input-group col-3">
                  <label class="col-sm-6 bg-light input-group-text">Duración</label>
                  <select name="duracion" class="form-control col-sm-6" id="duracion">
                    @foreach($duracion as $item)
                      <option value="{{$item['value']}}" @if($item['value']== '60') selected="selected" @endif >{{$item['duracion']}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="input-group col-3">
                <label class="col-sm-4 bg-light input-group-text">Color</label>
                <select name="color" class="form-control col-8" id="color">
                    <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
                    <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
                    <option style="color:#008000;" value="#008000">&#9724; Verde</option>                       
                    <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
                    <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
                    <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
                    <option style="color:#000;" value="#000">&#9724; Negro</option>
                </select>
              </div>
          </div>
          <div class="row my-3">
              <div class="input-group col-12">
                  <label class="input-group-text col-sm-3 bg-light">Descripción: </label>
                  <textarea
                      id="descripcion" 
                      name="description" 
                      class="form-control col-sm-9 @error('description') is-invalid @enderror"  
                      placeholder="Descripción" 
                      rows="7"
                      required
                  >{{old('description')}}</textarea>
                  <br>
                  @error('description')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
          </div>
          <div class="row my-3 justify-content-md-center">
              <div class="input-group col-12">
                  <label class="col-sm-11 bg-light text-center">Invitados Usuarios</label>
                  <select multiple name="invitados_usuarios[]"   style="width:100%;" id="invitados_usuarios">
                      @foreach(Auth::user()->empresa->usuarios->filter(function($item){ return $item->id != Auth::user()->id;  }) as $user)
                          <option value="{{$user->id}}">{{$user->nombre}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div class="row my-3 justify-content-md-center">
              <div class="input-group col-12">
                  <label class="col-sm-11 bg-light text-center">Invitados Contactos</label>
                  <select multiple name="contactos[]"   style="width:100%;" id="invitados_contactos">
                      @foreach(Auth::user()->empresa->contactos as $item)
                          <option value="{{$item->id}}">{{$item->full_name}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
      </div>
      <div class="row my-3 justify-content-md-center modal-footer">
        <div class="form-group">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary btn_submit">Guardar</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEventoUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center" id="title_form_evento"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal"  id="form_actualizar_evento" method="post">
      <div class="modal-body">
        @csrf
        <input type="hidden" name="_modal" value="#modalEventoUpdate">
        <input type="hidden" name="_method" id="_method">
          <div class="row my-3">
              <div class="input-group col-6">
                  <label class="input-group-text col-5 bg-light">Nombre del Evento:</label>
                  <input 
                      type="text" 
                      name="title" 
                      id="u_title" 
                      class="form-control col-7 @error('title') is-invalid @enderror"  
                      placeholder="Nombre del Evento" 
                      value="{{old('title')}}"
                      required
                  >
                  <br>
                  @error('title')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
              <div class="input-group col-6">
                  <label class="input-group-text col-5 bg-light">Lugar del Evento: </label>
                  <input 
                      type="text" 
                      name="lugar" 
                      id="u_lugar" 
                      class="form-control col-7 @error('lugar') is-invalid @enderror"  
                      placeholder="Lugar del Evento" 
                      value="{{old('lugar')}}"
                      required
                  >
                  <br>
                  @error('lugar')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
          </div>
          <div class="row my-3">
              <div class="input-group col-3">
                  <label class="input-group-text col-3 bg-light">Fecha: </label>
                  <input 
                      type="date"
                      id="u_fecha" 
                      value="{{date("Y-m-d")}}"
                      name="fecha" 
                      class="form-control col-9 @error('fecha') is-invalid @enderror"  
                      placeholder="Fecha" 
                      value="{{old('fecha')}}"
                  >
                  <br>
                  @error('fecha')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
              <div class="input-group col-3">
                  <label class="col-6 bg-light input-group-text">Hora</label>
                  <select name="hora" class="form-control col-6" id="u_hora">
                    @foreach($horas as $hora)
                      <option value="{{$hora}}">{{$hora}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="input-group col-3">
                  <label class="col-6 bg-light input-group-text">Duración</label>
                  <select name="duracion" class="form-control col-6" id="u_duracion">
                    @foreach($duracion as $item)
                      <option value="{{$item['value']}}">{{$item['duracion']}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="input-group col-3">
                <label class="col-4 bg-light input-group-text">Color</label>
                <select name="color" class="form-control col-8" id="u_color">
                    <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
                    <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
                    <option style="color:#008000;" value="#008000">&#9724; Verde</option>                       
                    <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
                    <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
                    <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
                    <option style="color:#000;" value="#000">&#9724; Negro</option>
                </select>
              </div>
          </div>
          <div class="row my-3">
              <div class="input-group col-12">
                  <label class="input-group-text col-3 bg-light">Descripción: </label>
                  <textarea
                      id="u_descripcion" 
                      name="description" 
                      class="form-control col-9 @error('description') is-invalid @enderror"  
                      placeholder="Descripción" 
                      rows="7"
                      value="{{old('description')}}"
                      required
                  ></textarea>
                  <br>
                  @error('descripcion')
                      <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                  @enderror
              </div>
          </div>
          <div class="row my-3 justify-content-md-center">
              <div class="input-group col-12">
                  <label class="col-11 bg-light text-center">Invitados Usuarios</label>
                  <select multiple name="invitados_usuarios[]"   style="width:100%;" id="u_invitados_usuarios">
                      @foreach(Auth::user()->empresa->usuarios->filter(function($item){ return $item->id != Auth::user()->id;  }) as $user)
                          <option value="{{$user->id}}">{{$user->nombre}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div class="row my-3 justify-content-md-center">
              <div class="input-group col-12">
                  <label class="col-11 bg-light text-center">Invitados Contactos</label>
                  <select multiple name="contactos[]"   style="width:100%;" id="u_invitados_contactos">
                      @foreach(Auth::user()->empresa->contactos as $item)
                          <option value="{{$item->id}}">{{$item->full_name}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn_submit">Guardar</button>
      </div>
    </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEventoShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center">Mostrar Evento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row my-3">
              <div class="input-group col-6">
                  <label class="input-group-text col-5 bg-light">Nombre del Evento: </label>
                  <input 
                      type="text" 
                      id="show_title" 
                      class="form-control col-7"  
                      readonly
                  >
          
              </div>
              <div class="input-group col-6">
                  <label class="input-group-text col-5 bg-light">Lugar del Evento: </label>
                  <input 
                      type="text" 
                      id="show_lugar" 
                      class="form-control col-7"  
                      readonly="" 
                  >
              </div>
              
          </div>
          <div class="row my-3">
              <div class="input-group col-3">
                  <label class="input-group-text col-3 bg-light">Fecha: </label>
                  <input 
                      type="date"
                      id="show_fecha"
                      class="form-control col-9" 
                      readonly
                  >
              </div>
              <div class="input-group col-3">
                  <label class="col-6 bg-light input-group-text">Hora</label>
                  <select class="form-control col-6" id="show_hora">
                    @foreach($horas as $hora)
                      <option>{{$hora}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="input-group col-3">
                  <label class="col-6 bg-light input-group-text">Duración</label>
                  <select class="form-control col-6" id="show_duracion">
                    @foreach($duracion as $item)
                      <option value="{{$item['value']}}">{{$item['duracion']}}</option>
                    @endforeach
                  </select>
              </div>
              <div class="input-group col-3">
                <label class="col-4 bg-light input-group-text">Color</label>
                <select class="form-control col-8" id="show_color">
                    <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
                    <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
                    <option style="color:#008000;" value="#008000">&#9724; Verde</option>                       
                    <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
                    <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
                    <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
                    <option style="color:#000;" value="#000">&#9724; Negro</option>
                </select>
              </div>
          </div>
          <div class="row my-3">
              <div class="input-group col-12">
                  <label class="input-group-text col-3 bg-light">Descripción: </label>
                  <textarea
                      id="show_descripcion" 
                      class="form-control col-9"  
                      rows="7"
                      readonly
                  ></textarea>
              </div>
          </div>
          <div class="row my-3 justify-content-md-center">
              <h4 class="text-center">Usuarios Invitados</h4>
              <table  class="table">
                <tbody id="tblody_invitados_user">
                  
                </tbody>
              </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalOption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center">Opciones del Evento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="option_value">
        <div class="row justify-content-md-center">
          <div class="btn-group-vertical">
            <button type="button" class="btn btn-inverse-secondary text-primary" id="copiar_evento">Copiar Evento</button>
            <button type="button" class="btn btn-inverse-secondary text-success" id="actualizar_evento">Actualizar Evento</button>
            <button type="button" class="btn btn-inverse-secondary text-danger" id="eliminar_evento">Eliminar Evento</button>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('admin/plugins/fullcalendar-5.3.2/lib/main.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/plugins/fullcalendar-5.3.2/lib/locales-all.min.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
    @if($errors->any())
      $('{{old('_modal')}}').modal();
    @endif
  });
  $(document).bind("contextmenu",function(e){
        return true;
       //return false;
    });
  document.addEventListener('DOMContentLoaded', function() {
        $('#invitados_usuarios, #invitados_contactos, #u_invitados_usuarios, #u_invitados_contactos').select2({allowClear: true});
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {

          initialView: 'dayGridMonth',
          height: 700,
          editable: true,
          selectable: true,
          headerToolbar: {
            left:'prev,next today',
            center:'title',
            right:'dayGridMonth,timeGridWeek,timeGridDay'
          },
          
          select: function(start, end) {
               change_view(start.startStr, start.endStr);
          },
          eventDidMount: function(arg) {
            arg.el.oncontextmenu = function() {
              $('#modalOption').modal();
              $('#option_value').val(arg.event.id);
            };
            arg.el.onclick = function() {
               let id = $('#option_value').val();
               axios.get('/eventos/'+arg.event.id)
                  .then(function (resp) {
                    console.log('resp', resp);
                     $('#modalEventoShow').modal('show');
                     $('#show_title').val(resp.data.title);
                    $('#show_lugar').val(resp.data.lugar);
                    $('#show_descripcion').val(resp.data.description);
                    $('#show_fecha').val(resp.data.fecha);
                    $('#show_hora option').each(function(i, e){ e.selected = false });
                    $('#show_hora option[value="'+resp.data.hora+'"]').attr("selected", true);
                    $('#show_duracion option').each(function(i, e){ e.selected = false });
                    $('#show_duracion option[value="'+resp.data.duracion+'"]').attr("selected", true);
                    $('#show_color option').each(function(i, e){ e.selected = false });
                    $('#show_color option[value="'+resp.data.color+'"]').attr("selected", true);
                    $('#tblody_invitados_user').empty();
                    $.each(resp.data.invitados_user, function(index, value){
                      estado = value.pivot.acepto == 1 ? 'bg-danger': 'bg-success';
                      $('#tblody_invitados_user').append('<tr><td class="'+estado+'">'+value.nombre+'</td></tr>');
                    });
                })
            }
          },
          eventDrop: function(event, delta, revertFunc) { // si changement de position
              edit(event);
          },
          eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
              edit(event);
          },
          events: '{{route('event.resource')}}'
        });
        calendar.setOption('locale', 'Es');
        calendar.render();

        function change_view(start, end){
          calendar.changeView('timeGrid', {
            start: start,
            end: end
          });
        }

        //edit de cambiar con mover
        function edit(data){
            $.post( "{{route('evento.update_ajax')}}", 
              {
                id:data.event.id, 
                start:data.event.startStr, 
                end:data.event.endStr,
                _token: "{{ csrf_token() }}"
              },
              function(rep) {
                if(rep == 'OK'){
                    alert('Evento se ha guardado correctamente');
                    emptyFields();
                }else{
                    alert('No se pudo guardar. Inténtalo de nuevo.'); 
                }
              });
        }

        //abrir modal de store
        $('#openModalEvento').on('click', function(){
          $('#modalEvento').modal();
        });

        //funciones modal option
        $('#copiar_evento').on('click', function (){
          let id = $('#option_value').val();
          $('#_method').val('post');
          $('#title_form_evento').text('Nuevo Evento');
          $('#modalEventoUpdate').modal();
          $('#form_actualizar_evento').attr('action', '{{route('eventos.store')}}')
          axios.get('/eventos/'+id)
              .then(function (response) {
                  actualizar_data_form(response, 'u_');
                  $('#modalOption').modal('hide');
              })
        });

        $('#actualizar_evento').on('click', function(){
           $('#modalEventoUpdate').modal();
           $('#_method').val('put');
           $('#title_form_evento').text('Actualizar Evento');
           let id = $('#option_value').val();
           axios.get('/eventos/'+id)
              .then(function (response) {
                  $('#form_actualizar_evento').attr('action', response.data.url_actualizar)
                  actualizar_data_form(response, 'u_');
                  $('#modalEventoUpdate').modal();
                  $('#modalOption').modal('hide');
            })
        });

        $('#eliminar_evento').on('click', function(){
          let id = $('#option_value').val();
          axios.delete('/eventos/'+id)
            .then(function (resp) {
              $('#modalOption').modal('hide');
              location.reload();
              //calendar.fullCalendar('removeEvents',id);
            })
        });

        //actualizar la data de el form por copia en store o  update 
        function actualizar_data_form(resp, i=''){
           let array_ids = [];
           $('#'+i+'title').val(resp.data.title);
           $('#'+i+'lugar').val(resp.data.lugar);
           $('#'+i+'descripcion').val(resp.data.description);
           $('#'+i+'fecha').val(resp.data.fecha);

           $('#'+i+'hora option').each(function(i, e){ e.selected = false });
           $('#'+i+'hora option[value="'+resp.data.hora+'"]').attr("selected", true);
           $('#'+i+'hora').trigger('change');

           $('#'+i+'duracion option').each(function(i, e){ e.selected = false });
           $('#'+i+'duracion option[value="'+resp.data.duracion+'"]').attr("selected", true);
           $('#'+i+'duracion').trigger('change');

           $('#'+i+'color option').each(function(i, e){ e.selected = false });
           $('#'+i+'color option[value="'+resp.data.color+'"]').attr("selected", true);
           $('#'+i+'color').trigger('change');

           $('#'+i+'invitados_usuarios option').each(function(i, e){ e.selected = false });
           $.each(resp.data.invitados_user, function(index, value){
                array_ids.push(value.id);
           });
           $('#'+i+'invitados_usuarios').val(array_ids);
           $('#'+i+'invitados_usuarios').trigger('change');
           $('#'+i+'invitados_contactos').val(resp.data.contactos);
           $('#'+i+'invitados_contactos').trigger('change');
        }
    });

      $( '#modalOption' ).on( 'hidden.bs.modal' , function() {
            $( 'body' ).addClass( 'modal-open' );
        } );

      
///////////*******************************************/////////////////////

    
</script>

@endsection

    
