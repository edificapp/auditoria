@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('styles')
    <style>
         .card{
            background: rgba(255,255,255,.5);
            -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
          }
    </style>    
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Evento {{$evento->title}}</h2>

                    <div class="d-flex">
                        <a href="{{ route('home') }}"><i class="mdi mdi-home breadcrumbs hover-cursor"></i>&nbsp;/&nbsp;</a>
                        <a href="{{ route('notificaciones') }}">Notificaciones</a>
                        <p class="breadcrumbs mb-0 hover-cursor">&nbsp;/&nbsp;Evento {{$evento->title}}&nbsp;/&nbsp;</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row w-100">
          <div class="col-lg-6 mx-auto">
           <div class="card">
                <div class="card-body text-center">
                  <div>
                    <img src="{{$evento->owner->curriculum ? $evento->owner->curriculum->avatar : asset('admin/images/usuario/usuario.jpg')}}" class="img-lg rounded-circle mb-2" alt="profile image">
                    <h4>{{$evento->owner->curriculum && $evento->owner->curriculum->nombres != NULL ? $evento->owner->curriculum->full_name : $evento->owner->nombre}}</h4>
                    {{--<p class="text-muted mb-0">Developer</p>--}}
                  </div>
                  <p class="mt-2 card-text">
                        @if($evento->owner_id == Auth::user()->id)
                            Creaste la reunión {{$evento->title}}
                        @elseif($invitacion->acepto != 1)
                            Has {!!$invitacion->acepto == 2 ? '<span class="text-success">Aceptado</span>' : '<span class="text-danger">Rechazado</span>' !!} ir a la reunión {{$evento->title}}
                        @else
                            Te han invitado a la reunión {{$evento->title}}. por favor confirma su asistencia.
                           <br>
                           <a href="{{route('evento.confirmacion', [$evento->id, 0])}}" class="btn btn-inverse-danger">No asistire</a>
                           <a href="{{route('evento.confirmacion', [$evento->id, 2])}}" class="btn btn-inverse-success">Si asistire</a>
                        @endif
                  </p>
                  {{--
                  <button class="btn btn-info btn-sm mt-3 mb-4">Follow</button>
                    --}}
                  <div class="border-top pt-3">
                    <div class="row">
                      <div class="col-4">
                        <h6>Fecha</h6>
                        <p><small>{{$evento->fecha}}</small></p>
                      </div>
                      <div class="col-4">
                        <h6>Hora y Duración</h6>
                        <p><small>{{$evento->hora}} ({{$evento->duracion_real}})</small></p>
                      </div>
                      <div class="col-4">
                        <h6>Lugar</h6>
                        <p><small>{{$evento->lugar}}</small></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>


@endsection

@section('scripts')


@endsection

    
