@extends('layouts.app')

@section('titulo')
informacion de la empresa
@endsection


@section('contenido')
<div class="row">
    <div class="d-flex ml-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb p-2">
                  <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">Entidades</li>
            </ol>
          </nav>
    </div>
    
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div>
            <ul class="nav nav-pills">
                @can('listar-proveedores')
                <li class="nav-item">
                    <a href="#proveedores" class="nav-link {{$tab_active == 'proveedor' ? 'active' : ''}} " data-toggle="tab" >
                        Proveedores 
                    </a>
                </li>
                @endcan
                @can('listar-clientes')
                <li class="nav-item">
                    <a href="#clientes" class="nav-link {{$tab_active == 'cliente' ? 'active' : ''}} " data-toggle="tab" >
                        Clientes
                    </a>
                </li>
                @endcan
                @can('listar-terceros')
                <li class="nav-item">
                    <a href="#terceros" class="nav-link {{$tab_active == 'tercero' ? 'active' : ''}} " data-toggle="tab" >
                        Terceros
                    </a>
                </li>
                @endcan
            </ul>
            <hr style="margin-top:0; margin-bottom: 1px; border:none; ">
        </div>

        {{--  --}}
        <div class="card" >
            <div class="card-body borde">
                <div class="my-3">
                    <div class="mx-auto ">
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                      @can('listar-proveedores')
                      <div class="tab-pane {{$tab_active == 'proveedor' ? 'active' : 'fade'}}" id="proveedores">
                          @include('entidades.components.proveedores')
                      </div>
                      @endcan
                      @can('listar-clientes')
                      <div class="tab-pane {{$tab_active == 'cliente' ? 'active' : 'fade'}}" id="clientes">
                          @include('entidades.components.clientes')
                      </div>
                      @endcan
                      @can('listar-terceros')
                      <div class="tab-pane {{$tab_active == 'tercero' ? 'active' : 'fade'}}" id="terceros">
                          @include('entidades.components.terceros')
                      </div>
                      @endcan
                    </div>  
                </div>  
            </div>
        </div>
    </div>
</div>

<div id="modalCreateContacto" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="tituloFormContacto"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('cliente-tercero.store')}}" method="post" enctype="multipart/form-data" id="formContactos">
                    @csrf
                    <input type="hidden" name="_method" id="_method" value="post">
                    <input type="hidden" name="tipo_contacto" id="tipo_contacto" value="{{old('tipo_contacto') ? old('tipo_contacto') : ''}}">
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-4 bg-light">Nombre: </label>
                            <input 
                                type="text" 
                                name="nombre"
                                id="nombre" 
                                class="form-control col-sm-7 @error('nombre') is-invalid @enderror"  
                                placeholder="Nombre" 
                                value="{{old('nombre')}}"
                            >
                            <br>
                            @error('nombre')
                                <span id="errorNombre" class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12" >
                            <label class="input-group-text col-sm-4 bg-light">Contacto: </label>
                            <select  id="contacto_id" class="form-control col-sm-7" name="contacto_id">
                                <option value="null" {{old('contacto_id') == null ? 'selected' : ''}}>sin contacto</option>
                                @foreach($contactos  as $contacto)
                                    <option value="{{$contacto->id}}" {{old('contacto_id') == $contacto->id ? 'selected' : ''}}>{{$contacto->full_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Tipo documento:</label>
                            <select  id="tipo_documento_contacto" class="form-control col-sm-7" name="tipo_documento">
                              <option value="Cedula de Ciudadania" {{old('tipo_documento') == 'Cedula de Ciudadania' ? 'selected' :''}}>Cedula de Ciudadania</option>
                              <option value="Cedula de Extranjeria" {{old('tipo_documento') == 'Cedula de Extranjeria' ? 'selected' :''}}>Cedula de Extranjeria</option>
                              <option value="Nit" {{old('tipo_documento') == 'Nit' ? 'selected' :''}}>Nit</option>
                            </select>
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">No. de documento: </label>
                            <input 
                                type="text" 
                                name="numero_documento"
                                id="numero_documento" 
                                class="form-control col-sm-7 @error('numero_documento') is-invalid @enderror"  
                                placeholder="Número de documento" 
                                value="{{old('numero_documento')}}"
                            >
                            <br>
                            @error('numero_documento')
                                <span id="errorNumeroDocumento" class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Dirección: </label>
                            <input 
                                type="text" 
                                name="direccion"
                                id="direccion" 
                                class="form-control col-sm-7 @error('direccion') is-invalid @enderror"  
                                placeholder="Dirección" 
                                value="{{old('direccion')}}"
                            >
                            <br>
                            @error('direccion')
                                <span id="errorDireccion" class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">No. de contacto: </label>
                            <input 
                                type="text" 
                                name="numero_contacto"
                                id="numero_contacto" 
                                class="form-control col-sm-7 @error('numero_contacto') is-invalid @enderror"  
                                placeholder="Número de contacto" 
                                value="{{old('numero_contacto')}}"
                            >
                            <br>
                            @error('numero_contacto')
                                <span id="errorNumeroContacto" class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-4 bg-light">Email: </label>
                            <input 
                                type="email" 
                                name="email"
                                id="email"
                                class="form-control col-sm-10 @error('email') is-invalid @enderror"  
                                placeholder="Email" 
                                value="{{old('email')}}"
                            >
                            <br>
                            @error('email')
                                <span id="errorEmail" class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>

                    <div class="row my-3 justify-content-md-center div_correo">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-12 bg-light">Proyectos: </label>
                            <select name="proyectos[]" multiple id="select_proyectos" class="col-sm-7" style="width: 100%">
                                @foreach($proyectos as $item)
                                    <option value="{{$item->id}}" {{(collect(old('proyectos'))->contains($item->id)) ? 'selected':''}}>{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-md-center">
                        <div class="col-md-12 text-center"><h3><b>Registro Unico Tributario</b></h3></div>
                        <br>
                        <div class="col-md-6">
                           <input class="file_certificado" name="certificado_bancario" type="file">
                        </div>
                    </div>
                    <div class="row my-5 justify-content-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="btnFormContactos">Guardar Cambios</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>


@endsection

@section('scripts')
    <script type="text/javascript">
         $(document).ready(function(){
            
            @if($errors->any())
                $('{{$tab_active == 'proveedor' ? '#modalCreateProveedor' : '#modalCreateContacto'}}').modal('show');
                $('#tituloFormContacto').text("{{old('_method') == 'put' ? 'Actualizar' : 'Crear'}} {{$tab_active}}");
            @endif 

            tipo_persona($('#tipo').val());

            $('#select_proyectos').select2({
                placeholder: "Selecciona los proyectos",
                width: 'resolve' // need to override the changed default
            });
        });


         $('#tipo').on('change', function(){
            tipo_persona($('#tipo').val());
         })

         function tipo_persona(tipo){
            if(tipo == 'Natural')
            {
                $('#fields-juridicos').hide();
                $('#rut').hide();
                $('#fields-naturales').show();
            }else{
                $('#fields-juridicos').show();
                $('#rut').show();
                $('#fields-naturales').hide();
            }
         }

         function modalCreateContacto(tipo){
             clearErrors();
            $('#modalCreateContacto').modal();
            $('#btnFormContactos').text('Guardar');
            $('#tituloFormContacto').text('Agregar '+tipo);
            $('#tipo_contacto').val(tipo);
            $('#_method').val('post');
            if(tipo == 'tercero'){
                $('.div_correo').show();
            }else{
                $('.div_correo').hide();
            }
            $('#nombre').val('');
            $("#tipo_documento_contacto option").each(function(){
                $(this).attr("selected", false);
             });
            $('#numero_documento').val('');
            $('#direccion').val('');
            $('#numero_contacto').val('');
            $('#email').val('');

         }
        

        function editContacto(url_ajax, tipo, url_form){
            clearErrors();
            $.get( url_ajax, function( data ) {
                console.log(data);
                $('#modalCreateContacto').modal();
                $('#formContactos').attr("action", url_form);;
                $('#_method').val('put');
                $('#btnFormContactos').text('Actualizar');
                $('#tituloFormContacto').text('Actualizar informacion '+tipo);
                $('#tipo_contacto').val(tipo);
                $('#nombre').val(data.nombre);
                $("#tipo_documento_contacto option[value='"+data.tipo_documento+"']").attr("selected", true);
                $('#numero_documento').val(data.numero_documento);
                $('#direccion').val(data.direccion);
                $('#numero_contacto').val(data.telefono_contacto);
                if(tipo == 'tercero'){
                    $('#email').val(data.email);
                    $('.div_correo').show();
                }else{
                    $('.div_correo').hide();
                }
            });
        }

         function clearErrors(){
            $('#errorNombre, #errorNumeroDocumento, #errorDireccion, #errorNumeroContacto, #errorEmail').text('');
            $('#nombre, #numero_documento, #direccion, #numero_contacto, #email').removeClass('is-invalid');
         }
    </script>
@stop
