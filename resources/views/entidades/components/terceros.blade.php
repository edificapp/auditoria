
<div class="text-right">
    @can('crear-terceros')
    <button class="btn-create-form" type="button" onclick="modalCreateContacto('tercero')">
        Agregar Tercero
    </button>
    @endcan
</div>
<div class="my-1">    
        <p class="card-title text-left">Listado Terceros</p>
    <div class="table-responsive">
        <table id="table" class="display" width="100%">
            <thead>
                <tr>
                    <th style="width:15%">Nombre Empresa</th>
                    <th style="width:30%">Direccion Contacto</th>
                    <th style="width:25%">Número Contacto</th>
                    <th style="width:10%">Accion</th>
                    <th style="width:5%" class="text-center">Rut</th>
                </tr>
            </thead>
            <tbody>
                @foreach(Auth::user()->empresa->clientes_terceros->filter(function ($item){return $item->tipo_contacto == 'tercero'; }) as $item)
                    <tr class="body-rows">
                        <td style="width:15%">{{strtolower($item->nombre)}}</td>
                        <td style="width:30%">{{$item->direccion}}</td>
                        <td style="width:25%">{{$item->telefono_contacto}}</td>
                        <td style="width:10%" class="text-center">
                            @can('editar-terceros')
                            <button onclick="editContacto('{{route('cliente-tercero.show', $item->id)}}', 'tercero', '{{route('cliente-tercero.update', $item->id)}}')" 
                                class="btn-edit-form"
                                type="button"
                                title="editar tercero">
                            </button>
                            @endcan
                        </td>
                        <td style="width:5%" class="text-center">
                            @if($item->certificado_bancario)
                            <a title="ver archivo" href="{{$item->certificado_bancario_public}}" class="btn-pdf-form">
                            </a>
                            @else
                                <span class="mdi mdi-file-pdf-box"></span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

