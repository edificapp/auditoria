<div class="text-right">
    @can('crear-proveedores')
    <button class="btn-create-form" type="button" data-toggle="modal" data-target="#modalCreateProveedor">
       Agregar Proveedor
    </button>
    @endcan
</div>
<div class="my-1">     
    <p class="card-title text-lef">Listado Proveedores</p>
    <div class="table-responsive">
        <table id="table" class="display" width="100%">
            <thead>
                <tr role="row">
                    <th>Nombre Empresa</th>
                    <th>Dirección Contacto</th>
                    <th>Número Contacto</th>
                    <th class="text-center">Accion</th>
                    <th class="text-center"><b>Rut</b></th>
                    <th class="text-center"><b>Certificado</b></th>
                </tr>
            </thead>
            <tbody>
                @foreach($proveedores as $item)
                    <tr class="body-rows">
                        <td>{{strtolower($item->nombre_empresa)}}</td>
                        <td>{{$item->telefono_contacto}}</td>
                        <td>{{$item->direccion}}</td>
                        <td class="text-center">
                            @can('editar-proveedores')
                            <button onclick="showProveedor('{{route('proveedor.edit', $item->id)}}')" 
                                    class="btn-edit-form"
                                    title="editar proveedor">
                            </button>
                            @endcan
                        </td>
                        <td class="text-center" title="Rut">
                            @if($item->rut)
                            <a href="{{$item->rut_public}}" class="btn-pdf-form">
                            </a>
                            @else
                                <span class="mdi mdi-file-pdf-box"></span>
                            @endif

                        </td>
                        <td class="text-center" title="Certificado Bancario">
                            @if($item->certificado_bancarios)
                            <a href="{{$item->certificado_bancario_public}}" class="btn-pdf-form"></a>
                            @else
                                <span class="mdi mdi-file-pdf-box"></span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div id="modalCreateProveedor" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11">Agregar Proveedor</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('proveedor.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-5 bg-light">Tipo de Persona: </label>
                            <select  id="tipo" class="form-control col-sm-7" name="tipo_persona">
                              <option>Juridico</option>
                              <option>Natural</option>
                            </select>
                        </div>
                    </div>
                    <div id="fields-juridicos">
                        <div class="row my-3">
                            <div class="input-group col-12">
                                <label class="input-group-text col-sm-4 bg-light">Nombre Empresa: </label>
                                <input 
                                    type="text" 
                                    name="nombre_empresa" 
                                    class="form-control col-sm-7 @error('nombre_empresa') is-invalid @enderror"  
                                    placeholder="Nombre Empresa" 
                                    value="{{old('nombre_empresa')}}"
                                >
                                <br>
                                @error('nombre_empresa')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Tipo documento:</label>
                            <select  id="tipo" class="form-control col-sm-7" name="tipo_documento">
                              <option>Cedula de Ciudadania</option>
                              <option>Cedula de Extranjeria</option>
                              <option>Nit</option>
                            </select>
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">No. de documento: </label>
                            <input 
                                type="text" 
                                name="numero_documento" 
                                class="form-control col-sm-7 @error('numero_documento') is-invalid @enderror"  
                                placeholder="Número de documento" 
                                value="{{old('numero_documento')}}"
                            >
                            <br>
                            @error('numero_documento')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div id="fields-naturales">
                        <div class="row my-3">
                            <div class="input-group col-6">
                                <label class="input-group-text col-sm-3 bg-light">Nombres: </label>
                                <input 
                                    type="text" 
                                    name="nombres" 
                                    class="form-control col-sm-9 @error('nombres') is-invalid @enderror"  
                                    placeholder="Nombres" 
                                    value="{{old('nombres')}}"
                                >
                                <br>
                                @error('nombres')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                            <div class="input-group col-6">
                                <label class="input-group-text col-sm-3 bg-light">Apellidos: </label>
                                <input 
                                    type="text" 
                                    name="apellidos" 
                                    class="form-control col-sm-9 @error('apellidos') is-invalid @enderror"  
                                    placeholder="Apellidos" 
                                    value="{{old('apellidos')}}"
                                >
                                <br>
                                @error('apellidos')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                        </div>

                        <div class="row my-3">
                            <div class="input-group col-9">
                                <label class="input-group-text col-sm-5 bg-light">Correo Electronico: </label>
                                <input 
                                    type="text" 
                                    name="email" 
                                    class="form-control col-sm-7 @error('email') is-invalid @enderror"  
                                    placeholder="Correo Electronico" 
                                    value="{{old('email')}}"
                                >
                                <br>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                            <div class="custom-control custom-switch col-3">
                              <input type="checkbox" class="custom-control-input col-sm-3" id="customSwitches" name="generar_usuario">
                              <label class="custom-control-label" for="customSwitches">Generar Usuario</label>
                            </div>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Telefono: </label>
                            <input 
                                type="text" 
                                name="telefono" 
                                class="form-control col-sm-7 @error('telefono') is-invalid @enderror"  
                                placeholder="telefono" 
                                value="{{old('telefono')}}"
                            >
                            <br>
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Direccion: </label>
                            <input 
                                type="text" 
                                name="direccion" 
                                class="form-control col-sm-7 @error('direccion') is-invalid @enderror"  
                                placeholder="Direccion" 
                                value="{{old('direccion')}}"
                            >
                            <br>
                            @error('direccion')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-12 text-center"><h3><b>Certificados</b></h3></div>
                        <br>
                        <div class="col-md-6">
                            <h5 class="text-center">Registro Unico Tributario</h5>
                           <input class="file_certificado" name="rut" type="file">
                        </div>
                        <div class="col-md-6">
                            <h5 class="text-center">Certificado Bancario</h5>
                           <input class="file_certificado" name="certificado_bancario" type="file">
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group  col-12">
                            <label class="input-group-text col-sm-5 bg-light">Contacto: </label>
                            <select  id="tipo" class="form-control col-sm-7" name="contacto_id">
                                    <option value="null" {{old('contacto_id') == null ? 'selected' : ''}}>sin contacto</option>
                                @foreach($proveedores  as $contacto)
                                    <option value="{{$contacto->id}}" {{old('contacto_id') == $contacto->id ? 'selected' : ''}}>{{$contacto->nombre_empresa}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row my-5 justify-content-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>
