<div class="text-right">
    @can('crear-clientes')
    <button class="btn-create-form" type="button" onclick="modalCreateContacto('cliente')">
        Agregar Cliente
    </button>
    @endcan
</div>
<div class="my-1">     
    <p class="card-title text-lef">Listado Clientes</p>
    <div class="table-responsive">
        <table id="table" class="display" width="100%">
            <thead>
                <tr role="row">
                    <th>Nombre Empresa</th>
                    <th>Dirección Contacto</th>
                    <th>Número Contacto</th>
                    <th class="text-center">Accion</th>
                    <th class="text-center"><b>Rut</b></th>
                </tr>
            </thead>
            <tbody>
                @foreach(Auth::user()->empresa->clientes_terceros->filter(function ($item){return $item->tipo_contacto == 'cliente'; }) as $item)
                    <tr class="body-rows">
                        <td>{{strtolower($item->nombre)}}</td>
                        <td>{{$item->direccion}}</td>
                        <td>{{$item->telefono_contacto}}</td>
                        <td class="text-center">
                            @can('editar-clientes')
                            <button onclick="editContacto('{{route('cliente-tercero.show', $item->id)}}', 'cliente', '{{route('cliente-tercero.update', $item->id)}}')" 
                                class="btn-edit-form"
                                title="editar cliente"
                            >
                            </button>
                            @endcan
                        </td>
                        <td class="text-center">
                            @if($item->certificado_bancario)
                            <a title="ver archivo" href="{{$item->certificado_bancario_public}}" class="btn-pdf-form"></a>
                             @else
                                <span class="mdi mdi-file-pdf">{{$item->certificado_bancarios}}</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
