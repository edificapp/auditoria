<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation">
    <tr>
        <td class="text-center">
            <center>
            	{{ Illuminate\Mail\Markdown::parse($slot) }}
            </center>
        </td>
    </tr>
</table>
