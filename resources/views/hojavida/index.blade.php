@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Hoja de vida</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row my-1 ml-0 justify-content-md-center">
    <nav class="nav nav-pills">
        <button class="nav-link col-sm border boton_cabecera active" type="button" data-target="#div_informacion_general">Información General</button>
        <button class="nav-link col-sm border boton_cabecera" type="button" data-target="#div_estudios">Estudios</button>
        <button class="nav-link col-sm border boton_cabecera" type="button" data-target="#div_logros">Publicaciones, investigaciones, logros e idiomas</button>
        <button class="nav-link col-sm border boton_cabecera" type="button" data-target="#div_experiencia">Experiencia laboral</button>
    </nav>
</div>
<div class="row">
    <div class="col-sm-12 grid-margin ">
        <form method="POST" action="{{ route('update_curriculum') }}" enctype="multipart/form-data">
            @csrf

            <div class="card collapse show collapse_Cabeceras " id="div_informacion_general">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="float-right estado_general"><i class="mdi mdi-checkbox-blank-circle text-danger"></i></div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6 ">
                                <div class="col-xl d-flex justify-content-center align-items-center">
                                    <img class="img-fluid img-thumbnail rounded img-hv" id="imagen" src="{{ $imagen != '' ? $imagen : asset('admin/images/usuario/usuario.jpg') }}">
                                </div>
                                <br>
                               <center><input class="form-control texto" type="file" name="inp_imagen" id="inp_imagen" value="{{ $imagen != '' ? $imagen : asset('admin/images/usuario/usuario.jpg') }}"></center>
                            </div>

                            <div class="col-md-6">

                                <div class="col-xl">
                                    <div class="form-group"><br>
                                        <label for="primer_apellido">Primer Apellido</label>
                                        <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $primer_apellido }}" placeholder="Primer Apellido">
                                    </div>
                                </div>

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="segundo_apellido">Segundo apellido</label>
                                        <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $segundo_apellido }}" placeholder="Segundo Apellido">
                                    </div>
                                </div>

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="nombres">Nombre(s)</label>
                                        <input type="text" class="form-control" id="nombres" name="nombres" value="{{ $nombres }}" placeholder="Nombre(s)">
                                    </div>
                                </div>

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="telefonos">Telefonos de contacto</label>
                                        <input type="text" class="form-control" id="telefonos" name="telefonos" value="{{ $telefonos }}" placeholder="Telefonos de contacto">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group col-xl">
                                    <label for="genero">Genero</label>
                                    <select class="form-control" id="genero" name="genero">
                                        <option value="">Seleccione un genero</option>
                                        <option value="M"{{ $genero == 'M' ? 'selected' : '' }}>Masculino</option>
                                        <option value="F"{{ $genero == 'F' ? 'selected' : '' }}>Femenino</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group col-xl">
                                    <label for="nacionalidad">Nacionalidad</label>
                                    <select class="form-control" id="nacionalidad" name="nacionalidad">
                                        <option value="">Seleccione una nacionalidad</option>
                                        <option value="COL"{{ $nacionalidad == 'COL' ? 'selected' : '' }}>Colombiano</option>
                                        <option value="EXT"{{ $nacionalidad == 'EXT' ? 'selected' : '' }}>Extranjero</option>
                                        <option value="DOB"{{ $nacionalidad == 'DOB' ? 'selected' : '' }}>Doble nacionalidad</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="municipio_pais_city_nacimiento">

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="mun_nacimiento">Municipio de nacimiento</label>
                                    <input type="text" class="form-control" id="mun_nacimiento" name="mun_nacimiento" value="{{ $mun_nacimiento }}" placeholder="Municipio de nacimiento">
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dep_nacimiento">Departamento de nacimiento</label>
                                    <input type="text" class="form-control" id="dep_nacimiento" name="dep_nacimiento" value="{{ $dep_nacimiento }}" placeholder="Departamento de nacimiento">
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="pais_nacimiento">Pais de nacimiento</label>
                                    <select class="form-control" id="pais_nacimiento" name="pais_nacimiento">
                                        <option value="">Seleccione un pais</option>
                                        @foreach ($paises as $pais)
                                        <option value="{{ $pais }}" {{ $pais_nacimiento == $pais ? 'selected' : '' }}>{!! $pais !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dep_nacimiento">Fecha de nacimiento</label>
                                    <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ $fecha_nacimiento }}">
                                </div>
                            </div>

                        </div>

                        <div class="row border-top pt-2" id="div_libreta">

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="libreta">Tipo de libreta</label>
                                    <select class="form-control" id="libreta" name="libreta">
                                        <option value="">Seleccione una libreta</option>
                                        <option value="1A"{{ $libreta == '1A' ? 'selected' : '' }}>1A</option>
                                        <option value="2A"{{ $libreta == '2A' ? 'selected' : '' }}>2A</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="numero_libreta">Número de libreta</label>
                                    <input type="text" class="form-control" id="numero_libreta" name="numero_libreta" value="{!! $numero_libreta !!}" placeholder="Número de libreta militar">
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dm">Distrito militar</label>
                                    <input type="text" class="form-control" id="dm" name="dm" value="{!! $dm !!}" placeholder="Distrito Militar">
                                </div>
                            </div>
                        </div>

                        <div class="row border-top pt-2" id="div_dni_nal">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dni_nal_tipo">Tipo de Documento</label>
                                    <select class="form-control" id="dni_nal_tipo" name="dni_nal_tipo">
                                        <option value="">Seleccione un tipo de documento</option>
                                        <option value="CC" {{ $dni_nal_tipo == 'CC' ? 'selected' : '' }}>Cedula de ciudadania</option>
                                        <option value="NIT" {{ $dni_nal_tipo == 'NIT' ? 'selected' : '' }}>Nit</option>
                                        <option value="OTRO" {{ $dni_nal_tipo == 'OTRO' ? 'selected' : '' }}>Otro</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dni_nal">Número de Documento</label>
                                    <input class="form-control" type="text" name="dni_nal" id="dni_nal" value="{!! $dni_nal !!}" placeholder="Número">
                                </div>
                            </div>
                        </div>

                        <div class="row border-top pt-2" id="div_extranjero">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="pais_extranjero">Pais extranjero</label>
                                    <select class="form-control" id="pais_extranjero" name="pais_extranjero">
                                        <option value="">Seleccione un pais extranjero</option>
                                        @foreach ($paises as $pais)
                                        <option value="{{ $pais }}" {{ $pais_extranjero == $pais ? 'selected' : '' }}>{!! $pais !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dni_ext_tipo">Tipo de Documento Extranjero</label>
                                    <select class="form-control" id="dni_ext_tipo" name="dni_ext_tipo">
                                        <option value="">Seleccione un tipo de documento</option>
                                        <option value="CE" {{ $dni_ext_tipo == 'CE' ? 'selected' : '' }}>Cedula de extranjeria</option>
                                        <option value="NIT" {{ $dni_ext_tipo == 'NIT' ? 'selected' : '' }}>Nit</option>
                                        <option value="PASAPORTE" {{ $dni_ext_tipo == 'PASAPORTE' ? 'selected' : '' }}>Pasaporte</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group">
                                    <label for="dni_ext">Número de Documento Extranjero</label>
                                    <input class="form-control" type="text" name="dni_ext" id="dni_ext" placeholder="Número">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('home') }}';" class="btn btn-secondary">Cancelar</button>
                        <button type="button" onclick="$('.boton_cabecera').get(1).click();" class="btn btn-primary">Siguiente</button>
                    </div>

            </div>

            <div class="card collapse collapse_Cabeceras" id="div_estudios">


                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right estado_general"><i class="mdi mdi-checkbox-blank-circle text-danger"></i></div>
                                <div id="secciones">



                                        <table class="table table-responsive table-bordered table-sm">
                                            <tbody >
                                                <tr class="table-active">
                                                    <th>Educación básica y media <strong class="text-info" title="Seleccione el último grado aprobado (Los grados de 1o. a 6o. de bachillerato equivalen a los grados 6o. a 11o. de educación básica secundaria y media).">(?)</strong></th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div class="row" style="padding:17px;">

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="grado">Ultimo grado</label>
                                                                    <select class="form-control input-sm" id="grado" name="grado">
                                                                        <option value="">Seleccione un grado</option>
                                                                        <option value="1"{{ $grado == '1' ? 'selected' : '' }}>1° (Primaria)</option>
                                                                        <option value="2"{{ $grado == '2' ? 'selected' : '' }}>2° (Primaria)</option>
                                                                        <option value="3"{{ $grado == '3' ? 'selected' : '' }}>3° (Primaria)</option>
                                                                        <option value="4"{{ $grado == '4' ? 'selected' : '' }}>4° (Primaria)</option>
                                                                        <option value="5"{{ $grado == '5' ? 'selected' : '' }}>5° (Primaria)</option>
                                                                        <option value="6"{{ $grado == '6' ? 'selected' : '' }}>6° (Secundaria)</option>
                                                                        <option value="7"{{ $grado == '7' ? 'selected' : '' }}>7° (Secundaria)</option>
                                                                        <option value="8"{{ $grado == '8' ? 'selected' : '' }}>8° (Secundaria)</option>
                                                                        <option value="9"{{ $grado == '9' ? 'selected' : '' }}>9° (Secundaria)</option>
                                                                        <option value="10"{{ $grado == '10' ? 'selected' : '' }}>10° (Media)</option>
                                                                        <option value="11"{{ $grado == '11' ? 'selected' : '' }}>11° (Media)</option>
                                                                    </select>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="titulo">Titulo obtenido</label>
                                                                    <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $titulo }}" placeholder="Titulo obtenido">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="escuela">Institución</label>
                                                                    <input type="text" class="form-control" id="escuela" name="escuela" value="{{ $escuela }}" placeholder="Institución">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="ciudad_escuela">Municipio de la Institución</label>
                                                                    <input type="text" class="form-control" id="ciudad_escuela" name="ciudad_escuela" value="{{ $ciudad_escuela }}" placeholder="Municipio de la Institución">
                                                                </div>
                                                            </div>

                                                            <div class="col-md">
                                                                <div class="form-group">
                                                                    <label for="fecha_fin">Fecha de terminacion</label>
                                                                    <input type="month" class="form-control" id="fecha_fin" name="fecha_fin" value="{{ $fecha_fin }}" placeholder="Fecha de terminación">
                                                                </div>
                                                            </div>

                                                            <div class="col-md">
                                                                <div class="form-group">
                                                                    <label for="certificado_basico">Certificado</label>
                                                                    <input value="{{ $certificado_basico }}" class="form-control" id="certificado_basico" name="certificado_basico" type="file" placeholder="Certificado">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </th>
                                                </tr>
                                                {{-- Estudios normales --}}
                                                {{-- Estudios superiores --}}
                                                <tr class="table-active" >
                                                    <th>
                                                        Educación superior (Pregrado y postgrado) <strong class="text-info" title="Seleccione el último grado aprobado (Los grados de 1o. a 6o. de bachillerato equivalen a los grados 6o. a 11o. de educación básica secundaria y media).">(?)</strong>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <small class="text-danger">* Diligencie este punto en estricto orden cronológico.</small>
                                                        <br>
                                                        <small class="text-danger">* En caso que sus estudios sean por módulos, créditos o años conviertalos a semestres.</small>
                                                        <div class="estudios_superiores" >

                                                            @if (count($estudios_superiores) == 0)

                                                            <div class="estudio container-fluid mt-3 border border-primary rounded p-3" >
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerEstudios(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="modalidad_ac_0">Modalidad Academica</label>
                                                                            <select class="form-control" id="modalidad_ac_0" name="modalidad_ac[]">
                                                                                <option value="">Seleccione modalidad</option>
                                                                                <option value="TC">Técnica</option>
                                                                                <option value="TL">Tecnológica</option>
                                                                                <option value="TE">Tecnológica Especializada</option>
                                                                                <option value="UN">Universitaria</option>
                                                                                <option value="ES">Especialización</option>
                                                                                <option value="MG">Maestria o Magister</option>
                                                                                <option value="DC">Doctorado o PHD</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="titulo_estudio_0">Nombre de los estudios / Titulo obtenido</label>
                                                                            <input type="text" class="form-control" id="titulo_estudio_0" name="titulo_estudio[]" placeholder="Nombre de los estudios / Titulo obtenido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="est_superiores_escuela">Nombre de Intitución Educativa</label>
                                                                            <input type="text" class="form-control" id="est_superiores_escuela_0" name="est_superiores_escuela[]" placeholder="Nombre de los estudios / Titulo obtenido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="graduado_0">Finalizado</label>
                                                                            <select class="form-control" id="graduado_0" name="graduado[]">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="si">Si</option>
                                                                                <option value="no">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="semestres_0">Semestres aprobados</label>
                                                                            <input type="text" class="form-control" id="semestres_0" name="semestres[]" placeholder="Semestres aprobados">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="fecha_fin_estudio_0">Fecha de terminacion</label>
                                                                            <input type="month" class="form-control" id="fecha_fin_estudio_0" name="fecha_fin_estudio[]" placeholder="Fecha de terminación">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="tarjeta_profesional">Numero de tarjeta profesional</label>
                                                                            <input type="text" class="form-control" id="tarjeta_profesional_0" name="tarjeta_profesional[]" placeholder="Numero de tarjeta profesional">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="certificado_0">Certificado gg</label>
                                                                            <input class="form-control" id="certificado_0" name="certificado[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @endif

                                                            @foreach($estudios_superiores as $index => $estudio_superior)

                                                            <div class="estudio container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerEstudios(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="modalidad_ac_{{ $index }}">Modalidad Academica</label>
                                                                            <select class="form-control" id="modalidad_ac_{{ $index }}" name="modalidad_ac[]">
                                                                                <option value="">Seleccione modalidad</option>
                                                                                <option value="TC" {{ $estudio_superior->modalidad == 'TC' ? 'selected' : '' }}>Técnica</option>
                                                                                <option value="TL" {{ $estudio_superior->modalidad == 'TL' ? 'selected' : '' }}>Tecnológica</option>
                                                                                <option value="TE" {{ $estudio_superior->modalidad == 'TE' ? 'selected' : '' }}>Tecnológica Especializada</option>
                                                                                <option value="UN" {{ $estudio_superior->modalidad == 'UN' ? 'selected' : '' }}>Universitaria</option>
                                                                                <option value="ES" {{ $estudio_superior->modalidad == 'ES' ? 'selected' : '' }}>Especialización</option>
                                                                                <option value="MG" {{ $estudio_superior->modalidad == 'MG' ? 'selected' : '' }}>Maestria o Magister</option>
                                                                                <option value="DC" {{ $estudio_superior->modalidad == 'DC' ? 'selected' : '' }}>Doctorado o PHD</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="titulo_estudio_{{ $index }}">Nombre de los estudios / Titulo obtenido</label>
                                                                            <input value="{!! $estudio_superior->estudio !!}" type="text" class="form-control" id="titulo_estudio_{{ $index }}" name="titulo_estudio[]" placeholder="Nombre de los estudios / Titulo obtenido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="est_superiores_escuela_{{ $index }}">Nombre de Intitución Educativa</label>
                                                                            <input value="{!! $estudio_superior->escuela !!}" type="text" class="form-control" id="est_superiores_escuela_{{ $index }}" name="est_superiores_escuela[]" placeholder="Nombre de los estudios / Titulo obtenido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="graduado_{{ $index }}">Finalizado</label>
                                                                            <select class="form-control" id="graduado_{{ $index }}" name="graduado[]">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="si" {!! $estudio_superior->finalizado == 'si' ? 'selected' : ''  !!}>Si</option>
                                                                                <option value="no" {!! $estudio_superior->finalizado == 'no' ? 'selected' : ''  !!}>No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="semestres_{{ $index }}">Semestres aprobados</label>
                                                                            <input value="{!! $estudio_superior->semestres !!}" type="text" class="form-control" id="semestres_{{ $index }}" name="semestres[]" placeholder="Semestres aprobados">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="fecha_fin_estudio_{{ $index }}">Fecha de terminacion</label>
                                                                            <input value="{!! $estudio_superior->fecha_fin !!}" type="month" class="form-control" id="fecha_fin_estudio_{{ $index }}" name="fecha_fin_estudio[]" placeholder="Fecha de terminación">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="tarjeta_profesional_{{ $index }}">Numero de tarjeta profesional</label>
                                                                            <input value="{!! $estudio_superior->tarjeta !!}" type="text" class="form-control" id="tarjeta_profesional_{{ $index }}" name="tarjeta_profesional[]" placeholder="Numero de tarjeta profesional">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="certificado_{{ $index }}">Certificado</label>
                                                                            <input value="{{ $estudio_superior->certificado }}" class="form-control" id="certificado{{ $index }}" name="certificado[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @endforeach

                                                        </div>
                                                        <div class="mt-3 d-flex justify-content-center">
                                                            <button type="button" class="btn btn-primary" onclick="agregarEstudio();" title="Añadir estudio superior">Añadir estudio</button>
                                                        </div>
                                                    </th>
                                                </tr>
                                                {{-- Estudios superiores --}}
                                                {{-- Otros estudios --}}
                                                <tr class="table-active">
                                                    <th>
                                                        Otros estudios
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <small class="text-danger">* CAP del sena, estudios de educación no formal, simposios, talleres y demas estudios que pueda certificar.</small>
                                                        <div class="otros_estudios">

                                                            @if (count($otros_estudios) == 0)

                                                            <div class="otro_estudio container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerOtrosEstudios(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="titulo_otro_estudio_0">Nombre del estudio</label>
                                                                            <input type="text" class="form-control" id="titulo_otro_estudio_0" name="titulo_otro_estudio[]" placeholder="Nombre del estudio">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="otros_estudios_escuela_0">Nombre de la Institución</label>
                                                                            <input type="text" class="form-control" id="otros_estudios_escuela_0" name="otros_estudios_escuela[]" placeholder="Nombre del la Institución">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-1">
                                                                        <div class="form-group">
                                                                            <label for="otros_estudios_horas_0">Horas</label>
                                                                            <input type="text" class="form-control" id="otros_estudios_horas_0" name="otros_estudios_horas[]" placeholder="Horas">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-1">
                                                                        <div class="form-group">
                                                                            <label for="otros_estudios_ano_0">Año</label>
                                                                            <input type="text" class="form-control" id="otros_estudios_ano_0" name="otros_estudios_ano[]" placeholder="Año">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="certificado_otros_est_0">Certificado</label>
                                                                            <input class="form-control" id="certificado_otros_est_0" name="certificado_otros_est[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            @endif

                                                            @foreach($otros_estudios as $index => $otro_estudio)

                                                            <div class="otro_estudio container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerOtrosEstudios(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="titulo_otro_estudio_{{ $index }}">Nombre del estudio</label>
                                                                            <input value="{!! $otro_estudio->estudio !!}" type="text" class="form-control" id="titulo_otro_estudio_{{ $index }}" name="titulo_otro_estudio[]" placeholder="Nombre del estudio">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="otros_estudios_escuela_{{ $index }}">Nombre de la Institución</label>
                                                                            <input value="{!! $otro_estudio->escuela !!}" type="text" class="form-control" id="otros_estudios_escuela_{{ $index }}" name="otros_estudios_escuela[]" placeholder="Nombre de la Institución">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label for="otros_estudios_horas_{{ $index }}">Horas</label>
                                                                            <input value="{!! $otro_estudio->horas !!}" type="text" class="form-control" id="otros_estudios_horas_{{ $index }}" name="otros_estudios_horas[]" placeholder="Horas">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label for="otros_estudios_ano_{{ $index }}">Año</label>
                                                                            <input value="{!! $otro_estudio->ano !!}" type="text" class="form-control" id="otros_estudios_ano_{{ $index }}" name="otros_estudios_ano[]" placeholder="Horas">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="certificado_otros_est_{{ $index }}">Certificado</label>
                                                                            <input value="{{ $otro_estudio->certificado }}" class="form-control" id="certificado_otros_est_{{ $index }}" name="certificado_otros_est[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            @endforeach

                                                        </div>
                                                        <div class="mt-3 d-flex justify-content-center">
                                                            <button type="button" class="btn btn-primary" onclick="agregarOtroEstudio();" title="Añadir otro estudio">Añadir otro estudio</button>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>


                    <div class="btn-group">
                        <button type="button" onclick="$('.boton_cabecera').get(0).click();" class="btn btn-secondary">Anterior</button>
                        <button type="button" onclick="$('.boton_cabecera').get(2).click();" class="btn btn-primary">Siguiente</button>
                    </div>


            </div>

            <div class="card collapse collapse_Cabeceras" id="div_logros">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12" style="padding: 0px;">
                                <div class="float-right estado_general"><i class="mdi mdi-checkbox-blank-circle text-danger"></i></div>
                                <div id="secciones">



                                        {{-- Logros e idiomas --}}
                                        <table class="table table-bordered table-responsive" style="display:table;">
                                            <tbody>
                                                {{-- Publicaciones, investigaciones y/o logros laborales --}}
                                                <tr class="table-active">
                                                    <th>
                                                        Publicaciones, investigaciones y/o logros laborales
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 4px;">
                                                        <div class="logros">

                                                            @if (count($logros_laborales) == 0)
                                                            <div class="logro container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerLogro(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-7">
                                                                        <div class="form-group">
                                                                            <label for="logro_0">Logro</label>
                                                                            <textarea rows="1"  class="form-control" id="logro_0" name="logro[]" placeholder="Publicación, investigación y/o logro laboral"></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-5">
                                                                        <div class="form-group">
                                                                            <label for="certificado_logro_0">Certificado</label>
                                                                            <input   class="form-control" id="certificado_logro_0" name="certificado_logro[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @endif

                                                            @foreach ($logros_laborales as $index => $logro)
                                                            <div class="logro container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerLogro(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-7">
                                                                        <div class="form-group">
                                                                            <label for="logro_{{ $index }}">Logro</label>
                                                                            <textarea rows="1" class="form-control" id="logro_{{ $index }}" name="logro[]" placeholder="Publicación, investigación y/o logro laboral">{!! $logro->logro !!}</textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-5">
                                                                        <div class="form-group">
                                                                            <label for="certificado_logro_{{ $index }}">Certificado</label>
                                                                            <input  value="{{ $logro->certificado }}" class="form-control" id="certificado_logro_{{ $index }}" name="certificado_logro[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @endforeach

                                                        </div>
                                                        <div class="mt-3 d-flex justify-content-center">
                                                            <button type="button" class="btn btn-primary" onclick="agregarLogro();" title="Añadir logro">Añadir logro</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                {{-- Publicaciones, investigaciones y/o logros laborales --}}

                                                {{-- Idiomas --}}
                                                <tr class="table-active">
                                                    <th>
                                                        Idiomas
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 4px;">
                                                        <div class="idiomas">

                                                            @if (count($idiomas) == 0)
                                                            <div class="idioma container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerIdioma(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="idioma_0">Idioma</label>
                                                                            <input type="text" class="form-control" id="idioma_0" name="idioma[]" placeholder="Idioma">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="habla_0">Habla</label>
                                                                            <select class="form-control" id="habla_0" name="habla[]">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="R">Regular</option>
                                                                                <option value="B">Bien</option>
                                                                                <option value="MB">Muy bien</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="lee_0">Lee</label>
                                                                            <select class="form-control" id="lee_0" name="lee[]">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="R">Regular</option>
                                                                                <option value="B">Bien</option>
                                                                                <option value="MB">Muy bien</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="escribe_0">Escribe</label>
                                                                            <select class="form-control" id="escribe_0" name="escribe[]">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="R">Regular</option>
                                                                                <option value="B">Bien</option>
                                                                                <option value="MB">Muy bien</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="observacion_0">Observación</label>
                                                                            <textarea class="form-control" id="observacion_0" name="observacion[]" placeholder="Observación"></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="certificado_idioma_0">Certificado</label>
                                                                            <input class="form-control" id="certificado_idioma_0" name="certificado_idioma[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @endif

                                                            @foreach ($idiomas as $index => $idioma)
                                                            <div class="idioma container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerIdioma(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="idioma_{{ $index }}">Idioma</label>
                                                                            <input value="{!! $idioma->idioma !!}" type="text" class="form-control" id="idioma_{{ $index }}" name="idioma[]" placeholder="Idioma">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="habla_{{ $index }}">Habla</label>
                                                                            <select class="form-control" name="habla[]" id="habla_{{ $index }}">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="R" {!! $idioma->habla == 'R' ? 'selected' : '' !!}>Regular</option>
                                                                                <option value="B" {!! $idioma->habla == 'B' ? 'selected' : '' !!}>Bien</option>
                                                                                <option value="MB" {!! $idioma->habla == 'MB' ? 'selected' : '' !!}>Muy bien</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="lee_{{ $index }}">Lee</label>
                                                                            <select class="form-control" name="lee[]" id="lee_{{ $index }}">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="R"{!! $idioma->lee == 'R' ? 'selected' : '' !!}>Regular</option>
                                                                                <option value="B"{!! $idioma->lee == 'B' ? 'selected' : '' !!}>Bien</option>
                                                                                <option value="MB"{!! $idioma->lee == 'MB' ? 'selected' : '' !!}>Muy bien</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="escribe_{{ $index }}">Escribe</label>
                                                                            <select class="form-control" name="escribe[]" id="escribe_{{ $index }}">
                                                                                <option value="">Seleccione</option>
                                                                                <option value="R" {!! $idioma->escribe == 'R' ? 'selected' : '' !!}>Regular</option>
                                                                                <option value="B" {!! $idioma->escribe == 'B' ? 'selected' : '' !!}>Bien</option>
                                                                                <option value="MB" {!! $idioma->escribe == 'MB' ? 'selected' : '' !!}>Muy bien</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="observacion_{{ $index }}">Observación</label>
                                                                            <textarea class="form-control" id="observacion_{{ $index }}" name="observacion[]" placeholder="Observación">{!! $idioma->observacion !!}</textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="certificado_idioma_{{ $index }}">Certificado</label>
                                                                            <input value="{!! $idioma->certificado !!}" class="form-control" id="certificado_idioma_{{ $index }}" name="certificado_idioma[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @endforeach

                                                        </div>
                                                        <div class="mt-3 d-flex justify-content-center">
                                                            <button type="button" class="btn btn-primary" onclick="agregarIdioma();" title="Añadir idioma">Añadir idioma</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                {{-- Idiomas --}}
                                            </tbody>
                                        </table>
                                        {{-- Logros e idiomas --}}



                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="$('.boton_cabecera').get(2).click();" class="btn btn-secondary">Anterior</button>
                        <button type="button" onclick="$('.boton_cabecera').get(3).click();" class="btn btn-primary">Siguiente</button>
                    </div>


            </div>

            <div class="card collapse collapse_Cabeceras container-fluid" id="div_experiencia">
                       <div class="row">
                            <div class="col-sm-12" style="padding: 0px;">
                                <div class="float-right estado_general"><i class="mdi mdi-checkbox-blank-circle text-danger"></i></div>
                                <div id="secciones">

                                        {{-- Logros e idiomas --}}
                                        <table class="table table-bordered">
                                            <tbody>
                                                {{-- Publicaciones, investigaciones y/o logros laborales --}}
                                                <tr class="table-active">
                                                    <th>
                                                        Experiencia laboral
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 4px;">

                                                        <small class="text-danger">* Relacione su experiencia laboral o de servicios en estricto orden cronologico comenzando por el actual o ultimo empleo o servicio prestado.</small>

                                                        <input type="hidden" name="desdeaqui" value="sidesdeaqui">
                                                        <div class="experiencias_laborales">


                                                            @if (count($experiencias_laborales) == 0)
                                                            <div class="experiencia_laboral container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerExperienciaLaboral(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_entidad_0">Empresa o entidad</label>
                                                                            <textarea rows="1" class="form-control" id="explab_entidad_0" name="explab_entidad[]" placeholder="Empresa o entidad"></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_dedicacion_0">Tipo de dedicación</label>
                                                                            <select class="form-control" id="explab_dedicacion_0" name="explab_dedicacion[]">
                                                                                <option value="">Seleccione dedicación</option>
                                                                                <option value="tc">Tiempo completo</option>
                                                                                <option value="mt">Medio tiempo</option>
                                                                                <option value="tp">Tiempo parcial</option>
                                                                                <option value="otra">Otra dedicación</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 d-none" id="div_dedicacion_0">
                                                                        <div class="form-group">
                                                                            <label for="explab_od_0">Dedicación</label>
                                                                            <input type="text" class="form-control" id="explab_od_0" name="explab_od[]" placeholder="Otra dedicación">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_tipo_0">Tipo de empresa</label>
                                                                            <select class="form-control" id="explab_tipo_0" name="explab_tipo[]">
                                                                                <option value="">Seleccione un tipo</option>
                                                                                <option value="Publica">Publica</option>
                                                                                <option value="Privada">Privada</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="explab_cargo_0">Cargo o contrato actual</label>
                                                                            <input type="text" class="form-control" id="explab_cargo_0" name="explab_cargo[]" placeholder="Cargo o contrato actual">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="explab_dependencia_0">Dependencia</label>
                                                                            <input type="text" class="form-control" id="explab_dependencia_0" name="explab_dependencia[]" placeholder="Cargo o contrato actual">
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="explab_pais_0">País</label>
                                                                            <select class="form-control" id="explab_pais_0" name="explab_pais[]">
                                                                                <option value="">Seleccione un pais</option>
                                                                                @foreach ($paises as $pais)
                                                                                <option value="{{ $pais }}">{!! $pais !!}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4" id="div_departamento_0">
                                                                        <div class="form-group d-none">
                                                                            <label for="explab_departamento_0">Departamento</label>
                                                                            <select class="form-control" id="explab_departamento_0"  name="explab_departamento[]">
                                                                                <option value="">Seleccione un departamento</option>
                                                                                @foreach (json_decode($depto_ciudades,1) as $departamento => $ciudades)
                                                                                <option value="{{ $departamento }}">{!! $departamento !!}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="explab_inpdepartamento_0">Estado / Provincia / Departamento</label>
                                                                            <input type="text" class="form-control" id="explab_inpdepartamento_0" name="explab_inpdepartamento[]">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3" id="div_ciudad_0">
                                                                        <div class="form-group d-none">
                                                                            <label for="explab_ciudad_0">Ciudad</label>
                                                                            <select class="form-control" id="explab_ciudad_0" name="explab_ciudad[]">
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="explab_inpciudad_0">Ciudad</label>
                                                                            <input type="text" class="form-control" id="explab_inpciudad_0" name="explab_inpciudad[]">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_direccion_0">Dirección</label>
                                                                            <input type="text" class="form-control" id="explab_direccion_0" name="explab_direccion[]" placeholder="Dirección">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="explab_telefonos_0">Telefonos</label>
                                                                            <input type="text" class="form-control" id="explab_telefonos_0" name="explab_telefonos[]" placeholder="Telefonos">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="explab_fingreso_0">Fecha de ingreso</label>
                                                                            <input type="date" class="form-control" id="explab_fingreso_0" name="explab_fingreso[]" placeholder="Fecha de ingreso">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_fterminacion_0">Fecha de terminación</label>
                                                                            <input type="date" class="form-control" id="explab_fterminacion_0" name="explab_fterminacion[]" placeholder="Fecha de terminación">
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_certificado_0">Certificado laboral</label>
                                                                            <input type="text" class="form-control" id="explab_certificado_0" name="explab_certificado[]" placeholder="Certificado">
                                                                            <input class="form-control" id="explab_certificado_0" name="explab_certificado[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif

                                                            @foreach ($experiencias_laborales as $index => $experiencia)
                                                            <div class="experiencia_laboral container-fluid mt-3 border border-primary rounded p-3">
                                                                <div class="row d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerExperienciaLaboral(this);"><i class="mdi mdi-close"></i></button>
                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_entidad_{!! $index !!}">Empresa o entidad</label>
                                                                            <textarea rows="1" class="form-control" id="explab_entidad_{!! $index !!}" name="explab_entidad[]" placeholder="Empresa o entidad">{!! $experiencia->empresa !!}</textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_dedicacion_{!! $index !!}">Tipo de dedicación</label>
                                                                            <select class="form-control" id="explab_dedicacion_{!! $index !!}" name="explab_dedicacion[]">
                                                                                <option value="">Seleccione dedicación</option>
                                                                                <option value="tc"{!! $experiencia->dedicacion == 'tc' ? 'selected' : '' !!}>Tiempo completo</option>
                                                                                <option value="mt"{!! $experiencia->dedicacion == 'mt' ? 'selected' : '' !!}>Medio tiempo</option>
                                                                                <option value="tp"{!! $experiencia->dedicacion == 'tp' ? 'selected' : '' !!}>Tiempo parcial</option>
                                                                                <option value="otra"{!! collect(['tp','mt','tc',''])->search($experiencia->dedicacion) ? '' : 'selected' !!}>Otra dedicación</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 {!! collect(['tp','mt','tc'])->search($experiencia->dedicacion) ? 'd-none' : '' !!}" id="div_dedicacion_{!! $index !!}">
                                                                        <div class="form-group">
                                                                            <label for="explab_od_{!! $index !!}">Dedicación</label>
                                                                            <input type="text" class="form-control" id="explab_od_{!! $index !!}" name="explab_od[]" placeholder="Otra dedicación" value="{!! $experiencia->dedicacion !!}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_tipo_{!! $index !!}">Tipo de empresa</label>
                                                                            <select class="form-control" id="explab_tipo_{!! $index !!}" name="explab_tipo[]">
                                                                                <option value="">Seleccione un tipo</option>
                                                                                <option value="Publica" {!! $experiencia->tipo == 'Publica' ? 'selected' : '' !!}>Publica</option>
                                                                                <option value="Privada" {!! $experiencia->tipo == 'Privada' ? 'selected' : '' !!}>Privada</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="explab_cargo_{!! $index !!}">Cargo o contrato actual</label>
                                                                            <input type="text" class="form-control" id="explab_cargo_{!! $index !!}" name="explab_cargo[]" placeholder="Cargo o contrato actual" value="{!! $experiencia->cargo !!}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="explab_dependencia_{!! $index !!}">Dependencia</label>
                                                                            <input type="text" class="form-control" id="explab_dependencia_{!! $index !!}" name="explab_dependencia[]" placeholder="Cargo o contrato actual" value="{!! $experiencia->dependencia !!}">
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="explab_pais_{!! $index !!}">País</label>
                                                                            <select class="form-control" id="explab_pais_{!! $index !!}" name="explab_pais[]">
                                                                                <option value="">Seleccione un pais</option>
                                                                                @foreach ($paises as $pais)
                                                                                <option value="{{ $pais }}" {!! $experiencia->pais == $pais ? 'selected' : '' !!}>{!! $pais !!}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4" id="div_departamento_{!! $index !!}">
                                                                        <div class="form-group {!! $experiencia->pais != 'Colombia' ? 'd-none' : '' !!}">
                                                                            <label for="explab_departamento_{!! $index !!}">Departamento</label>
                                                                            <select class="form-control" id="explab_departamento_{!! $index !!}"  name="explab_departamento[]">
                                                                                <option value="">Seleccione un departamento</option>
                                                                                @foreach (json_decode($depto_ciudades,1) as $departamento => $ciudades)
                                                                                <option value="{{ $departamento }}" {{ $experiencia->departamento == $departamento ? 'selected' : '' }}>{!! $departamento !!}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group {!! $experiencia->pais == 'Colombia' ? 'd-none' : '' !!}">
                                                                            <label for="explab_inpdepartamento_{!! $index !!}">Estado / Provincia / Departamento</label>
                                                                            <input type="text" class="form-control" id="explab_inpdepartamento_{!! $index !!}" name="explab_inpdepartamento[{!! $index !!}]" value="{!! $experiencia->departamento !!}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3" id="div_ciudad_{!! $index !!}">
                                                                        <div class="form-group {!! $experiencia->pais != 'Colombia' ? 'd-none' : '' !!}">
                                                                            <label for="explab_ciudad_{!! $index !!}">Ciudad</label>
                                                                            <select class="form-control" id="explab_ciudad_{!! $index !!}" name="explab_ciudad[]" ciudadact="{!! $experiencia->ciudad !!}">
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group {!! $experiencia->pais == 'Colombia' ? 'd-none' : '' !!}">
                                                                            <label for="explab_inpciudad_{!! $index !!}">Ciudad</label>
                                                                            <input type="text" class="form-control" id="explab_inpciudad_{!! $index !!}" name="explab_inpciudad[{!! $index !!}]" value="{!! $experiencia->ciudad !!}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_direccion_{!! $index !!}">Dirección</label>
                                                                            <input type="text" class="form-control" id="explab_direccion_{!! $index !!}" name="explab_direccion[]" placeholder="Dirección" value="{!! $experiencia->direccion !!}">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row">

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="explab_telefonos_{!! $index !!}">Telefonos</label>
                                                                            <input type="text" class="form-control" id="explab_telefonos_{!! $index !!}" name="explab_telefonos[]" placeholder="Telefonos" value="{!! $experiencia->telefonos !!}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="explab_fingreso_{!! $index !!}">Fecha de ingreso</label>
                                                                            <input type="date" class="form-control" id="explab_fingreso_{!! $index !!}" name="explab_fingreso[]" placeholder="Fecha de ingreso" value="{!! $experiencia->fingreso !!}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_fterminacion_{!! $index !!}">Fecha de terminación</label>
                                                                            <input type="date" class="form-control" id="explab_fterminacion_{!! $index !!}" name="explab_fterminacion[]" placeholder="Fecha de terminación" value="{!! $experiencia->fterminacion !!}">
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="explab_certificado_{!! $index !!}">Certificado laboral</label>
                                                                            <input value="{!! $experiencia->certificado !!}" class="form-control" id="explab_certificado_{!! $index !!}" name="explab_certificado[]" type="file" placeholder="Certificado">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach

                                                        </div>
                                                        <div class="mt-3 d-flex justify-content-center">
                                                            <button type="button" class="btn btn-primary" onclick="agregarExperiencia();" title="Añadir logro">Añadir Experiencia</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                {{-- Publicaciones, investigaciones y/o logros laborales --}}
                                            </tbody>
                                        </table>
                                        {{-- Logros e idiomas --}}


                                </div>
                            </div>
                        </div>


                    <div class="btn-group">
                        <button type="button" onclick="$('.boton_cabecera').get(1).click();" class="btn btn-secondary">Anterior</button>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>


            </div>

            <input type="hidden" name="contador_estudios" id="contador_estudios">
            <input type="hidden" name="contador_otros_estudios" id="contador_otros_estudios">
            <input type="hidden" name="contador_logros" id="contador_logros">
            <input type="hidden" name="contador_idiomas" id="contador_idiomas">
            <input type="hidden" name="contador_experiencias" id="contador_experiencias">
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    let deptos_ciudades_col = {!! $depto_ciudades !!};

    let cantidad_estudios = '{!! count($estudios_superiores) == 0 ? 1 : count($estudios_superiores) !!}';
    let cantidad_otros_estudios = '{!! count($otros_estudios) == 0 ? 1 : count($otros_estudios) !!}';
    let cantidad_logros = '{!! count($logros_laborales) == 0 ? 1 : count($logros_laborales) !!}';
    let cantidad_idiomas = '{!! count($idiomas) == 0 ? 1 : count($idiomas) !!}';
    let cantidad_experiencia = '{!! count($experiencias_laborales) == 0 ? 1 : count($experiencias_laborales) !!}';

    let contador_estudios = {!! count($estudios_superiores) == 0 ? 1 : count($estudios_superiores) !!};
    let contador_otros_estudios = {!! count($otros_estudios) == 0 ? 1 : count($otros_estudios) !!};
    let contador_logros = '{!! count($logros_laborales) == 0 ? 1 : count($logros_laborales) !!}';
    let contador_idiomas = '{!! count($idiomas) == 0 ? 1 : count($idiomas) !!}';
    let contador_experiencias = '{!! count($experiencias_laborales) == 0 ? 1 : count($experiencias_laborales) !!}';

    let route_prefix = "{{ url(config('lfm.url_prefix')) }}";

    $(document).ready(function() {
        $('#div_extranjero').hide();
        $('#div_dni_nal').hide();
        $('#div_libreta').hide();

        setTimeout(function() {
            $('#nacionalidad').change();
            $('#genero').change();

            $.each($('[name="explab_departamento[]"]'), function(index, elm) {
                let elemento = $(elm);
                elemento.change();

                let posicion = elemento.attr('id').split('_').pop();
                let pais = $('#explab_pais_'+posicion).val();

                console.log(pais);

                setTimeout(function() {
                    if (pais == 'Colombia') {

                        let exp_ciudad = $('#explab_ciudad_'+posicion);
                        let ciudad_actual = exp_ciudad.attr('ciudadact');

                        $.each(exp_ciudad.find('option'), function(index, opt) {

                            let opcion = $(opt);
                            let ciudad = opcion.attr('value');

                            if (ciudad == ciudad_actual) {
                                opcion.attr('selected', 'true');
                            }
                        });
                    }
                }, 500);

            });
        }, 500);contador_experiencias

        $('#div_informacion_general').on('change', function(event) {
            validarEstadoPestana(this);
        });

        $('#div_estudios').on('change', function(event) {
            validarEstadoPestana(this);
        });

        $('#div_logros').on('change', function(event) {
            validarEstadoPestana(this);
        });

        $('#div_experiencia').on('change', function(event) {
            validarEstadoPestana(this);
        });

        $('.experiencias_laborales').delegate('[name="explab_dedicacion[]"]', 'change', function(event) {
            let input_dedicacion = $(this);
            let dedicacion = input_dedicacion.val();
            let posicion = input_dedicacion.attr('id').split('_').pop();

            let div_dedicacion = $('#div_dedicacion_'+posicion);
            let dedicacion_input = div_dedicacion.find('input');

            if (dedicacion == 'otra') {
                dedicacion_input.val('');
                div_dedicacion.removeClass('d-none');
            } else {
                div_dedicacion.addClass('d-none');
            }
        });

        $('.experiencias_laborales').delegate('[name="explab_pais[]"]', 'change', function(event) {
            let input_pais = $(this);
            let pais = input_pais.val();
            let posicion = input_pais.attr('id').split('_').pop();

            if (pais != '') {
                let campos_departamentos = $('#div_departamento_'+posicion);
                let cd_select = campos_departamentos.find('select');
                let cd_input = campos_departamentos.find('input');

                if (pais == 'Colombia') {

                    cd_select.parents('.form-group').removeClass('d-none');
                    cd_input.parents('.form-group').addClass('d-none');

                } else {
                    cd_select.parents('form-group').addClass('d-none');
                    cd_input.parents('form-group').removeClass('d-none');
                }
            }
        });

        $('.experiencias_laborales').delegate('[name="explab_departamento[]"]', 'change', function(event) {
            let input_departamento = $(this);
            let departamento = input_departamento.val();
            let posicion = input_departamento.attr('id').split('_').pop();
            let div_ciudad = $('#div_ciudad_'+posicion);
            let ciudad_select = div_ciudad.find('select');
            let ciudad_input = div_ciudad.find('input');

            if (departamento != '') {
                let ciudades = deptos_ciudades_col[departamento];

                ciudad_select.html('');

                let opciones  = '<option value="">Seleccione una ciudad</option>';
                $.each(ciudades, function(ind,ciudad) {
                    opciones += '<option value="'+ciudad+'">'+ciudad+'</option>';
                });

                ciudad_select.html(opciones);

                ciudad_select.parents('.form-group').removeClass('d-none');
                ciudad_input.parents('.form-group').addClass('d-none');
            } else {
                ciudad_select.parents('.form-group').addClass('d-none');
                ciudad_input.parents('.form-group').removeClass('d-none');
            }
        });

        $('.boton_cabecera').on('click', function(event) {
            $('.boton_cabecera').removeClass('active');
            $('.collapse_Cabeceras').removeClass('show');

            let elemento = $(this);
            let div_id = elemento.attr('data-target');

            $(div_id).collapse('show');
            elemento.addClass('active');

            $('#div_informacion_general').change();
            $('#div_estudios').change();
            $('#div_logros').change();
        });

        $('#nacionalidad').on('change', function(event) {
            $('#div_dni_nal').hide();
            $('#div_extranjero').hide();
            $('#municipio_pais_city_nacimiento').hide();

            let nacionalidad = $(this).val();

            if (nacionalidad == 'COL') {
                $('#municipio_pais_city_nacimiento').show();
                $('#div_extranjero').hide();
                $('#div_dni_nal').show();
            } else if (nacionalidad == 'EXT') {
                $('#municipio_pais_city_nacimiento').hide();
                $('#div_extranjero').show();
                $('#div_dni_nal').hide();
            } else if (nacionalidad == 'DOB') {
                $('#municipio_pais_city_nacimiento').show();
                $('#div_extranjero').show();
                $('#div_dni_nal').show();
            }
        });

        $('#genero').on('change', function(event) {
            $('#div_libreta').hide();
            let genero = $(this).val();

            if (genero == 'M') {
                $('#div_libreta').show();
            } else if (genero == 'F') {
                $('#div_libreta').hide();
            }
        });

        $("#inp_imagen").change(function() {
            readURL(this);
        });
    });

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imagen').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

/* Validar estado pestana */
function validarEstadoPestana(elemento) {
    elemento = $(elemento)

    var vacios = 0;

    var div_infgen_input = elemento.find('input[type="text"]');
    var div_infgen_select = elemento.find('select');
    var div_infgen_textarea = elemento.find('textarea');

    var total = div_infgen_input.length + div_infgen_select.length;

    div_infgen_input.each(function(esto,lotro) {
        if($(lotro).is(':visible') && $(lotro).val() == "") {
            vacios += (total / 10);
        }
    });

    div_infgen_select.each(function(esto,lotro) {
        if($(lotro).is(':visible') && $(lotro).val() == "") {
            vacios += (total / 10);
        }
    });

    div_infgen_textarea.each(function(esto,lotro) {
        if($(lotro).is(':visible') && $(lotro).val() == "") {
            vacios += (total / 10);
        }
    });

    total = (total / 10);

    if ((vacios >= (total * 5) && vacios <= (total * 10) ) || vacios > (total * 10) ) {
        elemento.find('.estado_general').find('i').removeClass('text-success');
        elemento.find('.estado_general').find('i').removeClass('text-warning');
        elemento.find('.estado_general').find('i').removeClass('text-danger');

        elemento.find('.estado_general').find('i').addClass('text-danger');
        elemento.find('.estado_general').attr('title', 'Incompleto.');
    } else if (vacios >= (total * 1) && vacios <= (total * 4)) {
        elemento.find('.estado_general').find('i').removeClass('text-success');
        elemento.find('.estado_general').find('i').removeClass('text-warning');
        elemento.find('.estado_general').find('i').removeClass('text-danger');

        elemento.find('.estado_general').find('i').addClass('text-warning');
        elemento.find('.estado_general').attr('title', 'A medias.');
    } else {
        elemento.find('.estado_general').find('i').removeClass('text-success');
        elemento.find('.estado_general').find('i').removeClass('text-warning');
        elemento.find('.estado_general').find('i').removeClass('text-danger');

        elemento.find('.estado_general').find('i').addClass('text-success');
        elemento.find('.estado_general').attr('title', 'Completo.');
    }

    contador_estudios = $('.estudios_superiores').find('.estudio').length;
    contador_otros_estudios = $('.otros_estudios').find('.otro_estudio').length;
    contador_logros = $('.logros').find('.logro').length;
    contador_idiomas = $('.idiomas').find('.idioma').length;
    contador_experiencias = $('.experiencias_laborales').find('.experiencia_laboral').length;

    $('#contador_estudios').val(contador_estudios);
    $('#contador_otros_estudios').val(contador_otros_estudios);
    $('#contador_logros').val(contador_logros);
    $('#contador_idiomas').val(contador_idiomas);
    $('#contador_experiencias').val(contador_experiencias);
}

/* Experiencia laboral */
function agregarExperiencia() {

    cantidad_experiencia++;

    nuevaExperiencia  = '<div class="experiencia_laboral container-fluid mt-3 border border-primary rounded p-md-3">';
    nuevaExperiencia += '    <div class="row d-flex justify-content-end">';
    nuevaExperiencia += '        <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerExperienciaLaboral(this);"><i class="mdi mdi-close"></i></button>';
    nuevaExperiencia += '    </div>';
    nuevaExperiencia += '    <div class="row">';
    nuevaExperiencia += '         <div class="col-md-3">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_entidad_'+cantidad_experiencia+'">Empresa o entidad</label>';
    nuevaExperiencia += '                <textarea class="form-control" id="explab_entidad_'+cantidad_experiencia+'" name="explab_entidad[]" placeholder="Empresa o entidad"></textarea>';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-3">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_dedicacion_'+cantidad_experiencia+'">Tipo de dedicación</label>';
    nuevaExperiencia += '                <select class="form-control" id="explab_dedicacion_'+cantidad_experiencia+'" name="explab_dedicacion[]">';
    nuevaExperiencia += '                    <option value="">Seleccione dedicación</option>';
    nuevaExperiencia += '                    <option value="tc">Tiempo completo</option>';
    nuevaExperiencia += '                    <option value="mt">Medio tiempo</option>';
    nuevaExperiencia += '                    <option value="tp">Tiempo parcial</option>';
    nuevaExperiencia += '                    <option value="otra">Otra dedicación</option>';
    nuevaExperiencia += '                </select>';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-3 d-none" id="div_dedicacion_'+cantidad_experiencia+'">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_od_'+cantidad_experiencia+'">Dedicación</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_od_'+cantidad_experiencia+'" name="explab_od[]" placeholder="Otra dedicación">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-3">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_tipo_'+cantidad_experiencia+'">Tipo de empresa</label>';
    nuevaExperiencia += '                <select class="form-control" id="explab_tipo_'+cantidad_experiencia+'" name="explab_tipo[]">';
    nuevaExperiencia += '                    <option value="">Seleccione un tipo</option>';
    nuevaExperiencia += '                    <option value="Publica">Publica</option>';
    nuevaExperiencia += '                    <option value="Privada">Privada</option>';
    nuevaExperiencia += '                </select>';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '     </div>';
    nuevaExperiencia += '     <div class="row">';
    nuevaExperiencia += '         <div class="col-md-6">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_cargo_'+cantidad_experiencia+'">Cargo o contrato actual</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_cargo_'+cantidad_experiencia+'" name="explab_cargo[]" placeholder="Cargo o contrato actual">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-6">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_dependencia_'+cantidad_experiencia+'">Dependencia</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_dependencia_'+cantidad_experiencia+'" name="explab_dependencia[]" placeholder="Cargo o contrato actual">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '     </div>';
    nuevaExperiencia += '     <div class="row">';
    nuevaExperiencia += '         <div class="col-md-2">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_pais_'+cantidad_experiencia+'">País</label>';
    nuevaExperiencia += '                <select class="form-control" id="explab_pais_'+cantidad_experiencia+'" name="explab_pais[]">';
    nuevaExperiencia += '                    <option value="">Seleccione un pais</option>';
    nuevaExperiencia += '                    @foreach ($paises as $pais)';
    nuevaExperiencia += '                    <option value="{{ $pais }}">{!! $pais !!}</option>';
    nuevaExperiencia += '                    @endforeach';
    nuevaExperiencia += '                </select>';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-4" id="div_departamento_'+cantidad_experiencia+'">';
    nuevaExperiencia += '            <div class="form-group d-none">';
    nuevaExperiencia += '                <label for="explab_departamento_'+cantidad_experiencia+'">Departamento</label>';
    nuevaExperiencia += '                <select class="form-control" id="explab_departamento_'+cantidad_experiencia+'"  name="explab_departamento[]">';
    nuevaExperiencia += '                    <option value="">Seleccione un departamento</option>';
    nuevaExperiencia += '                    @foreach (json_decode($depto_ciudades,1) as $departamento => $ciudades)';
    nuevaExperiencia += '                    <option value="{{ $departamento }}">{!! $departamento !!}</option>';
    nuevaExperiencia += '                    @endforeach';
    nuevaExperiencia += '                </select>';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_inpdepartamento_'+cantidad_experiencia+'">Estado / Provincia / Departamento</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_inpdepartamento_'+cantidad_experiencia+'" name="explab_inpdepartamento[]">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-3" id="div_ciudad_'+cantidad_experiencia+'">';
    nuevaExperiencia += '            <div class="form-group d-none">';
    nuevaExperiencia += '                <label for="explab_ciudad_'+cantidad_experiencia+'">Ciudad</label>';
    nuevaExperiencia += '                <select class="form-control" id="explab_ciudad_'+cantidad_experiencia+'" name="explab_ciudad[]">';
    nuevaExperiencia += '                </select>';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_inpciudad_'+cantidad_experiencia+'">Ciudad</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_inpciudad_'+cantidad_experiencia+'" name="explab_inpciudad[]">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-3">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_direccion_'+cantidad_experiencia+'">Dirección</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_direccion_'+cantidad_experiencia+'" name="explab_direccion[]" placeholder="Dirección">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '     </div>';
    nuevaExperiencia += '    <div class="row">';
    nuevaExperiencia += '         <div class="col-md-2">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_telefonos_'+cantidad_experiencia+'">Telefonos</label>';
    nuevaExperiencia += '                <input type="text" class="form-control" id="explab_telefonos_'+cantidad_experiencia+'" name="explab_telefonos[]" placeholder="Telefonos">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-4">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_fingreso_'+cantidad_experiencia+'">Fecha de ingreso</label>';
    nuevaExperiencia += '                <input type="date" class="form-control" id="explab_fingreso_'+cantidad_experiencia+'" name="explab_fingreso[]" placeholder="Fecha de ingreso">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '         <div class="col-md-3">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_fterminacion_'+cantidad_experiencia+'">Fecha de terminación</label>';
    nuevaExperiencia += '                <input type="date" class="form-control" id="explab_fterminacion_'+cantidad_experiencia+'" name="explab_fterminacion[]" placeholder="Fecha de terminación">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '          <div class="col-md-3">';
    nuevaExperiencia += '            <div class="form-group">';
    nuevaExperiencia += '                <label for="explab_certificado_'+cantidad_experiencia+'">Certificado laboral</label>';
    nuevaExperiencia += '                <input class="form-control" id="explab_certificado_'+cantidad_experiencia+'" name="explab_certificado[]" type="file" placeholder="Certificado">';
    nuevaExperiencia += '            </div>';
    nuevaExperiencia += '        </div>';
    nuevaExperiencia += '    </div>';
    nuevaExperiencia += '</div>';

    $('.experiencias_laborales').append(nuevaExperiencia);
}

function removerExperienciaLaboral(elemento) {
    elemento = $(elemento);
    elemento.parents('div.experiencia_laboral').remove();
}
/* Experiencia laboral */

/* Idiomas */
function agregarIdioma(elemento) {
    cantidad_idiomas++;

    nuevo_idioma  = '<div class="idioma container-fluid mt-3 border border-primary rounded p-3">';
    nuevo_idioma += '    <div class="row d-flex justify-content-end">';
    nuevo_idioma += '        <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerIdioma(this);"><i class="mdi mdi-close"></i></button>';
    nuevo_idioma += '    </div>';
    nuevo_idioma += '    <div class="row">';
    nuevo_idioma += '         <div class="col-md-2">';
    nuevo_idioma += '            <div class="form-group">';
    nuevo_idioma += '                <label for="idioma_'+cantidad_idiomas+'">Idioma</label>';
    nuevo_idioma += '                <input type="text" class="form-control" id="idioma_'+cantidad_idiomas+'" name="idioma[]" placeholder="Idioma">';
    nuevo_idioma += '            </div>';
    nuevo_idioma += '        </div>';
    nuevo_idioma += '         <div class="col-md-2">';
    nuevo_idioma += '            <div class="form-group">';
    nuevo_idioma += '                <label for="habla_'+cantidad_idiomas+'">Habla</label>';
    nuevo_idioma += '                <select class="form-control" name="habla[]" id="habla_'+cantidad_idiomas+'">';
    nuevo_idioma += '                    <option value="">Seleccione</option>';
    nuevo_idioma += '                    <option value="R">Regular</option>';
    nuevo_idioma += '                    <option value="B">Bien</option>';
    nuevo_idioma += '                    <option value="MB">Muy bien</option>';
    nuevo_idioma += '                </select>';
    nuevo_idioma += '            </div>';
    nuevo_idioma += '        </div>';
    nuevo_idioma += '         <div class="col-md-2">';
    nuevo_idioma += '            <div class="form-group">';
    nuevo_idioma += '                <label for="lee_'+cantidad_idiomas+'">Lee</label>';
    nuevo_idioma += '                <select class="form-control" name="lee[]" id="lee_'+cantidad_idiomas+'">';
    nuevo_idioma += '                    <option value="">Seleccione</option>';
    nuevo_idioma += '                    <option value="R">Regular</option>';
    nuevo_idioma += '                    <option value="B">Bien</option>';
    nuevo_idioma += '                    <option value="MB">Muy bien</option>';
    nuevo_idioma += '                </select>';
    nuevo_idioma += '            </div>';
    nuevo_idioma += '        </div>';
    nuevo_idioma += '         <div class="col-md-2">';
    nuevo_idioma += '            <div class="form-group">';
    nuevo_idioma += '                <label for="escribe_'+cantidad_idiomas+'">Escribe</label>';
    nuevo_idioma += '                <select class="form-control" name="escribe[]" id="escribe_'+cantidad_idiomas+'">';
    nuevo_idioma += '                    <option value="">Seleccione</option>';
    nuevo_idioma += '                    <option value="R">Regular</option>';
    nuevo_idioma += '                    <option value="B">Bien</option>';
    nuevo_idioma += '                    <option value="MB">Muy bien</option>';
    nuevo_idioma += '                </select>';
    nuevo_idioma += '            </div>';
    nuevo_idioma += '        </div>';
    nuevo_idioma += '         <div class="col-md-2">';
    nuevo_idioma += '            <div class="form-group">';
    nuevo_idioma += '                <label for="observacion_'+cantidad_idiomas+'">Observación</label>';
    nuevo_idioma += '                <textarea class="form-control" id="observacion_'+cantidad_idiomas+'" name="observacion[]" placeholder="Observaciones"></textarea>';
    nuevo_idioma += '            </div>';
    nuevo_idioma += '        </div>';
    nuevo_idioma += '         <div class="col-2">';
    nuevo_idioma += '            <div class="form-group">';
    nuevo_idioma += '                <label for="certificado_idioma_'+cantidad_idiomas+'">Certificado</label>';
    nuevo_idioma += '                <input class="form-control" id="certificado_idioma_'+cantidad_idiomas+'" name="certificado_idioma[]" type="file" placeholder="Certificado">';
    nuevo_idioma += '            </div>';
    nuevo_idioma += '        </div>';
    nuevo_idioma += '     </div>';
    nuevo_idioma += '</div>';

    $('.idiomas').append(nuevo_idioma);
    $('#idioma_'+cantidad_idiomas).parents('div.idioma').find('input.form-control').first().focus();
}

function removerIdioma(elemento) {
    elemento = $(elemento);
    elemento.parents('div.idioma').remove();
}
/* Idiomas */

/* Logros */
function agregarLogro(elemento) {
    cantidad_logros++;

    nuevo_logro  = '<div class="logro container-fluid mt-3 border border-primary rounded p-3">';
    nuevo_logro += '    <div class="row d-flex justify-content-end">';
    nuevo_logro += '        <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerLogro(this);"><i class="mdi mdi-close"></i></button>';
    nuevo_logro += '    </div>';
    nuevo_logro += '    <div class="row">';
    nuevo_logro += '         <div class="col-md-7">';
    nuevo_logro += '            <div class="form-group">';
    nuevo_logro += '                <label for="logro_'+cantidad_logros+'">Logro</label>';
    nuevo_logro += '                <textarea class="form-control" id="logro_'+cantidad_logros+'" name="logro[]" placeholder="Publicación, investigación y/o logro laboral"></textarea>';
    nuevo_logro += '            </div>';
    nuevo_logro += '        </div>';
    nuevo_logro += '         <div class="col-5">';
    nuevo_logro += '            <div class="form-group">';
    nuevo_logro += '                <label for="certificado_logro_'+cantidad_logros+'">Certificado</label>';
    nuevo_logro += '                <input class="form-control" id="certificado_logro_'+cantidad_logros+'" name="certificado_logro[]" type="file" placeholder="Certificado">';
    nuevo_logro += '            </div>';
    nuevo_logro += '        </div>';
    nuevo_logro += '     </div>';
    nuevo_logro += '</div>';

    $('.logros').append(nuevo_logro);
    $('#logro_'+cantidad_otros_estudios).parents('div.otro_estudio').find('input.form-control').first().focus();
}

function removerLogro(elemento) {
    elemento = $(elemento);
    elemento.parents('div.logro').remove();
}
/* Logros */

/* Otros Estudios */
function agregarOtroEstudio() {

    cantidad_otros_estudios++;

    nuevo_otro_estudio  = '<div class="otro_estudio container-fluid mt-3 border border-primary rounded p-3">';
    nuevo_otro_estudio += '    <div class="row d-flex justify-content-end">';
    nuevo_otro_estudio += '        <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerOtrosEstudios(this);"><i class="mdi mdi-close"></i></button>';
    nuevo_otro_estudio += '    </div>';
    nuevo_otro_estudio += '    <div class="row">';
    nuevo_otro_estudio += '         <div class="col-md-3">';
    nuevo_otro_estudio += '            <div class="form-group">';
    nuevo_otro_estudio += '                <label for="titulo_otro_estudio_'+cantidad_otros_estudios+'">Nombre del estudio</label>';
    nuevo_otro_estudio += '                <input type="text" class="form-control" id="titulo_otro_estudio_'+cantidad_otros_estudios+'" name="titulo_otro_estudio[]" placeholder="Nombre del estudio">';
    nuevo_otro_estudio += '            </div>';
    nuevo_otro_estudio += '        </div>';
    nuevo_otro_estudio += '         <div class="col-md-3">';
    nuevo_otro_estudio += '            <div class="form-group">';
    nuevo_otro_estudio += '                <label for="otros_estudios_escuela_'+cantidad_otros_estudios+'">Nombre de la Institución</label>';
    nuevo_otro_estudio += '                <input type="text" class="form-control" id="otros_estudios_escuela_'+cantidad_otros_estudios+'" name="otros_estudios_escuela[]" placeholder="Nombre de la Institución">';
    nuevo_otro_estudio += '            </div>';
    nuevo_otro_estudio += '        </div>';
    nuevo_otro_estudio += '         <div class="col-md-1">';
    nuevo_otro_estudio += '            <div class="form-group">';
    nuevo_otro_estudio += '                <label for="otros_estudios_horas_'+cantidad_otros_estudios+'">Horas</label>';
    nuevo_otro_estudio += '                <input type="text" class="form-control" id="otros_estudios_horas_'+cantidad_otros_estudios+'" name="otros_estudios_horas[]" placeholder="Horas">';
    nuevo_otro_estudio += '            </div>';
    nuevo_otro_estudio += '        </div>';
    nuevo_otro_estudio += '         <div class="col-md-1">';
    nuevo_otro_estudio += '            <div class="form-group">';
    nuevo_otro_estudio += '                <label for="otros_estudios_ano_'+cantidad_otros_estudios+'">Año</label>';
    nuevo_otro_estudio += '                <input type="text" class="form-control" id="otros_estudios_ano_'+cantidad_otros_estudios+'" name="otros_estudios_ano[]" placeholder="Año">';
    nuevo_otro_estudio += '            </div>';
    nuevo_otro_estudio += '        </div>';
    nuevo_otro_estudio += '         <div class="col-md-4">';
    nuevo_otro_estudio += '            <div class="form-group">';
    nuevo_otro_estudio += '                <label for="certificado_otros_est_'+cantidad_otros_estudios+'">Certificado</label>';
    nuevo_otro_estudio += '                <input class="form-control" id="certificado_otros_est_'+cantidad_otros_estudios+'" name="certificado_otros_est[]" type="file" placeholder="Certificado">';
    nuevo_otro_estudio += '            </div>';
    nuevo_otro_estudio += '        </div>';
    nuevo_otro_estudio += '     </div>';
    nuevo_otro_estudio += '</div>';

    $('.otros_estudios').append(nuevo_otro_estudio);
    $('#titulo_otro_estudio_'+cantidad_otros_estudios).parents('div.otro_estudio').find('input.form-control').first().focus();
}

function removerOtrosEstudios(elemento) {
    elemento = $(elemento);
    elemento.parents('div.otro_estudio').remove();
}
/* Otros Estudios */

/* Estudios */
function agregarEstudio() {

    cantidad_estudios++;

    nuevo_estudio  = '<div class="estudio container-fluid mt-3 border border-primary rounded p-3">';
    nuevo_estudio += '    <div class="row d-flex justify-content-end">';
    nuevo_estudio += '        <button type="button" class="btn btn-outline-danger btn-xs" onclick="removerEstudios(this);"><i class="mdi mdi-close"></i></button>';
    nuevo_estudio += '    </div>';
    nuevo_estudio += '    <div class="row">';
    nuevo_estudio += '        <div class="col-md-2">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="modalidad_ac_'+cantidad_estudios+'">Modalidad Academica</label>';
    nuevo_estudio += '                <select class="form-control" id="modalidad_ac_'+cantidad_estudios+'" name="modalidad_ac[]">';
    nuevo_estudio += '                    <option value="">Seleccione modalidad</option>';
    nuevo_estudio += '                    <option value="TC">Técnica</option>';
    nuevo_estudio += '                    <option value="TL">Tecnológica</option>';
    nuevo_estudio += '                    <option value="TE">Tecnológica Especializada</option>';
    nuevo_estudio += '                    <option value="UN">Universitaria</option>';
    nuevo_estudio += '                    <option value="ES">Especialización</option>';
    nuevo_estudio += '                    <option value="MG">Maestria o Magister</option>';
    nuevo_estudio += '                    <option value="DC">Doctorado o PHD</option>';
    nuevo_estudio += '                </select>';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '        <div class="col-md">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="titulo_estudio_'+cantidad_estudios+'">Nombre de los estudios / Titulo obtenido</label>';
    nuevo_estudio += '                <input type="text" class="form-control" id="titulo_estudio_'+cantidad_estudios+'" name="titulo_estudio[]" placeholder="Nombre de los estudios / Titulo obtenido">';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '        <div class="col-md">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="est_superiores_escuela_'+cantidad_estudios+'">Nombre de la Institución educativa</label>';
    nuevo_estudio += '                <input type="text" class="form-control" id="est_superiores_escuela_'+cantidad_estudios+'" name="est_superiores_escuela[]" placeholder="Nombre de los estudios / Titulo obtenido">';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '        <div class="col-md-2">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="graduado_'+cantidad_estudios+'">Finalizado</label>';
    nuevo_estudio += '                <select class="form-control" id="graduado_'+cantidad_estudios+'" name="graduado[]">';
    nuevo_estudio += '                    <option value="">Seleccione</option>';
    nuevo_estudio += '                    <option value="si">Si</option>';
    nuevo_estudio += '                    <option value="no">No</option>';
    nuevo_estudio += '                </select>';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '    </div>';
    nuevo_estudio += '    <div class="row">';
    nuevo_estudio += '        <div class="col-md">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="semestres_'+cantidad_estudios+'">Semestres aprobados</label>';
    nuevo_estudio += '                <input type="text" class="form-control" id="semestres_'+cantidad_estudios+'" name="semestres[]" placeholder="Semestres aprobados">';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '        <div class="col-md">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="fecha_fin_estudio_'+cantidad_estudios+'">Fecha de terminacion</label>';
    nuevo_estudio += '                <input type="month" class="form-control" id="fecha_fin_estudio_'+cantidad_estudios+'" name="fecha_fin_estudio[]" placeholder="Fecha de terminación">';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '        <div class="col-md">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="tarjeta_profesional_'+cantidad_estudios+'">Numero de tarjeta profesional</label>';
    nuevo_estudio += '                <input type="text" class="form-control" id="tarjeta_profesional_'+cantidad_estudios+'" name="tarjeta_profesional[]" placeholder="Numero de tarjeta profesional">';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '        <div class="col-md">';
    nuevo_estudio += '            <div class="form-group">';
    nuevo_estudio += '                <label for="certificado_'+cantidad_estudios+'">Certificado</label>';
    nuevo_estudio += '                <input class="form-control" id="certificado_'+cantidad_estudios+'" name="certificado[]" type="file" placeholder="Certificado">';
    nuevo_estudio += '            </div>';
    nuevo_estudio += '        </div>';
    nuevo_estudio += '    </div>';
    nuevo_estudio += '</div>';


    $(".estudios_superiores").append(nuevo_estudio);
    $('#modalidad_ac_'+cantidad_estudios).parents('div.estudio').find('input.form-control').first().focus();
}

function removerEstudios(elemento) {
    elemento = $(elemento);
    elemento.parents('div.estudio').remove();
}
/* Estudios */
</script>
@endsection
