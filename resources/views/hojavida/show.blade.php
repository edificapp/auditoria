@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Hoja de vida</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="window.open('{{ route('exportarhv',$usuario->id) }}','_blank');" class="btn btn-primary mt-2 mt-xl-0"> Exportar hoja de vida <i class="mdi mdi-export"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="row my-1 ml-0 justify-content-md-center">
    <nav class="nav nav-pills">
        <button class="nav-link boton_cabecera nav-item border active" type="button" data-target="#div_informacion_general">Información General</button>
        <button class="nav-link boton_cabecera border " type="button" data-target="#div_estudios">Estudios</button>
        <button class="nav-link boton_cabecera border " type="button" data-target="#div_logros">Publicaciones,investigaciones,logros e idiomas</button>
        <button class="nav-link boton_cabecera border " type="button" data-target="#div_experiencia">Experiencia laboral</button>
    </nav>
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <form>
            <div class="card collapse show collapse_Cabeceras" id="div_informacion_general">
                <div class="card-body borde">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-md">
                                <div class="col-xl d-flex justify-content-center align-items-center">
                                    <img class="img-fluid img-thumbnail rounded" style="max-height: 250px !important;" src="{{ $imagen != '' ? \Storage::url($imagen) : asset('admin/images/usuario/usuario.jpg') }}">
                                </div>
                            </div>

                            <div class="col-md">

                                @if ($nacionalidad == 'COL' || $nacionalidad == 'DOB')
                                <label>
                                    <strong>Tipo de Documento:</strong> 
                                    @if ($dni_nal_tipo == 'CC')
                                    Cedula de ciudadania
                                    @elseif ($dni_nal_tipo == 'NIT')
                                    Nit
                                    @elseif ($dni_nal_tipo == 'OTRO')
                                    Otro
                                    @endif
                                </label>
                                <br>

                                <label><strong># Documento:</strong> {!! $dni_nal !!}</label>
                                @endif

                                @if ($nacionalidad == 'EXT' || $nacionalidad == 'DOB')
                                <label>
                                    <strong>Tipo de Documento Extranjero:</strong> 
                                    @if ($dni_ext_tipo == 'CE')
                                    Cedula de extranjeria
                                    @elseif ($dni_ext_tipo == 'NIT')
                                    Nit
                                    @elseif ($dni_ext_tipo == 'PASAPORTE')
                                    Pasaporte
                                    @endif
                                </label>

                                <br>

                                <label><strong># Documento Extranjero:</strong> {!! $dni_ext !!}</label>
                                @endif

                                <hr>

                                <label>
                                    <strong>Nombre(s):</strong> {{ $nombres }}
                                </label>

                                <br>

                                <label>
                                    <strong>Apellidos:</strong> {{ $primer_apellido }} {{ $segundo_apellido }}
                                </label>

                                <br>

                                <label>
                                    <strong>Genero: </strong>{{ $genero == 'M' ? 'Masculino' : 'Femenino' }}
                                </label>

                                <br>

                                <label>
                                    <strong>Telefonos de contacto:</strong> {{ $telefonos }}
                                </label>

                                <br>

                                <hr>

                                <label>
                                    <strong>Nacionalidad:</strong> 
                                    @if ($nacionalidad == 'COL')
                                    Colombiano
                                    @elseif ($nacionalidad == 'EXT')
                                    Extranjero
                                    @elseif ($nacionalidad == 'DOB')
                                    Doble nacionalidad
                                    @endif
                                </label>

                                <hr>

                                @if ($nacionalidad == 'COL' || $nacionalidad == 'DOB')
                                <br>

                                <label>
                                    <strong>Municipio de nacimiento:</strong> {{ $mun_nacimiento }}
                                </label>

                                <br>

                                <label>
                                    <strong>Departamento de nacimiento:</strong> {{ $dep_nacimiento }}
                                </label>

                                <br>
                                
                                <label>
                                    <strong>Pais de nacimiento:</strong> {!! $pais_nacimiento !!}
                                </label>
                                <br>
                                <label>
                                    <strong>Fecha de nacimiento:</strong> {!! $fecha_nacimiento !!}
                                </label>
                                <br>
                                <label>
                                    <strong>Edad:</strong> {!! $edad !!}
                                </label>
                                @endif

                                @if ($nacionalidad == 'EXT' || $nacionalidad == 'DOB')
                                <br>

                                <label>
                                    <strong>Pais extranjero:</strong> {!! $pais_extranjero !!}
                                </label>
                                @endif

                            </div>
                        </div>

                        @if ($genero == 'M')
                        <hr>
                        <div class="row">


                            <div class="col-md">
                                <label>
                                    <strong>Tipo de libreta:</strong> {!! $libreta !!}
                                </label>
                            </div>



                            <div class="col-md">
                                <div class="form-group">
                                    <label><strong>Número de libreta:</strong> {!! $numero_libreta !!}</label>
                                </div>
                            </div>


                            <div class="col-md">
                                <label><strong>Distrito militar:</strong> {!! $dm !!}</label>
                            </div>

                        </div>
                        @endif

                    </div>
                </div>
            </div>

            <div class="card collapse collapse_Cabeceras" id="div_estudios">
                <div class="card-body borde">
                    <div class="container-fluid">

                        <div class="mt-2 borde rounded  p-4">

                            <div class="row">
                                <div class="col-md">
                                    <h3>Educación básica y media</h3>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md">
                                    <label>
                                        <strong>Ultimo grado:</strong> 
                                        @if($grado == '1')
                                        1° (Primaria)
                                        @elseif($grado == '2')
                                        2° (Primaria)
                                        @elseif($grado == '3')
                                        3° (Primaria)
                                        @elseif($grado == '4')
                                        4° (Primaria)
                                        @elseif($grado == '5')
                                        5° (Primaria)
                                        @elseif($grado == '6')
                                        6° (Secundaria)
                                        @elseif($grado == '7')
                                        7° (Secundaria)
                                        @elseif($grado == '8')
                                        8° (Secundaria)
                                        @elseif($grado == '9')
                                        9° (Secundaria)
                                        @elseif($grado == '10')
                                        10° (Media)
                                        @elseif($grado == '11')
                                        11° (Media)
                                        @endif
                                    </label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Titulo obtenido:</strong> {{ $titulo }}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Fecha de terminacion:</strong> {{ $fecha_fin }}</label>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md">
                                    <label><strong>Institución:</strong> {{ $escuela }}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Municipio de Institución:</strong> {{ $ciudad_escuela }}</label>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md">
                                    {{-- <label><strong>Ver</strong> <a target="_blank" href="{{ url($certificado_basico) }}">Certificado</a></label> --}}
                                </div>
                            </div>
                        </div>


                        <div class="mt-2 borde rounded p-4">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Educación superior (Pregrado y postgrado)</h3>
                                </div>
                            </div>

                            <hr>

                            @if (count($estudios_superiores) == 0)
                            <div class="row">
                                <div class="col-md">
                                    <center>No cuenta con estudios superiores.</center>
                                </div>
                            </div>
                            @endif

                            @foreach($estudios_superiores as $index => $estudio_superior)

                            <div class="row">
                                <div class="col-md">
                                    <label><strong># tarjeta profesional:</strong> {!! $estudio_superior->tarjeta !!}</label>
                                </div>

                                <div class="col-2">
                                    <label><strong>Finalizado:</strong> {!! $estudio_superior->finalizado !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Fecha graduación:</strong> {!! $estudio_superior->fecha_fin !!}</label>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md">
                                    <label><strong>Titulo obtenido:</strong> {!! $estudio_superior->profesion->nombre !!}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md">
                                    <label><strong>Institución educativa:</strong> {!! $estudio_superior->escuela !!}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md">
                                    <label>
                                        <strong>Modalidad Academica:</strong> {!! $estudio_superior->profesion->tipo->nombre!!}
                                    </label>
                                </div>

                                <div class="col-md">
                                    <label><strong># Semestres:</strong> {!! $estudio_superior->semestres !!}</label>
                                </div>

                                @if ($estudio_superior->certificado)
                                <div class="col-md">
                                    <label><strong>Ver </strong><a target="_blank" href="{!! url($estudio_superior->certificado) !!}">Certificado</a></label>
                                </div>
                                @endif
                                
                            </div>

                            <hr>
                            @endforeach
                        </div>

                        <div class="mt-2 borde rounded p-4">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Otros estudios</h3>
                                </div>
                            </div>

                            <hr>

                            @if (count($otros_estudios) == 0)
                            <div class="row">
                                <div class="col-md">
                                    <center>No cuenta con otros estudios.</center>
                                </div>
                            </div>
                            @endif

                            @foreach($otros_estudios as $index => $otro_estudio)

                            <div class="row">

                                <div class="col-md">
                                    <label><strong>Nombre del estudio:</strong> {!! $otro_estudio->estudio !!}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md">
                                    <label><strong>Nombre del establecimiento:</strong> {!! $otro_estudio->escuela !!}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md">
                                    <label><strong>Horas:</strong> {!! $otro_estudio->horas !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Año:</strong> {!! $otro_estudio->ano !!}</label>
                                </div>

                                @if ($otro_estudio->certificado)
                                <div class="col-md">
                                    <label><strong>Ver</strong> <a target="_blank" href="{!! url($otro_estudio->certificado) !!}">Certificado</a></label>
                                </div>
                                @endif

                            </div>

                            <hr>

                            @endforeach
                        </div>

                    </div>

                </div>
            </div>

            <div class="card collapse collapse_Cabeceras" id="div_logros">
                <div class="card-body borde">
                    <div class="container-fluid">

                        <div class="mt-2 borde rounde p-4">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Publicaciones, investigaciones y/o logros laborales</h3>
                                </div>
                            </div>

                            <hr>

                            @if (count($logros_laborales) == 0)
                            <div class="row">
                                <div class="col-md">
                                    <center>No cuenta con publicaciones, investigaciones y/o logros laborales</center>
                                </div>
                            </div>
                            @endif

                            @foreach ($logros_laborales as $index => $logro)
                            <div class="row">

                                <div class="col-md">
                                    <label><strong>Logro:</strong> {!! $logro->logro !!}</label>
                                </div>
                            </div>

                            @if ($logro->certificado)
                            <div class="row">
                                <div class="col-3">
                                    <label><strong>Ver</strong> <a target="_blank" href="{!! url($logro->certificado) !!}">Certificado</a></label>
                                </div>
                            </div>
                            @endif

                            <hr>
                            @endforeach
                        </div>

                        <div class="mt-2 borde rounded p-4">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Idiomas</h3>
                                </div>
                            </div>

                            <hr>
                            @if (count($idiomas) == 0)
                            <div class="row">
                                <div class="col-md">
                                    <center>No cuenta con Idiomas</center>
                                </div>
                            </div>
                            @endif
                            
                            @foreach ($idiomas as $index => $idioma)
                                <div class="row">

                                    <div class="col-md">
                                        <label><strong>Idioma:</strong> {!! $idioma->idioma !!}</label>
                                    </div>

                                    @if ($idioma->certificado)
                                    <div class="col-md">
                                        <label><strong>Ver</strong> <a target="_blank" href="{!! url($idioma->certificado) !!}">Certificado</a></label>
                                    </div>
                                    @endif

                                </div>

                                <div class="row">

                                    <div class="col-md">
                                        <label>
                                            <strong>Habla: </strong>
                                            @if($idioma->habla == 'R')
                                            Regular
                                            @elseif($idioma->habla == 'B')
                                            Bien
                                            @elseif($idioma->habla == 'MB')
                                            Muy bien
                                            @endif
                                        </label>
                                    </div>

                                    <div class="col-md">
                                        <label>
                                            <strong>Lee: </strong>
                                            @if($idioma->lee == 'R')
                                            Regular
                                            @elseif($idioma->lee == 'B')
                                            Bien
                                            @elseif($idioma->lee == 'MB')
                                            Muy bien
                                            @endif
                                        </label>
                                    </div>

                                    <div class="col-md">
                                        <label>
                                            <strong>Escribe: </strong>
                                            @if($idioma->escribe == 'R')
                                            Regular
                                            @elseif($idioma->escribe == 'B')
                                            Bien
                                            @elseif($idioma->escribe == 'MB')
                                            Muy bien
                                            @endif
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md">
                                        <label><strong>Observación:</strong> {!! $idioma->observacion !!}</label>
                                    </div>
                                </div>
                                <hr>
                            @endforeach

                           
                        </div>
                    </div>
                </div>
            </div>

            <div class="card collapse collapse_Cabeceras" id="div_experiencia">
                <div class="card-body borde">
                    <div class="container-fluid">

                        <div class="mt-2  rounded p-4">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Experiencia laboral</h3>
                                </div>
                            </div>

                            <hr>

                            @if (count($experiencias_laborales) == 0)
                            <div class="row">
                                <div class="col-md">
                                    <center>No cuenta con Experiencia laboral</center>
                                </div>
                            </div>
                            @endif

                            @foreach ($experiencias_laborales as $index => $experiencia)

                            <div class="row">

                                <div class="col-md">
                                    <label><strong>Fecha de ingreso:</strong> {!! $experiencia->fingreso !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Fecha terminación:</strong> {!! $experiencia->fterminacion !!}</label>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md">
                                    <label><strong>Tipo de empresa:</strong> {!! $experiencia->tipo !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Empresa:</strong> {!! $experiencia->empresa !!}</label>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md">
                                    <label><strong>Cargo:</strong> {!! $experiencia->cargo !!}</label>
                                </div>

                                @if ($experiencia->certificado)
                                <div class="col-md">
                                    <label><strong>Ver</strong> <a target="_blank" href="{!!$experiencia->certificado_file !!}">Certificado</a></label>
                                </div>
                                @endif

                            </div>

                            <div class="row">

                                <div class="col-md">
                                    <label>
                                        <strong>Tipo de dedicación: </strong>
                                        @if($experiencia->dedicacion == 'tc')
                                        Tiempo completo
                                        @elseif($experiencia->dedicacion == 'mt')
                                        Medio tiempo
                                        @elseif($experiencia->dedicacion == 'tp')
                                        Tiempo parcial
                                        @else
                                        {!! $experiencia->dedicacion !!}
                                        @endif
                                    </label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Dependencia:</strong> {!! $experiencia->dependencia !!}</label>
                                </div>

                            </div>

                            
                            <div class="row">
                                <div class="col-md">
                                    <label><strong>Telefonos: </strong>{!! $experiencia->telefonos !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Dirección:</strong> {!! $experiencia->direccion !!}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md">
                                    <label><strong>Funciones: </strong>{!! $experiencia->funciones !!}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md">
                                    <label><strong>proyecto en que se desempeño: </strong>{!! $experiencia->proyectos !!}</label>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md">
                                    <label><strong>País:</strong> {!! $experiencia->pais !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Departamento:</strong> {!! $experiencia->departamento !!}</label>
                                </div>

                                <div class="col-md">
                                    <label><strong>Ciudad:</strong> {!! $experiencia->ciudad !!}</label>
                                </div>

                            </div>

                            <hr>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>

        </form>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {

        $('.boton_cabecera').on('click', function(event) {
            $('.boton_cabecera').removeClass('active');
            $('.collapse_Cabeceras').removeClass('show');

            let elemento = $(this);
            let div_id = elemento.attr('data-target');

            $(div_id).collapse('show');
            elemento.addClass('active');

            $('#div_informacion_general').change();
            $('#div_estudios').change();
            $('#div_logros').change();
        });

    });
</script>
@endsection
