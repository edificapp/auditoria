
    <ul class="nav nav-pills bg-transparent mb-1 col-12">
        <li class="nav-item">
            <a href="{{route('curriculum.index')}}" class="nav-link {{$select != 'informacion_general' ?:'active'}}">
                Información General
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('estudios.index')}}" class="nav-link {{$select != 'educacion_superior' ?:'active'}}">
                Educación Superior
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('otros-estudios.index') }}" class="nav-link {{$select != 'otros_estudios' ?:'active'}}">
                Otros Estudios
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('logros.index') }}" class="nav-link {{$select != 'logros' ?:'active'}}">
                Logros
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('idiomas.index') }}" class="nav-link {{$select != 'idiomas' ?:'active'}}">
                Idiomas
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('laboral.index') }}" class="nav-link {{$select != 'experiencia_laboral' ?:'active'}}">
                Experiencia Laboral
            </a>
        </li>     
    </ul>