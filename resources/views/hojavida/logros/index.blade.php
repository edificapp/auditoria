@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{-- <style type="text/css">
        tr{
           font-family: Roboto, sans-serif;
            font-size: 0.9375rem;
            line-height: 1.8;
            padding: 0.4rem 0px 0.4rem 0px;
            display: flex !important;
            list-style: outside none none;
            box-sizing: border-box;
            word-wrap: break-word;
            justify-content: flex-start !important;
            align-items: center !important;
        }
        .tr-border{
            border-bottom: 1px solid #80808061;
        }

    </style> --}}
@stop

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item active" aria-current="page" ><b>Logros</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div >
    @include('hojavida.components.tabs', ['select' => 'logros'])    
</div>
<div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body borde p-2">
                <a href="{{route('logros.create')}}" class="btn-create-form float-left">
                    Agregar Logro
                </a>
                <div class="text-center pt-2">
                    <p class="h4">Publicaciones, investigaciones y/o logros laborales</p>
                </div>
            </div>
        </div>

        <ul class="list-group mt-2">
            @foreach($curriculum->logros as $item)
                <li class="list-group-item">
                    @if($item->certificado_file != NULL)
                        <a href="{{$item->certificado_file}}" target="_blank" class="btn-pdf-form" title="Certificado">
                        </a>
                    @else
                        <button class="btn btn-inverse-secondary btn-sm btn-rounded" onclick="certificadoNull()" title="No tiene Certificado" >
                            <i class="mdi mdi-file-pdf-box"></i>
                        </button>
                    @endif
                    <a href="{{route('logros.edit', $item->id)}}" class="btn-edit-form" title="Editar"></a>
                    <button type="submit" class="btn-delete-form" onclick="destroy({{$item->id}})" title="Borrar"></button>
                    <b class="ml-3">{{$item->logro}}</b>
                    <form action="{{route('logros.destroy', $item->id)}}" id="destroy_{{$item->id}}" method="post">
                        @csrf
                        {!! method_field('delete') !!}
                    </form>
                </li>
            @endforeach
        </ul>
    </div>
</div>

@endsection


@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    function certificadoNull(){
         Swal.fire({
              title: 'Notificación',
              text: "No tiene Certificado",
              type: "danger",
              confirmButtonText: "Cerrar"
          })
    }

    function destroy(id){
        $('#destroy_'+id).submit();
    }
</script>
@endsection
