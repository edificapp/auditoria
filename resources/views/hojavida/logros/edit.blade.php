@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item" ><a href="{{ route('logros.index') }}">Logros</a></li>
                            <li class="breadcrumb-item active" aria-current="page" ><b>Editar Logro</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div >
    @include('hojavida.components.tabs', ['select' => 'logros']) 
</div>
<div class="row ">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body borde">
                <div class=" text-center mb-4">
                    <p class="h4">Publicaciones, investigaciones y/o logros laborales</p>
               </div>
                <div class="estudios_superiores">
                    <form enctype="multipart/form-data" action="{{route('logros.update', $logro->id)}}" method="post">
                        @csrf
                        {{method_field('put')}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="logro_0">Logro</label>
                                        <textarea class="form-control" name="logro" rows="4" placeholder="Publicación, investigación y/o logro laboral">{{$logro->logro}}</textarea>
                                    </div>
                                </div>
                            </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label for="certificado_0">Certificado</label>
                                    <input class="form-control" id="certificado_0" name="certificado" type="file" placeholder="Certificado">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center">
                           <div class="form-group">
                                <a href="{{route('logros.index')}}" class="btn btn-secondary">Atras</a>
                                <button type="submit" class="btn btn-primary ">Guardar</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>  
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection
