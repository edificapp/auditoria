@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('otros-estudios.index') }}">Otros Estudios</a></li>
                            <li class="breadcrumb-item active" aria-current="page" ><b>Editar otros estudios</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('hojavida.components.tabs', ['select' => 'otros_estudios'])    
</div>
<div class="row ">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="text-center mb-4">
                    <p class="h4">Otros Estudios</p>
                </div>
                <small class="text-danger">* CAP del sena, estudios de educación no formal, simposios, talleres y demas estudios que pueda certificar.</small>
                <br><br><br>
                <div class="estudios_superiores">
                    <form enctype="multipart/form-data" action="{{route('otros-estudios.update', $estudio->id)}}" method="post">
                        @csrf
                        {!! method_field('put') !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="titulo_otro_estudio_0">Nombre del estudio</label>
                                    <input type="text" class="form-control" name="estudio" value="{{$estudio->estudio}}" placeholder="Nombre del estudio">
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="otros_estudios_escuela_0">Nombre de la Institución</label>
                                    <input type="text" class="form-control" name="escuela" value="{{$estudio->escuela}}" placeholder="Nombre del la Institución">
                                </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="otros_estudios_horas_0">Horas</label>
                                    <input type="text" class="form-control" name="horas" value="{{$estudio->horas}}" placeholder="Horas">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="otros_estudios_ano_0">Año</label>
                                        <input type="text" class="form-control" name="ano" value="{{$estudio->ano}}" placeholder="Horas">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="certificado_0">Certificado</label>
                                    <input class="form-control" name="certificado" type="file" placeholder="Certificado">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center">
                           <div class="form-group">
                                <a href="{{route('otros-estudios.index')}}" class="btn btn-secondary">Atras</a>
                                <button type="submit" class="btn btn-primary ">Guardar</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection
