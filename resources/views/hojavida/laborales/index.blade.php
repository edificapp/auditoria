@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item active" aria-current="page" ><b>Experiencia laboral</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('hojavida.components.tabs', ['select' => 'experiencia_laboral']) 
</div>
<div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body borde p-2">
                <a href="{{route('laboral.create')}}" class="btn-create-form float-left">
                    Agregar Experiencia
                </a>
                <div class="text-center pt-2">
                    <p class="h4">
                        Experiencia Laboral Total {{Helper::timeOfAgeMonths($curriculum->laborales->sum('tiempo_meses'))}}
                    </p>
                </div>
            </div>
        </div>

        <div id="accordion" class="mt-2">
            @foreach($curriculum->laborales as $key => $item)
                <div class="card">
                    <div class="card-header borde" id="heading{{$key}}">
                        <h5 class="mb-0">
                            @if($item->certificado_file != NULL)
                                <a href="{{$item->certificado_file}}" target="_blank" class="btn-pdf-form" title="Certificado">
                                
                                </a>
                            @else
                                <button class="btn btn-inverse-secondary btn-sm btn-rounded" onclick="certificadoNull()" title="No tiene Certificado" >
                                    <i class="mdi mdi-file-pdf-box"></i>
                                </button>
                            @endif
                            <a href="{{route('laboral.edit', $item->id)}}" class="btn-edit-form" title="Editar"></a>
                            <button type="submit" class="btn-delete-form" onclick="destroy({{$item->id}})" title="Borrar">
                            
                            </button>
                            <button class="btn" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                <b>Empresa:</b> 
                                {{$item->empresa}}
                                <b class="text-muted mr-1"> {{$item->laborando == 'si' ? '( Actualmente )': ''}} </b>
                            </button>
                            <form action="{{route('laboral.destroy', $item->id)}}" id="destroy_{{$item->id}}" method="post">
                                @csrf
                                {!! method_field('delete') !!}
                            </form>
                        </h5>
                    </div>
    
                    <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Pais:</b> {{$item->pais}}
                                </div>
                                <div class="col-md-4">
                                    <b>Departamento:</b> {{$item->departamento}}
                                </div>
                                <div class="col-md-4">
                                    <b>Ciudad:</b> {{$item->ciudad}}
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-4">
                                    <b>Fecha de Ingreso:</b> {{$item->fingreso}}
                                </div>
                                <div class="col-md-4">
                                    <b>Fecha de Terminación:</b> {{$item->fterminacion}}
                                </div>
                                <div class="col-md-4">
                                    <b>Tiempo total:</b> {{timeOfAgeMonths($item->tiempo_meses)}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Tipo de Empresa:</b> {{$item->empresa}}
                                </div>
                                <div class="col-md-6">
                                    <b>Cargo:</b> {{$item->cargo}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Dedicación:</b> {{$item->dedicaciones}}
                                </div>
                                <div class="col-md-6">
                                    <b>Dependencia:</b> {{$item->dependencia}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Dirección:</b> {{$item->direccion}}
                                </div>
                                <div class="col-md-6">
                                    <b>Telefono:</b> {{$item->telefono}}
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <b>Funciones:</b> {{$item->funciones}}
                                </div>
                                <div class="col-md-6">
                                    <b>Proyecto en que se desempeño:</b> {{$item->proyectos}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    function certificadoNull(){
         Swal.fire({
              title: 'Notificación',
              text: "No tiene Certificado",
              type: "danger",
              confirmButtonText: "Cerrar"
          })
    }

    function destroy(id){
        $('#destroy_'+id).submit();
    }
</script>
@endsection
