@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('laboral.index') }}">Experiencia Laboral</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Editar experiencia laboral</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
     @include('hojavida.components.tabs', ['select' => 'experiencia_laboral'])  
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body borde">
                <div class="text-center mb-4">
                   <p class="h4">Experiencia laboral</p>
               </div>
                <div class="estudios_superiores">
                    <form enctype="multipart/form-data" action="{{route('laboral.update', $item->id)}}" method="post">
                        @csrf
                        {{method_field('put')}}
                        <small class="text-danger">* Relacione su experiencia laboral o de servicios en estricto orden cronologico comenzando por el actual o ultimo empleo o servicio prestado.</small>
                        <br><br><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                 <div class="form-group">
                                    <label for="explab_entidad_0">Empresa o entidad</label>
                                    <input type="text" class="form-control" id="explab_entidad_0" name="entidad"  value="{{$item->empresa}}" placeholder="Empresa o entidad">
                                </div>
                            </div>
                           <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_tipo_0">Tipo de empresa</label>
                                    <select class="form-control" id="explab_tipo_0" name="tipo">
                                        <option value="Publica" {{$item->tipo == 'Publica' ? 'selected' : ''}}>Publica</option>
                                        <option value="Privada" {{$item->tipo == 'Privada' ? 'selected' : ''}}>Privada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_direccion_0">Dirección</label>
                                    <input type="text" class="form-control" id="explab_direccion_0" name="direccion" value="{{$item->direccion}}" placeholder="Dirección">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_telefonos_0">Telefonos</label>
                                    <input type="text" class="form-control" id="explab_telefonos_0" name="telefono"  value="{{$item->telefonos}}"placeholder="Telefonos">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_cargo_0">Cargo </label>
                                    <input type="text" class="form-control" id="explab_cargo_0" name="cargo" value="{{$item->cargo}}" placeholder="Cargo">
                                </div>
                            </div>
                           <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_dependencia_0">Dependencia</label>
                                    <input type="text" class="form-control" id="explab_dependencia_0" name="dependencia" value="{{$item->dependencia}}"placeholder="Dependencia">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_dedicacion_0">Tipo de dedicación {{$item->laboral_od }}</label>
                                    <select class="form-control" id="explab_dedicacion" name="dedicacion">
                                        <option value="tc" {{$item->dedicacion == 'tc' ? 'selected' : ''}}>Tiempo completo</option>
                                        <option value="mt" {{$item->dedicacion == 'mt' ? 'selected' : ''}}>Medio tiempo</option>
                                        <option value="tp" {{$item->dedicacion == 'tp' ? 'selected' : ''}}>Tiempo parcial</option>
                                        <option value="otra" {{$item->laboral_od ? 'selected' : ''}}>Otra dedicación</option>
                                    </select>
                                </div>
                            </div>
                           <div class="col-xs-12 col-sm-6 col-md-3" id="div_od">
                                 <div class="form-group">
                                    <label for="explab_od_0">Dedicación</label>
                                    <input type="text" class="form-control" id="explab_od_0" name="explab_od"  value="{{$item->laboral_od ? $item->dedicacion : ''}}" placeholder="Otra dedicación">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_pais_0">País</label>
                                    <select class="form-control" id="explab_pais" name="pais">
                                        @foreach ($paises as $pais)
                                        <option value="{{ $pais }}" {{$item->pais == $pais ? 'selected' : ''}}>{!! $pais !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                             <div class="col-xs-12 col-sm-6 col-md-3" id="div_departamento">
                                <div class="form-group">
                                    <label for="explab_departamento_0">Departamento</label>
                                    <select class="form-control" id="departamento"  name="departamento">
                                        @foreach (json_decode($depto_ciudades,1) as $departamento => $ciudades)
                                        <option value="{{ $departamento }}" {{$item->departamento == $departamento ? 'selected' : ''}}>{!! $departamento !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                           <div class="col-xs-12 col-sm-6 col-md-3" id="div_estado">
                                <div class="form-group">
                                    <label for="explab_inpdepartamento_0">Estado / Provincia / Departamento</label>
                                    <input type="text" class="form-control" name="inpdepartamento" value="{{$item->verificarPais ? : $item->departamento}}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group" id="ciudad_local">
                                    <label for="explab_ciudad_0">Ciudad</label>
                                    <select class="form-control" id="ciudad" name="ciudad">
                                    </select>
                                </div>
                                <div class="form-group" id="ciudad_extranjera">
                                    <label for="explab_inpciudad_0">Ciudad</label>
                                    <input type="text" class="form-control" id="explab_inpciudad_0" name="inpciudad" value="{{$item->verificarPais ? : $item->ciudad}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_actualmente_0">Trabaja en la empresa Actualmente</label>
                                    <select class="form-control explab_actualmente" name="actualmente" id="explab_actualmente_0">
                                        <option value="no" {{$item->actualmente == 'no' ? 'selected' :''}}>No</option>
                                        <option value="si" {{$item->actualmente == 'si' ? 'selected' :''}}>Si</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="explab_fingreso_0">Fecha de ingreso</label>
                                    <input type="date" class="form-control" id="explab_fingreso_0" name="fingreso" value="{{$item->fingreso}}" placeholder="Fecha de ingreso">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-3 div_explab_0">
                                <div class="form-group">
                                    <label for="explab_fterminacion_0">Fecha de terminación</label>
                                    <input type="date" class="form-control" id="explab_fterminacion_0" name="fterminacion" value="{{$item->fterminacion}}" placeholder="Fecha de terminación">
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-3 div_explab_0">
                                <div class="form-group">
                                    <label for="explab_certificado_0">Certificado laboral</label>
                                    <input class="form-control" id="explab_certificado_0" name="certificado" type="file" placeholder="Certificado">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="funciones">Funciones del cargo</label>
                                    <textarea rows="4" class="form-control" id="funciones" name="funciones">{{$item->funciones}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="proyectos">Proyecto en que se desempeño.</label>
                                    <textarea rows="4" class="form-control" id="proyectos" name="proyectos">{{$item->proyectos}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center">
                           <div class="form-group">
                                <a href="{{route('laboral.index')}}" class="btn btn-secondary">Atras</a>
                                <button type="submit" class="btn btn-primary ">Guardar</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    let deptos_ciudades_col = {!! $depto_ciudades !!};
    let ciudad_seleccionada = '{!! $item->ciudad !!}';

     $(document).ready(function(){
        validarExpLabOnLoad();
        validarDedicacionbOnLoad();
        validarPaisbOnLoad();
        validarCiudadOnLoad();
    });

    function validarExpLabOnLoad(){
        $( ".explab_actualmente" ).each(function( index ) {
          let id = this.id;
            let value = this.value;
            validarExpLab(value,id);
        });
    }



    $(document).on('change','.explab_actualmente', function () { 
        let id = this.id;
        let value = this.value;
        validarExpLab(value,id);
    });

    function validarExpLab(value, id){
        if(value == 'si'){
            $('.div_explab_'+id.substr(id.length - 1)).hide()
        }else{
            $('.div_explab_'+id.substr(id.length - 1)).show()
        }
    }

    //////Dedicacion
    function validarDedicacionbOnLoad(){
        let value = $('#explab_dedicacion').val();
        validarDedicacion(value);
    }



    $(document).on('change','#explab_dedicacion', function () {
        let value = this.value;
        validarDedicacion(value);
    });

    function validarDedicacion(value){
        if(value == 'otra'){
            $('#div_od').show();
        }else{
            $('#div_od').hide();
        }
    }

    ////// pais
    function validarPaisbOnLoad(){
        let value = $('#explab_pais').val();
        validarPais(value);
    }

    $(document).on('change','#explab_pais', function () {
        let value = this.value;
        validarPais(value);
    });

    function validarPais(value){
        if(value == 'Colombia'){
            $('#div_departamento').show();
            $('#ciudad_local').show();
            $('#div_estado').hide();
            $('#ciudad_extranjera').hide();
        }else{
            $('#div_departamento').hide();
            $('#ciudad_local').hide();
            $('#div_estado').show();
            $('#ciudad_extranjera').show();
        }
    }

    ////// ciudad
    function validarCiudadOnLoad(){
        let value = $('#departamento').val();
        validarCiudad(value);
    }

    $(document).on('change','#departamento', function () {
        let value = this.value;
        validarCiudad(value);
    });

    function validarCiudad(value){
        let ciudades = deptos_ciudades_col[value];
        $('#ciudad').empty();
        $.each(ciudades, function(ind,ciudad) {
            if(ciudad_seleccionada == ciudad){
                $('#ciudad').append('<option value="'+ciudad+'" selected>'+ciudad+'</option>');
            }else{
                $('#ciudad').append('<option value="'+ciudad+'">'+ciudad+'</option>');
            }
        });
    }

</script>
@endsection
