@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('estudios.index') }}">Educacion superior</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Agregar Educacion superior</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('hojavida.components.tabs', ['select' => 'educacion_superior']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body borde">
                <div class="text-center mb-5">
                    <p class="h4">                 
                        Educación superior (Pregrado y postgrado) <strong class="text-muted" title="Seleccione el último grado aprobado (Los grados de 1o. a 6o. de bachillerato equivalen a los grados 6o. a 11o. de educación básica secundaria y media).">(?)</strong>
                    </p>
                </div>

                <small class="text-danger text-left">* Diligencie este punto en estricto orden cronológico.</small>
                <br>
                <small class="text-danger text-left">* En caso que sus estudios sean por módulos, créditos o años conviertalos a semestres.</small>
                <br><br><br>
                <div class="estudios_superiores">
                    <form enctype="multipart/form-data" action="{{route('estudios.store')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modalidad_ac_0">Modalidad Academica</label>
                                    <select class="form-control custom-select" id="tipo" name="modalidad_ac" required>
                                        @foreach($tipoProfesiones as $item)
                                        <option value="{{$item->id}}">{{$item->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="profesion_id">Nombre de los estudios / Titulo obtenido</label>
                                    <select  id="profesion" class="form-control col-12 custom-select" name="profesion_id" required>
                                    </select>
                                </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="escuela">Nombre Institución Educativa</label>
                                    <input type="text" class="form-control" name="escuela" placeholder="Nombre de la institución" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="finalizado">Finalizado</label>
                                    <select class="form-control graduado"  name="finalizado">
                                        <option value="si">Si</option>
                                        <option value="no">No</option>
                                        <option value="en curso">En curso</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="semestres">Semestres aprobados</label>
                                    <input type="text" class="form-control" name="semestres" placeholder="Semestres aprobados" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fecha_fin_estudio_0">Fecha de terminacion</label>
                                    <input type="month" class="form-control" name="fecha_fin" placeholder="Fecha de terminación" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md div_graduado_certificado_0">
                                <div class="form-group">
                                    <label for="tarjeta_profesional">Numero de tarjeta profesional</label>
                                    <input type="text" class="form-control" name="tarjeta" placeholder="Numero de tarjeta profesional">
                                </div>
                            </div>
                            <div class="col-md div_graduado_certificado_0">
                                <div class="form-group">
                                    <label for="certificado_0">Certificado</label>
                                    <input class="form-control" id="certificado_0" name="certificado" type="file" placeholder="Certificado">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center">
                           <div class="form-group">
                                <a href="{{route('estudios.index')}}" class="btn btn-secondary">Atras</a>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>  
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        cargarProfesiones();
    });

     $('#tipo').on('change', function(){
      cargarProfesiones();
    });

    function cargarProfesiones(){
      let value = $('#tipo').val();
      $.get(`/tipo-profesion/${value}`, function( data ) {
        $('#profesion').empty();
        $.each(data.profesiones, function( key, value ) {
          $('#profesion').append(`<option value="${value.id}">${value.nombre}</option>`);
        });
      });
    }

</script>
@endsection
