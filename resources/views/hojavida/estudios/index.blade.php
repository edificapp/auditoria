@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item "><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Educacion superior</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div >
    @include('hojavida.components.tabs', ['select' => 'educacion_superior'])    
</div>
<div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body borde p-2 ">
                <a href="{{route('estudios.create')}}" class="btn-create-form float-left">
                    Agregar Estudio
                </a>
                <div class="text-center pt-2 ">
                    <p class="h4">
                        Educación superior (Pregrado y postgrado) 
                        <strong class="text-muted" title="Seleccione el último grado aprobado (Los grados de 1o. a 6o. de bachillerato equivalen a los grados 6o. a 11o. de educación básica secundaria y media).">(?)</strong>
                    </p>
                </div>
            </div>
        </div>

        <div id="accordion" class="mt-2">
            @foreach($curriculum->estudios as $key => $item)
              <div class="card">
                <div class="card-header borde" id="heading{{$key}}">
                  <h5 class="mb-0">
                    
                        @if($item->certificado_file != NULL)
                            <a href="{{$item->certificado_file}}" target="_blank" class="btn-pdf-form" title="Certificado">
                            </a>
                        @else
                            <button class="btn btn-inverse-secondary btn-sm btn-rounded" onclick="certificadoNull()" title="No tiene Certificado" >
                                 <i class="mdi mdi-file-pdf-box"></i>
                            </button>
                        @endif
                        <a href="{{route('estudios.edit', $item->id)}}" class="btn-edit-form" title="Editar">
                        </a>
                        <button type="submit" class="btn-delete-form" onclick="destroy({{$item->id}})" title="Borrar">
                        </button>
                        
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                         <b>Estudio:</b>
                         {{$item->profesion->nombre}}
                        </button>
                        <form action="{{route('estudios.destroy', $item->id)}}" id="destroy_{{$item->id}}" method="post">
                            @csrf
                            {!! method_field('delete') !!}
                        </form>
                  </h5>
                </div>
        
                <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                  <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <b>Escuela:</b> {{$item->escuela}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <b>Semestre:</b> {{$item->semestres}}
                        </div>
                        <div class="col-md-6">
                            <b>Modalidad:</b> {{$item->profesion->tipo->nombre}}
                        </div>
                    </div>
                    <div class="row">
                       <div class="col-md-6">
                            <b>Finalizado:</b> {{$item->finalizado == 'si' ? $item->fecha_fin : 'No'}}
                        </div>
                        <div class="col-md-6">
                            <b>Tarjeta profesional:</b> {{$item->tarjeta}}
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
        </div> 
    </div>
</div>


@endsection

@section('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style type="text/css">
        .row-card{
           font-family: Roboto, sans-serif;
            font-size: 0.9375rem;
            line-height: 1.8;
            border-bottom: 1px solid rgb(243, 243, 243);
            padding: 0.4rem 0px 0.4rem 0px;
            display: flex !important;
            list-style: outside none none;
            box-sizing: border-box;
            word-wrap: break-word;
            justify-content: flex-start !important;
            align-items: center !important;
        }

        .row{
            margin-bottom: 10px;
        }
    </style>
@stop

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    function certificadoNull(){
         Swal.fire({
              title: 'Notificación',
              text: "No tiene Certificado",
              type: "danger",
              confirmButtonText: "Cerrar"
          })
    }

    function destroy(id){
        $('#destroy_'+id).submit();
    }
</script>
@endsection
