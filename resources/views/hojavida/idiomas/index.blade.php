@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style type="text/css">
        .row-card{
           font-family: Roboto, sans-serif;
            font-size: 0.9375rem;
            line-height: 1.8;
            border-bottom: 1px solid rgb(243, 243, 243);
            padding: 0.4rem 0px 0.4rem 0px;
            display: flex !important;
            list-style: outside none none;
            box-sizing: border-box;
            word-wrap: break-word;
            justify-content: flex-start !important;
            align-items: center !important;
        }

        .row{
            margin-bottom: 10px;
        }
    </style>
@stop

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item "><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Idiomas</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('hojavida.components.tabs', ['select' => 'idiomas'])    
</div>
<div class="row">
    <div class="col-12 grid-margin">
        <div class="card ">
            <div class="card-body borde p-2">
                <a href="{{route('idiomas.create')}}" class="btn-create-form float-left">
                    Agregar Idioma
                </a>
                <div class="text-center pt-2">
                    <p class="h4">
                        Idiomas
                    </p>
                </div>
            </div>
        </div>
        <div id="accordion">
            @foreach($curriculum->idiomas as $key => $item)
                <div class="card">
                    <div class="card-header" id="heading{{$key}}">
                    <h5 class="mb-0">
                            @if($item->certificado_file != NULL)
                                <a href="{{$item->certificado_file}}" target="_blank" class="btn-pdf-form" title="Certificado">
                                    
                                </a>
                            @else
                                <button class="btn btn-inverse-secondary btn-sm btn-rounded" onclick="certificadoNull()" title="No tiene Certificado" >
                                    <i class="mdi mdi-file-pdf-box"></i>
                                </button>
                            @endif
                            <a href="{{route('idiomas.edit', $item->id)}}" class="btn-edit-form" title="Editar"></a>
                            <button type="submit" class="btn-delete-form" onclick="destroy({{$item->id}})" title="Borrar"></button>
                            
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                <b>{{$item->idioma}}</b>
                            </button>
                            <form action="{{route('idiomas.destroy', $item->id)}}" id="destroy_{{$item->id}}" method="post">
                                @csrf
                                {!! method_field('delete') !!}
                            </form>
                    </h5>
                    </div>
        
                    <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Habla:</b> {{$item->calificacion($item->habla)}}
                                </div>
                                <div class="col-md-4">
                                    <b>Lee:</b> {{$item->calificacion($item->lee)}}
                                </div>
                                <div class="col-md-4">
                                    <b>Escribe:</b> {{$item->calificacion($item->escribe)}}
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                                    <b>Observación:</b> {{$item->observacion}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</div>

@endsection


@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    function certificadoNull(){
         Swal.fire({
              title: 'Notificación',
              text: "No tiene Certificado",
              type: "danger",
              confirmButtonText: "Cerrar"
          })
    }

    function destroy(id){
        $('#destroy_'+id).submit();
    }
</script>
@endsection
