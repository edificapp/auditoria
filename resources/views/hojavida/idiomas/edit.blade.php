@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item" ><a href="{{route('idiomas.index')}}">Idiomas</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Editar idioma</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('hojavida.components.tabs', ['select' => 'idiomas'])  
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="text-center mb-4">
                    <p class="h4">idiomas</p>
                </div>
                <div class="estudios_superiores">
                    <form enctype="multipart/form-data" action="{{route('idiomas.update', $idioma->id)}}" method="post">
                        @csrf
                        {{method_field('put')}}
                        <div class="row">
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="idioma_0">Idioma</label>
                                    <input type="text" class="form-control" name="idioma" value="{{$idioma->idioma}}" placeholder="Idioma">
                                </div>
                            </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label for="habla_0">Habla</label>
                                    <select class="form-control" name="habla">
                                        <option value="R" {{$idioma->habla == 'R' ? 'selected' : ''}}>Regular</option>
                                        <option value="B" {{$idioma->habla == 'B' ? 'selected' : ''}}>Bien</option>
                                        <option value="MB" {{$idioma->habla == 'MB' ? 'selected' : ''}}>Muy bien</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lee_0">Lee</label>
                                    <select class="form-control" name="lee">
                                        <option value="R" {{$idioma->lee == 'R' ? 'selected' : ''}}>Regular</option>
                                        <option value="B" {{$idioma->lee == 'B' ? 'selected' : ''}}>Bien</option>
                                        <option value="MB" {{$idioma->lee == 'MB' ? 'selected' : ''}}>Muy bien</option>
                                    </select>
                                </div>
                            </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label for="escribe_0">Escribe</label>
                                    <select class="form-control" name="escribe">
                                        <option value="R" {{$idioma->escribe == 'R' ? 'selected' : ''}}>Regular</option>
                                        <option value="B" {{$idioma->escribe == 'B' ? 'selected' : ''}}>Bien</option>
                                        <option value="MB" {{$idioma->escribe == 'MB' ? 'selected' : ''}}>Muy bien</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label for="observacion_0">Observación</label>
                                        <textarea class="form-control" name="observacion" placeholder="Observaciones">{{$idioma->observacion}}</textarea>
                                    </div>
                                </div>
                            </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label for="certificado_idioma_0">Certificado</label>
                                    <input class="form-control" name="certificado" type="file" placeholder="Certificado">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center">
                           <div class="form-group">
                                <a href="{{route('idiomas.index')}}" class="btn btn-secondary">Atras</a>
                                <button type="submit" class="btn btn-primary ">Guardar</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>  
@endsection

@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection
