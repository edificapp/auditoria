<!DOCTYPE html>
<html lang="es">

<head>
    <title>Auditorias - Hoja de vida</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ public_path('admin/plugins/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ public_path('admin/plugins/base/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ public_path('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    {{-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}

    <link rel="stylesheet" href="{{ public_path('admin/css/style.css') }}">
    <link rel="shortcut icon" href="{{ public_path('admin/images/favicon.png') }}" />

    {{-- @yield('styles') --}}
</head>
<body>
    <div class="container-fluid" >
        <div class="content-wrapper" >

            <div class="row m-0 p-0">
                <center><h3>HOJA DE VIDA</h3></center>
                <hr style="color: #9c9 !important;">
            </div>
             <div class="row m-0 p-0">
                <button class="btn btn-secondary col-sm border boton_cabecera" type="button" data-target="#div_logros">Datos Personales</button>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin">
                    <div class="card" id="div_informacion_general">
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row">

                                    <div class="col-md">
                                        <div class="col-xl d-flex justify-content-center align-items-center">
                                            <center><img class="img-fluid img-thumbnail rounded" style="max-height: 250px !important;" src="data:image/jpeg;base64,{!! base64_encode(@file_get_contents($imagen)) !!}"></center>
                                        </div>
                                    </div>

                                    <div class="col-md">

                                        <hr>
                                        <br>
                                        @if (in_array($nacionalidad,['COL','EXT','DOB']))
                                        <label>
                                            <strong>Nacionalidad:</strong>
                                            @if ($nacionalidad == 'COL')
                                            Colombiano
                                            @elseif ($nacionalidad == 'EXT')
                                            Extranjero
                                            @elseif ($nacionalidad == 'DOB')
                                            Doble
                                            @endif
                                        </label>
                                        @endif
                                        @if ($nacionalidad == 'COL' || $nacionalidad == 'DOB')
                                            <label class="ml-3">
                                                <strong>T. Documento:</strong>
                                                @if ($dni_nal_tipo == 'CC')
                                                    CC
                                                @elseif ($dni_nal_tipo == 'NIT')
                                                    Nit
                                                @elseif ($dni_nal_tipo == 'OTRO')
                                                    Otro
                                                @endif
                                            </label>
                                            <label><strong># </strong> {!! $dni_nal !!}</label>
                                        @endif

                                        @if ($nacionalidad == 'EXT' || $nacionalidad == 'DOB')
                                            <label class="ml-3">
                                                <strong>T. Documento Extranjero:</strong>
                                                @if ($dni_ext_tipo == 'CE')
                                                    CE
                                                @elseif ($dni_ext_tipo == 'NIT')
                                                    Nit
                                                @elseif ($dni_ext_tipo == 'PASAPORTE')
                                                    Pasaporte
                                                @endif
                                            </label>
                                            <label><strong># </strong> {!! $dni_ext !!}</label>
                                        @endif

                        <!------------------------------------------------------------------------------------------------------------------>
                                        {!!$nombres || $genero ? '<br/>' : ''!!}    
                                        @if ($nombres)
                                            <label>
                                                <strong>Nombre(s) y apellidos:</strong> {{ $nombres }} {{ $primer_apellido }} {{ $segundo_apellido }}.
                                            </label>
                                        @endif

                                        @if ($genero)
                                            <label class="ml-5">
                                                <strong>Genero: </strong>{{ $genero == 'M' ? 'Masculino' : 'Femenino' }}
                                            </label>
                                        @endif

                        <!------------------------------------------------------------------------------------------------------------------>
                                        <br>
                                        @if ($telefonos)
                                            <label>
                                                <strong>Telefonos de contacto:</strong> {{ $telefonos }}
                                            </label>
                                        @endif

                                        @if ($nacionalidad == 'COL' || $nacionalidad == 'DOB')
                                        <label class="ml-3">
                                            <strong>Lugar de nacimiento:</strong> {{ $mun_nacimiento }}, {{ $dep_nacimiento }}, {!! $pais_nacimiento !!}.
                                        </label>
                                        @endif

                                        @if ($nacionalidad == 'EXT' || $nacionalidad == 'DOB')
                                            <label class="ml-3">
                                                <strong>Pais extranjero:</strong> {!! $pais_extranjero !!}
                                            </label>
                                        @endif
                                    </div>
                                </div>

                                @if ($genero == 'M' && $libreta)
                                    <hr>
                                    <div class="row">
                                        <div class="col-md">
                                            <label>
                                                <strong>Numero libreta, categoria:</strong> {!! $libreta !!}-{!! $numero_libreta !!}, {!! $dm !!}.
                                            </label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="row m-0 p-0">
                <button class="btn btn-secondary col-sm border boton_cabecera" type="button" data-target="#div_logros">Estudios</button>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin">
                    <div class="card" id="div_estudios">
                        <div class="card-body">
                            <div class="mt-3 border border-primary rounded p-3">
                                <div class="row">
                                    <div class="col-md">
                                        <h3>Educación básica y media</h3>
                                    </div>
                                </div>

                                <hr>
                                @if ($grado || $titulo || $fecha_fin || $escuela || $ciudad_escuela || $certificado_basico)
                                    <div class="row">
                                        <div class="col-md">
                                            <label>
                                                <strong>Ultimo grado:</strong>
                                                    {{$grado}}° 
                                                    @if($grado < 6)
                                                        (Primaria)
                                                    @elseif($grado < 10 && $grado > 5)
                                                        (Secundaria)
                                                    @else
                                                        (Media)    
                                                    @endif
                                                    - {{ $titulo }} - {{ $fecha_fin }}
                                            </label>
                                        </div>
                                        <div class="col-md">
                                            <label><strong>Establecimiento:</strong> {{ $escuela }}, Finaliza {{ $ciudad_escuela }}</label>
                                        </div>
                                    </div>
                                    {{--
                                    @if ($certificado_basico)
                                    <div class="row">
                                        <div class="col-md">
                                            <a href="{{ url($certificado_basico) }}" target="_blank">Certificado</a>
                                        </div>
                                    </div>
                                    @endif
                                        --}}
                                @else
                                    <center><h4>Sin estudios basicos</h4></center>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if (count($estudios_superiores) > 0)
                <hr>
                 <div class="row m-0 p-0">
                    <button class="btn btn-secondary col-sm border boton_cabecera" type="button" data-target="#div_logros">Educación superior (Pregrado y postgrado)</button>
                </div>
                @foreach($estudios_superiores as $index => $estudio_superior)
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card" id="div_estudios">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md">
                                            <label><strong>Titulo:</strong> {!! $estudio_superior->estudio !!}</label>
                                            <label class="ml-3"><strong>Instituto:</strong>{!! $estudio_superior->escuela !!}</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            @if($estudio_superior->tarjeta)
                                                <label><strong># tarjeta profesional:</strong>{!! $estudio_superior->tarjeta !!}</label>
                                            @endif
                                            <label class="ml-5">
                                                <strong>Modalidad Academica:</strong> 
                                                @if($estudio_superior->modalidad == 'TL')
                                                Técnica
                                                @elseif($estudio_superior->modalidad == 'TC')
                                                Tecnológica
                                                @elseif($estudio_superior->modalidad == 'TE')
                                                Tecnológica Especializada
                                                @elseif($estudio_superior->modalidad == 'UN')
                                                Universitaria
                                                @elseif($estudio_superior->modalidad == 'ES')
                                                Especialización
                                                @elseif($estudio_superior->modalidad == 'MG')
                                                Maestria o Magister
                                                @elseif($estudio_superior->modalidad == 'DC')
                                                Doctorado o PHD
                                                @endif
                                                - {!! $estudio_superior->semestres !!} Semestres.
                                            </label>
                                            <label class="ml-5"><strong>Finalizado:</strong>{!! $estudio_superior->finalizado !!}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            
            @if (count($otros_estudios) > 0)
                <hr>
                <div class="row m-0 p-0">
                    <button class="btn btn-secondary col-sm border boton_cabecera" type="button" data-target="#div_logros">Otros estudios</button>
                </div>
                @foreach($otros_estudios as $index => $otro_estudio)
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card" id="div_estudios">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md">
                                            <label><strong>Estudio:</strong> {!! $otro_estudio->estudio !!}, {!! $otro_estudio->escuela !!}</label>
                                            <label class="ml-5"><strong>Duración </strong> {!! $otro_estudio->horas !!} Horas, Año {!! $otro_estudio->ano !!}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

         
           

            @if ( count($idiomas) > 0 || count($logros_laborales) > 0)

                <div class="row m-0 p-0">
                    <button class="btn btn-secondary col-sm border boton_cabecera" type="button" data-target="#div_logros">Publicaciones, investigaciones, logros e idiomas</button>
                </div>

                @if (count($logros_laborales) > 0)
                    <div class="row">
                        <div class="col-md">
                            <h3>Publicaciones, investigaciones y/o logros laborales</h3>
                        </div>
                    </div>
                    @foreach ($logros_laborales as $index => $logro)
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card" id="div_logros">
                                    <div class="card-body">
                                        <div class="container-fluid">
                                            <div class="mt-3 border border-primary rounded p-3">
                                                    @if($logro->logro != '')
                                                        <div class="row">
                                                            <div class="col-md">
                                                                <label><strong>Logro:</strong> {!! $logro->logro !!}</label>
                                                            </div>
                                                        </div>

                                                        @if ($logro->certificado)
                                                            <div class="row">
                                                                <div class="col-md">
                                                                    <a href="{{ url($logro->certificado) }}" target="_blank">Certificado</a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <hr>
                                                        <br>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>            
                    @endforeach
                @endif


                @if ( count($idiomas) > 0)
                    <div class="row">
                        <div class="col-md">
                            <h3>Publicaciones, investigaciones y/o logros laborales</h3>
                        </div>
                    </div>
                    @foreach ($idiomas as $index => $idioma)
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card" id="div_logros">
                                    <div class="card-body">
                                        <div class="container-fluid">
                                            <div class="mt-3 border border-primary rounded p-3">
                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Idioma:</strong> {!! $idioma->idioma !!}</label>
                                                    </div>
                                                    <div class="col-md">
                                                        <label>
                                                            <strong>Habla: </strong>
                                                            @if($idioma->habla == 'R')
                                                            Regular
                                                            @elseif($idioma->habla == 'B')
                                                            Bien
                                                            @elseif($idioma->habla == 'MB')
                                                            Muy bien
                                                            @endif
                                                        </label>
                                                    </div>
                                                    <div class="col-md">
                                                        <label>
                                                            <strong>Lee: </strong>
                                                            @if($idioma->lee == 'R')
                                                            Regular
                                                            @elseif($idioma->lee == 'B')
                                                            Bien
                                                            @elseif($idioma->lee == 'MB')
                                                            Muy bien
                                                            @endif
                                                        </label>
                                                    </div>
                                                    <div class="col-md">
                                                        <label>
                                                            <strong>Escribe: </strong>
                                                            @if($idioma->escribe == 'R')
                                                            Regular
                                                            @elseif($idioma->escribe == 'B')
                                                            Bien
                                                            @elseif($idioma->escribe == 'MB')
                                                            Muy bien
                                                            @endif
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Observación:</strong> {!! $idioma->observacion !!}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            @endif

            @if (count($experiencias_laborales) != 0)
                <div class="row m-0 p-0">
                    <button class="btn btn-secondary col-sm border boton_cabecera" type="button" data-target="#div_experiencia">Experiencia laboral</button>
                </div>
                @foreach ($experiencias_laborales as $index => $experiencia)
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card" id="div_experiencia">
                                <div class="card-body">
                                    @if($experiencia->empresa!= '' || $experiencia->cargo != '' )
                                        <div class="container-fluid">
                                            <div class="mt-3 border border-primary rounded p-3">
                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Fecha de ingreso:</strong> {!! $experiencia->fingreso !!}</label>
                                                        <label class="ml-5"><strong>Fecha terminación:</strong> {!! $experiencia->laborando == 'si' ? 'Laborando Actualmente' : $experiencia->fterminacion !!}</label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Empresa:</strong> {!! $experiencia->empresa !!}</label>
                                                        <label class="ml-5"><strong>Tipo de empresa:</strong> {!! $experiencia->tipo !!}</label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Cargo o contrato actual:</strong> {!! $experiencia->cargo !!}</label>
                                                        <label class="ml-5">
                                                            <strong>Tipo de dedicación: </strong>
                                                            @if($experiencia->dedicacion == 'tc')
                                                            Tiempo completo
                                                            @elseif($experiencia->dedicacion == 'mt')
                                                            Medio tiempo
                                                            @elseif($experiencia->dedicacion == 'tp')
                                                            Tiempo parcial
                                                            @else
                                                            {!! $experiencia->dedicacion !!}
                                                            @endif
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Dependencia:</strong> {!! $experiencia->dependencia !!}</label>
                                                        <label class="ml-3"><strong>Telefonos: </strong>{!! $experiencia->telefonos !!}</label>
                                                        <label class="ml-3"><strong>Dirección:</strong> {!! $experiencia->direccion !!}</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md">
                                                        <label><strong>Ciudad:</strong> {!! $experiencia->ciudad !!}</label>
                                                        <label class="ml-3"><strong>Departamento:</strong> {!! $experiencia->departamento !!}</label>
                                                        <label class="ml-3"><strong>País:</strong> {!! $experiencia->pais !!}</label>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{{ public_path('admin/plugins/base/vendor.bundle.base.js') }}"></script>
<script src="{{ public_path('admin/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ public_path('admin/plugins/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ public_path('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ public_path('admin/js/off-canvas.js') }}"></script>
<script src="{{ public_path('admin/js/hoverable-collapse.js') }}"></script>
<script src="{{ public_path('admin/js/template.js') }}"></script>

<script src="{{ public_path('admin/js/jquery.dataTables.js') }}"></script>
<script src="{{ public_path('admin/js/dataTables.bootstrap4.js') }}"></script>

</body>

</html>
