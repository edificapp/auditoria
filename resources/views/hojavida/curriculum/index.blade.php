@extends('layouts.app')

@section('titulo')
Hoja de vida
@endsection

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('curriculum.index')}}">Hoja de vida</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>informacion general</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div >
    @include('hojavida.components.tabs', ['select' => 'informacion_general'])   
</div>

<div class="row">
    <div class="col-md-12 grid-margin">
        <form method="POST" action="{{ route('curriculum.update', $curriculum->id) }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="card">
                <div class="card-body borde">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="float-right estado_general"><i class="mdi mdi-checkbox-blank-circle text-danger"></i></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                <div class="col-xl">
                                    <img class="img-fluid img-thumbnail rounded mx-auto d-block" id="imagen_avatar" style="max-height: 200px !important;" src="{{ asset($curriculum->avatar) }}">                                    
                                </div>
                                <br>
                                <input type="file" name="avatar" id="inp_imagen" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Subir Imagen">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Subir</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="primer_apellido">Primer Apellido</label>
                                        <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $curriculum->p_apellido }}" placeholder="Primer Apellido">
                                    </div>
                                </div>

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="segundo_apellido">Segundo apellido</label>
                                        <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $curriculum->s_apellido }}" placeholder="Segundo Apellido">
                                    </div>
                                </div>

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="nombres">Nombre(s)</label>
                                        <input type="text" class="form-control" id="nombres" name="nombres" value="{{ $curriculum->nombres }}" placeholder="Nombre(s)">
                                    </div>
                                </div>

                                <div class="col-xl">
                                    <div class="form-group">
                                        <label for="telefonos">Telefonos de contacto</label>
                                        <input type="text" class="form-control" id="telefonos" name="telefonos" value="{{ $curriculum->telefonos }}" placeholder="Telefonos de contacto">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="genero">Genero</label>
                                    <select class="form-control" id="genero" name="genero">
                                        <option value="M"{{ $curriculum->genero == 'M' ? 'selected' : '' }}>Masculino</option>
                                        <option value="F"{{ $curriculum->genero == 'F' ? 'selected' : '' }}>Femenino</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="nacionalidad">Nacionalidad</label>
                                    <select class="form-control" id="nacionalidad" name="nacionalidad">
                                        <option value="COL"{{ $curriculum->nacionalidad == 'COL' ? 'selected' : '' }}>Colombiano</option>
                                        <option value="EXT"{{ $curriculum->nacionalidad == 'EXT' ? 'selected' : '' }}>Extranjero</option>
                                        <option value="DOB"{{ $curriculum->nacionalidad == 'DOB' ? 'selected' : '' }}>Doble nacionalidad</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="dep_nacimiento">Fecha de nacimiento</label>
                                    <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ $curriculum->fecha_nacimiento }}">
                                </div>
                            </div>
                        </div>

                        <div class="row" id="municipio_pais_city_nacimiento">

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="mun_nacimiento">Municipio de nacimiento</label>
                                    <input type="text" class="form-control" id="mun_nacimiento" name="mun_nacimiento" value="{{ $curriculum->nacimiento_municipio }}" placeholder="Municipio de nacimiento">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="dep_nacimiento">Departamento de nacimiento</label>
                                    <input type="text" class="form-control" id="dep_nacimiento" name="dep_nacimiento" value="{{ $curriculum->nacimiento_departamento }}" placeholder="Departamento de nacimiento">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="pais_nacimiento">Pais de nacimiento</label>
                                    <select class="form-control" id="pais_nacimiento" name="pais_nacimiento">
                                        @foreach ($paises as $pais)
                                        <option value="{{ $pais }}" {{ $curriculum->nacimiento_pais == $pais ? 'selected' : '' }}>{!! $pais !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                             

                        </div>

                        <div class="row border-top pt-2" id="div_libreta">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="libreta">Tipo de libreta</label>
                                    <select class="form-control" id="libreta" name="libreta">
                                        <option value="1A"{{ $curriculum->libreta == '1A' ? 'selected' : '' }}>1A</option>
                                        <option value="2A"{{ $curriculum->libreta == '2A' ? 'selected' : '' }}>2A</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="numero_libreta">Número de libreta</label>
                                    <input type="text" class="form-control" id="numero_libreta" name="numero_libreta" value="{!! $curriculum->numero_libreta !!}" placeholder="Número de libreta militar">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="dm">Distrito militar</label>
                                    <input type="text" class="form-control" id="dm" name="dm" value="{!! $curriculum->dm !!}" placeholder="Distrito Militar">
                                </div>
                            </div>
                        </div>

                        <div class="row border-top pt-2" id="div_nacional">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="dni_nal_tipo">Tipo de Documento</label>
                                    <select class="form-control" id="dni_nal_tipo" name="dni_nal_tipo">
                                        <option value="CC" {{ $curriculum->dni_nal_tipo == 'CC' ? 'selected' : '' }}>Cedula de ciudadania</option>
                                        <option value="NIT" {{ $curriculum->dni_nal_tipo == 'NIT' ? 'selected' : '' }}>Nit</option>
                                        <option value="OTRO" {{ $curriculum->dni_nal_tipo == 'OTRO' ? 'selected' : '' }}>Otro</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="dni_nal">Número de Documento</label>
                                    <input class="form-control" type="text" name="dni_nal" id="dni_nal" value="{!! $curriculum->dni_nal !!}" placeholder="Número">
                                </div>
                            </div>
                        </div>

                        <div class="row border-top pt-2" id="div_extranjero">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="pais_extranjero">Pais extranjero</label>
                                    <select class="form-control" id="pais_extranjero" name="pais_extranjero">
                                        @foreach ($paises as $pais)
                                            @if($pais != 'Colombia')
                                                <option value="{{ $pais }}" {{ $curriculum->pais_extranjero == $pais ? 'selected' : '' }}>{!! $pais !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>                                

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="dni_ext_tipo">Tipo de Documento Extranjero</label>
                                    <select class="form-control" id="dni_ext_tipo" name="dni_ext_tipo">
                                        <option value="CE" {{ $curriculum->dni_ext_tipo == 'CE' ? 'selected' : '' }}>Cedula de extranjeria</option>
                                        <option value="NIT" {{ $curriculum->dni_ext_tipo == 'NIT' ? 'selected' : '' }}>Nit</option>
                                        <option value="PASAPORTE" {{ $curriculum->dni_ext_tipo == 'PASAPORTE' ? 'selected' : '' }}>Pasaporte</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="dni_ext">Número de Documento Extranjero</label>
                                    <input class="form-control" type="text" name="dni_ext" id="dni_ext" placeholder="Número">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body borde">
                    <div class="text-center mb-3">
                        <p class="h4">Educación básica y media 
                            <strong class="text-muted" title="Seleccione el último grado aprobado (Los grados de 1o. a 6o. de bachillerato equivalen a los grados 6o. a 11o. de educación básica secundaria y media).">(?)</strong>
                        </p>
                    </div>
                    <div class="estudios_superiores">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="grado">Ultimo grado</label>
                                    <select class="form-control" id="grado" name="grado">
                                        <option value="1"{{ $curriculum->grado == '1' ? 'selected' : '' }}>1° (Primaria)</option>
                                        <option value="2"{{ $curriculum->grado == '2' ? 'selected' : '' }}>2° (Primaria)</option>
                                        <option value="3"{{ $curriculum->grado == '3' ? 'selected' : '' }}>3° (Primaria)</option>
                                        <option value="4"{{ $curriculum->grado == '4' ? 'selected' : '' }}>4° (Primaria)</option>
                                        <option value="5"{{ $curriculum->grado == '5' ? 'selected' : '' }}>5° (Primaria)</option>
                                        <option value="6"{{ $curriculum->grado == '6' ? 'selected' : '' }}>6° (Secundaria)</option>
                                        <option value="7"{{ $curriculum->grado == '7' ? 'selected' : '' }}>7° (Secundaria)</option>
                                        <option value="8"{{ $curriculum->grado == '8' ? 'selected' : '' }}>8° (Secundaria)</option>
                                        <option value="9"{{ $curriculum->grado == '9' ? 'selected' : '' }}>9° (Secundaria)</option>
                                        <option value="10"{{ $curriculum->grado == '10' ? 'selected' : '' }}>10° (Media)</option>
                                        <option value="11"{{ $curriculum->grado == '11' ? 'selected' : '' }}>11° (Media)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="titulo">Titulo obtenido</label>
                                    <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $curriculum->titulo }}" placeholder="Titulo obtenido">
                                </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="escuela">Institución</label>
                                    <input type="text" class="form-control" id="escuela" name="escuela" value="{{ $curriculum->escuela }}" placeholder="Institución">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ciudad_escuela">Municipio de la Institución</label>
                                    <input type="text" class="form-control" id="ciudad_escuela" name="ciudad_escuela" value="{{ $curriculum->ciudad_escuela }}" placeholder="Municipio de la Institución">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fecha_fin">Fecha de terminacion</label>
                                    <input type="month" class="form-control" id="fecha_fin" name="fecha_fin" value="{{ $curriculum->fecha_fin }}" placeholder="Fecha de terminación">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    @if($curriculum->certificado_file != NULL)
                                        <a href="{{$curriculum->certificado_file}}" target="_blank" class="btn btn-danger">Certificado</a>
                                    @else
                                        <label for="certificado_basico">Certificado</label>
                                    @endif
                                    <input type="file" id="certificado_basico" name="certificado_basico" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled="" placeholder="Subir Certificado">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-primary" type="button">Subir</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row justify-content-md-center">
                           <div class="form-group">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>    
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        validarGeneroOnLoad();
        validarNacionalidadOnLoad();
    });

    function validarGeneroOnLoad(){
        let value = $('#genero').val();
        validarGenero(value);
    }

    $(document).on('change','#genero', function () { 
        let value = this.value;
        validarGenero(value);
    });

    function validarGenero(value){
        if(value == 'M'){
            $('#div_libreta').show()
        }else{
            $('#div_libreta').hide()
        }
    }

    //nacionalidad
    function validarNacionalidadOnLoad(){
        let value = $('#nacionalidad').val();
        validarNacionalidad(value);
    }

    $(document).on('change','#nacionalidad', function () { 
        let value = this.value;
        validarNacionalidad(value);
    });

    function validarNacionalidad(value){
        if(value == 'COL'){
            $('#div_nacional').show()
            $('#div_extranjero').hide()
        }else if(value == 'EXT'){
            $('#div_nacional').hide()
            $('#div_extranjero').show()
        }else{
            $('#div_nacional').show()
            $('#div_extranjero').show()
        }
    }



</script>
@endsection
