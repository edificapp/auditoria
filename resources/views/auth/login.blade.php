<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <title>GAP - login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="{{ asset('img/corporativo/G.png') }}" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <style type="text/css">
            html, body{
              height:100%
              margin 0;
                font-family: 'Roboto', sans-serif;
            }
            #login{
                background-color: #edeef0;
                width: 100% !important;
                height: 100% !important;
            }
            input {
                background-color: #edeef0 !important;
                color:black !important;
            }
            #imagen_principal{
                height: 100vh;
                background: url('{{asset('img/corporativo/principal.jpg')}}');
                -webkit-background-size: cover;
               -moz-background-size: cover;
               -o-background-size: cover;
               background-size: cover;
               width: 100% !important;
               min-height: 100vh; 
            }

            #div_login{
                height: 100vh;
            }

            .input-redondeado{
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 50px !important;
                border: 2px solid #49505766;
                padding: 0 4px 0 4px;
                font-size: 16px;
             }
             .btn-redondeado{
                border-radius: 30px !important;
                background: #5c5c5d;
                color: white;
             }

             .nopadding > div[class^="col-"] {
                margin: 0px !important;
                padding: 0px !important;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid" id="container">
            <div class="row nopadding justify-content-md-center">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" id="div_login">
                    <div class="text-left px-5 px-sm-5" id="login">
                        <div class="brand-logo">
                            <a href="#">
                                <img src="{{asset('/img/corporativo/logo-abreviado-fergon-sinLetras.png')}}" alt="Logo Gap" class="img-fluid mt-5 mb-3">
                            </a>
                        </div>
                        <form class="mt-4 pt-3" method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group">
                                <label class="text-muted"><b>Usuario</b></label>
                                <input type="email" class="form-control input-redondeado @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required placeholder=" &#xF007; Correo electronico" style="font-family:Arial, FontAwesome" >
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group ">
                                <label class="text-muted"><b>Contraseña</b></label>
                                <input type="password" class="form-control input-redondeado @error('password') is-invalid @enderror" id="password" name="password" placeholder=" &#xf09c; Contraseña" style="font-family:Arial, FontAwesome" required>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="mt-2" style="text-align: center !important;">
                                <button class="btn btn-secondary btn-redondeado pl-4 pr-4" style="background-color: #5b5b5f !important;" type="submit">INGRESAR</button>
                            </div>

                            <div class="mt-3 d-flex justify-content-center" style="text-align: center !important;">
                                <a href="{{ route('password.request') }}" class="text-muted mt-5">Recuperar tu contraseña</a>
                            </div>
                            <div class="my-2" style="text-align: center !important;">
                                    <label class="text-muted">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        Mantenerme en el sistema
                                    </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-9 col-lg-9" id="imagen_principal">
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if(screen.width > 1600){
                $('#container').removeClass('container-fluid').addClass('container');
            }
        });
    </script>
</html>






