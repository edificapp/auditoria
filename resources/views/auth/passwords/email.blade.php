@extends('layouts.auth')

@section('tipo')
Recuperar cuenta
@endsection

@section('contenido')
<div class="brand-logo">
    
    <center>
        <img src="{{ asset('img/corporativo/logo-email-fergon.png') }}" alt="logo" width="25%" style="min-width: 250px;"><br>
    </center>
</div>

@if(session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif

<form method="POST" action="{{ route('password.email') }}">
    @csrf

    <div class="form-group">
        <label>Ingresa tu correo electrónico al cual enviaremos un enlace para recuperar el acceso a tu cuenta.</label>
        <input type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo electronico" autofocus>

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

    </div>

    <div class="mt-3">
        <div class="btn-group col-sm">
            <button type="button" onclick="location.href='{{ route('login') }}'" class="btn btn-xs btn-secondary font-weight-medium auth-form-btn" title="Regresar al login"><i class="mdi mdi-keyboard-backspace"></i></button>
            <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">Recuperar contraseña</button>
        </div>
    </div>
</form>
@endsection