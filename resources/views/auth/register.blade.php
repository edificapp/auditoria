@extends('layouts.auth')

@section('titulo')
Crear usuario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('usuarios.registrarse') }}">
                    @csrf

                    <input type="hidden" name="inv_activa" id="inv_activa" value="si">
                    <input type="hidden" name="rol" value="4">

                    <h4 class="card-title">Información personal</h4>
                    <p class="card-description">
                        Se completa esta información para asi enviar la invitación via correo electronico al usuario.
                    </p>
                    <br><br><br>

                    <div class="row">
                        <div class="col-md-12">
                            <label>Escoge una Empresa</label>
                            <select name="empresa" class="form-control" id="selectEmpresa"></select>
                        </div>
                    </div>
                    <br><br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usuario">Nombre de usuario</label>
                                <input type="text" class="form-control @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! old('usuario') !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{!! old('email') !!}" required placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('login') }}';" class="btn btn-secondary">Estoy Registrado</button>
                        <button type="submit" class="btn btn-primary">Registrarse</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            cargarEmpresas();
        });

        function cargarEmpresas(){
            $.get('{{route('empresasAjax')}}', function( data ) {
                $.each(data, function( index, value ) {
                  $('#selectEmpresa').append('<option value="'+value.id+'">'+value.nombre+'</option>')
                });
            });
        }
    </script>
@endsection