<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<div id="laglon"></div>

</body>
<script type="text/javascript">
	window.onload = function() {
		if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                alert("Lat: " + position.coords.latitude + "\nLon: " + position.coords.longitude); 
            }, function(objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                    	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
						    alert("Debe de activar el Gps");
						}else{
							alert("No se ha permitido el acceso a la posición del usuario.");
						}
                    break;
                    case objPositionError.POSITION_UNAVAILABLE:
                    alert("No se ha podido acceder a la información de su posición.");
                    break;
                    case objPositionError.TIMEOUT:
                    alert("El servicio ha tardado demasiado tiempo en responder.");
                    break;
                    default:
                    alert("Error desconocido.");
                };
            }, {
                maximumAge: 75000,
                timeout: 15000
            });

        } else {
            alert("Su navegador no soporta la API de geolocalización.");
        }
	};




</script>
</html>