
 @extends('layouts.app')

 @section('titulo')
	 Archivo
 @endsection
 @section('styles')
	<style>
		.row a {
			text-decoration: none;
		}
	</style>
 @endsection
 
 @section('contenido')
	 <div class="row">
		 <div class="col-md-12 grid-margin">
			 <div class="d-flex justify-content-between flex-wrap">
				 <div class="d-flex align-items-end flex-wrap">
					 <div class="d-flex">
						 <nav aria-label="breadcrumb">
							 <ol class="breadcrumb p-2">
								 <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								 <li class="breadcrumb-item active" aria-current="page"> <b>Archivo</b></li>
							 </ol>
						 </nav>
					 </div>
				 </div>
			 </div>
		 </div>
	 </div>
	 <div class="row">
		 <div class="col-md-12 grid-margin">
			 <div class="card">
				 <div class="card-body borde">
					<p class="h4">Archivo</p>
					<div class="row justify-content-end">
						<div class="col-md-3 text-right input-group">
							<label for="" class="input-group-text"><i class="mdi mdi-magnify"></i></label>
							<input type="text" class="form-control" placeholder="Buscar"  aria-describedby="search">
						</div>
					</div>
					<div class="row justify-content-end">
						<div class=" custom-control custom-switch mt-3">
							<label >Iconos</label>
							<input type="checkbox" class="custom-control-input" id="customSwitch1">
							<label class="custom-control-label ml-5" for="customSwitch1">Lista</label>
							<div class="row">
								<canvas class="text-left" id="resumenChart" width="100" height="20"></canvas>
							</div>
						</div>
					</div>
					<div class="row justify-content-end">
						<div class="text-left">
							<a href="" ><i class="mdi mdi-folder-plus mdi-36px mr-2" style="color:#F1C40F"></i>Crear carpeta</a><br>
							<a href="" ><i class="mdi mdi-folder-upload mdi-36px mr-2" style="color:#F1C40F"></i>Cargar carpeta</a><br>
							<a href="" ><i class="mdi mdi-file-document mdi-36px mr-2" style="color:#ABB2B9"></i>Cargar archivo</a>
						</div>
					</div>

					 
				 </div>
			 </div>
		 </div>
	 </div>
 @endsection