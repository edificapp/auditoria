<ul class="nav nav-pills bg-transparent">
    <li class="nav-item">
        <a href="{{route('radicacion.index')}}" class="nav-link {{$select != 'radicacion' ?: 'active'}}">Radicacion</a>
    </li>
    <li class="nav-item">
        <a href="{{route('bandeja.recibido')}}" class="nav-link  {{$select != 'bandeja' ?:'active'}}">Bandeja</a>
    </li>
    <li class="nav-item">
        <a href="{{route('correspondencia.consulta')}}" class="nav-link  {{$select != 'consulta' ?:'active'}}">Consulta</a>
    </li>
</ul>

<hr style="margin-top: 0;margin-bottom:2px; border: 0.5px solid #D7DBDD ;">