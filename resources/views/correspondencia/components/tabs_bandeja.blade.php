<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{route('bandeja.recibido')}}" class="nav-link {{$select != 'recibido'?: 'active' }}">Recibido</a>
    </li>
    <li class="nav-item">
        <a href="{{route('bandeja.enviado')}}" class="nav-link {{$select != 'enviado'?: 'active' }}">Enviado</a>
    </li>
    <li class="nav-item">
        <a href="{{route('bandeja.salida')}}" class="nav-link {{$select != 'salida'?: 'active' }}">Salida</a>
    </li>
    <li class="nav-item">
        <a href="{{route('bandeja.interno')}}" class="nav-link {{$select != 'interno'?: 'active' }}">Interno <small style="color:white;border-radius:2px; background-color: red;">1</small></a>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Filtrar</a>
        <ul class="dropdown-menu">
            @if ($select == 'recibido')
                <li><a class="dropdown-item" href="#">Redirigido</a></li>
                <li><a class="dropdown-item" href="#">Devoluciones</a></li>
            @elseif ($select == 'enviado')
                <li><a class="dropdown-item" href="#">Redirigido</a></li>
                <li><a class="dropdown-item" href="#">Devoluciones</a></li>
                <li><a class="dropdown-item" href="#">Priorizado</a></li>
            @else
                <li><a class="dropdown-item" href="#">Priorizado</a></li>
            @endif
        </ul>
    </li>
</ul>
