<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{route('radicacion.recibido')}}" class="nav-link {{$select != 'recibido'?: 'active' }}">Recibido</a>
    </li>
    <li class="nav-item">
        <a href="{{route('radicacion.enviado')}}" class="nav-link {{$select != 'enviado'?: 'active' }}">Enviado</a>
    </li>
    <li class="nav-item">
        <a href="{{route('radicacion.salida')}}" class="nav-link {{$select != 'salida'?: 'active' }}">Salida</a>
    </li>
    <li class="nav-item">
        <a href="{{route('radicacion.interno')}}" class="nav-link {{$select != 'interno'?: 'active' }}">Interno</a>
    </li>
</ul>