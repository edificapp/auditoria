
@extends('layouts.app')

@section('titulo')
    Correspondencia
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Correspondencia</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('correspondencia.components.tabs',['select' => '' ])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
			
					<div class=" text-right">
						<h5 class="">
							Crear expediente 
							<button 
								class="btn btn-primary btn-sm ml-1" 
								type="button"
								data-toggle="modal"
								data-target="#modalExpediente"
							>
								<i class="mdi mdi-library-plus"></i>
							</button>
						</h5>
					</div>

					
					<div class="row mt-3 ml-0 p-0" >
						<p class="h5 ml-3"> Derechos de peticion </p>
						<canvas class="text-left" id="resumenChart" width="100" height="20"></canvas>
					</div>
					
                </div>
            </div>
        </div>
    </div>
	<div id="modalExpediente" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalExpediente" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Crear expediente</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text col-sm-3 bg-light">Nombre</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-3">Dependencia</label>
								<select name="" id="" class="form-control col-sm-9"></select>
							</div>
						</div>
						
						<div class="row mt-4 justify-content-md-center ">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Crear</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection



@section('scripts')
	<script>
		$(document).ready(function(){
			
			var Canvas = document.getElementById("resumenChart");

			Chart.defaults.global.defaultFontFamily = "Roboto";
			Chart.defaults.global.defaultFontSize = 17;

			var Data = {
				labels: [
					"Sin contestar %",
					"Contestado %"
				],
				datasets: [
					{
						data: [30, 70],
						backgroundColor: [
							"#7FB3D5",
							"#73C6B6"
						]
					}]
			};

			var pieChart = new Chart(Canvas, {
			type: 'pie',
			data: Data
			});
		});
		
	</script>
@stop
