
@extends('layouts.app')

@section('titulo')
    Correspondencia
@endsection



@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Correspondencia</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
    <div>
        @include('correspondencia.components.tabs',['select' => '' ])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">

					<p class="h4"> Derechos de peticion </p>

                    <div class="table-responsive">
						<table class="display" id="table" width="100%">
							<thead>
								<tr>
									<th>Nombre documento</th>
									<th>Expediente</th>
									<th>Encargado</th>
									<th>Fecha de radicacion</th>
									<th>Fecha limite de respuesta</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr class="body-rows">
									<td>Derecho de peticion Mari</td>
									<td>Evelyn martinez</td>
                                    <td>Evelyn martinez</td>
									<td>15-05-2021</td>
                                    <td>15-05-2021</td>
									<td><button class="btn-show-form" title="ver derecho de peticion"></button></td>
								</tr>
								<tr>
									<td>Derecho de peticion zona</td>
									<td>Circulares PE&S 2021</td>
                                    <td>Ivan gonzalez</td>
									<td>15-06-2021</td>
                                    <td>15-06-2021</td>
									<td><button class="btn-show-form" title="ver derecho de peticion"></button></td>
								</tr>
							</tbody>
						</table>
					</div>
					
                </div>
            </div>
        </div>
    </div>
@endsection

