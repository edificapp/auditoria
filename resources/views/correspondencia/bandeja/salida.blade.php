
@extends('layouts.app')

@section('titulo')
    Correspondencia
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="d-flex justify-content-between flex-wrap">
				<div class="d-flex align-items-end flex-wrap">
					<div class="d-flex">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb p-2">
								<li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								<li class="breadcrumb-item"><a href="{{route('correspondencia.index')}}">Correspondencia</a></li>
								<li class="breadcrumb-item"><a href="{{route('bandeja.recibido')}}">Bandeja</a></li>
								<li class="breadcrumb-item active" aria-current="page"> <b>Salida</b></li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div>
        @include('correspondencia.components.tabs',['select' => 'bandeja'])
    </div>
	<div>
        @include('correspondencia.components.tabs_bandeja',['select' => 'salida'])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
					<div class="table-responsive">
						<table  class="display" id="tabla" width="100%">
							<thead>
								<tr>
									{{-- <th></th> --}}
									<th>Tipo documental</th>
									<th>Nombre documento</th>
									<th>Expediente</th>
									<th>Fecha de radicacion</th>
									<th>Fecha limite de respuesta</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr class="body-rows">
									{{-- <td> <button class="btn" type="button" data-toggle="modal" data-target="#modal"><i class="mdi mdi-star" style="color:#F4D03F "></i></button></td> --}}
									<td>Oficio</td>
									<td>Informe alimentos</td>
									<td>PAE Yumbo</td>
									<td>02-02-2020</td>
									<td>-</td>
									<td>
										<div class="btn-group">
											<button class="btn-show-form" type="button" title="ver"></button>
											<button class="btn btn-inverse-secondary btn-sm btn-rounded" 
													type="button" 
													title="archivar"
													data-toggle="modal"
													data-target="#modalArchivar"
											>
												<i class="mdi mdi-email-variant"></i>
											</button>
										</div>
									</td>
								</tr>
						  </tbody>
						</table>
					</div>
				</div>
            </div>
        </div>
    </div>
	{{-- modal archivar --}}
	<div id="modalArchivar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="archivar" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-dialog-scrollable">
				<div class="modal-header">
					<h5 class="modal-title" id="archivar">Archivar</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						
						<table>
							<tbody>
								<tr >
									<td><i class="mdi mdi-folder mdi-36px" style="color:#F1C40F "></i></td>
									<td>Hoja de vida</td>
								</tr>
								<tr >
									<td><i class="mdi mdi-folder mdi-36px" style="color:#F1C40F "></i></td>
									<td>Cartas</td>
								</tr>
								<tr >
									<td><i class="mdi mdi-folder mdi-36px" style="color:#F1C40F "></i></td>
									<td>Facturas</td>
								</tr>
							</tbody>
						</table>

						<div class="row mt-3 d-flex justify-content-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	{{--  --}}
	<div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title"></h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4"></label>
								<select name="" id="" class="form-control">
									<option value="">COntabilidad</option>
									<option value="">Area Tecnica</option>
									<option value="">Mercadeo</option>
									<option value="">Recepcion</option>
								</select>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Persona</label>
								<input type="text" class="form-control">
							</div>
						</div>
						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$(document).ready( function () {
			$('#tabla').DataTable({
				ordering:false,searching:false,info:false,paging:false
			});
		} );
	</script>
@endsection