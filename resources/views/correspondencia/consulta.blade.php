
 @extends('layouts.app')

 @section('titulo')
	 Correspondencia
 @endsection
 
 
 @section('contenido')
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="d-flex justify-content-between flex-wrap">
				<div class="d-flex align-items-end flex-wrap">
					<div class="d-flex">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb p-2">
								<li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								<li class="breadcrumb-item"><a href="{{route('correspondencia.index')}}">Correspondencia</a></li>
								<li class="breadcrumb-item active" aria-current="page"> <b>Consulta</b></li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		 @include('correspondencia.components.tabs',['select' => 'consulta' ])
	</div>
	<div class="row">
		 <div class="col-md-12 grid-margin">
			 <div class="card">
				<div class="card-body borde">
					<p class="h4">Buscar</p>
					
					<div class=" row input-group  mb-3">

						<label class="input-group-text col-sm-2 bg-light "> Buscar por </label>
						<select class="select-multiple form-control col-sm-4" name="" multiple="multiple">
								<option value="">Nombre del documento</option>
								<option value="">Nombre del expediente</option>
								<option value="">Rango de fechas</option>
								<option value="">No. radicado</option>
								<option value="">Tipo documental</option>
						</select>
					</div>

					<form action="">
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text col-sm-5 bg-light">Nombre del documento</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="input-group col-md-4">
								<label for="" class="input-group-text col-sm-5 bg-light">Rango de fechas</label>
								<input type="date" class="form-control col-sm-9">

								<div class="input-group-append ml-3">
									<span class="input-group-text bg-light">a</span>
								</div>
							</div>
							<div class="input-group col-md-3">
								<input type="date" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="input-group col-md-4">
								<label for="" class="input-group-text col-sm-5 bg-light">Tipo documental</label>
								<select name="" id="" class="form-control">
									<option value="">Carta</option>
									<option value="">Oficio</option>
									<option value="">Cuenta</option>
									<option value="">Derecho de peticion</option>
								</select>
							</div>
						</div>

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Buscar</button>
							</div>
						</div>
					</form>
	
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$('.select-multiple').select2();
		});
	</script>
@endsection
