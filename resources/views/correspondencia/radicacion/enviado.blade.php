
@extends('layouts.app')


@section('titulo')
    Correspondencia
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								<li class="breadcrumb-item"><a href="{{route('correspondencia.index')}}">Correspondencia</a></li>
                                <li class="breadcrumb-item"><a href="{{route('radicacion.index')}}">Radicacion</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Enviado</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('correspondencia.components.tabs',['select' => 'radicacion' ])
    </div>
    <div>   
        @include('correspondencia.components.tabs_radicacion', ['select' => 'enviado'])
    </div>
	<div class="row">
		<div class="col md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					<p class="h4">Radicacion - Enviado</p>
					<hr>
					<p class="h5 ml-3 mt-3">Buscar destinatario</p>


                    <form action="">
						<div class="row">
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Tipo de identificacion</label>
								<select name="" id="" class="form-control col-sm-9">
									<option value="">NIT</option>
									<option value="">Cedula</option>
									<option value="">Cedula de extranjeria</option>
								</select>
							</div>
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">No. documento</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="input-group my-2 col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Nombre persona / Entidad </label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">No. radicado</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalInformacion">Buscar</button>
								<button class="btn btn-primary" type="submit">Crear</button>
							</div>
						</div>
					</form>

					<form action="">
						<div class="card-body borde mt-3">
							<h5 class="title"><u>Remitente</u></h5>
							<div class="row my-2">
								<div class="col-md-6 mt-1">
									<label for="">
										<span>Fergon Outsourcing S.A.S</span>
									</label>
								</div>
								<div class="col-md-6 mt-1">
									<label for="">
										<span>NIT 805.711.458.1</span>
									</label>
								</div>
							</div>
						</div>

						<div class="card-body borde mt-3">
							<h5 class="title"><u>Destinatario</u></h5>

							<div class="row">
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-5">Tipo de identificacion</label>
									<select name="" id="" class="form-control"></select>
								</div>
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">No. documento</label>
									<input type="text" class="form-control col-sm-9 ">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-12">
									<label for="" class="input-group-text bg-light col-sm-3">Nombre entidad</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-12">
									<label for="" class="input-group-text bg-light col-sm-3">Nombre del signatario</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">No. de telefono</label>
									<input type="text" class="form-control col-sm-9">
								</div>
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-3">Direccion</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">Departamento</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
								<div class="input-group  my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-3">Municipio</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
							</div>
							<div class="row">
								<div class="input-group my-2  col-md-12">
									<label for="" class="input-group-text bg-light col-sm-3">Correo electronico</label>
									<input class="form-control" type="email" name="" class="form-control col-sm-9">
								</div>
							</div>	
						</div>
						<div class="card-body borde mt-3">
							<div class="row">
								<div class="col-md-6  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Tipo de radicacion</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Nuevo</option>
										<option value="">Adjunto</option>
										<option value="">Respuesta</option>
									</select>
								</div>
								<div class="col-md-6  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Respuesta a rad</label>
									<input class="form-control col-sm-9" type="text" name="" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Asunto / Referencia</label>
									<textarea name="" id="" rows="2" class="form-control col-sm-9"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-8">No. folios / imagenes </label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
								<div class="col-md-4  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-6">Tipo de anexos</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Memoria USB</option>
										<option value="">CD'S</option>
										<option value="">Carpeta</option>
									</select>
								</div>
								<div class="col-md-4 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">No. anexos</label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-6">Fecha del documento</label>
									<input type="date" name="" id="" class="form-control col-sm-9" >
								</div>
								<div class="col-md-5 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Hora <i class="ml-2 mdi mdi-clock"></i></label>
									<input type="time" name="" id="" class="form-control col-sm-9" disabled>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Nombre de quien radica</label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
							</div>
						</div>
						
						<div class="card-body borde mt-3">
							<div class="row my-1">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-4">Dependencia</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-4">Expediente</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
							</div>
						</div>

						<div class="row  my-3 ml-2">
							<div class="input-group col-md-4">
								<label for="" class="input-group-text bg-transparent col-sm-3">Adjuntar</label>
								<input type="file" name="" id="" class="form-control">
							</div>
						</div>
						

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Radicar</button>
							</div>
						</div>


					</form>
                </div>
            </div>
        </div>
    </div>
@endsection