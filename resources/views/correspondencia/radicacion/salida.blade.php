
@extends('layouts.app')

@section('titulo')
Correspondencia
@endsection


@section('contenido')
<div class="row">
	<div class="col-md-12 grid-margin">
		<div class="d-flex justify-content-between flex-wrap">
			<div class="d-flex align-items-end flex-wrap">
				<div class="d-flex">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb p-2">
							<li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
							<li class="breadcrumb-item"><a href="{{route('correspondencia.index')}}">Correspondencia</a></li>
                            <li class="breadcrumb-item"><a href="{{route('radicacion.index')}}">Radicacion</a></li>
							<li class="breadcrumb-item active" aria-current="page"> <b>Salida</b></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
    <div>
        @include('correspondencia.components.tabs',['select' => 'radicacion' ])
    </div>
    <div>   
        @include('correspondencia.components.tabs_radicacion', ['select' => 'salida'])
    </div>
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					<p class="h4">Radicacion - Salida</p>
					<hr>
					
					<div class="table-responsive">
						<p class="card-title">Listado de visitas</p>
						<table  class="display" id="table" width="100%">
							<thead>
								<th><i class="mdi mdi-checkbox-marked-outline"></i></th>
								<th>Tipo documental</th>
								<th>Nombre documento</th>
								<th>Expediente</th>
								<th>Fecha radicacion</th>
								<th></th>
							</thead>
							<tbody>
								<tr class="body-rows">
									<td><input type="checkbox" class="form-check select_lugar" value=""></td>
									<td class="text-capitalize">contrato</td>
									<td>contrato evelyn martinez</td>
									<td>evelin martinez</td>
									<td>15-05-2021</td>
									<td><button class="btn-show-form" type="button"></button></td>
								</tr>
								<tr class="body-rows">
									<td><input type="checkbox" class="form-check select_lugar" value=""></td>
									<td class="text-capitalize">comunicaciones internas</td>
									<td>circular 006</td>
									<td>circulares PE&S 2021</td>
									<td>15-06-2021</td>
									<td><button class="btn-show-form" type="button"></button></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="row mt-3 justify-content-md-center">
						<div class="form-group">
							<button class="btn btn-primary" type="button">Radicar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection