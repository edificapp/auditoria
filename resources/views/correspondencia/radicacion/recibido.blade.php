
@extends('layouts.app')


@section('titulo')
    Correspondencia
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								<li class="breadcrumb-item"><a href="{{route('correspondencia.index')}}">Correspondencia</a></li>
                                <li class="breadcrumb-item"><a href="{{route('radicacion.index')}}">Radicacion</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Recibido</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('correspondencia.components.tabs',['select' => 'radicacion' ])
    </div>
    <div>   
        @include('correspondencia.components.tabs_radicacion', ['select' => 'recibido'])
    </div>
	<div class="row">
		<div class="col md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					<p class="h4">radicacion - recibida</p>
					<hr>
					<p class="h5 ml-3 mt-3">Buscar remitente</p>

					<form action="">
						<div class="row">
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Tipo de identificacion</label>
								<select name="" id="" class="form-control col-sm-9">
									<option value="">NIT</option>
									<option value="">Cedula</option>
									<option value="">Cedula de extranjeria</option>
								</select>
							</div>
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">No. documento</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="input-group my-2 col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Nombre persona / Entidad </label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">Tipo documental</label>
								<select name="" id="" class="form-control col-sm-9">
									<option value="">Carta</option>
									<option value="">Oficio</option>
									<option value="">Cuenta</option>
									<option value="">Derecho de peticion</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="input-group my-2 col-md-6">
								<label for="" class="input-group-text bg-light col-sm-4">No. radicado</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalInformacion">Buscar</button>
								<button class="btn btn-primary" type="submit">Crear</button>
							</div>
						</div>
					</form>

					<form action="">
						<div class="card-body borde">
							<h5 class="title"><u>Remitente</u></h5>
							<div class="row">
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">Tipo persona</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Natural</option>
										<option value="">Juridico</option>
										<option value="">Anonimo</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-5">Tipo de identificacion</label>
									<select name="" id="" class="form-control"></select>
								</div>
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">No. documento</label>
									<input type="text" class="form-control col-sm-9 ">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-12">
									<label for="" class="input-group-text bg-light col-sm-3">Nombre entidad / Persona</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-12">
									<label for="" class="input-group-text bg-light col-sm-3">Nombre del signatario</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">No. de telefono</label>
									<input type="text" class="form-control col-sm-9">
								</div>
								<div class="input-group my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-3">Direccion</label>
									<input type="text" class="form-control col-sm-9">
								</div>
							</div>
							<div class="row">
								<div class="input-group  my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-4">Departamento</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
								<div class="input-group  my-2 col-md-6">
									<label for="" class="input-group-text bg-light col-sm-3">Municipio</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
							</div>
							<div class="row">
								<div class="input-group my-2  col-md-12">
									<label for="" class="input-group-text bg-light col-sm-3">Correo electronico</label>
									<input class="form-control" type="email" name="" class="form-control col-sm-9">
								</div>
							</div>			
						</div>
						
						<div class="card-body borde mt-3">
							<div class="row">
								<div class="col-md-6  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Tipo de radicacion</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Nuevo</option>
										<option value="">Adjunto</option>
										<option value="">Respuesta</option>
									</select>
								</div>
								<div class="col-md-6  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Respuesta a rad</label>
									<input class="form-control col-sm-9" type="text" name="" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Asunto / Referencia</label>
									<textarea name="" id="" rows="2" class="form-control col-sm-9"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Medio de recepcion</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Correo</option>
										<option value="">Ventanilla</option>
										<option value="">Mensajeria</option> 
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-8">No. folios / imagenes </label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
								<div class="col-md-4  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-6">Tipo de anexos</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Memoria USB</option>
										<option value="">CD'S</option>
										<option value="">Carpeta</option>
									</select>
								</div>
								<div class="col-md-4 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">No. anexos</label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Nombre de quien radica</label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
							</div>
							<div class="row ">
								<div class="col-md-5 my-2  input-group my-3">
									<label for="" class="input-group-text bg-light col-sm-6">Fecha recepcion <i class=" ml-2 mdi mdi-calendar"></i></label>
									<input type="date" name="" id="" class="form-control col-sm-9" disabled>
								</div>
								<div class="col-md-5 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Hora <i class="ml-2 mdi mdi-clock"></i></label>
									<input type="time" name="" id="" class="form-control col-sm-9" disabled>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-6">Fecha del documento</label>
									<input type="date" name="" id="" class="form-control col-sm-9" >
								</div>
							</div>
						</div>
						
						<div class="card-body borde mt-3">
							<div class="row my-1">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-4">Dependencia</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-4">Expediente</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
							</div>
						</div>

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Radicar</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<div id="modalInformacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalInformacion" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				
				<div class="modal-body">
					<form action="">
						<div class="row">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-3">NIT</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-5">Nombre persona / Entidad</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Tipo documental</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-3">No. radicado</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row mt-4 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button class="btn btn-primary" type="button">Siguiente</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection