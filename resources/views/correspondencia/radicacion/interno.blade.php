
@extends('layouts.app')


@section('titulo')
    Correspondencia
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								<li class="breadcrumb-item"><a href="{{route('correspondencia.index')}}">Correspondencia</a></li>
                                <li class="breadcrumb-item"><a href="{{route('radicacion.index')}}">Radicacion</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Interno</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('correspondencia.components.tabs',['select' => 'radicacion' ])
    </div>
    <div>   
        @include('correspondencia.components.tabs_radicacion', ['select' => 'interno'])
    </div>
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="card">
				<div class="card-body">
					<p class="h4">radicacion - Interno</p>
					<hr>

					<form action="">
						<div class="row my-3">
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-2">Enviar a </label>
								<select name="" id="" class="form-control col-sm-8">
									<option value="">Persona</option>
									<option value="">Contabilidad</option>
									<option value="">Area Tecnica</option>
									<option value="">Recepcion</option>
									<option value="">Gerencia</option>
									<option value="">Todos los empleados activos</option>
									<option value="">Todos los contratistas activos</option>
									<option value="">Toda la base de datos</option>
								</select>
							</div>
							<div class="input-group col-md-6">
								<label for="" class="input-group-text bg-light col-sm-2">Nombre</label>
								<select name="" id="" class="form-control col-sm-8">
									<option value="">Diego vivas</option>
									<option value="">Leandro Monero</option>
									<option value="">Katherine Vazquez</option>
								</select>
							</div>
						</div>

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-primary" type="button">Siguiente</button>
								<button class="btn btn-secondary" type="button">Cancelar</button>
							</div>
						</div>
					</form>

					{{--  --}}

					<form action="">
						<div class="card-body borde mt-3">
							<h5 class="title"><u>Destinatario</u></h5>
							<div class="row my-2">
								<div class="col-md-6 mt-1">
									<label for="">
										<span>Evelyn Martinez</span>
									</label>
								</div>
							</div>
						</div>

						
						<div class="card-body borde mt-3">
							<div class="row">
								<div class="col-md-6  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Tipo de radicacion</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Nuevo</option>
										<option value="">Adjunto</option>
										<option value="">Respuesta</option>
									</select>
								</div>
								<div class="col-md-6  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Respuesta a rad</label>
									<input class="form-control col-sm-9" type="text" name="" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12  my-2 input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Asunto / Referencia</label>
									<textarea name="" id="" rows="2" class="form-control col-sm-9"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Medio de recepcion</label>
									<select name="" id="" class="form-control col-sm-9">
										<option value="">Correo</option>
										<option value="">Ventanilla</option>
										<option value="">Mensajeria</option> 
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-6">Fecha del radicado</label>
									<input type="date" name="" id="" class="form-control col-sm-9" >
								</div>
								<div class="col-md-5 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-3">Hora <i class="ml-2 mdi mdi-clock"></i></label>
									<input type="time" name="" id="" class="form-control col-sm-9" disabled>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-5">Nombre de quien radica</label>
									<input class="form-control col-sm-9" type="text" name="">
								</div>
							</div>
						</div>
						
						
						<div class="card-body borde mt-3">
							<div class="row my-1">
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-4">Dependencia</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
								<div class="col-md-6 my-2  input-group">
									<label for="" class="input-group-text bg-light col-sm-4">Expediente</label>
									<select name="" id="" class="form-control col-sm-9"></select>
								</div>
							</div>
						</div>

						<div class="row  my-3 ml-2">
							<div class="input-group col-md-4">
								<label for="" class="input-group-text bg-transparent col-sm-4">Adjuntar</label>
								<input type="file" name="" id="" class="form-control">
							</div>
						</div>
						

						<div class="row mt-3 justify-content-md-center">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Radicar</button>
							</div>
						</div>

					</form>
					{{--  --}}

				</div>
			</div>
		</div>
	</div>
@endsection