
@extends('layouts.app')

@section('titulo')
    Correspondencia
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
								<li class="breadcrumb-item"><a href="#">Correspondencia</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Radicacion</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('correspondencia.components.tabs',['select' => 'radicacion' ])
    </div>
    <div>   
        @include('correspondencia.components.tabs_radicacion', ['select' => ''])
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
                    <div class="row">
						<div class="col-md-6">
							<p class="h4 text-center">Radicados del año</p>
						</div>
						<div class="col-md-6 text-center">
							<p class="h4">Radicados por gestionar</p>
							<div class=" custom-control custom-switch mt-3">
								<label >Porcentual</label>
								<input type="checkbox" class="custom-control-input" id="customSwitch1">
								<label class="custom-control-label ml-5" for="customSwitch1">Nominal</label>
								<div class="row">
									<canvas class="text-left" id="resumenChart" width="100" height="20"></canvas>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			
			var Canvas = document.getElementById("resumenChart");

			Chart.defaults.global.defaultFontFamily = "Roboto";
			Chart.defaults.global.defaultFontSize = 17;

			var Data = {
				labels: [
					"Pendiente asignar %",
					"Sin revisar por dependencias %"
				],
				datasets: [
					{
						data: [30, 70],
						backgroundColor: [
							"#7FB3D5",
							"#73C6B6"
						]
					}]
			};
			var pieChart = new Chart(Canvas, {
			type: 'pie',
			data: Data
			});
		});
	</script>
@stop
