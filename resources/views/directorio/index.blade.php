@extends('layouts.app')

@section('titulo')
informacion de la empresa
@endsection



{{-- @section('breadcrumbs')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Directorio</li>
      </ol>
    </nav>
@stop --}}

@section('contenido')

    
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <a href="{{ route('home') }}"><i class="mdi mdi-home breadcrumbs hover-cursor"></i></a>
                        <p class="breadcrumbs mb-0 hover-cursor">&nbsp;/&nbsp;Directorio&nbsp;/&nbsp;</p>
                    </div>
                </div>
                <div class="d-flex justify-content-around align-items-end flex-wrap">
                    <button type="button" class="btn-create-form" id="btn_new">Agregar Contacto</button>
                </div>    
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body borde">
                    <div class="my-3">
                        <p class="card-title">Listado Contactos</p>
                        <div class="table-responsive">
                            <table id="table" class="display" width="100%">
                                <thead>
                                    <th>Entidad</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Celular</th>
                                    <th class="text-center">Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($contactos as $item)
                                        <tr class="body-rows">
                                            <td>
                                                <img src="{{$item->avatar}}" style="margin-right: 8px;" width="25px" /> 
                                                {{$item->contacto->nombre}}
                                            </td>
                                            <td>{{$item->nombres}}</td>
                                            <td>{{$item->apellidos}}</td>
                                            <td>{{$item->celular}}</td>
                                            <td class="text-center">
                                                <div class="btn-group col-sm">
                                                    <button class="btn-show-form" 
                                                            type="button" 
                                                            onclick="btn_show('{{route('directorio.show', $item->id)}}')"
                                                            title="Ver contacto">
                                                    </button>
                                                    <button class="btn-edit-form" 
                                                            type="button" 
                                                            onclick="btn_edit('{{route('directorio.show', $item->id)}}', '{{route('directorio.update', $item->id)}}')"
                                                            title="Editar Contacto.">
                                                    </button>
                                                    <button class="btn btn-xs btn-rounded btn-inverse-danger" 
                                                            onclick="location.href='{{ route('directorios.activo', $item->id) }}';" 
                                                            title="Eliminar Contacto">
                                                                <i class="mdi mdi-delete"></i>
                                                    </button>
                                            </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalContacto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header white">
                    <h4 class="modal-title white text-center" id="tituloFormContacto"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form  action="{{route('directorio.store')}}" method="post"  id="formContactos">
                        @csrf
                        <input type="hidden" name="tipo_contacto" id="tipo_contacto">
                        <div class="row my-3">
                            <div class="input-group col-6">
                                <label for="nombres" class="input-group-text col-sm-3 bg-light">Nombres: </label>
                                <input 
                                    for="nombres"
                                    type="text" 
                                    name="nombres"
                                    id="nombres"
                                    class="form-control col-sm-9 @error('nombres') is-invalid @enderror"  
                                    placeholder="Nombres" 
                                    value="{{old('nombres')}}"
                                >
                                <br>
                                @error('nombres')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                            <div class="input-group col-6">
                                <label for="apellidos" class="input-group-text col-sm-3 bg-light">Apellidos: </label>
                                <input 
                                     for="apellidos"
                                    type="text" 
                                    name="apellidos"
                                    id="apellidos"
                                    class="form-control col-sm-9 @error('apellidos') is-invalid @enderror"  
                                    placeholder="Apellidos" 
                                    value="{{old('apellidos')}}"
                                >
                                <br>
                                @error('apellidos')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-4">
                                <label for="celular" class="input-group-text col-sm-4 bg-light">Celular: </label>
                                <input 
                                    for="celular" 
                                    type="text" 
                                    name="celular"
                                    id="celular"
                                    class="form-control col-sm-9 @error('celular') is-invalid @enderror"  
                                    placeholder="Celular" 
                                    value="{{old('celular')}}"
                                >
                                <br>
                                @error('celular')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                            <div class="input-group col-4">
                                <label for="telefono" class="input-group-text col-sm-5 bg-light">Telefono: </label>
                                <input 
                                     for="telefono"
                                    type="text" 
                                    name="telefono"
                                    id="telefono"
                                    class="form-control col-sm-9 @error('telefono') is-invalid @enderror"  
                                    placeholder="Telefono" 
                                    value="{{old('telefono')}}"
                                >
                                <br>
                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                            <div class="input-group col-4">
                                <label for="ext" class="input-group-text col-sm-5 bg-light">Extensión: </label>
                                <input 
                                    form="ext"
                                    type="text" 
                                    name="ext"
                                    id="ext"
                                    class="form-control col-sm-8 @error('ext') is-invalid @enderror"  
                                    placeholder="Extensión" 
                                    value="{{old('ext')}}"
                                >
                                <br>
                                @error('ext')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-7">
                                <label for="email" class="input-group-text col-sm-3 bg-light">Correo: </label>
                                <input
                                    for="email" 
                                    type="email" 
                                    name="email"
                                    id="email"
                                    class="form-control col-sm-9 @error('email') is-invalid @enderror"  
                                    placeholder="Correo Electronico" 
                                    value="{{old('email')}}"
                                >
                                <br>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                            <div class="input-group col-5">
                                <label for="cargo" class="input-group-text col-sm-3 bg-light">Cargo: </label>
                                <input
                                    for="cargo"
                                    type="text" 
                                    name="cargo"
                                    id="cargo"
                                    class="form-control col-sm-9 @error('cargo') is-invalid @enderror"  
                                    placeholder="Cargo" 
                                    value="{{old('cargo')}}"
                                >
                                <br>
                                @error('cargo')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                            </div>
                        </div>
                        <div class="row my-3" id="div_entidad">
                            <div class="input-group col-7">
                                <label for="entidad" class="input-group-text col-sm-3 bg-light">Entidad: </label>
                                <select for="entidad" class="form-control col-sm-10" name="entidad" id="entidad">
                                    @foreach(Auth::user()->empresa->entidades() as $item)
                                        <option value="{{$item['select']}}">{{$item['nombre']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row my-3 justify-content-md-center">
                            <div class="form-group">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary" id="btn_save">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>  
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        const fields = ['nombres', 'apellidos', 'email', 'celular', 'telefono', 'ext', 'cargo'];

        $(document).ready(function(){
            @if($errors->any())
                $('#modalContacto').modal('show');
            @endif 
        });

        $('#btn_new').on('click', function(){
            $('#formContactos').attr('action', "{{route('directorio.store')}}");
            $('#_method').hide();
            $('#modalContacto').modal();
            $('#div_entidad').show();
            $('#btn_save').show();
            $('#tituloFormContacto').text('Nuevo Contacto');
            emptyFields();
        });

        function btn_edit(url_show, url_update){
            $('#formContactos').attr('action', url_update).append('<input type="hidden" value="patch" name="_method" id="_method">')
            $('#modalContacto').modal();
            $('#btn_save').show();
            $('#div_entidad').hide();
            $('#tituloFormContacto').text('Editar Contacto');
            $.get(url_show, function(rep) {
                $.each(fields, function( index, value ) {
                  $('#'+value).val(rep[value]).attr('readonly', false);
                });
            });
        }

        function btn_show(url_show){
            $('#formContactos').attr('action', '');
            $('#_method').hide();
            $('#modalContacto').modal();
            $('#div_entidad').hide();
            $('#btn_save').hide();
            $('#tituloFormContacto').text('Ver Contacto');
            $.get(url_show, function(rep) {
                $.each(fields, function( index, value ) {
                  $('#'+value).val(rep[value]).attr('readonly', true);
                });
            });
        }

        function emptyFields(){
            $('#entidad option').each(function(i, e){ e.selected = false });
            $.each(fields, function( index, value ) {
              $('#'+value).val('').attr('readonly', false);
            });
        }
    </script>
@stop
