@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Inicio</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card p-3">
            <div class="card-heard text-center">
                Contratistas del proyecto {{$proyecto->nombbre}}.
            </div>
        </div>
    </div>
</div>
@foreach($proyecto->contratistas as $item)
<div class="row">
    <a href="{{route('documentos.espectador', [$item->id, 'contratista'])}}">
        <div class="col-xs-12 col-sm-6 col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <img src="{{asset('img/folder.png')}}" class="img-fluid">
                     <b>
                        <small>
                            {{$item->contratista->nombre}}
                        </small>
                    </b>
                </div>
            </div>
        </div>
    </a>
</div>
@endforeach

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card p-3">
            <div class="card-heard text-center">
                Personas de Terceros del proyecto {{$proyecto->nombbre}}.
            </div>
        </div>
    </div>
</div>
@foreach($proyecto->terceros as $item)
<div class="row">
    <a href="{{route('documentos.espectador', [$item->id, 'tercero'])}}">
        <div class="col-xs-12 col-sm-6 col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <img src="{{asset('img/folder.png')}}" class="img-fluid">
                     <b>
                        <small>
                            {{$item->tercero->razon_social}}
                        </small>
                    </b>
                </div>
            </div>
        </div>
    </a>
</div>
@endforeach
@endsection

@section('scripts')
<script>

    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
