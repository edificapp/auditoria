@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Inicio</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-center">Mis Documentos del projecto {{$directorio->proyecto->nombre}}</p>
             </div>
        </div>
    </div>
</div>
<div class="row justify-content-md-center">
    <div class="col-md-10 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive text-center">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($directorio->radicados as $radicado)
                                @foreach ($radicado->documentos as $documento)
                                <tr>
                                   <td>
                                       <a class="nav-link" href="{{ $documento->ruta_storage }}" target="_blank">
                                            <i class="mdi {{$documento->tipo($documento->name_document['type'])}} "></i>
                                            <span class="menu-title">{{$documento->name_document['name']}}</span>
                                        </a>
                                    </td>

                                    <td>
                                        {{ $documento->created_at}}
                                    </td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
             </div>
        </div>
    </div>
</div>

           
@endsection

@section('scripts')
<script>

    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
