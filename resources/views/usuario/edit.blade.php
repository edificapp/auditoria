@extends('layouts.app')

@section('titulo')
Completar registro
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('usuarios.index') }}">Usuarios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Actualizar Usuario</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <form method="POST" action="{{ route('usuarios.update', $usuario->id) }}">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="row">

                        <div class="col-md-6" >
                            <div class="input-group my-3">
                                <label for="usuario" class="input-group-text col-sm-4 bg-light">Nombre de usuario</label>
                                <input type="text" class="form-control col-sm-9 @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! old('usuario') ? old('usuario') : $usuario->usuario !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="nombre" class="input-group-text col-sm-4 bg-light">Nombre completo</label>
                                <input type="text" class="form-control col-sm-9 @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{!! old('nombre') ? old('nombre') : $usuario->nombre !!}" required placeholder="Nombre completo">

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="email" class="input-group-text col-sm-4 bg-light">Correo electronico</label>
                                <input type="email" class="form-control col-sm-9 @error('email') is-invalid @enderror" id="email" name="email" value="{!! old('email') ? old('email') : $usuario->email !!}" required placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group my-3">
                                    <label for="rol" class="input-group-text  col-sm-4 bg-light">Rol de usuario</label>
                                
                                <select class="custom-select" for="rol" name="roles[]" multiple="multiple">
                                    @foreach (auth()->user()->empresa->roles as $rol)
                                        <option value="{!! $rol->id !!}" 
                                            @if(old('roles')) 
                                                {{in_array($rol->id, old('roles')) ? 'selected' : '' }}
                                            @else
                                                {{ $usuario->getRoleNames()->contains($rol->name) ? 'selected' : ''}}
                                            @endif
                                        >
                                            {!! $rol->name !!}
                                        </option>
                                    @endforeach
                                </select>
                                @error('roles')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="seccion_password">
                        <hr>
                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="password" class="input-group-text col-sm-4 bg-light">Contraseña</label>
                                <input type="password" class="form-control col-sm-9" id="password" name="password" placeholder="Contraseña" autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="input-group my-3">
                                <label for="password_confirmation" class="input-group-text col-sm-5 bg-light">Confirmar Contraseña</label>
                                <input type="password" class="form-control col-sm-9" id="password_confirmation" name="password_confirmation" placeholder="Contraseña" autocomplete="new-password">
                            </div>
                        </div>
                    </div>

                    <div class="row my-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Regresar</button>
                            <button type="submit" class="btn btn-primary">Actualizar Usuario</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        $('.select-multiple').select2();
    });
</script>
@endsection
