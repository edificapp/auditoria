@extends('layouts.app')

@section('titulo')
Usuarios
@endsection

{{-- @section('styles')
    <style> 
        th { white-space:nowrap; }

        table.dataTable.display thead th{
            font-size: 15px;
            width:12px;
            padding: 12px;
            padding-left:10px;
            padding-top: 10px;
            text-align: left;
            border-style: none;
            border-bottom: 1px solid #808B96;
        }

        table.dataTable.display tbody td {
            padding: 4px;
            padding-left:10px;
            font-size: 13px;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            /* border-style: none;
            border-bottom: 1px solid #D5DBDB; */
        }
        table.dataTable.display tbody tr.group td{
            text-align: left;
            font-size: 14px;
            font-weight: bold;
            padding: 3px; 
        }
        .body-rows{
        background-color:#FDFEFE !important;
        }
    </style>
@stop --}}

@section('contenido')

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item active"><a href="{{ route('usuarios.index') }}"><b>Usuarios</b></a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                @can('crear-empleados')
                    <div class="d-flex justify-content-around align-items-end flex-wrap">
                        <button type="button" onclick="location.href='{{ route('usuarios.create') }}';" class="btn-create-form">Crear usuario</button>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    @if(!is_null(auth()->user()->empresas_id))
        <ul class="nav nav-pills bg-transparent">
            <li class="nav-item">
                    <a href="{{route('usuarios.index')}}" class="nav-link active">
                        Gestionar  Usuarios
                    </a>
            </li>
            <li class="nav-item">
                    <a href="{{route('usuarios.filtrar')}}" class="nav-link" >
                        Consultas
                    </a>
            </li>
        </ul>
    @endif
    <hr style="margin-top:0; margin-bottom: 1px; border:none; ">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body borde" >
                    <p class="card-title">Listado de usuarios</p>

                    <div class="table-responsive">
                        <table id="table" class="display" width="100%">
                            <thead>
                                <tr>
                                    <th >Nombre de usuario</th>
                                    <th >Roles</th>
                                    <th >Correo electronico</th>
                                    <th >Estado</th>
                                    <th >Acciones</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach ($usuarios as $usuario)
                                <tr class="body-rows">
                                    <td class="text-capitalize" >{{strtolower($usuario->name)}}</td>
                                    <td >@foreach($usuario->getRoleNames() as $rol) {{--<div class="badge badge-outline-info badge-pill">--}}{{$rol}} {{--</div>--}} @endforeach </td>
                                    <td >{!! $usuario->email !!}</td>
                                    <td >
                                        @if ($usuario->inv_activa == 'no')
                                        {!! $usuario->activo == 'si' ? 'Activo' : 'Inactivo' !!}
                                        @else
                                        Inactivo
                                        @endif
                                    </td >
                                    <td class="text-center">
                                        <div class="btn-group col-sm">
                                            @if ($usuario->inv_activa == 'no')
                                                    <a href="{{route('personificacion', $usuario->id)}}" class="btn btn-sm btn-inverse-secondary btn-rounded">
                                                        <i class="mdi mdi-account-multiple-outline"></i>
                                                    </a>
                                                    @can('ver-empleados')
                                            {{----}}<button class="btn-show-form" 
                                                            type="button" 
                                                            onclick="location.href='{{ route('curriculum_ver',$usuario->id) }}';" 
                                                            title="Ver hoja de vida.">
                                                    </button>
                                                    @endcan
                                                    @can('actualizar-empleados')
                                            {{----}}<button class="btn-edit-form" 
                                                            type="button" 
                                                            onclick="location.href='{{ Auth::user()->id == $usuario->id ? route('perfil') : route('usuarios.edit',$usuario->id) }}';" 
                                                            title="Editar Empleado.">
                                                    </button>
                                                    @endcan
                                                    @can('activar-empleados')
                                            {{----}}@if (Auth::user()->id != $usuario->id)
                                                        <button class="{{$usuario->activo == 'no' ? 'btn-activo-form' : 'btn-inactivo-form'}}" 
                                                                onclick="location.href='{{ route('usuario_estado',$usuario->id) }}';" 
                                                                title="Habilitar|Desabilitar Empleado">
                                                        </button>
                                            {{----}}@endif
                                            
                                                    @endcan
                                            @else
                                                    @can('crear-empleados')
                                            {{----}}<button class="btn btn-xs btn-inverse-secondary btn-rounded" 
                                                            onclick="location.href='{{ route('enviar_invitacion',$usuario->id) }}';" 
                                                            title="Reenviar invitación al correo">
                                                                <i class="mdi mdi-email"></i>
                                                    </button>
                                                    @endcan
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // var groupColumn = 0;
        // $('#table_users').DataTable ({
        //     "language": {
        //         "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        //     },
        //     paging:false, info: false
        // });    
    </script>
@endsection
