@extends('layouts.app')

@section('titulo')
Empleados
@endsection

@section('styles')
    <style> 
        th { white-space:nowrap; }

        /* ESTILOS BOTONES EXPORTAR */

        .btn {
            /* display: inline-block; */
            font-size: 14px;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            user-select: none;
            background-image: none;
            border-radius: 4px;
        }


        .btn-app {
            color: white;
            box-shadow: none;
            border-radius: 3px;
            position: relative;
            padding: 10px 15px;
            margin: 0;
            min-width: 60px;
            max-width: 80px;
            text-align: center;
            border: 1px solid #ddd;
            background-color: #f4f4f4;
            font-size: 12px;
            font-family: Arial, Helvetica, sans-serif;
            transition: all .2s;
            background-color:#909497 !important;
        }


        .btn-app:hover {
            border-color: #aaa;
            transform: scale(1.1);
            color: #FDFEFE;
        }

        .pdf {
            background-color: #e03434 !important;
        }

        .excel {
            background-color: #3ca23c !important;
        }

    </style>
@stop
@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item "><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('usuarios.index')}}">Usuarios</a></li>
                                <li class="breadcrumb-item "><a href="{{route('usuarios.filtrar')}}"><b>Consultas</b></a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                @can('crear-empleados')
                    <div class="d-flex justify-content-around align-items-end flex-wrap">
                        <button type="button" onclick="location.href='{{ route('usuarios.create') }}';" class="btn-create-form">Crear usuario</button>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    @if(!is_null(auth()->user()->empresas_id))
        <ul class="nav nav-pills bg-transparent">
            <li class="nav-item">
                    <a href="{{route('usuarios.index')}}" class="nav-link ">
                        Gestionar  Empleados
                    </a>
            </li>
            <li class="nav-item">
                    <a href="{{route('usuarios.filtrar')}}" class="nav-link active" >
                        Consultas
                    </a>
            </li>
        </ul>
    @endif
    <hr style="margin-top:0; margin-bottom: 1px; border:none; ">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body borde">
                    <p class="card-title">Filtrar Usuarios</p>
                    <div>
                        <div class="dropdown col-lg-12 marginbottom10" style="">
                            <button  class="btn dropdown-toggle" type="button" data-toggle="dropdown">Columnas
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" style="margin-left: 10px; padding:20px;">
                                <li><input type="checkbox" data-column="0" checked> Nombre de usuario</li>
                                <li><input type="checkbox" data-column="1" checked> Edad</li>
                                <li><input type="checkbox" data-column="2" checked> Genero</li>
                                <li><input type="checkbox" data-column="3" checked> Experiencia Laboral</li>
                                <li><input type="checkbox" data-column="4" checked> Tiempo de Experiencia</li>
                                <li><input type="checkbox" data-column="5"> Estudios Profesionales</li>
                                <li><input type="checkbox" data-column="6"> Tiempo de Estudios (semestres)</li>
                                <li><input type="checkbox" data-column="7"> Otros Estudios</li>
                                <li><input type="checkbox" data-column="8"> Tiempo de Estudios (horas)</li>
                                <li><input type="checkbox" data-column="9"> Logros</li>
                                <li><input type="checkbox" data-column="10"> Idiomas</li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tablaUsuarios" class="table display" width="100%">
                            <thead>
                                <tr>
                                    <th>Nombre de usuario</th>
                                    <th>Edad</th>
                                    <th>Genero</th>
                                    <th>Experiencia Laboral</th>
                                    <th>Tiempo de Experiencia</th>
                                    <th>Estudios Profesionales</th>
                                    <th>Tiempo de Estudios (Semestres)</th>
                                    <th>Otros Estudios</th>
                                    <th>Tiempo de Estudios (Horas)</th>
                                    <th>Logros</th>
                                    <th>Idiomas</th>
                                </tr>
                            </thead>
                            
                            <tbody>

                                @foreach ($usuarios as $i => $usuario)
                                <tr class="body-rows">
                                    <td class="text-left">
                                        @if($usuario->curriculum)
                                            <a href="{{ route('curriculum_ver',$usuario->id) }}" class="btn btn-link">
                                                {{$usuario->curriculum->full_name}}
                                            </a>
                                        @else
                                           {{$usuario->name}}
                                        @endif
                                    </td>
                                    <td>
                                        {{strtolower($usuario->curriculum ? $usuario->curriculum->edad : 'No tiene Hoja de vida')}}
                                    </td>
                                    <td>
                                        {{strtolower($usuario->curriculum ? $usuario->curriculum->full_genero : 'No tiene Hoja de vida')}}
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->laborales->count() > 0)
                                                @foreach($usuario->curriculum->laborales as $laboral)
                                                    <span>- {{$laboral->cargo}} Tiempo: {{Helper::timeOfAgeMonths($laboral->tiempo_meses)}}</span><br>
                                                @endforeach
                                            @else
                                                No tiene Experiencia Laboral
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->laborales->count() > 0)
                                                {{Helper::timeOfAgeMonths($usuario->curriculum->laborales->sum('tiempo_meses'))}}  
                                            @else
                                                No tiene Experiencia Laboral
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->estudios->count() > 0)
                                                @foreach($usuario->curriculum->estudios as $estudio)
                                                    <span>- {{$estudio->estudio}} semestres: {{$estudio->semestres}}</span><br>
                                                @endforeach
                                            @else
                                                No tiene Estudios Superiores
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->estudios->count() > 0)
                                                {{$usuario->curriculum->estudios->sum('semestres')}} Semestres 
                                            @else
                                                No tiene Estudios Superiores
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->otrosEstudios->count() > 0)
                                                @foreach($usuario->curriculum->otrosEstudios as $otrosEstudios)
                                                    <span>- {{$otrosEstudios->estudio}} Horas: {{$otrosEstudios->horas}}</span><br>
                                                @endforeach
                                            @else
                                                No tiene Estudios Superiores
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->otrosEstudios->count() > 0)
                                                {{$usuario->curriculum->otrosEstudios->sum('horas')}} Horas
                                            @else
                                                No tiene Estudios Superiores
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->logros->count() > 0)
                                                @foreach($usuario->curriculum->logros as $logro)
                                                    <span>- {{$logro->logro}}</span><br>
                                                @endforeach
                                            @else
                                                No tiene Logros
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                    <td>
                                        @if($usuario->curriculum)
                                            @if($usuario->curriculum->idiomas->count() > 0)
                                                @foreach($usuario->curriculum->idiomas as $idioma)
                                                    <span>- {{$idioma->idioma}}</span><br>
                                                @endforeach
                                            @else
                                                No tiene Idiomas
                                            @endif
                                        @else
                                            No tiene Hoja de vida
                                        @endif
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('body').addClass('sidebar-icon-only');
        $('#tablaUsuarios thead tr').clone(true).appendTo( '#tablaUsuarios thead' );
        $('#tablaUsuarios thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
       var table =  $('#tablaUsuarios').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            filters:false,
            "columnDefs" : [
                { "targets": [5,6,7,8,9,10], "visible": false}
            ],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: "copyHtml5",
                    text:      '<i class="mdi mdi-content-copy"></i>Copiar',
                    title:'Consultas Usuarios',
                    titleAttr: 'Copiar',
                    className: 'btn btn-app export barras',
                    exportOptions: {
                        columns: ':not(.no-print):visible',
                        format: {
                          body: function(data, row, column, node) {
                            data_string = data.replace(/<[^>]+>/g, '');
                            return data_string;
                          },
                          header: function ( data, column, row ) {
                            return data;
                          },
                          footer: function(data, column) {
                            return data;
                          }
                        }   
                    }
                },
                {
                    extend: "excelHtml5",
                    text:   '<i class="mdi mdi-file-excel"></i>Excel',
                    title:'Consultas Usuarios',
                    titleAttr: 'Exportar a Excel',
                    className: 'btn btn-app export excel',
                    exportOptions: {
                        columns: ':not(.no-print):visible',
                        format: {
                          body: function(data, row, column, node) {
                            data_string = data.replace(/<[^>]+>/g, '');
                            return data_string;
                          },
                          header: function ( data, column, row ) {
                            return data;
                          },
                          footer: function(data, column) {
                            return data;
                          }
                        }   
                    }
                },
                {
                    
                    extend: "pdfHtml5",
                    text: '<i class="mdi mdi-file-pdf"></i>PDF',
                    title:'Consultas Usuarios',
                    titleAttr: 'Exportar a PDF',
                    className: 'btn btn-app export pdf',
                    exportOptions: {
                        columns: ':not(.no-print):visible',
                        format: {
                          body: function(data, row, column, node) {
                            data_string = data.replace(/<[^>]+>/g, '');
                            return data_string;
                          },
                          header: function ( data, column, row ) {
                            return data;
                          },
                          footer: function(data, column) {
                            return data;
                          }
                        }   
                    }
                },
            ]
        });

       $('[data-column]').on('click', function (e) {
 
            console.log($(this).data('column'))
            var column = table.column($(this).data('column'));
            console.log(column)
 
            // Toggle the visibility
            column.visible(!column.visible());
            console.log(table.column());
 
            return true
        });
    });
</script>
@endsection
