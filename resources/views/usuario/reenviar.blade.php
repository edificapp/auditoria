@extends('layouts.app')

@section('titulo')
Crear usuario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('usuarios.index')}}">Usuarios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Reenviar invitación de usuario</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <form method="POST" action="{{ route('reenviar_invitacion',$usuario->id) }}">
                    @csrf

                    <input type="hidden" name="inv_activa" id="inv_activa" value="si">

                    <h4 class="card-title">Información personal</h4>
                    <p class="card-description">
                        Se completa esta información para asi enviar la invitación via correo electronico al usuario.
                    </p>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usuario">Nombre de usuario</label>
                                <input type="text" class="form-control @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! $usuario->nombre !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{!! $usuario->email !!}" required placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Reenviar invitación</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#rol').on('change', function(event) {
            let roles = $(this).find('option');
            $.each(roles, function(index, val) {
                if ($(val).is(':checked')) {
                    let rol = $(val).html();
                    if (rol == "Administrador") {
                        $('.div_empresa').show();
                    } else {
                        $('.div_empresa').hide();
                    }
                }
            });
        });

        $('#_invitacion').change();
        $('#rol').change();
        $('#empresa').change();

    });
</script>
@endsection