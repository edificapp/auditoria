@extends('layouts.app')

@section('titulo')
Crear usuario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('usuarios.index') }}">Usuarios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Crear usuario</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <form method="POST" action="{{ route('usuarios.store') }}">
                    @csrf

                    <input type="hidden" name="inv_activa" id="inv_activa" value="si">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="input-group-text  bg-light col-sm-5"> Rol de usuario </label>
                            <select class="select-multiple form-control " name="roles[]" multiple="multiple">
                                @foreach (auth()->user()->empresa->roles as $rol)
                                    <option value="{!! $rol->id !!}" @if(old('roles')) {{in_array($rol->id, old('roles')) ? 'selected' : '' }} @endif>{!! $rol->name !!}</option>
                                @endforeach
                            </select>
                            @error('roles')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <hr>

                    <h4 class="card-title">Información personal</h4>
                    <p class="card-description">
                        Se completa esta información para asi enviar la invitación via correo electronico al usuario.
                    </p>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="usuario" class="input-group-text bg-light col-sm-5">Nombre de usuario</label>
                                <input type="text" class="form-control col-sm-9 @error('name') is-invalid @enderror" id="usuario" name="name" value="{!! old('name') !!}" placeholder="Nombre de usuario">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="email" class="input-group-text bg-light col-sm-5">Correo electronico</label>
                                <input type="email" class="form-control col-sm-9 @error('email') is-invalid @enderror" id="email" name="email" value="{!! old('email') !!}" required placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row my-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Regresar</button>
                            <button type="submit" class="btn btn-primary">Crear usuario</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('.select-multiple').select2();
    });
</script>
@endsection
