@extends('layouts.app')

@section('titulo')
Perfil
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('usuarios.index')}}">Usuarios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Perfil</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <h4 class="card-title">Información personal</h4>
                <p class="card-description">
                    Aqui se gestiona tu información basica.
                </p>
                <form method="POST" action="{{ route('update_perfil',$usuario->id) }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="usuario" class="input-group-text bg-light col-sm-4">Nombre de usuario</label>
                                <input type="text" class="form-control col-sm-9 @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! old('usuario') ? old('usuario') : $usuario->usuario !!}" placeholder="Nombre de usuario">
                                
                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="nombre" class="input-group-text bg-light col-sm-4">Nombre completo</label>
                                <input type="text" class="form-control col-sm-9 @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{!! old('nombre') ? old('nombre') : $usuario->nombre !!}" placeholder="Nombre completo">

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="email" class="input-group-text bg-light col-sm-4">Correo electronico</label>
                                <input type="email" class="form-control col-sm-9 @error('email') is-invalid @enderror" id="email" name="email" value="{!! old('email') ? old('email') : $usuario->email !!}" placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>

                    <p class="card-description">
                        Si no desea actualizar la contraseña, deje en blanco ambos campos.
                    </p>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="input-group my-3">
                                <label class="input-group-text bg-light col-sm-4" for="password">Contraseña</label>
                                <input type="password" class="form-control col-sm-9" id="password" name="password" placeholder="Contraseña" autocomplete="new-password">
                                
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="input-group my-3">
                                <label class="input-group-text bg-light col-sm-4" for="password_confirmation">Confirmar Contraseña</label>
                                <input type="password" class="form-control col-sm-9" id="password_confirmation" name="password_confirmation" placeholder="Contraseña" autocomplete="new-password">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="form-group">
                            <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Regresar</button>
                            <button type="submit" class="btn btn-primary">Actualizar información</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
