@extends('layouts.app')

@section('titulo')
Completar registro
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('usuarios.index') }}">Usuarios</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Completar registro</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <form id="form_forms" method="POST" action="{{ route('validar_update',$usuario->id) }}">
                    @csrf

                    <input type="hidden" value="no" id="inv_activa" name="inv_activa">
                    <input type="hidden" value="{!! $key !!}" id="_key" name="_key">

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h4 class="card-title">Información personal</h4>
                            <p class="card-description">
                                Completa los datos.
                            </p>
                            <hr>

                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control" readonly value="{!! $usuario->email !!}" placeholder="Correo electronico">
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    @if ($usuario->empresa)
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="empresa">Empresa</label>
                                <input type="empresa" class="form-control" readonly value="{!! $usuario->empresa->nombre !!}" placeholder="Correo electronico">
                            </div>
                        </div>
                        <div class="col-md-3"></div>                        
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usuario">Nombre de usuario</label>
                                <input type="text" class="form-control @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! $usuario->nombre !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password_confirmation">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Contraseña" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Finalizar registro</button>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection