@extends('layouts.app')

@section('titulo')
Crear formulario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('formularios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Formularios&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Ver formulario</p>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <div class="btn-group">
                    <button type="button" onclick="location.href='{{ route('formularios.index') }}';" class="btn btn-secondary mt-2 mt-xl-0">Volver</button>
                    <button type="button" onclick="location.href='';" class="btn btn-warning mt-2 mt-xl-0">Clonar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <h4>Formularios evaluados</h4>
                        <ul class="list-group">
                            @foreach ($formularios as $formulario)
                            
                            <li class="list-group-item">{!! $formulario->nombre !!} - Completado</li>
                            
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-12">
                        <hr>
                        <center>
                            <h4>Firma</h4>
                        </center>
                        <hr>

                        <form method="post" action="{{ route('visitas.firmar',$visita->id) }}">
                            @csrf
                            <input type="hidden" name="pos" id="pos">
                            <div class="row justify-content-md-center">
                                <div class="col col-xs-11 col-sm-8 col-md-5">
                                    <div class="form-group">
                                        <center>
                                            <div id="signature"></div>
                                            {{-- <canvas style="width: 100%; border: solid black 1px; height: auto;" id="cnv" name="cnv"></canvas> --}}
                                            <button type="button" class="btn btn-danger btn-sm" id="btn_firma_clear"><i class="mdi mdi-close"></i>Limpiar</button>
                                        </center>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="dataFirma" id="dataFirma">
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Guardar firma y finalizar proceso</button>
                            </div>
                        </form>

                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
<script src="{{ asset('admin/plugins/signature/flashcanvas.js')}}"></script>
<script src="{{ asset('admin/plugins/signature/jSignature.min.js')}}"></script>
<script>
    var imgWidth;
    var imgHeight;
    let lon = '';
    let lat = '';
    let error = '';


    $(document).ready(function() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(objPosition) {
                lon = objPosition.coords.longitude;
                lat = objPosition.coords.latitude;

                definirposicion();
            }, function(objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            alert("Debe de activar el Gps");
                        }else{
                            alert("No se ha permitido el acceso a la posición del usuario.");
                        }
                    break;
                    case objPositionError.POSITION_UNAVAILABLE:
                    alert("No se ha podido acceder a la información de su posición.");
                    break;
                    case objPositionError.TIMEOUT:
                    alert("El servicio ha tardado demasiado tiempo en responder.");
                    break;
                    default:
                    alert("Error desconocido.");
                };
            }, {
                maximumAge: 75000,
                timeout: 15000
            });

        } else {
            alert("Su navegador no soporta la API de geolocalización.");
        }

        // var dibujar = false;
        // var canvas = document.getElementById("cnv");
        // var ctx = canvas.getContext("2d");
        // var cw = canvas.width = 500,
        // cx = cw / 2;
        // var ch = canvas.height = 300,
        // cy = ch / 2;

        // canvas.addEventListener('mousedown', function(evt) {
        //     dibujar = true;
        //     ctx.beginPath();
        // }, false);

        // canvas.addEventListener("mousemove", function(evt) {
        //     if (dibujar) {
        //         var m = oMousePos(canvas, evt);
        //         ctx.lineTo(m.x, m.y);
        //         ctx.stroke();
        //     }
        // }, false);

        // canvas.addEventListener('mouseup', function(evt) {
        //     dibujar = false;
        // }, false);

        // canvas.addEventListener("mouseout", function(evt) {
        //     dibujar = false;

        //     setDataFirma(canvas.toDataURL());
        // }, false);
        
        var sig = $("#signature").jSignature();

        $('#btn_firma_clear').on('click',function(){
            sig.jSignature("reset");
        });

        $("#signature").bind('change', function(e){
            setDataFirma(sig.jSignature("getData"));
        })

    });

    function clearCanvaArea() {
        var canvas = document.getElementById("cnv");
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        setDataFirma('');
    };

    function setDataFirma(valor) {
        $('#dataFirma').val(valor);
    };

    function oMousePos(canvas, evt) {
        var ClientRect = canvas.getBoundingClientRect();
        return {
            x: Math.round(evt.clientX - ClientRect.left),
            y: Math.round(evt.clientY - ClientRect.top)
        }
    };

    async function definirposicion() {

        let url = '{{ route('visitas.visita',$visita->id) }}';
        $('#pos').val(lat+','+lon);
    }

</script>
@endsection
