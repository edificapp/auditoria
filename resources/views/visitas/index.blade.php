@extends('layouts.app')

@section('titulo')
Visitas pendientes
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('empresas.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Visitas pendientes</p></a>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary mt-2 mt-xl-0">Volver</button>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de visitas</p>
                <div class="table-responsive">
                    <table id="tabla_visitas" class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Fecha de visita</th>
                                <th>Fin de visita</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach (Auth::user()->visitas as $visita)
                            <tr>
                                <td>{!! $visita->nombre !!}</td>
                                <td>{!! $visita->fecha_visita !!}</td>
                                <td>
                                    @if ($visita->estado == 'fin')
                                    {!! $visita->fin_visita !!}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if (is_null($visita->estado))
                                    {!! 'Por iniciar' !!}
                                    @elseif ($visita->estado == 'proceso')
                                    {!! 'En proceso' !!}
                                    @else
                                    {!! 'Terminado' !!}
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group col-sm">
                                        @if (is_null($visita->estado))
                                            <button class="btn btn-xs btn-info" type="button" onclick="iniciarVisita({!! $visita->id !!});" title="Iniciar visita"><i class="mdi mdi-chevron-double-right"></i></button>
                                        @elseif($visita->estado == 'proceso')
                                            <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('visitas.visita',$visita->id) }}';" title="Continuar visita."><i class="mdi mdi-camera-timer"></i></button>                                        
                                        @else
                                            <button class="btn btn-xs btn-warning" type="button" onclick="location.href='{{ route('visitas.adjuntar',$visita->id) }}';" title="adjuntar"><i class="mdi mdi-paperclip"></i></button>
                                            <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('visitas.ver',$visita->id) }}';" title="Ver visita."><i class="mdi mdi-eye"></i></button>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let lon = '';
    let lat = '';
    let error = '';

    $(document).ready(function() {
        $('#tabla_visitas').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(objPosition) {
                lon = objPosition.coords.longitude;
                lat = objPosition.coords.latitude;

            }, function(objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            alert("Debe de activar el Gps");
                        }else{
                            alert("No se ha permitido el acceso a la posición del usuario.");
                        }
                    break;
                    case objPositionError.POSITION_UNAVAILABLE:
                    alert("No se ha podido acceder a la información de su posición.");
                    break;
                    case objPositionError.TIMEOUT:
                    alert("El servicio ha tardado demasiado tiempo en responder.");
                    break;
                    default:
                    alert("Error desconocido.");
                };
            }, {
                maximumAge: 75000,
                timeout: 15000
            });

        } else {
            alert("Su navegador no soporta la API de geolocalización.");
        }
    });

    async function iniciarVisita(visita_id) {

        let url = '{{ route('visitas.visita','@@@@') }}';
        let url_ = url.replace('@@@@', visita_id);
        location.href=url_+'?pos='+encodeURIComponent(lat+','+lon);
    }


    function localizacion(){

    }
</script>
@endsection
