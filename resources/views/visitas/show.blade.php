@extends('layouts.app')

@section('titulo')
Ver visita
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('visitas') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Visitas&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Ver visita</p>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <div class="btn-group">
                    <button type="button" onclick="location.href='{{ route('visitas') }}';" class="btn btn-secondary mt-2 mt-xl-0">Volver</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div>
                     <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#visita">Visita</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#formulario">Formulario</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#firma">Firma</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#archivos">Archivos Adjuntos</a>
                      </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane container active" id="visita">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombre" class="col-form-label text-md-right">{{ __('Name') }}</label>
                                    <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! $visita->nombre !!}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="estado" class="col-form-label text-md-right">Estado</label>
                                    <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! ($visita->estado == "0") ? 'Inactivo' : 'Activo' !!}">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="estado" class="col-form-label text-md-right">Fecha y ubicación de inicio</label>
                                    <div class="input-group">
                                        <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! $visita->inicio_visita !!}">
                                        <div class="input-group-append">
                                            <a class="btn btn-outline-secondary btn-xs" href="{{route('map', $visita->coordenadas_inicio)}}" target="_blank"><i class="mdi mdi-google-maps"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="estado" class="col-form-label text-md-right">Fecha y ubicación de fin</label>
                                    <div class="input-group">
                                        <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! $visita->fin_visita !!}">
                                        <div class="input-group-append">
                                            <a class="btn btn-outline-secondary btn-xs" href="{{route('map', $visita->coordenadas_inicio)}}" target="_blank"><i class="mdi mdi-google-maps"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="estado" class="col-form-label text-md-right">Fecha de firma</label>
                                    <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! $visita->updated_at->setTimezone('America/Bogota') !!}">
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="tab-pane container fade" id="formulario">
                        <div class="row">
                            <div class="col-12">
                                <hr>
                                <center>
                                    <h4>Formulario construido</h4>
                                </center>
                                <hr>
                                
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-8" style="border: 1px dotted black !important; padding: 10px !important;">

                                        @foreach ($formularios as $clave => $formulario)
                                        <fieldset class="border border-secondary p-4 mb-2">
                                            <legend style="font-size: 0.875rem;line-height: 1.4rem;vertical-align: top;margin-bottom: rem;">
                                                &nbsp;Formulario: {!! $formulario->nombre !!}
                                            </legend>

                                            <div class="row">
                                                <div style="display: none;" class="form_inputs" id="fb-editor_{!! $clave !!}"></div>
                                            </div>
                                        </fieldset>

                                        @endforeach
                                    </div>
                                    <div class="col-2"></div>
                                </div>                        

                            </div>
                        </div>
                      </div>
                      <div class="tab-pane container fade" id="firma">
                        <div class="row">
                            <hr>
                            <center>
                                <h4>Firma del auditor</h4>
                            </center>
                            <hr><br>
                            <div class="col-md-12">
                                <hr>
                                <center><img class="img-fluid img-thumbnail" src="{!! base64_decode($visita->imagen) !!}"></center>
                            </div>
                        </div>
                      </div>
                      <div class="tab-pane container-fluid fade" id="archivos">
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="card">
                                <div class="card-body">
                                  <h4 class="card-title">Archivos Adjuntos</h4>
                                  <div id="lightgallery" class="row lightGallery">
                                    @foreach ($visita->adjuntar as $file)
                                        <a href="{{$file->url_ruta}}" class="col-xs-6 col-sm-6 col-md-3 image-tile box p-2 sombra" style="height: 300px;" data-sub-html="{{$file->descripcion ? $file->descripcion : 'no tiene Descripción'}}">
                                            <img src="{{$file->url_ruta}}" alt="image small img-thumbnail" class="img-fluid" >
                                        </a>

                                    @endforeach
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
{{--
                        @foreach ($visita->adjuntar->chunk(4) as $chunk)
                            <div class="row">
                                @foreach ($chunk as $file)
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
                                        <div class="card sombra">
                                          <img src="{{$file->url_ruta}}" class="img-thumbnail" alt="{{$file->descripcion}}" style="height: 200px;">
                                          <div class="card-body">
                                            <small class="card-text">{{$file->descripcion ? $file->descripcion : 'no tiene Descripción'}}</small>
                                          </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <br>
                        @endforeach
    --}}
                      </div>
                    </div>
                <hr>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lg-fb-comment-box.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lg-transitions.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css">
    <style type="text/css">
        .sombra{
            -webkit-box-shadow: 3px 3px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 3px 3px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 3px 3px 5px 0px rgba(0,0,0,0.75);
        }

        @supports(object-fit: cover){
            .box img{
              height: 100%;
              object-fit: cover;
              object-position: center center;
            }
        }
    </style>
@stop

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script>
<script>
    var respanteriores = {!! $respvisita !!};

    $(document).ready(function() {
        setTimeout(function() {
            activarFormularios();
        }, 3000);
        $("#lightgallery").lightGallery();
    });

    function activarFormularios() {
        var options = {
            showActionButtons: false,
        };

        @foreach ($formularios as $clave => $formulario)
console.log("esta es la clave: {!! $clave !!}");
        var fb_ed = $(document.getElementById('fb-editor_{!! $clave !!}')).formBuilder(options);
        
        setTimeout(function() {
            fb_ed.actions.setData(JSON.parse('{!! $formulario->formulario !!}'));
            
            var formRenderOpts = {
                dataType: 'xml',
                formData: fb_ed.actions.getData('xml')
            };

            var renderedForm = $('<div>');
            renderedForm.formRender(formRenderOpts);

            $("#fb-editor_{!! $clave !!}").html(renderedForm.html());
            $("#fb-editor_{!! $clave !!}").show();

            sleep(1000);

            $.each($('#fb-editor_'+{!! $clave !!}).find('select,input,textarea'), function(ffindex, ffs_Select) {
                console.log("esta es la clave: {!! $clave !!}");
                var ffsel = $(ffs_Select);
                var n_id = "form_{!! $clave !!}_"+ffsel.attr('id');
                var n_name = "form_{!! $clave !!}_"+ffsel.attr('id');
                
                if (ffsel.attr('type') == 'hidden') {
                    ffsel.remove();
                }

                var hmtml_d = ffsel.prop('outerHTML');
                if (ffsel.attr('type') == undefined) {

                    if (hmtml_d.indexOf('<select ') > -1) {
                        $.each(ffsel.find('option'), function(index_opt, val_opt) {
                            $(val_opt).removeProp('selected');
                            if (respanteriores[n_id] != undefined) {
                                if (respanteriores[n_id].trim() == $(val_opt).val().trim()) {
                                    $(val_opt).prop('selected', 'true');
                                }
                            }

                        });
                    }
                }  else if (ffsel.attr('type') == 'text') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                }  else if (ffsel.attr('type') == 'radio') {
                    n_name = n_name.split('-');
                             n_name.pop();

                    n_name = n_name.join('-');

                    if (respanteriores[n_name] != undefined) {
                        if (respanteriores[n_name].trim() == $(ffsel).val().trim()) {
                            $(ffsel).prop('checked', 'true');
                        }
                    }


                } else if (ffsel.attr('type') == 'checkbox') {
                    if (respanteriores[n_id] != undefined) {
                        if (respanteriores[n_id] == $(ffsel).val()) {
                            $(ffsel).prop('checked', 'true');
                        }
                    }
                } else if (ffsel.attr('type') == 'textarea') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                } else if (ffsel.attr('type') == 'date') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                } else if (ffsel.attr('type') == 'time') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                } else if (ffsel.attr('type') == 'number') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                }

                ffsel.attr({
                    id: n_id,
                    name: n_name
                });

                ffsel.attr('disabled', 'true');
            });
            
        }, 1000);

        @endforeach
    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }
</script>
@endsection
