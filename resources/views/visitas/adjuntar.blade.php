@extends('layouts.app')

@section('titulo')
Visitas pendientes
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('visitas-proyecto.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Mis visitas</p></a>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary mt-2 mt-xl-0">Volver</button>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">archivos adjuntados a visita: {{$visita->nombre}}</p>
                <button id="btnAdjuntar" class="btn btn-primary">Adjuntar Fotos a visita</button>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>id</th>
                                <th>Foto</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($visita->adjuntar as $index => $foto)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{$foto->id}}</td>
                                <td><img src="{{$foto->url_ruta}}" width="100"></td>
                                <td>{{$foto->descripcion}}</td>
                                <td>
                                    <button class="btn btn-xs btn-success btnEdit" type="button" title="Editar Foto"><i class="mdi mdi-pencil"></i></button>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal" id="modalAdjuntar">
          <div class="modal-dialog">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title">Adjuntar Fotos</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                    <form action="{{route('visitas.upload')}}" method="POST" id="formUpload" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="visita_id" value="{{$visita->id}}">
                        <input type="file" name="photo[]" multiple accept="image/png, .jpeg, .jpg">
                    </form>
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnSubmit">Subir Archivos</button>
              </div>

            </div>
          </div>
        </div>

        <div class="modal" id="modalEdit">
          <div class="modal-dialog">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title" id="titleEdit"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                    <form action="{{route('visitas.upload')}}" method="POST" id="formUploadEdit" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" required>
                        <input type="hidden" name="_method" value="PUT">
                        <textarea name="descripcion" id="textareaEdit" class="form-control" placeholder="escribe una descripcion"></textarea>
                        <input type="file" name="photo" accept="image/png, .jpeg, .jpg">
                    </form>
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEditSubmit">Subir Archivos</button>
              </div>

            </div>
          </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
    $(document).ready( function () {
       var table =  $('.table').DataTable();

     $('.table').delegate('.btnEdit', 'click', function(event) {
          var data = table.row( $(this).parents('tr') ).data();
          $('#formUploadEdit').attr('action', '/visitas/upload/'+data[1]);
          $('#titleEdit').text('Editar Foto #'+data[0])
          $('#textareaEdit').val(data[3])
          $('#modalEdit').modal('show');
     });
       
    } );

    $('#btnAdjuntar').on('click', function(){
        $('#modalAdjuntar').modal('show');
    });

    $('#btnSubmit').on('click', function(){
        $('#formUpload').submit();
    });
    

    $('#btnEditSubmit').on('click', function(){
        $('#formUploadEdit').submit();
    });
     




    </script>
@endsection
