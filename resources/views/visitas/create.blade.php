@extends('layouts.app')

@section('titulo')
Crear proyecto
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('usuarios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Proyectos&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Crear proyecto</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('proyectos.store') }}">
                    @csrf

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="estado" class="col-md-4 col-form-label text-md-right">Estado</label>

                                <select id="estado" type="text" class="form-control @error('estado') is-invalid @enderror" name="estado" required autofocus>
                                    <option value="0">Inactivo</option>
                                    <option value="1">Activo</option>
                                </select>

                                @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="rol">Supervisor</label>
                                <select class="form-control" id="supervisor" name="supervisor" placeholder="Seleccione un supervisor" required>
                                    <option>Seleccione un supervisor</option>
                                </select>

                                @error('supervisor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="rol">Formulario</label>
                                
                                <div class="input-group">
                                    <select class="form-control" id="formulario" name="formulario" required>
                                        <option value="" selected>Seleccione un formulario</option>

                                        @foreach ($formularios as $formulario)
                                        <option value="{!! $formulario->id !!}" {!! old('formulario') == $formulario->id ? 'selected' : '' !!}>{!! $formulario->nombre !!}</option>
                                        @endforeach

                                    </select>

                                    <div class="input-group-append">
                                        <button class="btn btn-sm btn-secondary text-warning border" id="mostrar_formulario" type="button" title="Ver formulario en ventane externa"><i class="mdi mdi-eye"></i></button>
                                        <button class="btn btn-sm btn-secondary text-white border" type="button" onclick="updateFormularios();" title="Actualizar listado de formularios"><i class="mdi mdi-refresh"></i> </button>
                                        <button class="btn btn-sm btn-secondary text-primary border" type="button" onclick="window.open('{{ route('formularios.create') }}','_blank');" title="Agregar nuevo formulario"><i class="mdi mdi-book-plus"></i> </button>
                                    </div>
                                </div>

                                @error('formulario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="d-flex justify-content-center">
                        <div class="col-6">

                            <div id="contenedor_auditores"></div>

                            <div class="text-center">
                                <div class="btn-group">
                                    <button type="button" id="add_auditor" class="btn btn-warning">
                                        Añadir auditor
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                    <br>
                    <hr>

                    <div class="row">
                        <div class="pull-rigth">
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary" onclick="location.href='{{ route('proyectos.index') }}';">
                                    Cancelar
                                </button>
                                <button type="button" class="btn btn-primary submit_class">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let cantidad_auditores = 0;
    $(document).ready(function() {
        $('#mostrar_formulario').on('click', function(event) {
            let formulario = $('#formulario');
            let formulario_id = formulario.val();

            if (formulario_id == '') {
                Swal.fire(
                  'Cuidado!',
                  'Para obtener una vista previa debes elegir un formulario!',
                  'warning'
                  );

                return false;
            }

            let _ruta = '{{ route('formularios.show','xxxx') }}';
            let ruta = _ruta.replace('xxxx', formulario_id);

            window.open(
                ruta,
                '_blank'
                );
        });

        $('#add_auditor').on('click', function(event) {

            cantidad_auditores++;

            let nuevo_campo = '';
            nuevo_campo += '<div class="auditor form-group col-md">';
            nuevo_campo += '    <label>Auditor</label>';
            nuevo_campo += '    <div class="input-group">';
            nuevo_campo += '        <select class="form-control" name="auditor[]" id="auditor_'+cantidad_auditores+'" placeholder="Seleccione un auditor"></select>';
            nuevo_campo += '        <div class="input-group-append">';
            nuevo_campo += '            <button class="btn btn-sm btn-secondary" type="button" onclick="updateAuditores(this);" title="Actualizar listado de auditores"><i class="mdi mdi-refresh"></i> </button>';
            
            if(cantidad_auditores > 1) {
                nuevo_campo += '<button class="btn btn-sm btn-danger" type="button" onclick="removerAuditor(this);" title="Remover auditor"><i class="mdi mdi-close"></i> </button>';
            }

            nuevo_campo += '        </div>';
            nuevo_campo += '    </div>';
            nuevo_campo += '</div>  ';

            $('#contenedor_auditores').append(nuevo_campo);
            updateAuditores(document.getElementById('auditor_'+cantidad_auditores));
        });

        updateSupervisores();
        $('#add_auditor').click();
    });

    async function updateSupervisores() {
        let elemento = $('#supervisor');

        $.ajax({
            url: '{!! route('getUsuarios') !!}',
        })
        .done(function(res) {
            elemento.find('option').remove();
            elemento.append("<option>Seleccione un supervisor</option>");

            $.each(res, function(index, val) {
                elemento.append('<option value="'+val.id+'">'+val.text+'</option>');
            });
        });
    }

    async function updateAuditores(elemento) {
        elemento = $(elemento).parents('div.auditor').find('select');

        $.ajax({
            url: '{!! route('getUsuarios') !!}',
        })
        .done(function(res) {
            elemento.find('option').remove();
            elemento.append("<option>Seleccione un auditor</option>");
            
            $.each(res, function(index, val) {
                elemento.append('<option value="'+val.id+'">'+val.text+'</option>');
            });
        });
        
    }

    function removerAuditor(elemento) {
        elemento = $(elemento).parents('div.auditor');
        if (elemento.find('select').attr('id') != 'auditor_1') {
            elemento.remove();
        }
    }

</script>
@endsection