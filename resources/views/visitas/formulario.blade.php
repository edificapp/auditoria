@extends('layouts.app')

@section('titulo')
Formulario visita
@endsection

@section('styles')
<style>
    .form-control {
        border: 1px solid black !important;
    }
</style>
@endsection


@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('formularios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Visitas&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Visitando</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nombre" class="col-form-label text-md-right">{{ __('Name') }}</label>
                            <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! $visita->nombre !!}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estado" class="col-form-label text-md-right">Fecha y hora de inicio</label>
                            <input id="nombre" readonly type="text" class="form-control" name="nombre" value="{!! $visita->inicio_visita !!}">
                        </div>
                    </div>
                </div>


                <form id="frm_formularios" method="post" action="{!! route('visitas.guardar',$visita->id) !!}">
                    @csrf
                    <div class="row">

                        <div class="col-12">

                            @foreach ($formularios as $clave => $formulario)
                            
                            
                            <fieldset class="border border-secondary p-4 mb-2">
                                <legend style="font-size: 0.875rem;line-height: 1.4rem;vertical-align: top;margin-bottom: rem;">
                                    &nbsp;Formulario: {!! $formulario->nombre !!}
                                </legend>

                                <div class="row">
                                    <div style="display: none;" class="form_inputs" id="fb-editor_{!! $clave !!}"></div>
                                </div>
                            </fieldset>

                            @endforeach

                        </div>

                        <div class="col-12">
                            <div class="btn-group">
                                <button type="button" onclick="location.href='{!! route('visitas') !!}';" class="btn btn-secondary">Volver</button>
                                <button type="button" class="btn_sub_form_forms btn btn-primary" value="Guardar y firmar">Guardar</button>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
<script>
    var respanteriores = {!! $respvisita !!};

    $(document).ready(function() {

        $('.btn_sub_form_forms').on('click', function(event) {
            event.preventDefault();
            $('#frm_formularios').submit();
        });

        setTimeout(function() {
            activarFormularios();
        }, 3000);

    });

    function activarFormularios() {
        var options = {
            showActionButtons: false,
        };

        @foreach ($formularios as $clave => $formulario)

        var fb_ed = $(document.getElementById('fb-editor_{!! $clave !!}')).formBuilder(options);
        
        setTimeout(function() {
            fb_ed.actions.setData(JSON.parse('{!! $formulario->formulario !!}'));

            var formRenderOpts = {
                dataType: 'xml',
                formData: fb_ed.actions.getData('xml')
            };

            var renderedForm = $('<div>');
            renderedForm.formRender(formRenderOpts);

            $("#fb-editor_{!! $clave !!}").html(renderedForm.html());
            $("#fb-editor_{!! $clave !!}").show();

            sleep(1000);

            $.each($('#fb-editor_'+{!! $clave !!}).find('select,input,textarea'), function(ffindex, ffs_Select) {
                var ffsel = $(ffs_Select);
                var n_id = "form_{!! $clave !!}_"+ffsel.attr('id');
                var n_name = "form_{!! $clave !!}_"+ffsel.attr('id');
                
                if (ffsel.attr('type') == 'hidden') {
                    ffsel.remove();
                }

                var hmtml_d = ffsel.prop('outerHTML');
                if (ffsel.attr('type') == undefined) {

                    if (hmtml_d.indexOf('<select ') > -1) {
                        $.each(ffsel.find('option'), function(index_opt, val_opt) {
                            $(val_opt).removeProp('selected');
                            if (respanteriores[n_id] != undefined) {
                                if (respanteriores[n_id].trim() == $(val_opt).val().trim()) {
                                    $(val_opt).prop('selected', 'true');
                                }
                            }

                        });
                    }
                }  else if (ffsel.attr('type') == 'text') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                }  else if (ffsel.attr('type') == 'radio') {
                    n_name = n_name.split('-');
                             n_name.pop();

                    n_name = n_name.join('-');

                    if (respanteriores[n_name] != undefined) {
                        if (respanteriores[n_name].trim() == $(ffsel).val().trim()) {
                            $(ffsel).prop('checked', 'true');
                        }
                    }


                } else if (ffsel.attr('type') == 'checkbox') {
                    if (respanteriores[n_id] != undefined) {
                        if (respanteriores[n_id] == $(ffsel).val()) {
                            $(ffsel).prop('checked', 'true');
                        }
                    }
                } else if (ffsel.attr('type') == 'textarea') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                } else if (ffsel.attr('type') == 'date') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                } else if (ffsel.attr('type') == 'time') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                } else if (ffsel.attr('type') == 'number') {
                    if (respanteriores[n_id] != undefined) {
                        ffsel.val(respanteriores[n_id]);
                    }
                }

                ffsel.attr({
                    id: n_id,
                    name: n_name
                });

            });
            
        }, 1000);

        @endforeach
    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }
</script>
@endsection
