@extends('layouts.app')

@section('titulo')
Informacion de la empresa
@endsection


@section('contenido')

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex align-items-end flex-wrap">
            <div class="d-flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-2">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home "></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Información de la empresa</b></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-md-center">
    <div class="col-sm-10 col-md-8 col-lg-8 col-xg-6">
        <div class="card">
            <div class="card-body borde">
                <div class="my-sm-3">
                    <center>
                        @include('empresa.components.tab', ['select' => 'informacion']) 
                    </center>   
                </div>  
                <br><br> 
                <div class="my-3">     
                    <div class="my-2 mx-4 row">
                        <h3>
                            <b class="text-muted">{{$empresa->nombre}}</b>
                            @can('actualizar-informacion-empresa')
                            <a title="Actualizar Datos de la Empresa" data-toggle="modal" data-target="#modalFormUpdate">
                                <span class="mdi mdi-pencil text-muted"></span>
                            </a>
                            @endcan
                        </h3>
                    </div> 
                    <br><br>   
                    <div class="my-2 row">
                        <div class="col-6">  
                            <div class="table-reponsive">        
                                <table class="table table-borderless">
                                    <tbody>
                                            <tr>
                                            <td><b>Nit: </b></td>
                                            <td >{{$empresa->nit}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Ciudad: </b></td>
                                            <td>{{$empresa->ciudad}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Barrio: </b></td>
                                            <td>{{$empresa->barrio}}</td>
                                        </tr>
                                        <tr >
                                            <td><b>Dirección: </b></td>
                                            <td>{{$empresa->direccion}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Telefono: </b></td>
                                            <td>{{$empresa->telefono}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Correo Electronico: </b></td>
                                            <td>{{$empresa->email}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Representante Legal: </b></td>
                                            <td>{{$empresa->representante_legal}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalFormUpdate">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11"> Actualizar datos de la empresa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('empresa.update.informacion', $empresa->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="put">
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Nombre: </label>
                        <input 
                            type="text" 
                            name="nombre" 
                            class="form-control col-9 @error('nombre') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('nombre') ? old('nombre') : $empresa->nombre}}"
                        >
                        <br>
                        @error('nombre') 
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Siglas: </label>
                        <input 
                            type="text" 
                            name="siglas" 
                            class="form-control col-9 @error('siglas') is-invalid @enderror"  
                            placeholder="Siglas de la Empresa" 
                            value="{{old('siglas') ? old('siglas') : $empresa->siglas}}"
                        >
                        <br>
                        @error('siglas') 
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Nit: </label>
                        <input 
                            type="text" 
                            name="nit" 
                            class="form-control col-9 @error('nit') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('nit') ? old('nit') : $empresa->nit}}"
                        >
                        <br>
                        @error('nit')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Ciudad: </label>
                        <input 
                            type="text" 
                            name="ciudad" 
                            class="form-control col-9 @error('ciudad') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('ciudad') ? old('ciudad') : $empresa->ciudad}}"
                        >
                        <br>
                        @error('ciudad')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Barrio: </label>
                        <input 
                            type="text" 
                            name="barrio" 
                            class="form-control col-9 @error('barrio') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('barrio') ? old('barrio') : $empresa->barrio}}"
                        >
                        <br>
                        @error('barrio')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Dirección: </label>
                        <input 
                            type="text" 
                            name="direccion" 
                            class="form-control col-9 @error('direccion') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('direccion') ? old('direccion') : $empresa->direccion}}"
                        >
                        <br>
                        @error('direccion')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Telefono: </label>
                        <input 
                            type="text" 
                            name="telefono" 
                            class="form-control col-9 @error('telefono') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('telefono') ? old('telefono') : $empresa->telefono}}"
                        >
                        <br>
                        @error('telefono') 
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Correo Electronico: </label>
                        <input 
                            type="email" 
                            name="email" 
                            class="form-control col-9 @error('email') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('email') ? old('email') : $empresa->email}}"
                        >
                        <br>
                        @error('email') 
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>
                    <div class="input-group my-3">
                        <label class="input-group-text col-3 bg-light"> Representante Legal: </label>
                        <input 
                            type="text" 
                            name="representante_legal" 
                            class="form-control col-9 @error('representante_legal') is-invalid @enderror"  
                            placeholder="Nombre de la Empresa" 
                            value="{{old('representante_legal') ? old('representante_legal') : $empresa->representante_legal}}"
                        >
                        <br>
                        @error('representante_legal')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                        @enderror
                    </div>

                    <div class="col-md-12 text-center"><h3><b>Imagenes</b></h3></div>
                    <div class="input-group my-3">

                        <div class="col-md-6">
                            <h5 class="text-center"> Imagen Web </h5>
                                <input 
                                    type="file" 
                                    name="url_logo_web" 
                                    accept="image/*"
                                    class="form-control col-9 @error('url_logo_web') is-invalid @enderror file_certificado"  
                                    value="{{old('url_logo_web')}}"
                                    onchange="cambiar()"
                                >
                                <br>
                                @error('url_logo_web')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                                @enderror
                        </div>

                        <div class="col-md-6">
                            <h5 class="text-center"> Imagen Movil </h5>
                            <input 
                                type="file" 
                                name="url_logo_movil" 
                                accept="image/*"
                                class="form-control col-9 @error('url_logo_movil') is-invalid @enderror file_certificado"  
                                value="{{old('url_logo_movil')}}"
                            >
                            <br>
                            @error('url_logo_movil')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                        
                    </div>
                    
                    <div class="row m-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" wire:click="save">Guardar Cambios</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
         $(document).ready(function(){
            @if($errors->any())
                $('#modalFormUpdate').modal('show');
            @endif 
        });
    </script>
    
@stop
