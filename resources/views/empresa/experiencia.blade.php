@extends('layouts.app')

@section('titulo')
Experiencia de la empresa
@endsection



@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex align-items-end flex-wrap">
            <div class="d-flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-2">
                        <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Experencia de la empresa</b></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="d-flex justify-content-end flex-wrap">
            @can('crear-experiencia-empresa')
                <div class="justify-content-end">
                    <button class="btn-create-form" id="create">Agregar experiencia </button>
                </div>
            @endcan 
        </div>
    </div>
</div>
<div>
    <center>
        @include('empresa.components.tab', ['select' => 'experiencia']) 
    </center>   
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body borde">   
                <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="table" class="display" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre Contrato</th>
                                        <th>Numero Contrato</th>
                                        <th>Entidad Contratante</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($empresa->experiencias as $experiencia)
                                        <tr class="body-rows">
                                            <td >{{$experiencia->nombre_contrato}}</td>
                                            <td>{{$experiencia->numero_contrato}}</td>
                                            <td>{{$experiencia->entidad_cliente->nombre}}</td>
                                            <td class="text-center">
                                                <div class="btn-group col-sm">
                                                    @can('ver-experiencia-empresa')
                                                        <button class="btn-show-form" 
                                                            title="ver experiencia"
                                                            type="button"   
                                                            onclick="show('{{route('empresa-experiencia.show', $experiencia->id)}}')" 
                                                        >
                                                    </button>
                                                    @endcan
                                                    <button class="btn-edit-form" 
                                                        title="editar experiencia" 
                                                        type="button" >
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


@can('crear-experiencia-empresa')
<div id="modalFormCreate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11">Agregar Experiencia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('empresa-experiencia.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-3 bg-light">Nombre Contrato: </label>
                            <input 
                                type="text" 
                                name="nombre_contrato" 
                                class="form-control col-sm-9 @error('nombre_contrato') is-invalid @enderror"  
                                placeholder="Nombre del contrato" 
                                value="{{old('nombre_contrato')}}"
                            >
                            <br>
                            @error('nombre_contrato')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Número Contrato: </label>
                            <input 
                                type="text" 
                                name="numero_contrato" 
                                class="form-control col-sm-7 @error('numero_contrato') is-invalid @enderror"  
                                placeholder="Número del contrato" 
                                value="{{old('numero_contrato')}}"
                            >
                            <br>
                            @error('numero_contrato')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Entidad Contratante: </label>
                            <select name="entidad_contratante" class="form-control">
                                @foreach(Auth::user()->empresa->clientes_terceros->filter(function($item){ return $item->tipo_contacto == 'cliente'; }) as $cliente)
                                    <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
                                @endforeach
                            </select>
                            <br>
                            @error('entidad_contratante')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Fecha Inicial: </label>
                            <input 
                                type="date" 
                                name="fecha_inicial" 
                                class="form-control col-sm-7 @error('fecha_inicial') is-invalid @enderror"  
                                placeholder="Fecha inicial" 
                                value="{{old('fecha_inicial')}}"
                            >
                            <br>
                            @error('fecha_inicial')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Fecha Final: </label>
                            <input 
                                type="date" 
                                name="fecha_final" 
                                class="form-control col-sm-7 @error('fecha_final') is-invalid @enderror"  
                                placeholder="Fecha final" 
                                value="{{old('fecha_final')}}"
                            >
                            <br>
                            @error('fecha_final')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12"  id="div_codigos_unspsc">
                            <label class="input-group-text col-sm-3 bg-light">Codigos UNSPSC: </label>
                            <input type="text" id="input_unspscs" class="form-control col-9">
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-10">
                            <label class="input-group-text col-sm-5 bg-light">valor del contrato (pesos): </label>
                            <input 
                                type="text" 
                                name="valor_del_contrato" 
                                class="form-control col-7 @error('valor_del_contrato') is-invalid @enderror"  
                                placeholder="Valor del contrato" 
                                value="{{old('valor_del_contrato')}}"
                            >
                            <br>
                            @error('valor_del_contrato')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="row my-3">
                            <div class="col-md-12 text-center"><h3><b>Certificados</b></h3></div>
                            <br>
                            <div class="col-md-6">
                                <h5 class="text-center">Contrato</h5>
                               <input class="file_certificado" name="file_contrato" type="file">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-center">Certificado</h5>
                               <input class="file_certificado"  name="file_certificado" type="file">
                            </div>
                        </div>
                    </div>
                    <div class="row m-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button"  class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" wire:click="save">Guardar Cambios</button>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>  
    </div>
</div>
@endcan
<div id="modalCode" class="modal fade" role="dialog" style="overflow:hidden;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11">Codigos Unspsc</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="text" id="buscar_code" class="form-control">
                <br><br>
                <div class="btn-group-vertical">
                </div>
                <div class="row m-3 justify-content-md-center">
                    <div class="form-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>

{{-- form ver experiencia   --}}
@can('ver-experiencia-empresa')
<div id="showContrato" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11">Detalles Experiencia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form >
                    <div class="input-group my-3">
                        <label for="" class="input-group-text col-sm-3 bg-light"> Nombre contrato </label>
                        <label id="nombre_contrato" class="form-control col-sm-7"></label>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-6 bg-light">Número Contrato: </label>
                            <label id="numero_contrato" class="form-control col-sm-6"></label>
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Entidad contratante </label>
                            <label id="entidad_contratante" class="form-control col-sm-9"></label>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Fecha Inicial <i class="mdi mdi-calendar-range"></i> </label>
                            <label id="fecha_inicial" class="form-control col-sm-9"></label>
                        </div>
                        <div class="input-group col-6">
                            <label class="input-group-text col-sm-5 bg-light">Fecha Final <i class="mdi mdi-calendar-range"></i> </label>
                            <label id="fecha_final" class="form-control col-sm-9"></label>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-5 bg-light">Valor del Contrato (Pesos)</label>
                            <label id="valor" class="form-control col-sm-9"></label>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-5 bg-light">Valor del Contrato (SMLV) </label>
                            <label id="smlv" class="form-control col-sm-9"></label>
                        </div>
                    </div>
                    <div class="col-md-12 text-center"><h4><b>Codigos UNSPSC</b></h4></div>
                    <div class="row my-3">
                        <div class="input-group col-sm-12">
                            <label id="codes" class="form-control-text col-sm-9 "></label>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <span id="spanCertificado"> No tiene pdf de Certificado </span>
                            <b><a href="" id="urlCertificado" target="_blank"><i class="mdi mdi-file-pdf-box text-danger"></i> Contrato en Pdf</a></b>
                        </div>
                    </div>
                    
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <span id="spanContrato"> No tiene pdf de Contrato </span>
                            <b><a href="" id="urlContrato" target="_blank"><i class="mdi mdi-file-pdf-box text-danger"></i> Certificado en Pdf</a></b>
                        </div>
                    </div>
                </form>

                <div class="row m-3 justify-content-md-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
                
                
            </div>
        </div>  
    </div>
</div>
@endcan
{{-- fin form  --}}


@endsection

@section('scripts')
    <script type="text/javascript">

        var codes_select = [];
        $(document).ready(function(){
            @if($errors->any())
                $('#modalFormCreate').modal('show');
            @endif 

            $('#select-unspsc').select2({
                multiple: true,
                allowClear: true,
            });
            $('#select-unspsc').val(null).trigger('change');
        });

        $('#create').on('click', function(){
           $('#modalFormCreate').modal('show'); 
         });


        function show(url){
            $.get(url, function( data ) {
                console.log(data);
                $('#showContrato').modal('show');
                $('#nombre_contrato').text(data.nombre_contrato);
                $('#numero_contrato').text(data.numero_contrato);
                $('#entidad_contratante').text(data.entidad_contratante);
                $('#fecha_inicial').text(data.fecha_inicial);
                $('#fecha_final').text(data.fecha_final);
                $('#valor').text(data.valor);
                if(data.certificado == ''){
                    $('#spanCertificado').show();
                    $('#urlCertificado').css('display', 'none');
                }else{
                    $('#spanCertificado').css('display', 'none');
                    $('#urlCertificado').show().attr('href', data.certificado);
                }

                if(data.contrato == ''){
                    $('#spanContrato').show();
                    $('#urlContrato').css('display', 'none');
                }else{
                    $('#spanContrato').css('display', 'none');
                    $('#urlContrato').show().attr('href', data.contrato);
                }
                $('#codes').text('');
                $.each(data.codes, function( index, value ) {
                  $('#codes').append(value.code+'  '+value.name+'<br>');
                });
            });
        }

        ///////////////////////////////
        ///
        $('#input_unspscs').on('click', function(){
            $('#modalCode').modal('show');
        });

        $('#buscar_code').on('keyup', function(){
            var val = $(this).val();
            if(val.length > 3){
                $.get( "/code-unspsc/"+val, function( data ) {
                    var datos = data.filter(function(item){
                        return codes_select.includes(item.id) == false;
                    })
                    $('.btn-group-vertical').empty();

                    $.each(datos, function( index, value ) {
                        $('.btn-group-vertical').append('<button onclick="button_code('+value.id+','+value.code+')" class="btn btn-block btn-secondary" id="button_code-'+value.code+'">'+value.code+' - '+value.name+'</button>');
                    });
                });
            }
        })

        function button_code(id, code){
            codes_select.push(id);
            var val_input = $('#input_unspscs').val();
            $('#div_codigos_unspsc').append('<input type="hidden" name="codigos_unspsc[]" value="'+id+'"/>');
            if(val_input == ''){
                $('#input_unspscs').val(code);
            }else{
                $('#input_unspscs').val(val_input+' | '+code);
            }
            $('#button_code-'+code).addClass('invisible');
        }

        $( '#modalCode' ).on( 'hidden.bs.modal' , function() {
            $( 'body' ).addClass( 'modal-open' );
        } );
    </script>
@stop
