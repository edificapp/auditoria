@extends('layouts.app')

@section('titulo')
Crear empresa
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item" ><a href="{{ route('empresas.index') }}">Empresas</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Crear Empresa</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde" >
                <h4 class="card-title">Información general de la empresa</h4>
                <form method="POST" action="{{ route('empresas.store') }}">
                    @csrf

                    <div class="row my-3">
                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="nit" class="input-group-text bg-light col-sm-5">Nit</label>
                                <input type="text" class="form-control col-sm-9 @error('nit') is-invalid @enderror" id="nit" name="nit" value="{{ old('nit') }}" placeholder="Nit">

                                @error('nit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="nombre" class="input-group-text bg-light col-sm-5">Nombre</label>
                                <input type="text" class="form-control col-sm-9 @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{{ old('nombre') }}" placeholder="Nombre completo">

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="direccion" class="input-group-text bg-light col-sm-5">Dirección</label>
                                <input type="text" class="form-control col-sm-10 @error('direccion') is-invalid @enderror" id="direccion" name="direccion" value="{{ old('direccion') }}" placeholder="Nombre completo">

                                @error('direccion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="telefono" class="input-group-text bg-light col-sm-5">Telefono</label>
                                <input type="text" class="form-control col-sm-9 @error('telefono') is-invalid @enderror" id="telefono" name="telefono" value="{{ old('telefono') }}" placeholder="Nombre completo">

                                @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="email" class="input-group-text bg-light col-sm-5">Correo electronico</label>
                                <input type="email" class="form-control col-sm-9 @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="activo" class="input-group-text bg-light col-sm-5">Estado</label>
                                <select class="form-control col-sm-9" id="activo" name="activo">
                                    <option value="si"{{ old('email') == 'si' ? 'selected' : '' }}>Activo</option>
                                    <option value="no"{{ old('email') == 'no' ? 'selected' : '' }}>Inactivo</option>
                                </select>

                                @error('activo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>

                    <h4 class="card-title">Responsable</h4>
                    <p class="card-description">
                        Se completa esta información basica del Responsable de la empresa, se le enviara una invitación al correo electronico.
                    </p>

                    <div class="row my-3">

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="usuario" class="input-group-text bg-light col-sm-5">Nombre de usuario</label>
                                <input type="text" class="form-control col-sm-9 @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! old('usuario') !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label class="input-group-text bg-light col-sm-5" for="email_usuario">Correo electronico</label>
                                <input type="email" class="form-control col-sm-9 @error('email_usuario') is-invalid @enderror" id="email_usuario" name="email_usuario" value="{!! old('email_usuario') !!}" required placeholder="Correo electronico">

                                @error('email_usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row my-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary">Regresar</button>
                            <button type="submit" class="btn btn-primary">Crear empresa</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
