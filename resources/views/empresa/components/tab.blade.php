 {{-- TAB ORIGINAL  --}}
{{-- <div class="mx-auto">
    <div class="btn-group flex-wrap" role="group" aria-label="Basic example">
        @can('ver-informacion-empresa')
        <a href="{{route('empresa.informacion')}}" class="btn btn-outline-secondary {{$select != 'informacion' ?:'active'}}">
            Información
        </a>
        @endcan
        @can('listar-experiencia-empresa')
        <a href="{{route('empresa-experiencia.index')}}" class="btn btn-outline-secondary {{$select != 'experiencia' ?:'active'}}">
            Experiencia
        </a>
        @endcan
        @can('listar-documentos-empresa')
        <a href="{{route('empresa-documentos.index')}}" class="btn btn-outline-secondary {{$select != 'documentos' ?:'active'}}">
            Documentos
        </a>
        @endcan
    </div>
    <hr style="margin-top: 0; border: 1px solid rgba(104,104,104,.5);">
</div> --}}

{{-- VARIACIONES  --}}

{{-- #1 --}}
    {{-- <ul class="nav nav-tabs  bg-light " >
            <li class="nav-item">
                @can('ver-informacion-empresa')
                <a 
                    href="{{route('empresa.informacion')}}" 
                    class="nav-link {{$select != 'informacion' ?:'active'}}"
                >
                    Información
                </a>
                @endcan
            </li>
            <li class="nav-item">
                @can('listar-experiencia-empresa')
                <a href="{{route('empresa-experiencia.index')}}" class="nav-link {{$select != 'experiencia' ?:'active'}}">
                    Experiencia
                </a>
                @endcan
            </li>
            <li class="nav-item">
                @can('listar-documentos-empresa')
                <a href="{{route('empresa-documentos.index')}}" class="nav-link {{$select != 'documentos' ?:'active'}}">
                    Documentos
                </a>
                @endcan
            </li>
    </ul> --}}
    {{-- <hr style="margin-top: 0; "> --}}

{{-- #2 --}}
    <ul class="nav nav-pills bg-transparent">
        <li class="nav-item ">
            @can('ver-informacion-empresa')
            <a href="{{route('empresa.informacion')}}" class="nav-link {{$select != 'informacion' ?:'active'}}">
                Información
            </a>
            @endcan
        </li>
        <li class="nav-item">
            @can('listar-experiencia-empresa')
            <a href="{{route('empresa-experiencia.index')}}" class="nav-link {{$select != 'experiencia' ?:'active'}}">
                Experiencia
            </a>
            @endcan
        </li>
        <li class="nav-item">
            @can('listar-documentos-empresa')
            <a href="{{route('empresa-documentos.index')}}" class="nav-link {{$select != 'documentos' ?:'active'}}">
               Documentos
            </a>
            @endcan
        </li>
    </ul>
    {{-- <hr style="margin-top: 0;margin-bottom:0; border: 1px solid rgba(104,104,104,.5);"> --}}
    {{-- <br> --}}
{{-- #3
    <nav>
        <div class="nav nav-tabs bg-orange" id="nav-tab" role="tablist">
            <button 
                class="nav-link {{$select != 'informacion' ?:'active'}}"
                id="nav-contact-tab" 
                data-bs-toggle="tab"  
                type="button" 
                role="tab" 
                aria-controls="nav-contact" 
                aria-selected="true"
            >
                @can('ver-informacion-empresa')
                    <a href="{{route('empresa.informacion')}}" >
                        Información
                    </a>
                @endcan
        
            </button>
            <button 
                class="nav-link {{$select != 'experiencia' ?:'active'}}"
                id="nav-contact-tab" 
                data-bs-toggle="tab"  
                type="button" 
                role="tab" 
                aria-controls="nav-contact" 
                aria-selected="false"
            >
                @can('listar-experiencia-empresa')
                    <a href="{{route('empresa-experiencia.index')}}" >
                        Experiencia
                    </a>
                @endcan
            </button>
            <button 
            class="nav-link {{$select != 'documentos' ?:'active'}}"
                id="nav-contact-tab" 
                data-bs-toggle="tab"  
                type="button" 
                role="tab" 
                aria-controls="nav-contact" 
                aria-selected="false"
            >
                @can('listar-documentos-empresa')
                    <a href="{{route('empresa-documentos.index')}}" >
                        Documentos
                    </a>
                @endcan
            </button>
        </div>
    </nav>
    <hr> --}}
{{-- #4 --}}
{{--   
<style>
    
</style> --}}