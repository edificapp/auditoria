@extends('layouts.app')

@section('titulo')
Empresas
@endsection


@section('contenido')

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb ">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Empresas</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="d-flex justify-content-sm-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('empresas.create') }}';" class="btn-create-form">Crear empresa</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body dashboard-tabs p-0" style="border: solid 1px #d0d3d4;">
                <div class="tab-content py-0 px-0">
                    <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                        <div class="d-flex flex-wrap justify-content-xl-between">
                            <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                                <i class="mdi mdi mdi-counter mr-3 icon-lg text-danger"></i>
                                <div class="d-flex flex-column justify-content-around">
                                    <small class="mb-1 text-muted">Cantidad de empresas creadas</small>
                                    <h5 class="mr-2 mb-0 text-center">{{ $empresas->count() }}</h5>
                                </div>
                            </div>
                            <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                                <i class="mdi mdi mdi-account mr-3 icon-lg text-success"></i>
                                <div class="d-flex flex-column justify-content-around">
                                    <small class="mb-1 text-muted">Cantidad de usuarios en empresas</small>
                                    <h5 class="mr-2 mb-0 text-center">{!! $usuarios->count() !!}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card border-card" >
            <div class="card-body borde">
                <p class="card-title">Listado de empresas</p>
                <div class="table-responsive">
                    <table id="table" class="display" width="100%" >
                        <thead>
                            <tr>
                                <th style="width:15%">Nit</div></th>
                                <th style="width:30%">Nombre</th>
                                <th style="width:40%">Correo electronico</th>
                                <th style="width:50%">Estado</th>
                                <th style="width:50%">Cantidad de usuarios</th>
                                <th style="width:50%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($empresas as $empresa)
                            <tr class="body-rows">
                                <td style="width:15%">{!! $empresa->nit !!}</td>
                                <td style="width:30%" >{!! $empresa->nombre !!}</td>
                                <td style="width:40%" >{!! $empresa->email !!}</td>
                                <td style="width:50%" >{!! $empresa->activo == 'si' ? 'Activo' : 'Inactivo' !!}</td>
                                <td style="width:50%" >{!! $empresa->usuarios->count() !!}</td>
                                <td  style="width:50%" class="text-center">
                                    <div class="btn-group col-sm">
                                        <button class="btn-edit-form" type="button" onclick="location.href='{{ route('empresas.edit',$empresa->id) }}';" title="Editar empresa.">
                                            
                                        </button>
                                        
                                        @if ($empresa->activo == 'no')
                                        
                                        <button class="btn  btn-inverse-success btn-rounded" onclick="location.href='{{ route('empresa_estado',$empresa->id) }}';" title="Habilitar empresa"><i class="mdi mdi-lock-open-outline"></i></button>
                                        
                                        @else
                                        
                                        <button class="btn btn-inverse-danger btn-rounded" onclick="location.href='{{ route('empresa_estado',$empresa->id) }}';" title="Inhabilitar empresa"><i class="mdi mdi-lock-outline"></i></button>
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
