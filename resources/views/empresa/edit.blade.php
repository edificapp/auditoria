@extends('layouts.app')

@section('titulo')
Editar empresa
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item" ><a href="{{ route('empresas.index') }}">Empresas</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Editar Empresa</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <h4 class="card-title">Información general de la empresa</h4>
                <form id="form_forms" method="POST" action="{{ route('empresas.update',$empresa->id) }}">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label class="input-group-text bg-light col-sm-5" for="nit">Nit</label>
                                <input type="text" class="form-control col-sm-9" id="nit" name="nit" value="{!! $empresa->nit !!}" placeholder="Nit">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label for="nombre" class="input-group-text bg-light col-sm-5">Nombre</label>
                                <input type="text" class="form-control col-sm-9" id="nombre" name="nombre" value="{!! $empresa->nombre !!}" placeholder="Nombre completo">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label class="input-group-text bg-light col-sm-5" for="email">Correo electronico</label>
                                <input type="email" class="form-control col-sm-9" id="email" name="email" value="{!! $empresa->email !!}" placeholder="Correo electronico">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group my-3">
                                <label class="input-group-text bg-light col-sm-5" for="activo">Estado</label>
                                <select class="form-control col-sm-9" id="activo" name="activo">
                                    <option {!! $empresa->activo == 'si' ? 'selected' : '' !!} value="si">Activo</option>
                                    <option {!! $empresa->activo == 'no' ? 'selected' : '' !!} value="no">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center ">
                        <div class="form-group">
                            <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary">Regresar</button>
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
