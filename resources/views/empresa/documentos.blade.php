@extends('layouts.app')

@section('titulo')
Documentos de la empresa
@endsection

@section('contenido')


<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex align-items-end flex-wrap">
            <div class="d-flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-2">
                        <li class="breadcrumb-item" ><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Documentos de la empresa</b></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="d-flex justify-content-end flex-wrap">
            @can('crear-documentos-empresa')
                <div>
                    <button title="agregar documento" class="btn-create-form" id="create">
                        Agregar documento  
                    </button>
                </div>
            @endcan
        </div>
    </div>
</div>
<div >
    <center>
        @include('empresa.components.tab', ['select' => 'documentos']) 
    </center>   
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body borde">
                <div class="my-3">     
                    <div class="col-sm-12"  >
                        <div class="table-responsive">
                            <table id="table" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Documento</th>
                                            <th>Fecha Generacion</th>
                                            <th>Fecha vencimiento</th>
                                            <th>Estado</th>
                                            <th>Ver</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($empresa->documentos as $item)
                                            <tr class="body-rows">
                                                <td>{{$item->nombre}}</td>
                                                <td>{{$item->fecha_generacion}}</td>
                                                <td>{{$item->fecha_vencimiento}}</td>
                                                <td class="{{$item->estado == 'Vencido' ? 'text-danger':''}}">{{$item->estado}}</td>
                                                <td class="text-center">
                                                    @can('ver-documentos-empresa')
                                                    <a href="{{$item->ruta_documento}}" target="_blank" class="btn-pdf-form" 
                                                        title="ver archivo">
                                                    </a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@can('crear-documentos-empresa')
<div id="modalFormCreate" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header white">
                <h4 class="modal-title white text-center" id="myModalLabel11"> Agregar Documento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('empresa-documentos.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row my-3">
                        <div class="input-group col-sm-12">
                            <label class="input-group-text col-sm-3 bg-light">Nombre de Documento: </label>
                            <input 
                                type="text" 
                                name="nombre" 
                                class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                                placeholder="Nombre de Documento" 
                                value="{{old('nombre')}}"
                            >
                            <br>
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3 justify-content-md-center">
                        <div class="col-md-8">
                           <input class="file_certificado" name="documento" type="file">
                        </div>
                    </div>
                   
                    <div class="row my-3">
                        <div class="input-group col-sm-12">
                            <label class="input-group-text col-sm-5 bg-light">Fecha Generación: </label>
                            <input 
                                type="date" 
                                name="fecha_generacion" 
                                class="form-control col-sm-7 @error('fecha_generacion') is-invalid @enderror"  
                                placeholder="Fecha de Generación" 
                                value="{{old('fecha_generacion')}}"
                            >
                            <br>
                            @error('fecha_generacion')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-sm-12">
                            <label class="input-group-text col-sm-5 bg-light">Fecha de vencimiento: </label>
                            <input 
                                type="date" 
                                name="fecha_vencimiento" 
                                class="form-control col-sm-7 @error('fecha_vencimiento') is-invalid @enderror"  
                                placeholder="Fecha de vencimiento" 
                                value="{{old('fecha_vencimiento')}}"
                            >
                            <br>
                            @error('fecha_vencimiento')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="row my-5 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" wire:click="save">Cargar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>
@endcan

@endsection

@section('scripts')
    <script type="text/javascript">
         $(document).ready(function(){
            @if($errors->any())
                $('#modalFormUpdate').modal('show');
            @endif 

            $('#modalFormCreate').on('shown.bs.modal', function (e) {
                $(this).find('#select-unspsc').select2({
                    dropdownParent: $(this).find('.modal-content')
                });
            })

            $('#select-unspsc').select2({
                multiple: true,
                allowClear: true,
            });
            $('#select-unspsc').val(null).trigger('change');

        });

         $('#create').on('click', function(){
           $('#modalFormCreate').modal('show'); 
         });

        function show(url){
            $.get(url, function( data ) {
                $('#showContrato').modal('show');
                $('#nombre_contrato').text(data.nombre_contrato);
                $('#numero_contrato').text(data.numero_contrato);
                $('#entidad_contratante').text(data.entidad_contratante);
                $('#fecha_inicial').text(data.fecha_inicial);
                $('#fecha_final').text(data.fecha_final);
                $('#valor').text(data.valor);
                $('#urlCertificado').attr('href', data.certificado);
                $('#urlContrato').attr('href', data.contrato);
                $('#codes').text('');
                $.each(data.codes, function( index, value ) {
                  $('#codes').append(value.code+'  '+value.name+'<br>');
                });
            });
        }
    </script>
@stop
