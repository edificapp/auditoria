@extends('layouts.app')

@section('titulo')
Usuarios de empresa
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('empresas.index') }}">Empresas</a></li>
                            <li class="breadcrumb-item">[ {!! $empresa->nombre !!} ]</li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Usuarios por empresa</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary mt-2 mt-xl-0">Volver</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <p class="card-title">Listado de usuarios</p>
                <div class="table-responsive">
                    <table id="table" class="display" width="100%">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($usuarios as $usuario)
                            <tr class="body-rows">
                                <td>{!! $usuario->usuario !!}</td>
                                <td>{!! $usuario->nombre !!}</td>
                                <td>{!! $usuario->email !!}</td>
                                <td>{!! $usuario->activo == 'si' ? 'Activo' : 'Inactivo' !!}</td>
                                <td>
                                    <div class="btn-group col-sm">
                                        <button class="btn-edit-form" type="button" onclick="location.href='{{ route('empresas.edit',$usuario->id) }}';" title="Editar empresa.">
                                            
                                        </button>
                                        
                                        @if ($usuario->activo == 'no')
                                        
                                        <button class="btn-activo-form" onclick="location.href='{{ route('empresa_estado',$usuario->id) }}';" title="Habilitar empresa">
                                        </button>
                                        @else
                                        
                                        <button class="btn-inactivo-form" onclick="location.href='{{ route('empresa_estado',$usuario->id) }}';" title="Inhabilitar empresa"></button>
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
