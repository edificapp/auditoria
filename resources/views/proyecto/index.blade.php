@extends('layouts.app')

@section('titulo')
Proyectos
@endsection


@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Proyectos adjudicados</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" 
                        class="btn-create-form" 
                        data-toggle="modal" data-target="#modalcreate"
                        title="creación de un proyecto Adjudicado"
                >
                  Agregar Proyecto
                </button>
                
            </div>
        </div>
    </div>
</div>
<div >
    @include('proyecto.components.tabs', ['select' => 'adjudicados'])   
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <p class="card-title">Listado de proyectos Adjudicados</p>
                <div class="table-responsive">
                    <table  id="table" class="display" width="100%">
                        <thead>
                            <tr>
                                <th style="width:20%">Nombre</th>
                                <th style="width:15%">Entidad Contratante</th>
                                <th style="width:10%">No. Proceso</th>
                                <th style="width:15%">Contacto</th>
                                <th style="width:3%" class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($proyectos as $proyecto)
                            <tr class="body-rows">
                                <td style="width:20%" >{{strtolower($proyecto->nombre)}}</td>
                                <td style="width:15%" >{!! $proyecto->entidad_contratante !!}</td>
                                <td style="width:10%" >{!! $proyecto->n_proceso !!}</td>
                                <td style="width:15%" >{!! $proyecto->contacto ? $proyecto->contacto->nombre : 'No tiene contacto' !!}</td>
                                <td style="width:3%" class="text-center">
                                    <a href="{{route('proyectos.edit', $proyecto->id)}}" class="btn-edit-form"></a>
                                    @if($proyecto->documentos_contractuales->count() > 0 && $proyecto->documentos_tecnicos->count() > 0 && $proyecto->validar_personal($proyecto->personal))
                                    <a href="{{route('proyectos.cambiar.estado', ['ejecucion', $proyecto->id])}}" class="btn btn-inverse-primary"><span class="mdi mdi-arrow-right-bold"></span></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{--

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de proyectos</p>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Fecha de creación</th>
                                <th>Estado</th>
                                <th>Visitas pendientes</th>
                                <th>Visitas en proceso</th>
                                <th>Visitas terminadas</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($proyectos as $proyecto)
                            <tr>
                                <td>{!! $proyecto->nombre !!}</td>
                                <td>{!! $proyecto->created_at->format('d/m/Y') !!}</td>
                                <td>{!! $proyecto->activo == 'si' ? 'Activo' : 'Inactivo' !!}</td>

                                <td>{!! \App\Visita::where('proyecto_id',$proyecto->id)->where('estado',null)->get()->count() !!}</td>
                                <td>{!! \App\Visita::where('proyecto_id',$proyecto->id)->where('estado','proceso')->get()->count() !!}</td>
                                <td>{!! \App\Visita::where('proyecto_id',$proyecto->id)->where('estado','terminado')->get()->count() !!}</td>
                                
                                <td>
                                    <div class="btn-group col-sm">

                                        <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('proyectos.edit',$proyecto->id) }}';" title="Editar proyecto.">
                                            <i class="mdi mdi-grease-pencil"></i>
                                        </button>

                                        @if ($proyecto->activo == 'no')
                                        
                                        
                                        @else
                                        
                                        <button class="btn btn-xs btn-secondary" onclick="location.href='{{ route('proyectos.show',$proyecto->id) }}';" title="Gestionar visitas"><i class="mdi mdi-view-list"></i></button>
                                       
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
--}}

<!-- Modal -->
<div class="modal fade" id="modalcreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Formulario para la creación de un proyecto Adjudicado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('proyectos.store') }}">
                    @csrf
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-3 bg-light">Nombre: </label>
                            <input 
                                type="text" 
                                name="nombre" 
                                class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                                placeholder="Nombre del Proyecto" 
                                value="{{old('nombre')}}"
                            >
                            <br>
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-3 bg-light">Entidad Contratante: </label>
                            <input 
                                type="text" 
                                name="entidad_contratante" 
                                class="form-control col-sm-9 @error('entidad_contratante') is-invalid @enderror"  
                                placeholder="Entidad Contratante" 
                                value="{{old('entidad_contratante')}}"
                            >
                            <br>
                            @error('entidad_contratante')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-3 bg-light">Número de proceso: </label>
                            <input 
                                type="text" 
                                name="n_proceso" 
                                class="form-control col-sm-9 @error('n_proceso') is-invalid @enderror"  
                                placeholder="Número de proceso" 
                                value="{{old('n_proceso')}}"
                            >
                            <br>
                            @error('n_proceso')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="input-group col-12">
                            <label class="input-group-text col-sm-3 bg-light">Contacto: </label>
                            <select 
                                    class="form-control col-sm-7" 
                                    name="contacto_id"
                            >
                                @foreach($contactos  as $contacto)
                                    <option value="{{$contacto->id}}" {{old('contacto_id') == $contacto->id ? 'selected' : ''}}>{{$contacto->nombre}}</option>
                                @endforeach
                            </select>
                            <br>
                        </div>
                    </div>

                    <hr>

                    <div class="row my-3 justify-content-md-center">
                        <div class="form-group">
                                <button type="button"  class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

@endsection
