@extends('layouts.app')

@section('titulo')
Editar proyecto
@endsection

@section('styles')
<style>
    .supervisor {
        margin: 5px 1px 1px 1px !important;
    }

    .form-control {
        width: 100%;
    }
    .multiselect-container {
        box-shadow: 0 3px 12px rgba(0,0,0,.175);
        margin: 0;
    }
    .multiselect-container .checkbox {
        margin: 0;
    }
    .multiselect-container li {
        margin: 0;
        padding: 0;
        line-height: 0;
    }
    .multiselect-container li a {
        line-height: 25px;
        margin: 0;
        padding:0 35px;
    }
    .custom-btn {
        width: 100% !important;
    }
    .custom-btn .btn, .custom-multi {
        text-align: left;
        width: 100% !important;
    }
    .dropdown-menu > .active > a:hover {
        color:inherit;
    }

</style>
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Editar proyecto</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('proyecto.components.tabs', ['select' => 'adjudicados'])
</div>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                {{--  --}}

                <div class="row">
                    <div class="col-12 text-right mt-2">
                        <label for=""><b> Fecha de entrega actual:</b> </label> 
                        <small>#######</small>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-body p-3 border-0" style="background-color:#F4F6F6;">
                        <div class="header-detail-propuesta">
                            <div class="row">
                                <div class="col-4">
                                    <label class="text-muted">
                                    </b> {{$proyecto->nombre}}</b>
                                    </label>
                                </div>
                                <div class="col-3">
                                    <label for=""> 
                                    <span class="font-weight-bold"> Nº Proceso:</span> 
                                    <small>{{$proyecto->n_proceso}}</small>
                                    </label>
                                </div>
                                <div class="col-5">
                                    <label for="">
                                    <span class="font-weight-bold">Fecha de creación de propuesta : </span>
                                    <small>#########</small>
                                    </label>
                                </div>
                                        
                            </div>
                            <div class="row mt-2">
                                <div class="col-4">
                                    <label for="">
                                    <span class="font-weight-bold">Entidad contratante: </span>
                                    <small></small>
                                    </label>
                                </div>
                                <div class="col-3">
                                    <label for="">
                                    <span class="font-weight-bold">Contacto: </span>
                                    <small></small>
                                    </label>
                                </div>
                                <div class="col-5">
                                    <label for="">
                                    <span class="font-weight-bold">Fecha inicial de entrega: </span> 
                                    <small>##########</small>
                                    </label>
                                </div>      
                            </div>
                        </div>
                    </div>
                </div>

                {{--  --}}
                <div class="mt-4" id="accordion">
                    <div  id="headingOne">
                        <h5 class="mb-0">
                            <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Documentos Contractuales
                            </span>
                      </h5>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <button class="btn-add-form mb-2" data-toggle="modal" data-target="#documentosContractual" title="Adjuntar Documentos">
                            </button>
                            <div class="table-responsive">
                                <table class="display" id="tablaDocumentos" width="100%">
                                    <thead class="thead-light">
                                        <tr>
                                            <th></th>
                                            <th>Archivo</th>     
                                            <th>Fecha</th>   
                                            <th>Tamaño</th>   
                                            <th>Descripción Contenido</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($proyecto->documentos_contractuales as $contractual)
                                            <tr class="body-rows">
                                                <td>{{$contractual->name}}</td>  
                                                <td><a href="{{$contractual->url_public}}" target="blank" class="btn btn-link">{{$contractual->nombre}}</a></td>   
                                                <td>{{$contractual->fecha}}</td>   
                                                <td title="{{$contractual->size}}Mb">{{str_limit($contractual->size, 9)}}</td>   
                                                <td>{{$contractual->description}}</td>  
                                            </tr>
                                        @endforeach 
                                        <tr>
                                            <td>Contrato</td>
                                            <td>contrato.pdf</td>
                                            <td>#####</td>
                                            <td>320 kb</td>
                                            <td>asdasdasdasdasdasdasdassa </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-2" id="accordionTwo">
                    <div  id="headingTwo">
                        <h5 class="mb-0">
                            <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapse">
                                <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Documentos Tecnicos
                            </span>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionTwo">
                        <div class="card-body">
                            <button class="btn-add-form mb-2" data-toggle="modal" data-target="#documentostecnicos" title="Adjuntar Documentos">
                                
                            </button>
                            <div class="table-responsive">
                                <table class="display" id="tablaDocumentos" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Archivo</th>
                                            <th>Fecha</th>
                                            <th>Tamaño</th>
                                            <th>Descripción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($proyecto->documentos_tecnicos as $contractual)
                                            <tr class="body-rows">
                                                <td>{{$contractual->name}}</td>
                                                <th><a href="{{$contractual->url_public}}" target="blank" class="btn btn-link">{{$contractual->nombre}}</a></th>
                                                <th>{{$contractual->fecha}}</th>
                                                <th>{{$contractual->size}}</th>
                                                <th>{{$contractual->description}}</th>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td>Contrato</td>
                                            <td>contrato.pdf</td>
                                            <td>#####</td>
                                            <td>320 kb</td>
                                            <td>asdasdasdasdasdasdasdassa </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>        
                
                <div class="mt-2" id="accordionThree">
                    <div id="headingThree">
                        <h5 class="mb-0">
                            <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                  <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Relacionar Personal
                            </span>
                        </h5>
                    </div>

                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionThree">
                        <div class="card-body">
                            <button class="btn btn-xs btn-inverse-primary btn-rounded" data-toggle="modal" data-target="#personal" title="relacionar personal">
                                <i class="mdi mdi-account-multiple-plus"></i>
                            </button>
                            <div class="row mt-3">
                                <div class="col-3"><b>Nombre</b></div>
                                <div class="col-3"><b>Cargo</b></div>
                            </div>
                            @foreach($proyecto->personal as $personal)
                                <div class="row mt-2">
                                    <div class="col-3">{{$personal->user->name}}</div>   
                                    <div class="col-3">{{$personal->cargo}}</div> 
                                    <div class="col-3">
                                        <form action="{{route('personal-proyecto.destroy', $personal->id)}}" method="post">
                                            {!! method_field('delete') !!}
                                            {!! csrf_field() !!}
                                            <button type="submit" 
                                                    class="btn btn-sm btn-rounded {{$personal->activo ? 'btn-inverse-danger' : 'btn-inverse-success'}}" 
                                                    title="{{$personal->activo ? 'Desactivar' : 'Activar'}}"
                                            >
                                                <i class="mdi {{$personal->activo ? 'mdi-lock-open-outline' : 'mdi-lock-outline'}}"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                            <form action="" method="post" id="formDeletePersonal">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                            </form>
                        </div>
                    </div>
                </div>

                {{-- <div class="row mt-3">
                    <div class="col-3 "><b>Personal <button class="btn-icon-form" data-toggle="modal" data-target="#personal">+</button></b></div> 
                    <div class="col-3"><b>Cargo</b></div>
                </div>
                <br>

                @foreach($proyecto->personal as $personal)
                    <div class="row mt-2">
                        <div class="col-3">{{$personal->user->name}}</div>   
                        <div class="col-3">{{$personal->cargo}}</div> 
                        <div class="col-3">
                            <form action="{{route('personal-proyecto.destroy', $personal->id)}}" method="post">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                                <button type="submit" 
                                        class="btn btn-sm btn-rounded {{$personal->activo ? 'btn-inverse-danger' : 'btn-inverse-success'}}" 
                                        title="{{$personal->activo ? 'Desactivar' : 'Activar'}}"
                                >
                                    <i class="mdi {{$personal->activo ? 'mdi-lock-open-outline' : 'mdi-lock-outline'}}"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                @endforeach
                <form action="" method="post" id="formDeletePersonal">
                    {!! method_field('delete') !!}
                    {!! csrf_field() !!}
                </form> --}}

                <div class="d-flex justify-content-lg-end align-items-end flex-wrap">
                    <button type="button" onclick="location.href='{{ route('proyectos.index') }}';" class="btn btn-secondary">Regresar</button>
                </div>  
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="documentosContractual" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adjuntar Documentos Contractuales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('documentos-proyecto.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="tipo" value="contractual">
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Nombre: </label>
                    <input 
                        type="text" 
                        name="nombre" 
                        class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                        placeholder="Nombre del Archivo" 
                        value="{{old('nombre')}}"
                    >
                    <br>
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Descripción: </label>
                    <input 
                        type="text" 
                        name="description" 
                        class="form-control col-sm-9 @error('description') is-invalid @enderror"  
                        placeholder="Descripción" 
                        value="{{old('description')}}"
                    >
                    <br>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <br>
                <div class="col-md-6">
                    <input class="documentos" name="documentos" type="file">
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <div class="form-group">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="documentostecnicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adjuntar Documentos Tecnicos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('documentos-proyecto.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <input type="hidden" name="tipo" value="tecnico">
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Nombre: </label>
                    <input 
                        type="text" 
                        name="nombre" 
                        class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                        placeholder="Nombre del Archivo" 
                        value="{{old('nombre')}}"
                    >
                    <br>
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Descripción: </label>
                    <input 
                        type="text" 
                        name="description" 
                        class="form-control col-sm-9 @error('description') is-invalid @enderror"  
                        placeholder="Descripción" 
                        value="{{old('description')}}"
                    >
                    <br>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <br>
                <div class="col-md-6">
                    <input class="documentos" name="documentos" type="file">
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <div class="form-group">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="personal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Relacionar Personal al Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('personal-proyecto.store') }}">
                    @csrf
                    <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
                    <div class="row my-3">
                       <div class="col-md-6">
                            <div class="input-group">
                                <label for="personal" class="input-group-text col-sm-4" >Personal</label>
                                <select id="personal" class="form-control col-sm-9" name="user_id">
                                    @foreach(Auth::user()->empresa->usuarios as $personal)
                                        <option value="{{$personal->id}}">{{$personal->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="cargo" class="input-group-text col-sm-5">Cargo</label>
                                <select id="cargo3"  class="form-control col-sm-9" name="cargo">
                                        <option >Coordinador</option>
                                        <option >Supervisor</option>
                                        <option >Inspector</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-md-center">
                        <div class="form-group">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="personalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Relacionar Funcionarios al Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" id="formUpdatePersonal">
                    @csrf
                    <input type="hidden" name="_method" value="patch">
                    <div class="row">
                       <div class="col-md-6">
                            <div class="form-group">
                                <label for="contacto" class="col-form-label">Personal</label>
                                <select class="form-control" name="user_id" id="personalSelect">
                                    @foreach(Auth::user()->empresa->usuarios as $personal)
                                        <option value="{{$personal->id}}">{{$personal->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contacto" class="col-form-label">Cargo</label>
                                <select class="form-control" name="cargo" id="cargoSelect">
                                        <option value="Coordinador">Coordinador</option>
                                        <option value="Supervisor">Supervisor</option>
                                        <option value="Inspector">Inspector</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="pull-rigth">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#userSelect').select2();
        $('#tablaDocumentos, .table-crud').DataTable ({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            paging:false, info: false, ordering:false, searching:false
            
        });

    });

    $(".documentos").fileinput({
        'language': "es",
        'theme': "fa",
        'showUpload' : false , 
        'maxFilesNum': 10,
    });

    function editarPersonalProyecto(edit, update){
        $.get(edit, function(data){
            $('#personalEdit').modal();
            $(`#personalSelect option[value="${data.user_id}"]`).attr("selected",true);
            $(`#cargoSelect option[value="${data.cargo}"]`).attr("selected",true);
            $('#formUpdatePersonal').attr('action', update);
        });
    }

    function borrarPersonalProyecto(url){
        $('#formDeletePersonal').attr('action', url).submit();
    }

</script>
@endsection
