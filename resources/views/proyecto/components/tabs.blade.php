<ul class="nav nav-pills mb-0">
    {{-- <div class="btn-group flex-wrap" role="group"> --}}
        <li class="nav-item">
            <a href="{{route('proyectos.index')}}" class="nav-link {{$select != 'adjudicados' ?:'active'}}">
                Adjudicados
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('proyectos.ejecucion')}}" class="nav-link {{$select != 'ejecucion' ?:'active'}}">
                En ejecución
            </a>
        </li>
        {{--
        <li class="nav-item">
            <a href="{{route('proyectos.cerrados')}}" class="nav-link {{$select != 'cerrados' ?:'active'}}">
                Cerrados
            </a>
        </li>
            --}}
        <li class="nav-item">
            <a href="{{route('formularios.index')}}" class="nav-link {{$select != 'formularios' ?:'active'}}">
                Formularios
            </a>
        </li>
    {{-- </div> --}}
</ul>
<hr style="margin-top: 0;margin-bottom:2px; border: 0.5px solid #D7DBDD ;">
