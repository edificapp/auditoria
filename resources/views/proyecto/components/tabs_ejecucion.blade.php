    <div class="card">
        <div class="card-body borde p-2">
            <div class="row">
                <div class="col-7">
                    <b>Proyecto:</b> <small>{!!$proyecto->nombre!!}</small>
                </div>   
                <div class="col-3">
                    <b>Número del proceso:</b> <small>{!!$proyecto->n_proceso!!}</small>
                </div>                       
            </div> 
        </div>
    </div>
     
    <ul class=" nav nav-tabs mt-0">
            <li class="nav-item">
                <a href="{{route('proyectos.sub_menu', [$proyecto->id, 'documentos'])}}" class="nav-link {{$select != 'documentos' ?:'active'}}">
                    Documentos
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('proyectos.sub_menu', [$proyecto->id, 'informacion_adicional'])}}" class="nav-link {{$select != 'informacion_adicional' ?:'active'}}">
                    Info. adicional
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('proyectos.sub_menu', [$proyecto->id, 'ejecucion'])}}" class="nav-link {{$select != 'ejecucion' ?:'active'}}">
                    Ejecución
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('proyecto.informe.formulario', $proyecto->id)}}" class="nav-link {{$select != 'informes' ?:'active'}}">
                    Informes
                </a>
            </li>
            {{--
            <li class="nav-item">
                <a href="{{route('proyectos.sub_menu', [$proyecto->id, 'visitas'])}}" class="nav-link {{$select != 'visitas' ?:'active'}}">
                    Visitas
                </a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link {{$select != 'financiero' ?:'active'}}">
                    Financiero
                </a>
            </li>
                --}}
    </ul>

<style>
    /* .nav .nav-item:hover .nav-link { */
        /* color: yellow !important; */
    /* } */
    /* .nav .nav-item{ */
        /* background-color: red; */
    /* } */
</style>
