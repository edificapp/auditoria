@extends('layouts.app')

@section('titulo')
INforme Proyecto
@endsection

@section('styles')
    <style>

        .supervisor {
            margin: 5px 1px 1px 1px !important;
        }

        .form-control {
            width: 100%;
        }
        .multiselect-container {
            box-shadow: 0 3px 12px rgba(0,0,0,.175);
            margin: 0;
        }
        .multiselect-container .checkbox {
            margin: 0;
        }
        .multiselect-container li {
            margin: 0;
            padding: 0;
            line-height: 0;
        }
        .multiselect-container li a {
            line-height: 25px;
            margin: 0;
            padding:0 35px;
        }
        .custom-btn {
            width: 100% !important;
        }
        .custom-btn .btn, .custom-multi {
            text-align: left;
            width: 100% !important;
        }
        .dropdown-menu > .active > a:hover {
            color:inherit;
        }

    </style>
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Informe proyecto - {{$proyecto->nombre}}</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3" >
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'informes']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
            	<div class="table-responsive">
                    <table class="display" width="100%" id="table_informes">
                        <thead class="thead-light table table-striped table-hover">
                            <tr>
                                <th>Sede</th>
                                <th>Fecha visita</th>     
                                <th>Tercero</th>
                                <th>Inspector</th>
                                @foreach($preguntas as $pregunta)
                                <th>{{$pregunta}}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($visitas as $i => $visita)
                            <tr>
                                <td>{{$visita->sede->nombre}}</td>
                                <td>{{$visita->fecha_visita}}</td>
                                <td>{{$visita->sede->tercero_proyecto->cliente_tercero->nombre}}</td>
                                <td>{{$visita->personal_proyecto->user->nombre}}</td>
                                @foreach($visita->visita_formulario_select_respuestas($formulario->formulario_g_formulario->pluck('id')->toArray()) as $respuesta)
                                	<td>
                                        @if(gettype($respuesta) == 'object')
                                            {{implode(",", $respuesta->toArray())}}
                                        @else
                                            {{$respuesta}}
                                        @endif   
                                    </td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('body').addClass('sidebar-icon-only');
        $('#table_informes thead tr').clone(true).appendTo( '#table_informes thead' );
        $('#table_informes thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
       var table =  $('#table_informes').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            filters:false,
            /*
            "columnDefs" : [
                { "targets": [5,6,7,8,9,10], "visible": false}
            ],
            */
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: "copyHtml5",
                    text:      '<i class="mdi mdi-content-copy"></i>Copiar',
                    title:'Informe de proyecto {{$proyecto->nombre}}',
                    titleAttr: 'Copiar',
                    className: 'btn btn-app export barras',
                    exportOptions: {
                        columns: ':not(.no-print):visible',
                        format: {
                          body: function(data, row, column, node) {
                            data_string = data.replace(/<[^>]+>/g, '');
                            return data_string;
                          },
                          header: function ( data, column, row ) {
                            return data;
                          },
                          footer: function(data, column) {
                            return data;
                          }
                        }   
                    }
                },
                {
                    extend: "excelHtml5",
                    text:   '<i class="mdi mdi-file-excel"></i>Excel',
                    title:'Informe de proyecto {{$proyecto->nombre}}',
                    titleAttr: 'Exportar a Excel',
                    className: 'btn btn-success export excel',
                    exportOptions: {
                        columns: ':not(.no-print):visible',
                        format: {
                          body: function(data, row, column, node) {
                            data_string = data.replace(/<[^>]+>/g, '');
                            return data_string;
                          },
                          header: function ( data, column, row ) {
                            return data;
                          },
                          footer: function(data, column) {
                            return data;
                          }
                        }   
                    }
                },
                {
                    
                    extend: "pdfHtml5",
                    text: '<i class="mdi mdi-file-pdf"></i>PDF',
                    title:'Informe de proyecto {{$proyecto->nombre}}',
                    titleAttr: 'Exportar a PDF',
                    className: 'btn btn-danger export pdf',
                    exportOptions: {
                        columns: ':not(.no-print):visible',
                        format: {
                          body: function(data, row, column, node) {
                            data_string = data.replace(/<[^>]+>/g, '');
                            return data_string;
                          },
                          header: function ( data, column, row ) {
                            return data;
                          },
                          footer: function(data, column) {
                            return data;
                          }
                        }   
                    }
                },
                'colvis'
            ],
        });

       $('[data-column]').on('click', function (e) {
 
            console.log($(this).data('column'))
            var column = table.column($(this).data('column'));
            console.log(column)
 
            // Toggle the visibility
            column.visible(!column.visible());
            console.log(table.column());
 
            return true
        });
    });
</script>
<script>
</script>
@endsection
