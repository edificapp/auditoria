@extends('layouts.app')

@section('titulo')
Editar proyecto
@endsection

@section('styles')
    <style>

        .supervisor {
            margin: 5px 1px 1px 1px !important;
        }

        .form-control {
            width: 100%;
        }
        .multiselect-container {
            box-shadow: 0 3px 12px rgba(0,0,0,.175);
            margin: 0;
        }
        .multiselect-container .checkbox {
            margin: 0;
        }
        .multiselect-container li {
            margin: 0;
            padding: 0;
            line-height: 0;
        }
        .multiselect-container li a {
            line-height: 25px;
            margin: 0;
            padding:0 35px;
        }
        .custom-btn {
            width: 100% !important;
        }
        .custom-btn .btn, .custom-multi {
            text-align: left;
            width: 100% !important;
        }
        .dropdown-menu > .active > a:hover {
            color:inherit;
        }

    </style>
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Informes proyecto - {{$proyecto->nombre}}</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3" >
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'informes']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
            	<div class="dropdown show">
				  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Selecciona un Formulario
				  </a>
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				  		@foreach($formularios as $formulario)
				    		<a class="dropdown-item" href="{{route('proyecto.informe', [$formulario->id, $proyecto->id])}}">{{$formulario->nombre}}</a>
				  		@endforeach
				  </div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
</script>
@endsection
