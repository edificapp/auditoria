@extends('layouts.app')

@section('titulo')
Grupo de Formularios
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
               <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.show',$proyecto->id) }}">Documentos proyecto {{$proyecto->nombre}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Editar Grupo de Formularios</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'documentos'])   
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <form id="form_forms" method="POST" action="{{ route('grupo-formularios.update', $grupo_formulario->id) }}">
                    @csrf
                    <input type="hidden" name="_method" value="patch">
                    <div class="row my-3">
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="name" class="input-group-text bg-light col-sm-3">Nombre</label>
                                <input  type="text" class="form-control col-sm-9 @error('name') is-invalid @enderror" name="name" value="{{ old('name') ? old('name') : $grupo_formulario->name}}" required autocomplete="nombre" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="name" class="input-group-text bg-light col-sm-5">Tipo de Muestra</label>
                                <select class="form-control" name="tipo_muestra">
                                    <option {{old('tipo_muestra') == 'Complementario' ||  $grupo_formulario->tipo_muestra == 'Complementario' ? 'selected' : ''}} value="Complementario">
                                        Complementario 
                                    </option>
                                    <option {{old('tipo_muestra') == 'Almuerzo' ||  $grupo_formulario->tipo_muestra == 'Almuerzo' ? 'selected' : ''}} value="Almuerzo">
                                        Almuerzo
                                    </option>
                                    <option {{old('tipo_muestra') == 'Industrial' ||  $grupo_formulario->tipo_muestra == 'Industrial' ? 'selected' : ''}} value="Industrial">
                                        Industrial
                                    </option>
                                    <option {{old('tipo_muestra') == 'Libre' ||  $grupo_formulario->tipo_muestra == 'Libre' ? 'selected' : ''}} value="Libre">
                                        Libre
                                    </option>
                               </select>
                            </div>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-4">
                            <div class="input-group">
                                <label for="muestra_minima" class="input-group-text bg-light col-sm-5">Muestras minimas</label>
                                <input  type="number" class="form-control col-sm-7 @error('muestra_minima') is-invalid @enderror" name="muestra_minima" value="{{ old('muestra_minima') ? old('muestra_minima') : $grupo_formulario->muestra_minima }}" required autocomplete="# muestra minima" autofocus>
                                @error('muestra_minima')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="input-group">
                                <label for="muestra_maxima" class="input-group-text bg-light col-sm-5">Muestras maximas</label>
                                <input  type="number" class="form-control col-sm-7 @error('muestra_maxima') is-invalid @enderror" name="muestra_maxima" value="{{ old('muestra_maxima') ? old('muestra_maxima') : $grupo_formulario->muestra_maxima }}" required autocomplete="# muestra maxima" autofocus>
                                @error('muestra_maxima')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <label for="name" class="input-group-text bg-light col-sm-5">Obligatorio</label>
                                <select class="form-control" name="muestra_obligatorio">
                                    <option {{old('obligatorio') == 'Si' || $grupo_formulario->muestra_obligatorio == 'Si'}} value="Si">
                                        Si
                                    </option>
                                    <option {{old('obligatorio') == 'No' || $grupo_formulario->muestra_obligatorio == 'No'}} valu
                                    e="No">
                                        No
                                    </option>
                               </select>
                            </div>
                        </div>
                    </div>
                    <div class="row my-5 ocultar" id="row_componentes">
                        <table class="table" id="table_components">
                            <thead>
                                <tr>
                                    <th>Componentes</th>
                                    <th>4 - 8 años y 11 meses</th>
                                    <th>9 - 13 años y 11 meses</th>
                                    <th>14 - 17 años y 11 meses</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <hr>
                    <hr>
                    @foreach($formularios->chunk(2) as $chunk)
                    <div class="row">
                        @foreach($chunk as $item)
                        <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" 
                                name="formularios[]" value="{{$item->id}}"
                                {{in_array($item->id, $grupo_formulario->formularios->pluck('id')->toArray()) ? 'checked':''}}>
                                {{$item->nombre}}
                              </label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                    <hr>

                    <div class="form-group row mt-2 mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
@stop


@section('scripts')
    <script type="text/javascript">
        let componentes = @json(DataCollection::componentes_muestra());
        let data_table = @json($grupo_formulario->peso_esperados);
        let tp_muestra_global = '{!!$grupo_formulario->tipo_muestra!!}';
        $(document).ready(function(){
            table_components($('select[name="tipo_muestra"]').val());
        });


        $('select[name="tipo_muestra"]').on('change', function(){
           table_components($(this).val());
        });

        function table_components(valor){
             $('#table_components>tbody').empty();
             if(valor != 'Libre'){
                array_componentes = componentes.find(e => e.tipo == valor);
                array_componentes.data.forEach((e, i) => {
                    if(valor == tp_muestra_global){
                        $('#table_components>tbody')
                        .append(`<tr>
                            <td>${e}</td>
                            <td><input name="rango_0[]" class="form-control" required type="number" value="${data_table[0][i]}"/></td>
                            <td><input name="rango_1[]" class="form-control" required type="number" value="${data_table[1][i]}"/></td>
                            <td><input name="rango_2[]" class="form-control" required type="number" value="${data_table[2][i]}"/></td>
                            </tr>`);
                    }else{
                        $('#table_components>tbody')
                        .append(`<tr>
                            <td>${e}</td>
                            <td><input name="rango_0[]" class="form-control" required type="number"/></td>
                            <td><input name="rango_1[]" class="form-control" required type="number"/></td>
                            <td><input name="rango_2[]" class="form-control" required type="number"/></td>
                            </tr>`);
                    }
                });
                $('#row_componentes').show('2000');
            }else{
                $('#row_componentes').hide('2000');
            }
        }

    </script>
@endsection
