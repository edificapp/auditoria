@extends('layouts.app')

@section('titulo')
Grupo de Formularios
@endsection

@section('styles')
    
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home "></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Grupo de Formularios</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('grupo-formularios.create') }}';" class="btn-create-form">Nuevo </button>
            </div>
        </div>
    </div>
</div>
<div>
    @include('proyecto.components.tabs', ['select' => 'formularios']) 
</div>
<div>
    @include('formulario.components.tabs', ['select' => 'grupos'])  
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <p class="card-title">Listado de Grupo de formularios</p>
                <div class="table-responsive">
                    <table  class="display" id="table" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Cantidad Formularios</th>
                                <th width="100px">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (auth()->user()->empresa->grupo_formularios as $item)
                                <tr class="body-rows">
                                    <td>{{strtolower( $item->name)}}</td>
                                    <td>{{$item->formularios->count()}}</td>
                                    <td width="100px">
                                        <div class="btn-group col-sm">
                                            <button class="btn-show-form" 
                                                onclick="location.href='{{ route('grupo-formularios.show', $item->id) }}'">
                                            </button>
                                            <button class="btn-edit-form" 
                                                onclick="location.href='{{ route('grupo-formularios.edit',$item->id) }}';" title="Editar formulario">
                                             </button>
                                            <button class="{{$item->activo  ? 'btn-activo-form':'btn-inactivo-form'}}"    onclick="location.href='{{ route('grupo-formularios.estado',$item->id) }}';" 
                                                title="Habilitar|Desabilitar formulario">
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
