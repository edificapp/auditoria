@extends('layouts.app')

@section('titulo')
Grupo de Formularios
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.show',$proyecto->id) }}">Documentos proyecto {{$proyecto->nombre}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Ver Grupo de Formularios</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3">
    @include('proyecto.components.tabs', ['select' => 'ejecucion'])   
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'documentos'])  
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <div class="row my-4">
                    <h4 class="text-center">{{$grupo_formulario->name}}</h4>
                </div>
                @foreach($grupo_formulario->formularios->chunk(2) as $chunk)
                <div class="row">
                    @foreach($chunk as $item)
                    <div class="col-md-6">
                        <a href="{{route('formularios.show', $item->id)}}" class="btn btn-link" target="_blank">
                            - {{$item->nombre}}
                        </a>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
@stop


@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
@endsection
