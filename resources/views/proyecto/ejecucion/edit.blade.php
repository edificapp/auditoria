
@extends('layouts.app')

@section('titulo')
    Editar proyecto
@endsection


@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Editar</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-3" >
        @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
    </div>
    <div >
        @include('proyecto.components.tabs_ejecucion', ['select' => '']) 
    </div>
	<div class="row">
		<div class="col-md-12 grid-margin">
			<div class="card">
				<div class="card-body borde">
					
					<div class="card">
						<div class="card-body p-3 border-0" style="background-color:#F4F6F6;">
							<div class="header-detail-propuesta">
								<div class="row">
									<div class="col-4">
										<label class="text-muted">
										</b> DASDASDASDASDASDAS </b>
										</label>
									</div>
									<div class="col-3">
										<label for=""> 
										<span class="font-weight-bold"> Nº Proceso:</span> 
										<small>#######</small>
										</label>
									</div>
									<div class="col-5">
										<label for="">
										<span class="font-weight-bold">Valor contrato: </span>
										<small>#########</small>
										</label>
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-4">
										<label for="">
										<span class="font-weight-bold">Entidad contratante: </span>
										<small></small>
										</label>
									</div>
									<div class="col-3">
										<label for="">
										<span class="font-weight-bold">Contacto: </span>
										<small>Susana Quintero</small>
										</label>
									</div>
									<div class="col-5">
										<label for="">
										<span class="font-weight-bold">Fecha de contrato: </span> 
										<small>##########</small>
										</label>
									</div>      
								</div>
								<div class="row mt-2">
									<div class="col-4"></div>
									<div class="col-3">
										<label for="">
											<span class="font-weight-bold"> Fecha acta de inicio</span>
											<small>#####</small>
										</label>
									</div>
									<div class="col-5">
										<label for="">
											<span class="font-weight-bold">Plazo de ejecucion</span>
											<small>#####</small>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row mt-4">
						<div class="col-md-6 text-center">
							<p class="h4">Ejecucion</p>
							<div class=" custom-control custom-switch mt-2">
								<label >Porcentual</label>
								<input type="checkbox" class="custom-control-input" id="customSwitch1">
								<label class="custom-control-label ml-5" for="customSwitch1">Nominal</label>
								<div class="card">
									<div class="card-body">
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6 text-center">
							<p class="h4 ">Facturacion</p>
							<div class=" custom-control custom-switch mt-2">
								<label >Porcentual</label>
								<input type="checkbox" class="custom-control-input" id="customSwitch2">
								<label class="custom-control-label ml-5" for="customSwitch2">Nominal</label>
								<div class="card">
									<div class="card-body">
										
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
