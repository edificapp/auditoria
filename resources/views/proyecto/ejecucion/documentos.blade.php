@extends('layouts.app')

@section('titulo')
Editar proyecto
@endsection

@section('styles')
    <style>

        .supervisor {
            margin: 5px 1px 1px 1px !important;
        }

        .form-control {
            width: 100%;
        }
        .multiselect-container {
            box-shadow: 0 3px 12px rgba(0,0,0,.175);
            margin: 0;
        }
        .multiselect-container .checkbox {
            margin: 0;
        }
        .multiselect-container li {
            margin: 0;
            padding: 0;
            line-height: 0;
        }
        .multiselect-container li a {
            line-height: 25px;
            margin: 0;
            padding:0 35px;
        }
        .custom-btn {
            width: 100% !important;
        }
        .custom-btn .btn, .custom-multi {
            text-align: left;
            width: 100% !important;
        }
        .dropdown-menu > .active > a:hover {
            color:inherit;
        }

    </style>
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Documentos proyecto - {{$proyecto->nombre}}</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3" >
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'documentos']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <div id="accordion" class="mt-4">
                   <div class="" id="headingOne">
                        <h5 class="mb-0">
                            <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Documentos Adicionales
                            </span>
                        </h5>
                   </div>
                   <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <button class="btn-add-form " type="button" data-toggle="modal" data-target="#documentosAdicionales" title="Documentos Adicionales">
                                
                            </button>
                            <div class="row mt-3 mb-4">
                                <div class="col-3"><b>Nombre</button></b></div>   
                                <div class="col-3"><b>Archivo</b></div>   
                                <div class="col-1"><b>Fecha</b></div>   
                                <div class="col-2"><b>Tamaño (Mb)</b></div>   
                                <div class="col-3"><b>Descripción Contenido</b></div>
                            </div>
                            @foreach($proyecto->todos_documentos as $documento)
                                <div class="row mt-2">
                                    <div class="col-3 text-small">{{$documento->nombre}}</div>   
                                    <div class="col-3 text-small"><a href="{{$documento->url_public}}" target="blank" class="btn btn-link">{{$documento->name}}</a></div>   
                                    <div class="col-1 text-small">{{$documento->fecha}}</div>   
                                    <div class="col-2 text-small" title="{{$documento->size}} Mb">{{str_limit($documento->size, 9)}}</div>   
                                    <div class="col-3 text-small">{{$documento->description}}</div>  
                                </div>
                            @endforeach 
                        </div>
                   </div>
                   {{----}}
                    <div class="mt-2" id="headingTwo">
                        <h5 class="mb-0">
                            <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Registro Reuniones  
                            </span>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <button class="btn-add-form" type="button"  data-toggle="modal" data-target="#modal_reuniones" title="Registro de Reuniones">
                            </button> 
                            <div class="row mt-3">
                                <div class="col-3 "><b>Motivo</b></div> 
                                <div class="col-2"><b>Fecha</b></div>
                            </div>
                            <br>

                            @foreach($proyecto->reuniones as $reunion)
                            <div class="row mt-1">
                                <div class="col-3">{{$reunion->motivo}}</div>   
                                <div class="col-3">{{$reunion->fecha}}</div> 
                                <div class="col-3">
                                    <button class="btn-show-form" title="Ver reunión" onclick="ver_reunion('{{route('reuniones-proyecto.show', $reunion->id)}}')"></button>
                                    <button class="btn-delete-form" onclick="borrar_reunion('{{route('reuniones-proyecto.destroy', $reunion->id)}}')" title="Borrar reunión"></button>
                                </div> 
                            </div>
                            @endforeach
                        </div>
                    </div>
                    {{----}}
                    <div class="mt-2" id="headingThree">
                        <h5 class="mb-0">
                            <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Personal  
                            </span>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <button type="button" class="btn btn-xs btn-inverse-primary btn-rounded" data-toggle="modal" data-target="#personal" title="Personal del Proyecto">
                                <i class="mdi mdi-account-multiple-plus"></i>
                            </button>
                            <div class="row mt-3">
                                <div class="col-3 "><b>Personal</b></div> 
                                <div class="col-3"><b>Cargo</b></div>
                            </div>
                            <br>
                            @foreach($proyecto->personal as $personal)
                                <div class="row mt-2">
                                    <div class="col-3">{{$personal->user->name}}</div>   
                                    <div class="col-3">{{$personal->cargo}}</div> 
                                    <div class="col-3">
                                        <button class="btn-edit-form"
                                            title="editar" 
                                            onclick="editarPersonalProyecto('{{route('personal-proyecto.edit', $personal->id)}}', '{{ route('personal-proyecto.update', $personal->id)}}')"
                                        >
                                        </button>
                                        <button type="submit" 
                                            class="btn  {{$personal->activo ? 'btn-inactivo-form' : 'btn-activo-form'}}" 
                                            title="{{$personal->activo ? 'Desactivar' : 'Activar'}}"
                                            onclick="borrarPersonalProyecto('{{route('personal-proyecto.destroy', $personal->id)}}')"    
                                        >
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                            <form action="" method="post" id="formDeletePersonal">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                            </form>
                        </div>
                    </div>
                    {{----}}
                    <div class="mt-2" id="headingFor">
                        <h5 class="mb-0">
                            <span class="collapsed pd-2 my-4" data-toggle="collapse" data-target="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                                <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i>Grupo de Formularios  
                            </span>
                        </h5>
                    </div>
                    <div id="collapseFor" class="collapse" aria-labelledby="headingFor" data-parent="#accordion">
                        <div class="card-body">
                            <button class="btn-add-form" type="button" onclick="nuevoGrupoFormulario()" title="grupo de Formularios">
                            </button>
                            <div class="table-responsive">
                                <table  class="display" id="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:20%">Nombre</th>
                                            <th style="width:20%">Cantidad Formularios</th>
                                            <th style="width:10%">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($proyecto->grupo_formularios as $item)
                                            <tr class="body-rows">
                                                <td style="width:20%">{{strtolower( $item->name)}}</td>
                                                <td style="width:20%">{{$item->formularios->count()}}</td>
                                                <td style="width:10%">
                                                    <div class="btn-group col-sm">
                                                        <button class="btn-show-form" 
                                                            onclick="location.href='{{ route('grupo-formularios.show', [$proyecto->id, $item->id]) }}'">
                                                        </button>
                                                        <button class="btn-edit-form" 
                                                            onclick="location.href='{{ route('grupo-formularios.edit', [$proyecto->id, $item->id]) }}';" title="Editar formulario">
                                                        </button>
                                                        <button class="{{$item->activo  ? 'btn-activo-form':'btn-inactivo-form'}}"    onclick="location.href='{{ route('grupo-formularios.estado',$item->id) }}';" 
                                                            title="Habilitar|Desabilitar formulario">
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="documentosAdicionales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adjuntar Documentos Contractuales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('documentos-proyecto.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="tipo" value="adicional">
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <div class="row my-3">
                <div class="input-group col-12">

                    <label class="input-group-text col-sm-3 bg-light">Nombre: </label>
                    <input 
                        type="text" 
                        name="nombre" 
                        class="form-control col-sm-9 @error('nombre') is-invalid @enderror"  
                        placeholder="Nombre del Archivo" 
                        value="{{old('nombre')}}"
                    >
                    <br>
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Descripción: </label>
                    <input 
                        type="text" 
                        name="description" 
                        class="form-control col-sm-9 @error('description') is-invalid @enderror" 
                        placeholder="Descripción" 
                        value="{{old('description')}}"
                    >
                    <br>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <br>
                <div class="col-md-6">
                   <input class="documentos" name="documentos" type="file" multiple>
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <div class="form-group">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_reuniones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Registro o Reunión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('reuniones-proyecto.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Motivo: </label>
                    <input 
                        type="text" 
                        name="motivo" 
                        class="form-control col-9 @error('motivo') is-invalid @enderror"  
                        placeholder="Motivo" 
                        value="{{old('motivo')}}"
                    >
                    <br>
                    @error('motivo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Fecha: </label>
                    <input 
                        type="date" 
                        name="fecha" 
                        class="form-control col-9 @error('fecha') is-invalid @enderror"  
                        placeholder="Fecha" 
                        value="{{old('fecha')}}"
                    >
                    <br>
                    @error('fecha')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Lugar: </label>
                    <input 
                        type="text" 
                        name="lugar" 
                        class="form-control col-9 @error('lugar') is-invalid @enderror"  
                        placeholder="Lugar" 
                        value="{{old('lugar')}}"
                    >
                    <br>
                    @error('lugar')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="row my-3 justify-content-md-center">
                <div class="col-md-6">
                   <input class="documentos" name="documentos[]" type="file" multiple>
                </div>
            </div>
            <div class="row my-3 justify-content-md-center">
                <div class="form-group">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_borrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Borrar Reunión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="" id="form_borrar">
            @csrf
            <input type="hidden" name="_method" value="delete">
            <p class="h4 text-center">¿Estas seguro de borrar esta reunión?</p>
            <br>
            <div class="row justify-content-md-center">
                <div class="form-group">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary" value="Si estoy Seguro">
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_ver_reunion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <h5 class="modal-title" id="exampleModalLabel">Detalles de Reunion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('reuniones-proyecto.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Motivo: </label>
                    <input 
                        type="text"  
                        id="show_motivo"
                        readonly 
                        class="form-control col-9" 
                    >
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Fecha: </label>
                    <input 
                        type="text"  
                        id="show_fecha"
                        readonly 
                        class="form-control col-9" 
                    >
                </div>
            </div>
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-3 bg-light">Lugar: </label>
                    <input 
                        type="text"  
                        id="show_lugar"
                        readonly 
                        class="form-control col-9" 
                    >
                </div>
            </div>
            <div class="row my-3">
                <div class="col-sm-6">
                   Nombre del documento
                </div>
                <div class="col-sm-2">
                   Tamaño
                </div>
            </div>
            <hr>
            <div class="row my-3">
                <table class="table">
                    <thead>
                        <th>Nombre del documento</th>
                        <th>Tamaño</th>
                    </thead>
                    <tbody id="show_documentos" class="text-small"></tbody>
                </table>
            </div>
        </form>
        <div class="row justify-content-md-center">
            <div class="form-group">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="personal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Relacionar Funcionarios al Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{ route('personal-proyecto.store') }}">
            @csrf
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <div class="row my-3">
                <div class="col-md-6">
                    <div class="input-group">
                        <label for="contacto" class="input-group-text bg-light col-sm-4">Personal</label>
                        <select class="form-control col-sm-9" name="user_id">
                            @foreach(Auth::user()->empresa->usuarios as $personal)
                                <option value="{{$personal->id}}">{{$personal->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <label for="contacto" class="input-group-text bg-light col-sm-4">Cargo</label>
                        <select class="form-control col-sm-9" name="cargo">
                                <option >Coordinador</option>
                                <option >Supervisor</option>
                                <option >Inspector</option>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="row justify-content-md-center">
                <div class="form-group">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="personalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Funcionarios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" id="formUpdatePersonal">
                    @csrf
                    <input type="hidden" name="_method" value="patch">
                    <div class="row my-3">
                       <div class="col-md-6">
                            <div class="input-group">
                                <label for="contacto" class="input-group-text bg-light col-sm-4">Personal</label>
                                <select class="form-control col-sm-9" name="user_id" id="personalSelect">
                                    @foreach(Auth::user()->empresa->usuarios as $personal)
                                        <option value="{{$personal->id}}">{{$personal->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="contacto" class="input-group-text bg-light col-sm-4">Cargo</label>
                                <select class="form-control col-sm-9" name="cargo" id="cargoSelect">
                                        <option value="Coordinador">Coordinador</option>
                                        <option value="Supervisor">Supervisor</option>
                                        <option value="Inspector">Inspector</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-md-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#userSelect').select2();

        $('#tablaDocumentos, .table-crud').DataTable ({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            paging:false, info: false, ordering:false, searching:false
            
        });
    });

    $(".documentos").fileinput({
        'language': "es",
        'theme': "fa",
        'showUpload' : false , 
        'maxFilesNum': 10,
    });

    function nuevoGrupoFormulario(){
        //localStorage.setItem('proyecto_id', {!!$proyecto->id!!});
        location.href="{!!Route('grupo-formularios.create', $proyecto->id)!!}";
    }

    function ver_reunion(url){
        $.get(url, function(data){
            console.log(data);
            $('#show_motivo').val(data.motivo);
            $('#show_fecha').val(data.fecha);
            $('#show_lugar').val(data.lugar);
            $('#show_documentos').empty();
            console.log(data.documentos);
            $.each(data.documentos, function( index,value ) {
              $('#show_documentos').append('<tr>'+
                '<td>'+
                    '<a href="'+value.url_public+'">'+value.name+'</a>'+
                '</td>'+
                '<td>'+
                    value.size+
                '<td>'+
            '</tr>');
            });
        });

        $('#modal_ver_reunion').modal();
    }

     function borrar_reunion(url){
        $('#modal_borrar').modal();
        $('#form_borrar').attr('action', url);
    }

     function editarPersonalProyecto(edit, update){
        $.get(edit, function(data){
            $('#personalEdit').modal();
            $(`#personalSelect option[value="${data.user_id}"]`).attr("selected",true);
            $(`#cargoSelect option[value="${data.cargo}"]`).attr("selected",true);
            $('#formUpdatePersonal').attr('action', update);
        });
    }

    function borrarPersonalProyecto(url){
        $('#formDeletePersonal').attr('action', url).submit();
    }

</script>
@endsection
