@extends('layouts.app')

@section('titulo')
Proyectos
@endsection
@section('styles')
    
@stop

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Poyectos en ejecucion</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <p class="card-title">Listado de proyectos en Ejecución</p>
                <div class="table-responsive">
                    <table class="display" id="table" width="100%">
                        <thead>
                            <tr>
                                <th style="width: 15%">Nombre</th>
                                <th style="width: 10%">Entidad Contratante</th>
                                <th style="width: 10%">No. Proceso</th>
                                <th style="width: 15%">Contacto</th>
                                <th style="width: 4%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($proyectos as $proyecto)
                            <tr class="body-rows">
                                <td style="width: 15%">{{strtolower($proyecto->nombre)}}</td>
                                <td style="width: 10%">{!! $proyecto->entidad_contratante !!}</td>
                                <td style="width: 10%">{!! $proyecto->n_proceso !!}</td>
                                <td style="width: 15%">{!! $proyecto->contacto ? $proyecto->contacto->nombre : 'No tiene contacto' !!}</td>
                                <td style="width: 4%">
                                    <button class="btn-edit-form" onclick="location.href='{{ route('proyectos.show',$proyecto->id) }}';" title="Gestionar Proyecto"></button>
                                    {{--
                                    @if($proyecto->documentos_contractuales->count() > 0 && $proyecto->documentos_tecnicos->count() > 0 && $proyecto->validar_personal($proyecto->personal))
                                        <a href="{{route('proyectos.cambiar.estado', ['cerrado', $proyecto->id])}}" class="btn btn-inverse-primary"><span class="mdi mdi-arrow-right-bold"></span></a>
                                    @endif
                                        --}}
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection
