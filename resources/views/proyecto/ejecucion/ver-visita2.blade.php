@extends('layouts.app')

@section('titulo')
Editar proyecto
@endsection
@section('styles')
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css">
 <style type="text/css">
   .a-foto:hover{
      -webkit-box-shadow: 3px -1px 24px -4px rgba(0,0,0,0.75);
      -moz-box-shadow: 3px -1px 24px -4px rgba(0,0,0,0.75);
      box-shadow: 3px -1px 24px -4px rgba(0,0,0,0.75);
   }

   .map {
        height: 500px;
        width: 100%;
    }
 </style>
@endsection


@section('contenido')
<div class="row" id="app">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Ejecución del Proyecto - {{$proyecto->nombre}}</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3" >
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'ejecucion']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-header" style="font-size: 12px;">
              <div class="row mt-2">
                <div class="col-md-4 col-xs-12">
                    <b>Fecha de la visita:</b> 
                    {!!$visita->fecha_visita!!}
                </div>
                 <div class="col-md-4 col-xs-12">
                    <b>Inspector:</b> 
                      {!!$visita->personal_proyecto->user->name!!}
                </div>
                <div class="col-md-4 col-xs-12">
                    <b>{!!$proyecto->tipo_terceros!!}:</b> 
                      {{$visita->sede->tercero_proyecto->cliente_tercero->nombre}}
                </div>
              </div>
              <div class="row mt-2">
                <div class="col-md-6 col-xs-12">
                    <b>{{$tipo_nucleo = is_null($proyecto->tipo_nucleo) ? 'no tiene Nucleo' : $proyecto->tipo_nucleo}}:</b> 
                    {{$visita->sede->instituto ? $visita->sede->instituto->nombre : 'Sin '.$tipo_nucleo}}
                </div>
                <div class="col-md-6 col-xs-12">
                    <b>{!!$proyecto->tipo_lugar!!}:</b> 
                      {!!$visita->sede->nombre!!}
                </div>
              </div>
              @if(!is_null($visita->observacion))
              <div class="row">
                <div class="col-12">
                    <p style="background: rgba(0,0,0,.4); color:white; border-radius: 5px; padding: 4px;">
                      {{$visita->observacion}}.
                    </p>
                </div>
              </div>
              @endif
            </div>
            <div class="card-body borde">
              @foreach($visita->visita_formularios as $fv)
              <div class="row">
              </div><div class="col-6">
                <table>
                  <tr>
                    <td>{!!json_decode($fv->formulario_g_formulario->formulario->formulario)!!}</td>
                    <td>{!!$fv->respuestas!!}</td>
                  </tr>
                  {{--
                  @foreach(json_decode($fv->formulario_g_formulario->formulario->formulario) as $input)
                    <tr>
                      <td>{{$input->name}}</td>
                      <td>{{$input}}</td>
                    </tr>
                  @endforeach
                    --}}
                </table>
              </div>
              {{--
              </div><div class="col-6">
                <table>
                  @foreach($fv->respuestas->pluck('res', 'name')->toArray() as $index => $res)
                    <tr>
                      <td>{{$index}}</td>
                      <td>{{$res}}</td>
                    </tr>
                  @endforeach
                </table>
              </div>
                --}}
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-render.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqueryform/form-builder.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsP_NIbbB0MO99Rx5xZswqTdP2-qEzukw&callback=initMap"></script>
<script>
    var map = [];
      

    $(document).ready(function() {
        setTimeout(function() {
            activarFormularios();
        }, 3000);
        $("#lightgallery").lightGallery();
    });

    function activarFormularios() {
        var options = {
            showActionButtons: false,
        };

        @foreach ($visita->visita_formularios as $clave => $v_formulario)

        pintarMapas(
          'map_inicio{{$clave}}', 
          'Ubicación Inicial {{$v_formulario->ffhh_inicio}}', 
          parseFloat({{$v_formulario->latitud_inicio}}), 
          parseFloat({{$v_formulario->longitud_inicio}})
        );

        pintarMapas(
          'map_final{{$clave}}', 
          'Ubicación Final {{$v_formulario->ffhh_final}}', 
          parseFloat({{$v_formulario->latitud_final}}), 
          parseFloat({{$v_formulario->longitud_final}})
        );

        var fb_ed = $(document.getElementById('fb-editor_{!! $clave !!}')).formBuilder(options);
        
        setTimeout(function() {
            fb_ed.actions.setData(@json($v_formulario->formulario));
            
            var formRenderOpts = {
                dataType: 'xml',
                formData: fb_ed.actions.getData('xml')
            };

            var renderedForm = $('<div>');
            renderedForm.formRender(formRenderOpts);

            $("#fb-editor_{!! $clave !!}").html(renderedForm.html());
            $("#fb-editor_{!! $clave !!}").show();

            sleep(1000);

            $.each($('#fb-editor_'+{!! $clave !!}).find('select,input,textarea'), function(ffindex, ffs_Select) {
                var ffsel = $(ffs_Select);
                ffsel.attr('disabled', 'true');
            });     
        }, 1000);

        @endforeach
    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }

    function pintarMapas(id, title, lat, lon){
        console.log(id, [lat,lon,title])
        map[id] = new google.maps.Map(document.getElementById(id), {
          center: {lat: lat, lng: lon},
          zoom: 15
        });

        var marker = new google.maps.Marker({
          position: {lat: lat, lng: lon},
          map: map[id],
          title: title
        });
    }
</script>
@endsection