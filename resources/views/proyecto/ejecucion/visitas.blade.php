@extends('layouts.app')

@section('titulo')
Editar proyecto
@endsection

@section('styles')
<style>
    #alert_danger{
        display: none;
    }
</style>
@endsection

@section('contenido')
<div class="alert alert-danger" id="alert_danger" role="alert">
    <strong id="text_alert_danger"></strong>
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Visitas del Proyecto - {{$proyecto->nombre}}</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3" >
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'visitas']) 
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <div class="col-md-12 grid-margin">
                    <div class="d-flex justify-content-between flex-wrap">
                        <div class="d-flex align-items-end flex-wrap">
                            <div class="d-flex">
                                <button class="btn-create-form" type="button" data-toggle="modal" data-target="#modal_add_visita">
                                    Agregar Visita
                                </button>
                            </div>
                        </div>
                        @can('crear-empleados')
                            <div class="d-flex justify-content-around align-items-end flex-wrap">
                                <div class="input-group">
                                    <label class="input-group-text col-sm-3 bg-light "><b>Asignar</b></label>
                                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-inverse-secondary col-sm-auto">
                                        Selecciona una Asignación
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><button class="btn btn-link" onclick="asignar('inspector')">Asignar Inspector(es)</button></li>
                                        <li><button class="btn btn-link" onclick="asignar('formulario')">Asignar Formulario(s)</button></li>
                                    </ul>
                                </div>
                            </div>
                        @endcan
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="table-responsive">
                            <p class="card-title">Listado de visitas</p>
                            <table  class="display" id="table" width="100%">
                                <thead>
                                    <th><i class="mdi mdi-checkbox-marked-outline"></i></th>
                                    <th>Lugar</th>
                                    <th>Fecha Visita</th>
                                    <th>Dirección</th>
                                    <th>Ciudad</th>
                                    <th>Observaciones</th>
                                    <th title="Inspectores" class="text-center"><span class="mdi mdi-account-switch"></span></th>
                                    <th title="Formularios" class="text-center"><span class="mdi mdi-view-quilt"></span></th>
                                </thead>
                                <tbody>
                                    @foreach($proyecto->visitas as $item)
                                        <tr class="body-rows">
                                            <td><input type="checkbox" class="form-check select_lugar" value="{{$item->id}}"></td>
                                            <td>salon{{$item->nombre}}</td>
                                            <td>{{$item->fecha_visita}}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{$item->observacion}}</td>
                                            <td title="Inspectores">
                                                <button class="btn btn-inverse-secondary" onclick="inspectores('{{route('visitas.inspectores.ajax', $item->id)}}')">
                                                    <span class="mdi mdi-account-switch"></span>
                                                    {{-- {{$item->inspectores->count()}} --}}
                                                </button>
                                            </td>
                                            <td title="Formularios">
                                                <button class="btn btn-inverse-secondary" onclick="formularios('{{route('visitas.formularios.ajax', $item->id)}}')">
                                                    <span class="mdi mdi-view-quilt"></span>
                                                    {{-- {{$item->formularios->count()}} --}}
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>         
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL AGREGAR VISITA-->
<div class="modal fade" id="modal_add_visita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Visita</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('visitas-proyecto.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
            <input type="hidden" name="tipo" value="manual" id="tipo">
            <div class="row my-3">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Creación: </label>
                    <select class="form-control col-sm-7" name="contacto_id" id="select_tipo">
                        <option>Manualmente</option>
                        <option>Ingresar Base de Datos</option>
                    </select>
                    <button type="button"
                            class="btn btn-outline-secondary btn-rounded btn-icon ml-1" 
                            data-html="true" 
                            data-toggle="tooltip" 
                            title="<img src='{{url('img/ayudas/ayuda_lugar.png')}}' />"
                            id="ayuda_lugar">
                        <i class="mdi mdi-comment-question-outline"></i>
                    </button>
                </div>
            </div>
            <div class="row mb-4 manualmente">
                <div class="input-group col-6 ">
                    <label class="input-group-text col-sm-3 bg-light">Lugar: </label>
                    <input class="form-control col-sm-9" name="lugar">
                </div>
                <div class="input-group col-6 ">
                    <label class="input-group-text col-sm-3 bg-light">Fecha: </label>
                    <input type="date" class="form-control col-sm-9" name="fecha">
                </div>
            </div>
            <div class="row mb-4 manualmente">
                <div class="input-group col-6">
                    <label class="input-group-text col-sm-5 bg-light">Departamento: </label>
                    <select class="form-control" id="departamento" name="departamento">
                        @foreach($departamentos as $item)
                        <option value="{{$item->id}}">{{$item->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group col-6">
                    <label class="input-group-text col-sm-3 bg-light">Ciudad: </label>
                    <select class="form-control" id="ciudad" name="ciudad">
                    </select>
                </div>
            </div>
            <div class="row mb-4 manualmente">
                <div class="input-group col-6">
                    <label class="input-group-text col-sm-3 bg-light">Dirección: </label>
                    <input class="form-control col-sm-9" name="direccion" placeholder="Dirección">
                </div>
                <div class="input-group col-6">
                    <label class="input-group-text col-sm-3 bg-light">Barrio: </label>
                    <input class="form-control col-sm-9" name="barrio" placeholder="Barrio">
                </div>
            </div>
            <div class="row mb-4 manualmente">
                <div class="input-group col-12">
                    <label class="input-group-text col-sm-3 bg-light">Observaciones: </label>
                    <textarea rows="4" class="form-control" name="observacion"></textarea>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-6" id="documento">
                    <input class="documentos" name="documento" type="file">
                </div>
            </div>

            <div class="row mt-3 justify-content-md-center">
                <div class="form-group">
                    <button type="button"  class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>

{{-- MODAL ASIGNAR --}}
<div class="modal fade" id="modal_asignar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Asignar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('visita.asignar')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="tipo_asignacion" id="tipo_asignacion">
            <div id="contenedor_visitas" style="display: none;"></div>

            <table class="table table-hover" id="table_formulario">
                <tbody>
                    @foreach($formularios as $item)
                        <tr >
                            <td><input type="checkbox" class="form-check select_formulario" name="formulario[]" value="{{$item->id}}"></td>
                            <td class="col-xs">{{$item->nombre}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
          
            <table class="table table-hover" id="table_inspector">
                <tbody>
                    @foreach($proyecto->personal->filter(function($item){  return $item->cargo == 'Inspector'; }) as $item)
                        <tr>
                            <td><input type="checkbox" class="form-check select_inspector" name="inspector[]" value="{{$item->user->id}}"></td>
                            <td>{{$item->user->nombre}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            

            <div class="row m-3 justify-content-md-center">
                <div class="form-group">
                    <button type="button"  class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary"> Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

{{-- MODAL INSPECTORES --}}
<div class="modal fade" id="modal_inspectores_visita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Inspectores de la visita</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
            <thead>
                <th>Inspector</th>
            </thead>
            <tbody id="tbody_inspectores"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

{{-- MODAL FORMULARIOS VISITA --}}
<div class="modal fade" id="modal_formularios_visita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Formularios de la visita</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
            <thead>
                <th>Formularios</th>
            </thead>
            <tbody id="tbody_formularios"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
        select_tipo()
        table();
        departamento_ciudades();
        });


        $(".documentos").fileinput({
            'language': "es",
            'theme': "fa",
            'showUpload' : false , 
        });

        $('#select_tipo').on('change', function(){
            select_tipo();        
        })

        $('#departamento').on('change', function(){
            $('#ciudad').empty();
            departamento_ciudades();
        })

        function inspectores(url){
            $.get(url, function( data ) {
                $('#tbody_inspectores').empty();
                $.each(data, function( index, value ) {

                $('#tbody_inspectores').append(
                    '<tr>'+
                    '<td>'+
                    '<a href="'+value.url+'">'+value.nombre+
                    '</td>'+
                    '<tr>'
                );
                });

                $('#modal_inspectores_visita').modal();
            });
        }

        function formularios(url){
            $.get(url, function( data ) {
                $('#tbody_formularios').empty();
                $.each(data, function( index, value ) {
                $('#tbody_formularios').append(
                    '<tr>'+
                    '<td>'+
                    '<a href="'+value.url+'">'+value.nombre+
                    '</td>'+
                    '<tr>'
                );
                });

                $('#modal_formularios_visita').modal();
            });
        }

        function departamento_ciudades(){
            var id = $('#departamento').val();
            $.get( "/departamento/"+id, function( data ) {
                $.each(data, function( index, value ) {
                $('#ciudad').append('<option value="'+value.id+'">'+value.nombre+'</option>');
                });
            });
        }

        function select_tipo(){
            select = $('#select_tipo').val();
            if(select == "Manualmente"){
                $('.manualmente').show();
                $('#documento, #ayuda_lugar').hide();
                $('#tipo').val('manualmente');
            }else{
                $('.manualmente').hide();
                $('#documento, #ayuda_lugar').show();
                $('#tipo').val('documento');
            }
        }

        $('#ayuda_lugar').tooltip({
            animated: 'fade',
            placement: 'bottom',
            html: true
        });


        function asignar(tipo){
            var contador = 0;
            $('#contenedor_visitas').empty();
            $(".select_lugar").each(function(){
                if($(this).is(":checked")){
                    contador++
                    $('#contenedor_visitas').append('<input type="hidden" name="visita_id[]" value="'+$(this).val()+'">');
                }
            });
            if(contador == 0){
                $('#alert_danger').show();
                $('#text_alert_danger').text('debe escoger minimo 1 visita');
                setTimeout(function(){  $('#alert_danger').hide(); }, 3000);
            }else{
                $('#modal_asignar').modal();
                $('#tipo_asignacion').val(tipo);
                if(tipo == 'inspector'){
                    $('#table_inspector').show();
                    $('#table_formulario').hide();
                }else{
                    $('#table_inspector').hide();
                    $('#table_formulario').show();
                }
            }
        }

        



    </script>
@endsection
