
@extends('layouts.app')

@section('titulo')
    Editar proyecto
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Financiero</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-3" >
        @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
    </div>
    {{-- <div >
        @include('proyecto.components.tabs_ejecucion', ['select' => 'financiero']) 
    </div> --}}
    <div class="row">
        <div class="col-md-12 grid margin">
            <div class="card">
                <div class="card-body borde">
                    <div id="accordion" class="mt-3">

                        {{-- VIATICOS --}}
                        <div class="" id="headingOne">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Viaticos
                                </span>
                            </h5>
                       </div>
                       <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form " type="button" data-toggle="modal" data-target="#modalViaticos" title="Viaticos">
                                </button>

                                <table id="tabla"  class="display table-responsive" width="100%">
                                    <thead>
                                        <th>Beneficiario</th>
                                        <th>Motivo</th>
                                        <th>Destino</th>
                                        <th>Fecha</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-capitalize">viviana sanchez marmolejo</td>
                                            <td>Visita unica</td>
                                            <td>Jamundi</td>
                                            <td>10-06-2020</td>
                                            <td>
                                                <button class="btn-show-form" type="button"></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                       </div>

                       {{-- OTROS GASTOS --}}
                        <div class="mt-3" id="headingTwo">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Otros gastos
                                </span>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form " type="button" data-toggle="modal" data-target="#modalGastos" title="">                               
                                </button>
                            </div>
                        </div>

                        {{-- FACTURACION --}}
                        <div class="mt-3" id="headingThree">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Facturacion
                                </span>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <button class="btn-add-form " type="button" data-toggle="modal" data-target="#modalFactura" title="agregar facturas">                               
                                </button>

                                <table class="display table-responsive tabla" width="100%">
                                    <thead>
                                        <th>No. Factura</th>
                                        <th>Fecha</th>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>%</th>
                                        <th>Estado</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-capitalize">f0-00980</td>
                                            <td>20-06-2020</td>
                                            <td>Tercer cobro parcial</td>
                                            <td>$$$$$$$$$$</td>
                                            <td>30%</td>
                                            <td>Facturada</td>
                                            <td>
                                                <button class="btn-show-form" type="button"></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-capitalize">f0-00975</td>
                                            <td>20-05-2020</td>
                                            <td>Segundo cobro parcial</td>
                                            <td>$$$$$$$$$$</td>
                                            <td>20%</td>
                                            <td>Radicada</td>
                                            <td>
                                                <button class="btn-show-form" type="button"></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

	{{-- MODAL  --}}
    <div id="modalViaticos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Agregar desembolso de viaticos</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">

                        <div class="row my-3">
                            <div class="input-group col-md-12">
                                <label for="" class="input-group-text bg-light col-sm-3">Beneficiario</label>
                                <select name="" id="" class="form-control col-sm-9">
                                    <option value="">Leidy Johana Cartagena</option>
                                </select>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-md-12">
                                <label for="" class="input-group-text bg-light col-sm-2">Motivo</label>
                                <select name="" id="" class="form-control col-sm-9">
                                    <option value="">Visita unica</option>
                                    <option value="">Multiple visita</option>
                                    <option value="">Otro</option>
                                </select>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="input-group col-md-12">
                                <label for="" class="input-group-text bg-light col-sm-2">Destino</label>
                                <input type="text" class="form-control col-sm-9">
                            </div>
                        </div>

                        <div class="row my-3">
                            <div class="col-md-12 input-group">
                                <label for="" class="input-group-text bg-light col-sm-3">Fecha viaje</label>
                                <input type="text" class="form-control col-sm-9">
                            </div>
                        </div>

                        <div class="row my-3">
                            <div class="col-md-12 input-group">
                                <label for="" class="input-group-text bg-light col-sm-2">Valor</label>
                                <input type="text" class="form-control col-sm-9">
                            </div>
                        </div>

                        <div class="row mt-3 justify-content-md-center">
                            <div class="form-group">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

	{{-- MODAL FACTURA --}}
	<div id="modalFactura" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title">Agregar Facturas</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-3">No. Factura</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Fecha facturacion</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Descripcion</label>
								<textarea id="my-textarea" class="form-control" name="" rows="3"></textarea>
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Cantidad items</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-5">Valor antes impuestos</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">valor IVA</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Valor total</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="col-md-12 input-group">
								<label for="" class="input-group-text bg-light col-sm-4">Porcentaje factura</label>
								<input type="text" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row mt-4 justify-content-md-center">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script>
          $(document).ready(function(){
            $('#tabla, .tabla').DataTable ({
                paging:false, info:false, searching:false, ordering:false
            });
          });
    </script>
@endsection