
@extends('layouts.app')

@section('titulo')
    Editar proyecto
@endsection

@section('styles')
	<style>
		 /* TABLAS */
		 th { white-space:nowrap; }
        
		table.tablaInfo thead th{
			font-size: 15px;
			width:12px;
			padding: 12px;
			padding-left:10px;
			padding-top: 10px;
			text-align: center;
			/* border-style: none; */
			border: 1px solid #808B96;
		}
	
		table.tablaInfo tbody td {
			padding: 4px !important;
			padding-left:10px !important;
			font-family: 'Roboto', sans-serif !important;
			color: #030000 !important;
			font-size: 15px;
			border: 1px solid #808B96;
		}
	</style>
@endsection

@section('contenido')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="d-flex">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-2">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                                <li class="breadcrumb-item"><a href="#">Financiero</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <b>Viaticos</b></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-3" >
        @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
    </div>
    {{-- <div >
        @include('proyecto.components.tabs_ejecucion', ['select' => 'financiero']) 
    </div> --}}
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body borde">
                    <h5 class="card-title">Soporte viaticos</h5>

                    <div class="row">
                        <div class="col-sm-2">
                            <label for="">
								<span>Motivo</span>
							</label>
							<p>
								
							</p>
                        </div>
						<div class="input-group col-sm-6">
							<label for="" class="ml-5 p-2"> 
								Visita unic. Lugar(es)
							</label>
							<input type="text" placeholder="asginar" class="form-control" >
						</div>
						<div class="col-sm-4">
							<label for="">
								<span>Valor asignado</span>
							</label>
							$$$$$$$$$$$$$
						</div>
                    </div>
					
					<div id="accordion">
						{{-- HOSPEDAJE --}}
						<div class="mt-3" id="headingOne">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Hospedaje
                                </span>
                            </h5>
                       </div>
                  
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
								<button class="btn-add-form mb-3" type="button" title="añadir hospedaje" data-toggle="modal" data-target=""></button>
								<table class="table-responsive tablaInfo" width="100%">
									<thead>
										<tr>
											<th rowspan="2">No.</th>
											<th colspan="2">Fecha</th>
											<th rowspan="2">Detalle</th>
											<th rowspan="2">Proveedor</th>
											<th colspan="2">Comprobante</th>
											<th rowspan="2">Valor</th>
											<th rowspan="2">Soporte</th>
										</tr>
										<tr>
											<th>Desde</th>
											<th>Hasta</th>
											<th>Tipo</th>
											<th>Numero</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td style="width:10%">06-02-2020</td>
											<td style="width:10%">07-02-2020</td>
											<td>#######</td>
											<td>#######</td>
											<td>#######</td>
											<td>##</td>
											<td>#####</td>
											<td>$$$$$$$$$$$$</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						{{-- ALIMENTACION --}}

						<div class="mt-3" id="headingTwo">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Alimentacion
                                </span>
                            </h5>
                       </div>
                  
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
								<button class="btn-add-form mb-3" type="button" title="añadir hospedaje" data-toggle="modal" data-target="#modalViaticos"></button>
								<table class="table-responsive tablaInfo" width="100%">
									<thead>
										<tr>
											<th rowspan="2">No.</th>
											<th rowspan="2">Fecha</th>
											<th rowspan="2">Lugar</th>
											<th rowspan="2">Detalle</th>
											<th rowspan="2">Proveedor</th>
											<th colspan="2">Comprobante</th>
											<th rowspan="2">Valor</th>
											<th rowspan="2">Soporte</th>
										</tr>
										<tr>
											<th>Tipo</th>
											<th>Numero</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td style="width:10%">06-02-2020</td>
											<td style="width:10%">07-02-2020</td>
											<td>#######</td>
											<td>#######</td>
											<td>#######</td>
											<td>##</td>
											<td>#####</td>
											<td>$$$$$$$$$$$$</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						{{-- MOVILIDAD --}}

						<div class="mt-3" id="headingThree">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Movilidad 
                                </span>
                            </h5>
                       </div>
                  
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
								<button class="btn-add-form mb-3" type="button" title="añadir hospedaje" data-toggle="modal" data-target=""></button>

								<table class="table-responsive tablaInfo" width="100%">
									<thead>
										<tr>
											<th rowspan="2">No.</th>
											<th rowspan="2">Fecha</th>
											<th rowspan="2">Lugar</th>
											<th rowspan="2">Detalle</th>
											<th rowspan="2">Proveedor</th>
											<th colspan="2">Comprobante</th>
											<th rowspan="2">Valor</th>
											<th rowspan="2">Soporte</th>
										</tr>
										<tr>
											<th>Tipo</th>
											<th>Numero</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td style="width:10%">06-02-2020</td>
											<td style="width:10%">cali valle</td>
											<td>######################</td>
											<td>#######</td>
											<td>#######</td>
											<td>##</td>
											<td>#####</td>
											<td>$$$$$$$$$$$$</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						{{-- GASTOS SIN COMPROBANTES--}}

						<div class="mt-3" id="headingFour">
                            <h5 class="mb-0">
                                <span class="pd-2 my-4 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    <i class="mdi mdi-menu-right" style="color:black;font-weight:bold"></i> Gastos sin comprobantes
                                </span>
                            </h5>
                       </div>
                  
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
								<button class="btn-add-form mb-3" type="button" title="añadir hospedaje" data-toggle="modal" data-target=""></button>

								<table class="table-responsive tablaInfo" width="100%">
									<thead>
										<tr>
											<th>No.</th>
											<th>Fecha</th>
											<th>Lugar</th>
											<th>Detalle</th>
											<th>Valor</th>
											<th>Soporte</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td style="width:10%">06-02-2020</td>
											<td style="width:10%">cali valle</td>
											<td>######################</td>
											<td>$$$$$$$$$$$$</td>
											<td>######</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="d-flex justify-content-end flex-wrap">
						<div class="d-flex align-items-end flex-wrap">
							<label for="">
								<span>Total soportado</span>
								$ $$$$$$$$$$$
							</label>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
	{{-- MODAL--}}
	<div id="modalViaticos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="my-modal-title"></h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Fecha</label>
								<input type="date" name="" id="" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Lugar</label>
								<input type="text" name="" id="" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Detalle</label>
								<input type="text" name="" id="" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Proveedor</label>
								<input type="text" name="" id="" class="form-control col-sm-9">
							</div>
						</div>
						<div class="row my-3">
							<div class="input-group col-md-3">
								<label for="" class="input-group-text pl-2 bg-transparent col-sm-12">Comprobante</label>
							</div>
							<div class="input-group col-md-4">
								<label for="" class="input-group-text bg-light col-sm-5">Tipo</label>
								<input type="text" name="" id="" class="form-control col-sm-9">
							</div>
							<div class="input-group col-md-5">
								<label for="" class="input-group-text bg-light col-sm-6">Numero</label>
								<input type="text" name="" id="" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Valor</label>
								<input type="text" name="" id="" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row my-3">
							<div class="input-group col-md-12">
								<label for="" class="input-group-text bg-light col-sm-3">Soporte</label>
								<input type="file" name="" id="" class="form-control col-sm-9">
							</div>
						</div>

						<div class="row justify-content-md-center mt-4">
							<div class="form-group">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection