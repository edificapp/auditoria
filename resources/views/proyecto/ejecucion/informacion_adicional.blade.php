@extends('layouts.app')

@section('titulo')
Editar proyecto
@endsection

@section('styles')
<style>
    #alert_danger{
        display: none;
    }
</style>
@endsection

@section('contenido')
<div class="alert alert-danger" id="alert_danger" role="alert">
    <strong id="text_alert_danger"></strong>
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.ejecucion') }}">Proyectos en ejecucion</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> <b>Informacion Adicional Proyecto - {{$proyecto->nombre}}</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my-3" >
    @include('proyecto.components.tabs', ['select' => 'ejecucion']) 
</div>
<div >
    @include('proyecto.components.tabs_ejecucion', ['select' => 'informacion_adicional']) 
</div>
<div class="row" id="app">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body borde">
                <div class="row mt-3">
                    <div class="col-12">
                        <informacion-adicional :proyecto="{{json_encode($proyecto)}}"></informacion-adicional>
                    </div>         
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_add_visita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Visita</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('visitas-proyecto.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
              <input type="hidden" name="tipo" value="manual" id="tipo">
              <div class="row my-3">
                  <div class="input-group col-12 mb-4">
                      <label class="input-group-text col-sm-3 bg-light">Creación: </label>
                      <select class="form-control col-sm-7" name="contacto_id" id="select_tipo">
                          <option>Manualmente</option>
                          <option>Ingresar Base de Datos</option>
                      </select>
                      <button type="button"
                              class="btn btn-outline-secondary btn-rounded btn-icon ml-1" 
                              data-html="true" 
                              data-toggle="tooltip" 
                              title="<img src='{{url('img/ayudas/ayuda_lugar.png')}}' />"
                              id="ayuda_lugar">
                          <i class="mdi mdi-comment-question-outline"></i>
                      </button>
                  </div>
              </div>
              <div class="row mb-4 manualmente">
                  <div class="input-group col-6 ">
                      <label class="input-group-text col-sm-3 bg-light">Lugar: </label>
                      <input class="form-control col-sm-9" name="lugar">
                  </div>
                  <div class="input-group col-6 ">
                      <label class="input-group-text col-sm-3 bg-light">Fecha: </label>
                      <input type="date" class="form-control col-sm-9" name="fecha">
                  </div>
              </div>
              <div class="row mb-4 manualmente">
                  <div class="input-group col-6">
                      <label class="input-group-text col-sm-5 bg-light">Departamento: </label>
                      <select class="form-control" id="departamento" name="departamento">
                          @foreach($departamentos as $item)
                          <option value="{{$item->id}}">{{$item->nombre}}</option>
                          @endforeach
                      </select>
                  </div>
                  <div class="input-group col-6">
                      <label class="input-group-text col-sm-3 bg-light">Ciudad: </label>
                      <select class="form-control" id="ciudad" name="ciudad">
                      </select>
                  </div>
              </div>
              <div class="row mb-4 manualmente">
                  <div class="input-group col-6">
                      <label class="input-group-text col-sm-3 bg-light">Dirección: </label>
                      <input class="form-control col-sm-9" name="direccion" placeholder="Dirección">
                  </div>
                  <div class="input-group col-6">
                      <label class="input-group-text col-sm-3 bg-light">Barrio: </label>
                      <input class="form-control col-sm-9" name="barrio" placeholder="Barrio">
                  </div>
              </div>
              <div class="row mb-4 manualmente">
                  <div class="input-group col-12">
                      <label class="input-group-text col-sm-3 bg-light">Observaciones: </label>
                      <textarea rows="4" class="form-control" name="observacion"></textarea>
                  </div>
              </div>
              <div class="row justify-content-md-center">
                  <div class="col-6" id="documento">
                      <input class="documentos" name="documento" type="file">
                  </div>
              </div>

                <div class="row my-3 justify-content-md-center">
                    <div class="form-group">
                        <button type="button"  class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
              </div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection

