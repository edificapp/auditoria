@extends('layouts.app')

@section('titulo')
Proyectos
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb p-2">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('proyectos.index') }}">Proyectos</a></li>
                            <li class="breadcrumb-item">{!! $proyecto->nombre !!}</li>
                            <li class="breadcrumb-item active" aria-current="page"><b>Visitas</b></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card col-md-12">
        <div class="card-body">
          <h4 class="card-title">{{$proyecto->nombre}}</h4>
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#visitas" role="tab" aria-selected="true">Visitas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#espectadores" role="tab" aria-selected="false">Espectadores</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#contratistas" role="tab" aria-selected="false">Contratistas</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#terceros" role="tab" aria-selected="false">Terceros</a>
            </li>
          </ul>
            <div class="tab-content mt-4">
                <div class="tab-pane fade active show" id="visitas" role="tabpanel" aria-labelledby="home-tab">
                    <div class="form-group">
                        <button type="button" onclick="location.href='{{ route('visitas_crear',$proyecto->id) }}';" class="btn btn-primary">Crear visita</button>
                        <span class="mt-3">Listado de visitas</span>
                    </div>
                    <div class="table-responsive">
                        <table id="recent-purchases-listing" class="table">
                            <thead>
                                <tr>
                                    <th>Fecha de visita</th>
                                    <th>Nombre</th>
                                    <th>Auditor</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($proyecto->visitas as $visita)
                                <tr>
                                    <td>{!! $visita->fecha_visita !!}</td>
                                    <td>{!! $visita->nombre !!}</td>
                                    <td>{!! $visita->auditor->nombre !!}</td>
                                    <td>
                                        {!! $visita->activo == 'si' ? 'Activo' : 'Inactivo' !!}
                                    </td>
                                    <td>
                                        <div class="btn-group col-sm">

                                            @if (is_null($visita->estado))
                                                <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('visitas_editar',[$proyecto->id,$visita->id]) }}';" title="Editar visita."><i class="mdi mdi-grease-pencil"></i></button>
                                            @elseif($visita->estado == 'proceso')
                                                <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('visitas.ver',$visita->id) }}';" title="Ver visita."><i class="mdi mdi-eye"></i></button>                                      
                                            @else
                                                <button class="btn btn-xs btn-warning" type="button" onclick="location.href='{{ route('visitas.adjuntar',$visita->id) }}';" title="adjuntar"><i class="mdi mdi-paperclip"></i></button>
                                                <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('visitas.ver',$visita->id) }}';" title="Ver visita."><i class="mdi mdi-eye"></i></button>
                                            @endif

                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="espectadores" role="tabpanel" aria-labelledby="profile-tab">
                       <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><i class="mdi mdi-account-check"></i></th>
                                        <th>Email</th>
                                        <th>Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (Auth::user()->empresa->tecnicos as $tecnico)
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-info">
                                                <label class="form-check-label">
                                                  <input type="checkbox" class="form-check-input" onclick="formularioEspectador({{$tecnico->id}})" {{$tecnico->validarEspectadorProyecto($proyecto->id, $tecnico->id) == '' ?:  'checked=""'}}>
                                                <i class="input-helper"></i></label>
                                            </div>
                                        </td>
                                        <td>{!! $tecnico->nombre !!}</td>
                                        <td>{!! $tecnico->email !!}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                </div>
                <div class="tab-pane fade" id="contratistas" role="tabpanel" aria-labelledby="contact-tab">
                       <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><i class="mdi mdi-account-check"></i></th>
                                        <th>Email</th>
                                        <th>Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (Auth::user()->empresa->contratistas as $contratista)
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-info">
                                                <label class="form-check-label">
                                                  <input type="checkbox" class="form-check-input"  onclick="formularioContratista({{$contratista->id}})" {{$contratista->validarContratistaProyecto($proyecto->id, $contratista->id) == '' ?:  'checked=""'}}>
                                                <i class="input-helper"></i></label>
                                            </div>
                                        </td>
                                        <td>{!! $contratista->nombre !!}</td>
                                        <td>{!! $contratista->email !!}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                </div>
                 <div class="tab-pane fade" id="terceros" role="tabpanel" aria-labelledby="contact-tab">
                       <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><i class="mdi mdi-account-check"></i></th>
                                        <th>Identificación</th>
                                        <th>Razón Social</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (Auth::user()->empresa->terceros as $tercero)
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-info">
                                                <label class="form-check-label">
                                                  <input type="checkbox" class="form-check-input"  onclick="formularioTercero({{$tercero->id}})" {{$tercero->validarTerceroP($proyecto->id, $tercero->id) == '' ?:  'checked=""'}}>
                                                <i class="input-helper"></i></label>
                                            </div>
                                        </td>
                                        <td>{!! $tercero->num_dc !!}</td>
                                        <td>{!! $tercero->razon_social !!}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
@section('scripts')
<script>
    const proyecto= {!!$proyecto->id!!}; 
    $(function() {
        $('.table').DataTable({
            "language": {
                search: "Buscar"
            },
            info: false
        });
    });

    function formularioEspectador(user){
        axios.post('{{route('espectador-proyecto.store')}}', {
            user: user,
            proyecto: proyecto
          })
          .then(function (action, state) {
            console.log("Data: " + action + "\nStatus: " + state);
          })
          .catch(function (error) {
            console.log(error);
          });
    }

     function formularioContratista(user){
        axios.post('{{route('contratista-proyecto.store')}}', {
            user: user,
            proyecto: proyecto
          })
          .then(function (action, state) {
            console.log("Data: " + action + "\nStatus: " + state);
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    function formularioTercero(user){
        axios.post('{{route('tercero-proyecto.store')}}', {
            user: user,
            proyecto: proyecto
          })
          .then(function (action, state) {
            console.log("Data: " + action + "\nStatus: " + state);
          })
          .catch(function (error) {
            console.log(error);
          });
    }
</script>
@endsection
