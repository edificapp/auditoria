$(document).ready(function(){
    $('.custom-select').select2();
    load_buttons();

    $('#table , .table-crud').DataTable ({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        paging:true, info: true
    });

    $(".file_certificado").fileinput({
        'language': "es",
        'theme': "fa",
        'showUpload': false,
    });
});

// estilos para los botones de editar, crear, pdf  ver
function load_buttons(){
    $('.btn-edit-form').append('<i class="mdi mdi-pencil-box-outline"></i>').addClass('btn btn-sm btn-inverse-info btn-rounded');
    $('.btn-create-form').append('<i class="mdi mdi-flask mdi-note-plus"></i>').addClass('btn btn-primary');
    $('.btn-show-form').append('<i class="mdi mdi-eye"></i>').addClass('btn btn-sm btn-inverse-primary btn-rounded');
    $('.btn-delete-form').append('<i class="mdi mdi-delete"></i>').addClass('btn btn-sm btn-inverse-danger btn-rounded');
    $('.btn-pdf-form').append('<i class="mdi mdi-file-pdf-box"></i>').addClass('btn btn-sm btn-inverse-danger btn-rounded');
    $('.btn-activo-form').append('<i class="mdi mdi-lock-outline"></i>').addClass('btn btn-sm btn-inverse-success btn-rounded');
    $('.btn-inactivo-form').append('<i class="mdi mdi-lock-open-outline"></i>').addClass('btn btn-sm btn-inverse-danger btn-rounded');
    $('.btn-copy-form').append('<i class="mdi mdi-content-copy"></i>').addClass('btn btn-sm btn-inverse-secondary btn-rounded');
    $('.btn-add-form').append('<i class="mdi mdi-library-plus"></i>').addClass('btn btn-sm btn-inverse-primary btn-rounded ');
}


(function($) {
  'use strict';
  $(function() {
    $('.file-upload-browse').on('click', function() {
      var file = $(this).parent().parent().parent().find('.file-upload-default');
      file.trigger('click');
    });
    $('.file-upload-default').on('change', function() {
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });
})(jQuery);

document.getElementById("inp_imagen").onchange = function(e) {
  // Creamos el objeto de la clase FileReader
  let reader = new FileReader();

  // Leemos el archivo subido y se lo pasamos a nuestro fileReader
  reader.readAsDataURL(e.target.files[0]);

  // Le decimos que cuando este listo ejecute el código interno
  reader.onload = function(){
    let image = document.getElementById('imagen_avatar');
    image.src = reader.result;  };
}
