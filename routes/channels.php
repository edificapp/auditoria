<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
/*
Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});


Broadcast::channel('authUser', function ($user) {
    return $user != null && $user->id != auth()->id();
});
*/
/*
Broadcast::channel('teams', function ($user) {
    return $user != null;
});
*/
/*
Broadcast::channel('messages.{id}', function ($user, $id) {
    return $user->id === (int) $id;
});
*/

Broadcast::channel('lchat', function ($user) {
    return auth()->check();
});

Broadcast::channel("privatechat{receiverid}", function ($user,$receiverid) {
    \Log::debug('desde private channel');
    return auth()->check();
});

Broadcast::channel("teamchat{receiverid}", function ($user,$receiverid) {
    \Log::debug('desde team channel teamchat');
    return auth()->check();
});

Broadcast::channel('plchat', function ($user) {
    if(auth()->check()){
        return $user;
    }
});