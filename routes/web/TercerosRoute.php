<?php

Route::group(['middleware' => ['web','auth']], function () {
    //todos los terceros de la empresa
    Route::get('/api-terceros', 'Api\TerceroController@index');//

    //todos los terceros de un proyecto
    Route::get('/api-terceros/proyecto/{proyecto}', 'Api\TerceroProyectoController@index');//p
    //relacionar un tercero al proyecto
    Route::post('/api-terceros/proyecto/relacionar/{proyecto}/', 'Api\TerceroProyectoController@agregarTerceroToProyecto');//p

    //listar sedes de un tercero
    Route::get('/api-terceros/sedes/{tercero_proyecto}', 'Api\TerceroSedesController@list');
    //guardar sedes a un tercero
    Route::post('/api-terceros/sedes/{tercero_proyecto}', 'Api\TerceroSedesController@storeSedes');//tp
    Route::post('/api-terceros/institutos/{tercero_proyecto}', 'Api\TerceroSedesController@storeInstitutos');
    //actualizar sedes a un proyecto
    Route::patch('/api-terceros/sedes-u/{tercero_proyecto}', 'Api\TerceroSedesController@updateSedes');//p
    Route::patch('/api-terceros/instituciones-u/{tercero_proyecto}', 'Api\TerceroSedesController@updateInstituciones');
    //mostrar sede
    Route::get('/api-terceros/sedes/{tercero_sede}/show', 'Api\TerceroSedesController@show');

    //guardar sedes a un tercero
    Route::post('/api-terceros/parametros/{tercero_proyecto}', 'Api\TerceroParametroController@store');//tp
    Route::patch('/api-terceros/parametros/{tercero_parametro}', 'Api\TerceroParametroController@update');//tp
    Route::delete('/api-terceros/parametros/{tercero_parametro}', 'Api\TerceroParametroController@destroy');//tp
});
