<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CorrespondenciaController;

Route::get('correspondencia', [CorrespondenciaController::class, 'index'])->name('correspondencia.index');
Route::get('correspondencia/consulta', [CorrespondenciaController::class, 'consulta'])->name('correspondencia.consulta');

// RADICACION
    Route::get('correspondencia/radicacion', [CorrespondenciaController::class, 'getRadicacion'])->name('radicacion.index');
    Route::get('correspondencia/radicacion/recibido', [CorrespondenciaController::class, 'getRadicacionRecibido'])->name('radicacion.recibido');
    Route::get('correspondencia/radicacion/salida', [CorrespondenciaController::class, 'getRadicacionSalida'])->name('radicacion.salida');
    Route::get('correspondencia/radicacion/enviado', [CorrespondenciaController::class, 'getRadicacionEnviado'])->name('radicacion.enviado');
    Route::get('correspondencia/radicacion/interno', [CorrespondenciaController::class, 'getRadicacionInterno'])->name('radicacion.interno');

// BANDEJA
    Route::get('correspondencia/bandeja/recibido', [CorrespondenciaController::class, 'getBandejaRecibido'])->name('bandeja.recibido');
    Route::get('correspondencia/bandeja/salida', [CorrespondenciaController::class, 'getBandejaSalida'])->name('bandeja.salida');
    Route::get('correspondencia/bandeja/enviado', [CorrespondenciaController::class, 'getBandejaEnviado'])->name('bandeja.enviado');
    Route::get('correspondencia/bandeja/interno', [CorrespondenciaController::class, 'getBandejaInterno'])->name('bandeja.interno');
