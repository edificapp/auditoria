<?php

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/mensajes/{user_id}', 'MessageController@getMessage');
    Route::post('/mensajes', 'MessageController@setMessage')->name('getMessage');
});

