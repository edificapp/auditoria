<?php

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('tipo-profesion/{tipo}', 'Api\TipoProfesionController@show');
});