<?php

Route::group(['middleware' => ['web','auth']], function () {
    Route::post('/estado', 'EstadoController@store');
    Route::get('/estado', 'EstadoController@index');
    Route::get('/estado/{estado}', 'EstadoController@show');

    Route::get('/estados/{estado}/likes', 'EstadoController@like');
    Route::delete('/estados/{estado}/likes', 'EstadoController@unLike');

    Route::post('estado/comentarios', 'ComentarioController@store');
    Route::get('/comentarios/{comentario}/likes', 'ComentarioController@like');
    Route::delete('/comentarios/{comentarios}/likes', 'ComentarioController@unLike');
});