<?php

Route::get('/users', 'UsuariosController@usersApi')->name('users');

Route::get('/private-messages/{user}', 'ChatMessageController@privateMessages')->name('privateMessages');
Route::post('/private-messages/{user}', 'ChatMessageController@sendPrivateMessage')->name('privateMessages.store');

Route::get('/team-messages/{team}', 'ChatMessageController@teamMessages');
Route::post('/team-messages/{team}', 'ChatMessageController@sendTeamMessage');