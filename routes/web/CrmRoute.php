<?php

    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\CrmController;

    Route::get('crm',[CrmController::class, 'index'] )->name('crm.index');

    Route::get('crm/servicios/cliente', [CrmController::class, 'getServicioCliente'])->name('getServicioCliente');
    Route::get('crm/servicios/devolucion', [CrmController::class, 'getServicioDevolucion'])->name('getServicioDevolucion');
    Route::get('crm/servicios/servicio-tecnico', [CrmController::class, 'getServicioTecnico'])->name('getServicioTecnico');

    Route::get('crm/ventas', [CrmController::class, 'ventas'])->name('ventas.index');
    Route::get('crm/ventas/gestion', [CrmController::class, 'getVentasGestion'])->name('ventas.gestion');
    Route::get('crm/ventas/cotizacion', [CrmController::class, 'getVentasCotizacion'])->name('ventas.cotizacion');

    Route::get('crm/ventas/ventas', [CrmController::class, 'getVentas'])->name('ventas.ventas');
    Route::get('crm/ventas/ventas-mes', [CrmController::class, 'getVentasMes'])->name('ventas.mes');
    Route::get('crm/ventas/ventas-ano', [CrmController::class, 'getVentasAno'])->name('ventas.ano');
    Route::get('crm/ventas/ventas-proyectos', [CrmController::class, 'getVentasProyecto'])->name('ventas.proyecto');
