<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArchivoController;

Route::get('archivo', [ArchivoController::class,'index'])->name('archivo.index');