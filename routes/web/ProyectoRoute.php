<?php

Route::group(['middleware' => ['web','auth']], function () {
    //cambiar campos de un proyecto
    Route::post('proyecto/{proyecto}/field/{field}', 'Api\ProyectoController@updateField');
    Route::get('proyecto/{proyecto}/personal', 'Api\ProyectoController@personal');

    Route::get('api-gformulario/{proyecto}', 'Api\GFormularioController@index');

    Route::get('api-visitas/proyecto/{proyecto}', 'Api\VisitaController@index');
    Route::post('api-visitas/proyecto/{proyecto}', 'Api\VisitaController@store')->name('api.visita.store');
    Route::patch('api-visitas/edit/{visita}', 'Api\VisitaController@update');
    Route::delete('api-visitas/destroy/{visita}', 'Api\VisitaController@destroy');
});

