<?php
Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

Auth::routes();

Route::get('/php', function() {
    phpinfo();
});

Route::get('/map', function(){ 
    return view('map');
})->name('ip');

Route::get('/', 'ActivityController@index')->name('inicio');

Route::post('usuario/registrarse', 'UsuariosController@registrarsePublic')->name('usuarios.registrarse');

Route::get('/validar/{token}', 'UsuariosController@validar')->name('registro');
Route::get('/empresas/todas', 'EmpresasController@ajaxEmpresas')->name('empresasAjax');

Route::get('/terceros/validar/{num}', 'TerceroController@verificarTercero')->name('tercero.uploadForm');
Route::get('/subir-archivos/terceros', 'TerceroController@form')->name('tercero.uploadForm');
Route::post('/subir-archivos/terceros', 'TerceroController@store')->name('tercero.store');
Route::get('/radicado/{tipo}/{id}/{radicado_id}', 'RadicadoController@pdf')->name('radicado.export');
Route::get('/search-unspsc/{search}', 'PropuestasController@searchUnspsc');


Route::get('/import/{tipo}', 'ImportController@tipos');
Route::post('/import/{tipo}', 'ImportController@tiposImport')->name('import.store');


Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');

