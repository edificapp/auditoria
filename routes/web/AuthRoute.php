<?php



Route::group(['prefix' => 'equipos', 'namespace' => 'Teamwork'], function()
{
    Route::get('/', [App\Http\Controllers\Teamwork\TeamController::class, 'index'])->name('teams.index');
    Route::get('create', [App\Http\Controllers\Teamwork\TeamController::class, 'create'])->name('teams.create');
    Route::post('teams', [App\Http\Controllers\Teamwork\TeamController::class, 'store'])->name('teams.store');
    Route::get('edit/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'edit'])->name('teams.edit');
    Route::put('edit/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'update'])->name('teams.update');
    Route::delete('destroy/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'destroy'])->name('teams.destroy');
    Route::get('switch/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'switchTeam'])->name('teams.switch');

    Route::get('members/{id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'show'])->name('teams.members.show');
    Route::get('members/resend/{invite_id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'resendInvite'])->name('teams.members.resend_invite');
    Route::post('members/{id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'invite'])->name('teams.members.invite');
    Route::delete('members/{id}/{user_id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'destroy'])->name('teams.members.destroy');

	Route::get('accept/{token}', [App\Http\Controllers\Teamwork\AuthController::class, 'acceptInvite'])->name('teams.accept_invite');
	
	Route::get('/userBelongsToTeam/{user}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'userBelongsToTeam']);
});


Route::group(['middleware' => ['web','auth']], function () {	

	Route::get('personificacion/{usuario}', 'PersonificacionController@store')->name('personificacion');
	Route::get('personificacion-borrar', 'PersonificacionController@destroy')->name('personificacion-borrar');


	Route::get('activities', 'ActivityController@activities');
	Route::get('activity-properties/{activity}', 'ActivityController@showProperties');
	Route::get('/departamento/{id}', 'CiudadController@departamento_ciudades');

	Route::get('/map/{coordenadas}', function($cordenadas){
		$arr = explode(",", $cordenadas);
		return view('proyecto.visitas.map', ['lat'=>$arr[0], 'lon'=>$arr[1]]);
	})->name('map');
	Route::get('/mantenimiento', function(){  return view('mantenimiento'); })->name('mantenimiento');
	Route::get('/inicio', 'ActivityController@index')->name('home');
	Route::get('/salir', 'HomeController@salir')->name('salir');

	Route::get('/perfil', 'UsuariosController@perfil')->name('perfil');
	Route::post('/perfil/{id}/update_perfil', 'UsuariosController@update_perfil')->name('update_perfil');


	Route::get('/visitas', 'VisitasController@index')->name('visitas');
	Route::get('/visitas/{id}/visitando', 'VisitasController@visita')->name('visitas.visita');
	Route::post('/visitas/{proyecto}/visitando/guardar', 'VisitasController@store')->name('visitas.guardar');
	Route::get('/visitas/{visita}/ver', 'VisitasController@ver')->name('visitas.ver');
	Route::get('/visitas/{id}/adjuntar', 'VisitasController@adjuntar')->name('visitas.adjuntar');
	Route::post('/visitas/uploap', 'VisitasController@upload')->name('visitas.upload');
	Route::put('/visitas/upload/{id}', 'VisitasController@updateUpload')->name('visitas.adjuntar.update');

	Route::get('/visita-ajax/formularios/{id}', 'VisitasController@formularios_ajax')->name('visitas.formularios.ajax');
	Route::get('/visita-ajax/inspectores/{id}', 'VisitasController@inspectores_ajax')->name('visitas.inspectores.ajax');

	Route::get('/firmar/{id}', 'VisitasController@firmar')->name('firmar');
	Route::post('/firmar/{id}/guardar', 'VisitasController@firmadoguardar')->name('visitas.firmar');


	///hja de vida
	Route::resource('curriculum', 'CurriculumController');
	Route::resource('estudios', 'EstudiosController');
	Route::resource('otros-estudios', 'OestudiosController');
	Route::resource('logros', 'LogrosController');
	Route::resource('idiomas', 'IdiomasController');
	Route::resource('laboral', 'ElaboralController');
	/* 
	Route::get('/curriculum', 'UsuariosController@curriculum')->name('curriculum');
	Route::post('/curriculum/guardar', 'UsuariosController@update_curriculum')->name('update_curriculum');
	*/

	Route::get('/hv/{id}', 'UsuariosController@curriculum_ver')->name('curriculum_ver');
	Route::get('/hv/{id}/exportar', 'UsuariosController@exportarhv')->name('exportarhv');

	Route::post('usuarios/{id}/validar_update', 'UsuariosController@validar_update')->name('validar_update');

	/* Formularios activos */	
	Route::get('formularios/selector/formularios/', 'FormulariosController@getFormularios')->name('getFormularios');
	/* Usuarios activos */	Route::get('usuarios/selector/', 'UsuariosController@getUsuarios')->name('getUsuarios');


	Route::get('/mis-documentos/projectos', 'DocumentoContratistaController@projectos')->name('documentos.projectos');
	Route::get('/mis-documentos/contratista-documentos/{id}', 'ContratistaProyectoController@documentos')->name('documentos.contratista');

	Route::get('/documentos/contratistas/{id}', 'ProyectosController@contratistas')->name('documentos.perfil.contratistas');
	Route::get('/documentos/espectador-documentos/{id}/{tipo}', 'ContratistaProyectoController@documentosPublic')->name('documentos.espectador');

	//contratista
		Route::post('/documentos/{id}', 'DocumentoContratistaController@store')->name('documentos.store');
	    Route::get('/documento/{id}', 'DocumentoContratistaController@borrado')->name('documento.borrado');

	/* Solo super administradores */
		Route::resource('empresas', 'EmpresasController');
		Route::get('empresas/estado/{id}', 'EmpresasController@estado')->name('empresa_estado');
		Route::get('empresas/{id}/usuarios', 'EmpresasController@usuarios')->name('empresa_usuarios');

	//empresa administrador
		Route::resource('usuarios', 'UsuariosController');
		Route::get('usuarios-filtrar', 'UsuariosController@usuariosFiltrar')->name('usuarios.filtrar');
		Route::get('usuarios/estado/{id}', 'UsuariosController@estado')->name('usuario_estado');
		Route::get('usuarios/{id}/enviar_invitacion', 'UsuariosController@reenviar')->name('enviar_invitacion');
		Route::post('usuarios/{id}/reenviar_invitacion', 'UsuariosController@reenviar_update')->name('reenviar_invitacion');

		Route::get('empresa/informacion', 'EmpresasController@informacion')->name('empresa.informacion');
		Route::put('empresa/informacion/{id}', 'EmpresasController@updateInformacion')->name('empresa.update.informacion');
		Route::resource('empresa-experiencia', 'EmpresaExperienciaController');
		Route::get('code-unspsc/{code}', 'CodeUnspscController@codes')->name('code.unspsc');
		Route::resource('empresa-documentos', 'EmpresaDocumentoController');
		Route::resource('cliente-tercero', 'ClienteTerceroController');
		Route::post('clientes-tercero', 'ClienteTerceroController@storeClientes')->name('clientes.store');
		Route::put('clientes-tercero', 'ClienteTerceroController@updateClientes')->name('clientes.update');
		Route::get('cliente-tercero-listar/{tab_active}', 'ClienteTerceroController@listar')->name('contactos.listar');
		Route::resource('proveedor', 'ProveedorController');

	//coordinador
		Route::resource('proyectos', 'ProyectosController');
		Route::get('ejecucion/proyectos', 'ProyectosController@ejecucion')->name('proyectos.ejecucion');
		Route::get('cerrados/proyectos', 'ProyectosController@cerrados')->name('proyectos.cerrados');
		Route::get('proyectos/{id_proyecto}/visitas/crear', 'ProyectosController@visitas_crear')->name('visitas_crear');
		Route::post('proyectos/{id_proyecto}/visitas/crear/store', 'ProyectosController@visitas_crear_store')->name('visitas_crear_store');
		Route::get('proyectos/{id_proyecto}/visitas/{id_visita}', 'ProyectosController@visitas_editar')->name('visitas_editar');
		Route::post('proyectos/{id_proyecto}/visitas/{id_visita}/update', 'ProyectosController@visitas_editar_update')->name('visitas_editar_update');
		Route::get('proyectos/{estado}/{proyecto_id}', 'ProyectosController@update_estado')->name('proyectos.cambiar.estado');
		Route::get('/proyecto/{proyecto}/terceros', 'ProyectosController@terceros');
		Route::post('/proyecto/{proyecto}/relacionarterceros', 'ProyectosController@relacionarTerceros');
		Route::get('/proyecto/informe-formulario/{proyecto}', 'ProyectoInformeController@formularios_informe')->name('proyecto.informe.formulario');
		Route::get('/proyecto/informe/{formulario}/{proyecto}', 'ProyectoInformeController@informe')->name('proyecto.informe');
		


		Route::get('gestionar-proyecto/{proyecto_id}/{vista}', 'ProyectosController@sub_menu')->name('proyectos.sub_menu');
		Route::resource('documentos-proyecto', 'DocumentoProyectoController');
		Route::resource('personal-proyecto', 'PersonalProyectoController');
		Route::resource('reuniones-proyecto', 'ReunionProyectoController');
		Route::resource('visitas-proyecto', 'VisitasController');
		Route::post('visita-asignar', 'VisitasController@asignar')->name('visita.asignar');

		Route::resource('formularios', 'FormulariosController');
		Route::get('formularios/{id}/estado', 'FormulariosController@estado')->name('formulario_estado');
		Route::get('formularios/{formulario}/refresh', 'FormulariosController@refresh')->name('formulario_refresh');
		Route::get('formularios/{id}/formulario', 'FormulariosController@get_formulario')->name('formulario_cuerpo');
		Route::get('formularios/{id}/clonar', 'FormulariosController@clone')->name('formularios.clone');

		Route::resource('grupo-formularios', 'GFormularioController');
		Route::get('grupo-formulario/{proyecto}/create', 'GFormularioController@create')->name('grupo-formularios.create');
		Route::get('grupo-formulario/{proyecto}/{grupo_formulario}/edit', 'GFormularioController@edit')->name('grupo-formularios.edit');
		Route::get('grupo-formulario/{proyecto}/{grupo_formulario}', 'GFormularioController@show')->name('grupo-formularios.show');
		Route::get('grupo-formulario/{grupo_formulario}', 'GFormularioController@estado')->name('grupo-formularios.estado');

		Route::post('espectador-proyecto', 'EspectadorProyectoController@store')->name('espectador-proyecto.store');

		Route::post('contratista-proyecto', 'ContratistaProyectoController@store')->name('contratista-proyecto.store');
		Route::post('tercero-proyecto', 'TerceroProyectoController@store')->name('tercero-proyecto.store');

		Route::resource('roles', 'RoleController');
		

		Route::resource('eventos', 'EventoController');
		Route::get('evento/data', 'EventoController@data')->name('event.resource');
		Route::get('evento/tareas/{id}', 'EventoController@eventos_users')->name('eventos.notify.users');
		Route::get('evento/contactos/{id}', 'EventoController@eventos_entidades')->name('eventos.notify.contacto');
    	Route::post('evento/update-ajax', 'EventoController@update_ajax')->name('evento.update_ajax');
    	Route::get('evento/confirmacion/{id}/{resp}', 'EventoController@confirmacion')->name('evento.confirmacion');

		Route::get('noticaficaciones', 'NotificationController@list')->name('notificaciones');
		Route::get('noticaficacion/{notificacion}', 'NotificationController@link')->name('notificacion.link');

		Route::resource('directorio', 'ContactoController');
		Route::get('directorios/{id}', 'ContactoController@activo')->name('directorios.activo');



		Route::resource('propuestas', 'PropuestasController');
		Route::get('propuestas-borrador', 'PropuestasController@getBorrador')->name('propuestas.borrador');
		Route::get('propuestas-borrador-edit/{id}', 'PropuestasController@editPropuestaBorrador')->name('propuestas.borrador.edit');
		Route::post('propuestas-experiencia', 'PropuestasController@storeExperiencia')->name('propuestas.experiencia');
		Route::post('propuestas-experiencia-update', 'PropuestasController@updateExperiencia')->name('propuestas.experiencia.update');
		Route::post('propuestas-talento', 'PropuestasController@storeTalento')->name('propuestas.talento');
		Route::get('profesion-competentes/{talento}', 'Api\PropuestaTalentoHumanoController@competentes')->name('competentes.profesion');
		Route::patch('profesion-competentes/{talento}', 'Api\PropuestaTalentoHumanoController@update')->name('competentes.update');
		Route::post('propuestas-code', 'PropuestasController@storeCode')->name('propuesta-code.store');
		
		Route::resource('propuesta-documentos', 'PropuestaDocumentoController');
		Route::get('propuestas-preparacion', 'PropuestasController@getpreparacion')->name('propuestas.preparacion');
		Route::get('propuestas-preparacion-editar/{id}', 'PropuestasController@editPropuestaPreparacion')->name('propuestas.preparacion.edit');
		Route::post('propuestas-preparacion-contrato', 'PropuestasController@storePropuestaPreparacionContrato')->name('propuestas.preparacion.contrato');
		// Route::get('cerrados/propuestas', 'PropuestasController@cerrados')->name('propuestas.cerrados');
		// Route::get('propuestas/{id_proyecto}/visitas/crear', 'PropuestasController@visitas_crear')->name('visitas_crear');
		// Route::post('propuestas/{id_proyecto}/visitas/crear/store', 'PropuestasController@visitas_crear_store')->name('visitas_crear_store');
		// Route::get('propuestas/{id_proyecto}/visitas/{id_visita}', 'PropuestasController@visitas_editar')->name('visitas_editar');
		// Route::post('propuestas/{id_proyecto}/visitas/{id_visita}/update', 'PropuestasController@visitas_editar_update')->name('visitas_editar_update');
		// Route::get('propuestas/{estado}/{proyecto_id}', 'PropuestasController@update_estado')->name('propuestas.cambiar.estado');
	/* Peticiones especiales */

	// Route::get('usuarios/{id}/enviar_invitacion', 'UsuariosController@enviar_invitacion_url')->name('enviar_invitacion');
});


