<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth', 'namespace' => 'Api'], function ($router) {
    Route::post('login', 'AuthController@login');
});



Route::group(['prefix' => 'auth', 'middleware' => 'jwt.auth', 'namespace' => 'Api'], function () {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['prefix' => 'panel', 'middleware' => 'jwt.auth', 'namespace' => 'Api'], function (){
    Route::get('getproyectos', 'ProyectoController@getProyectos');
    Route::get('getvisitas/proyecto/{proyecto}', 'ProyectoController@getVisitasProyecto');
    Route::get('getformularios/visita/{visita}', 'VisitaController@getVisitaFormularios');
    Route::get('getformulario/visita/{visita_formulario}', 'VisitaFormularioController@getformulario');
    Route::get('getformatomuestra/visita/{visita}', 'VisitaController@formato_muestra');
    Route::get('getformatomuestralistar/visita/{visita}', 'VisitaMuestraController@listar');
    Route::post('setformatomuestra/visita/{visita}', 'VisitaMuestraController@store');

    Route::post('visita-respuestas/{visita}', 'VisitaFormularioController@storeResData');
    Route::post('visita-firmas/{visita}', 'VisitaFormularioController@storeFirmas');
    Route::get('visita-fotos/{visita}', 'VisitaController@getFotos');
    Route::post('visita-fotos/{visita}', 'VisitaController@storeFotos');
    Route::post('formulario/inicia/{formulario}', 'VisitaFormularioController@dataInicial');
    Route::post('formulario/final/{formulario}', 'VisitaFormularioController@dataFinal');
});




