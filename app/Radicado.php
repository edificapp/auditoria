<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DocumentoContratista;

class Radicado extends Model
{
    public function documentos(){
    	return $this->hasMany('App\DocumentoContratista');
    }

    public function contratistaProyecto(){
    	return $this->belongsTo('App\ContratistaProyecto');
    }
}
