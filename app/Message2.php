<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['from', 'to', 'message', 'is_read'];

    public function getTipoAttribute(){
        return  Auth::id() == $this->from ? 'sent' : 'received';
    }

    public function getDateAttribute(){
        return $this->created_at->format('Y-m-d  | g:i A');
    }
}
