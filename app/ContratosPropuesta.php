<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContratosPropuesta extends Model
{
    protected $table = "contratos_propuesta";


    public function code_uns(){
        return $this->hasMany('App\ContratoUnspscs', 'contrato_propuesta_id');
	}

}