<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function contacto(){
        return $this->belongsTo('App\Proveedor', 'contacto_id');
    }

    public function getRutPublicAttribute(){
    	return \Storage::url($this->rut);
    }

    public function getCertificadoBancarioPublicAttribute(){
    	return \Storage::url($this->certificado_bancarios);
    }

    public function getNombreAttribute(){
        return $this->nombre_empresa;
    }

    public function getSelectAttribute(){
        return 'proveedor-'.$this->id;
    }

    public function contactos()
    {
        return $this->morphMany('App\Contacto', 'contacto');
    }
}
