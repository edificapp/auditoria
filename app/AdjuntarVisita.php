<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AdjuntarVisita extends Model
{
	protected $fillable = ['ruta', 'descripcion', 'visita_id'];

    public function upload($file, $visita_id){
    	$adjuntar = new AdjuntarVisita;
    	$adjuntar->visita_id = $visita_id;
        $adjuntar->ruta =  $file->store('/public/visita/'.$visita_id);
    	$adjuntar->save();
    }

    public function uploadUpdate($file, $adjuntar){
        $this->deleteFile($adjuntar);
        return $file->store('/public/visita/'.$adjuntar->visita_id);
    }


    public function deleteFile($adjuntar){
        if($files = Storage::exists($adjuntar->ruta))
        Storage::delete($adjuntar->ruta);
    }

    public function deleteDir(){
        Storage::deleteDirectory($directory);
    }

    public function getUrlRutaAttribute(){
    	return \Storage::url($this->ruta);
    }
}
