<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionProyecto extends Model
{
    public function documentos(){
		return $this->hasMany('App\ReunionProyectoDocumento', 'reunion_proyecto_id');
	}

	public function proyecto(){
		return $this->belongsTo(Proyecto::class, 'proyecto_id');
	}
}
