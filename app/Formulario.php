<?php

namespace App;

use App\Scopes\EmpresaScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\AsCollection;

class Formulario extends Model
{
    protected $table = "formularios";

  //  protected $casts = ["formulario" => 'array'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'activo', 'old'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new EmpresaScope);
    }

    public function owner(){
        return $this->belongsTo('App\User', 'creador_id');
    }
    /*
    public function visitas(){
        return $this->belongsToMany('App\Visita', 'formularios_visita');
    }
*/

    public function grupo_formularios(){
        return $this->belongsToMany(GFormulario::class, 'formulario_g_formularios');
    }

    public function formulario_g_formulario(){
        return $this->hasMany(FormularioGFormulario::class, 'formulario_id');
    }

    public function validarFormulario($visita_id){
        $relacion = formulariosVisita::where('formulario_id', $this->id)->where('visita_id', $visita_id)->first();
        if($relacion != null){
            return true;
        }else{
            return false;
        }
    }
}
