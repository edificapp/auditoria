<?php

namespace App;
use Spatie\Permission\Models\Role as ModelRole;

class Role extends ModelRole
{
    protected $fillable = ['name', 'guard_name', 'empresa_id'];


}
