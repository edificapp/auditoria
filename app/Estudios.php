<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudios extends Model
{
    protected $table = "estudios";
     protected $fillable = [
		 'curriculum_id', 'profesion_id', 'escuela', 
		 'finalizado', 'semestres', 'fecha_fin',
		 'tarjeta', 'certificado'
		];

     //relations
    public function curriculum(){
    	return $this->belongsTo('App\Curriculum');
    }

	public function profesion(){
		return $this->belongsTo(Profesion::class, 'profesion_id');
	}

    //mutations
    public function getCertificadoFileAttribute(){
    	if($this->certificado != NULL){
        	return \Storage::url($this->certificado);
    	}else{
    		return $this->certificado;
    	}
    }

    public function getModalidadAcademicaAttribute(){
    	if($this->modalidad == 'TC'){
    		return 'Técnica';
    	}else if($this->modalidad == 'TL'){
			return 'Tecnológica';
		}else if($this->modalidad == 'TE'){
			return 'Tecnológica';
		}else if($this->modalidad == 'UN'){
			return 'Universitaria';
		}else if($this->modalidad == 'ES'){
			return 'Especialización';
		}else if($this->modalidad == 'MG'){
			return 'Maestria o Magister'; 
		}else if($this->modalidad == 'DC'){
			return 'Doctorado o PHD'; 
		}
	}
}
