<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaExperienciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_contrato' => ['required', 'string', 'max:255'],
            'numero_contrato' => ['required','numeric'],
            'entidad_contratante' => 'required',
            'fecha_inicial' => 'required',
            'fecha_final' => 'required',
            'valor_del_contrato' => ['required','numeric']
        ];
    }

    public function attributes()
    {
        return [
            'nombre_contrato' => 'Nombre del Contrato',
            'numero_contrato' => 'Número del Contrato',
            'entidad_contratante' => 'Entidad_contratante',
            'fecha_inicial' => 'Fecha inicial del Contrato',
            'fecha_final' => 'Fecha final del Contrato',
            'valor_del_contrato' => 'valor del Contrato'
        ];
    }
}
