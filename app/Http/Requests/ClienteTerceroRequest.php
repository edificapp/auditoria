<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteTerceroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getRedirectUrl(){
        $url = $this->redirector->getUrlGenerator();
        return $url->route('contactos.listar', $this->tipo_contacto);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => ['required', 'string', 'max:255'],
            'direccion' => ['required', 'string', 'max:255'],
            'numero_contacto' => ['required', 'string', 'max:255'],
            'numero_documento' => ['required', 'string', 'max:255'],
            'email' => 'required|unique:users,email,'.$this->user()->id
        ];
    }

    public function attributes()
    {
        return [
            'numero_contacto' => 'Numero del contacto',
            'numero_documento' => 'Numero del documento'
        ];
    }
}
