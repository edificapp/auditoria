<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'lugar' => 'required',
            'description' => 'required'
        ];

    }

    public function messages()
    {
        return [
            'titulo.required' => 'El :attribute es obligatorio.',
            'titulo.string' => 'El :attribute debe ser una cadena de caracteres.',
            'titulo.max' => 'El :attribute maximo 255 caracteres.',
            'lugar.required' => 'El :attribute es obligatorio.',
            'description.required' => 'El :attribute es obligatorio.'
        ];
    }

    public function attributes()
    {
        return [
            'titulo' => 'Nombre del Evento'
        ];
    }
}
