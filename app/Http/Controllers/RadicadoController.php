<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tercero;
use App\User;
use App\Radicado;
use Barryvdh\DomPDF\Facade as PDF;

class RadicadoController extends Controller
{
    public function pdf($tipo, $id, $radicado_id){
    	$radicado = Radicado::find($radicado_id);
    	if($tipo == 'contratista'){
    		$user = User::find($id);
    		$nombre = $user->nombre;
    		$conteo = Radicado::where('contratista_proyecto_id', '!=', null)->get()->count();
    		$radicadoConteo = 'Cont-'.++$conteo;
    	}else{
    		$user = Tercero::find($id);
    		$nombre = $user->razon_social;
    		$conteo = Radicado::where('tercero_proyecto_id', '!=', null)->get()->count();
    		$radicadoConteo = 'Ter-'.++$conteo;
    	}
    	$pdf = PDF::loadView('layouts.pdfs',compact('user', 'radicadoConteo', 'tipo', 'radicado', 'nombre'));
/*
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
*/
        return $pdf->stream();
        //return view('layouts.pdfs',compact('user', 'radicadoConteo', 'tipo', 'radicado'));
    }
}
