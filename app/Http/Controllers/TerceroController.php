<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tercero;
use App\Radicado;
use App\Proyecto;
use App\TerceroProyecto;
use App\DocumentoContratista;
use App\Traits\StorageTraits;
use Illuminate\Support\Facades\DB;

class TerceroController extends Controller
{

	use StorageTraits;
	public $carpeta = 'terceros.';

    public function form(){
    	$proyectos = Proyecto::all() ;
    	return view($this->carpeta.'form', compact('proyectos'));
    }

    public function store(Request $request){
    	$this->validate(
		    $request,[
	            'num_dc' => 'required',
	            'razon_social' => 'required'
	        ]
	    );
    	$tercero = Tercero::where('num_dc', $request->num_dc)->first();
    	$proyecto = Proyecto::find($request->proyecto_id);

    	if($tercero == null){
    		$tercero = new Tercero;
		    $tercero->num_dc = $request->num_dc;
		    $tercero->razon_social = $request->razon_social;
		    $tercero->empresa_id = $proyecto->owner->empresas_id;
		    $tercero->save();
    	}

    	$this->terceroProyecto($tercero->id, $request->proyecto_id);

	    $radicado =  new Radicado;
        $radicado->tercero_proyecto_id = $tercero->id;
        $radicado->save();

        foreach($request->file('documentos') as $doc){
            $documento = new DocumentoContratista;
            $ruta = $this->rutaTercero($request->proyecto_id, $radicado->id, $tercero->id);
            $documento->ruta = $this->uploadFile($doc, $ruta);
            $documento->borrado = '0';
            $documento->radicado_id = $radicado->id;
            $documento->save();
        }

        return redirect()->route('radicado.export', ['tercero', $tercero->id, $radicado->id]);
    }

    public function terceroProyecto($tercero_id, $proyecto_id){
    	$item = TerceroProyecto::where('tercero_id', $tercero_id)->where('proyecto_id', $proyecto_id)->get();
    	if(!$item->isEmpty()){
    		$update = $item->first();
    		$update->activo = $update->activo == '1' ? '0': '1';
    		$update->save();
    		$data = 'se actualizo';
    	}else{
    		$create = new TerceroProyecto;
	        $create->proyecto_id = $proyecto_id;
	        $create->tercero_id = $tercero_id;
	        $create->activo = '1';
			$create->save();
    		$data = 'se creo';
    	}
    }

    public function verificarTercero($num){
    	$tercero = Tercero::where('num_dc', $num)->first();
    	return response()->json($tercero);
    }
}
