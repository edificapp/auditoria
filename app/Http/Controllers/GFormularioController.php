<?php

namespace App\Http\Controllers;

use App\Proyecto;
use App\Formulario;
use App\GFormulario;
use Illuminate\Http\Request;
use App\Helpers\DataCollection;

class GFormularioController extends Controller
{
    protected $ruta_view = "proyecto.ejecucion.g_formularios.";

    public function index(){
        return view('formulario.index');
    }

    public function create(Proyecto $proyecto){
        $formularios = Formulario::where('activo', 'si')->get()->filter(function($item){  return $item->owner->empresa_id == auth()->user()->empresa_id; });
        return view("{$this->ruta_view}create", compact('formularios', 'proyecto'));
    }

    public function store(Request $request){
        if($request->tipo_muestra != 'Libre'):
            $peso_esperados = collect();
            foreach(DataCollection::rango_edad() as $i => $rango): 
                $peso_esperados->push($request["rango_{$i}"]);
            endforeach;
            $request->request->add(['peso_esperados' => $peso_esperados]);
        endif;
        $new = GFormulario::create($request->all());
        $new->formularios()->sync($request->formularios);
        return redirect()->route('proyectos.sub_menu', [$new->proyecto_id, 'documentos']);
    }

    public function edit(Proyecto $proyecto ,GFormulario $grupo_formulario){
        if($grupo_formulario->visita): 
            flash('no se puede modificar este grupo de formularios, esta relacionado a una visita.')->warning();
            return back();
        endif;

        $formularios = Formulario::where('activo', 'si')->get()->filter(function($item){  return $item->owner->empresa_id == auth()->user()->empresa_id; });
        return view("{$this->ruta_view}edit", compact('grupo_formulario', 'formularios', 'proyecto'));
    }

    public function show(Proyecto $proyecto, GFormulario $grupo_formulario){
        return view("{$this->ruta_view}show", compact('grupo_formulario', 'proyecto'));
    }

    public function update(Request $request, GFormulario $grupo_formulario){
        if($grupo_formulario->visita): 
            flash('no se puede modificar este grupo de formularios, esta relacionado a una visita.')->warning();
            return back();
        endif;
        if($request->tipo_muestra != 'Libre'):
            $peso_esperados = collect();
            foreach(DataCollection::rango_edad() as $i => $rango): 
                $peso_esperados->push($request["rango_{$i}"]);
            endforeach;
            $request->request->add(['peso_esperados' => $peso_esperados]);
        endif;
        $grupo_formulario->update($request->all());
        $grupo_formulario->formularios()->sync($request->formularios);
        return redirect()->route('proyectos.sub_menu', [$grupo_formulario->proyecto_id, 'documentos']);
    }

    public function estado(GFormulario $grupo_formulario){
        $grupo_formulario->activo = !$grupo_formulario->activo;
        $grupo_formulario->save();
        return back();
    }
}
