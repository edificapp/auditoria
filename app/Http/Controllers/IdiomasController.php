<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\VerifyTraits;
use App\Traits\StorageTraits;
use App\Idiomas;
use Auth;

class IdiomasController extends Controller
{
    use VerifyTraits, StorageTraits;

    private $model = 'App\Idiomas';

    public function index(){
    	$curriculum = $this->verifyCurriculum();
    	return view('hojavida.idiomas.index', compact('curriculum'));
    }

    public function create(){
    	return view('hojavida.idiomas.create');
    }

    public function store(Request $request){
    	$idioma = new Idiomas;
        $idioma->curriculum_id = Auth::user()->curriculum->id;
        $idioma->idioma = $request->idioma;
        $idioma->habla = $request->habla;
        $idioma->lee = $request->lee;
        $idioma->escribe = $request->escribe;
        $idioma->observacion = $request->observacion;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('Idiomas');
            $idioma->certificado = $this->uploadFile($request->certificado, $ruta);
        }

        $idioma->save();

        $this->activityCreate(
            ['attribute' => $idioma], 
            $this->model, 
            $idioma->id, 
            "ha relacionado un nuevo idioma a su hoja de vida."
        );

        flash('Se ha Creado un Nuevo Idioma.')->success();
        return redirect()->route('idiomas.index');
    }

    public function show($id){
    }

    public function edit($id){
    	$idioma = Idiomas::find($id);
    	return view('hojavida.idiomas.edit', compact('idioma'));
    }

    public function update(Request $request, $id){
    	$idioma = Idiomas::find($id);
        $old = Idiomas::find($id);
        $idioma->idioma = $request->idioma;
        $idioma->habla = $request->habla;
        $idioma->lee = $request->lee;
        $idioma->escribe = $request->escribe;
        $idioma->observacion = $request->observacion;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('idiomas');
            $idioma->certificado = $this->uploadFile($request->certificado, $ruta, $idioma->certificado);
        }

        $idioma->save();

         $this->activityUpdate(
                ['attribute' => $idioma, 'old' => $old], 
                $this->model, $idioma->id, 
                "ha actualizado un idioma de la hoja de vida."
            );

        flash('Se ha Edita el Idioma con Exito.')->success();
        return redirect()->route('idiomas.index');
    }

    public function destroy($id){
    	$item = Idiomas::find($id);
    	$this->deleteFile($item->certificado);
    	$item->delete();

        $this->activityDelete(
            $this->model, 
            $item->id,
            "ha eliminado un idioma de su hoja de vida."
        );


    	flash('Se ha Borrado el idioma con Exito.')->error();
        return redirect()->route('idiomas.index');
    }
}
