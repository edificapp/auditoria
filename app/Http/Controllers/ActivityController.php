<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{

    public function index(){
        if(Auth::guest()){
            return redirect()->route('login');
        }else{
            if(is_null(auth()->user()->empresas_id)):
                return redirect()->route('usuarios.index');
            else: 
                return view('inicio.index');
            endif;
        }
    }

    public function activities(){
        return response()->json(auth()->user()->actividades_log->map(function($i){
            return [
                'id'=> $i->id,
                'causer_id' => $i->causer_id,
                'causer' => $i->causer,
                'created_at' => $i->created_at->format('d.m.Y H:i:s'),
                'time' => $i->time,
                'description' => $i->description
            ];
        }));
    }

    public function showProperties(Activity $activity){
        //$result = array();
         
        $result = '';
        if(!empty($activity->properties)):
            foreach ($activity->properties as $key => $value):
                $titulo = $key == 'old' ? 'Datos Viejos' : 'Datos Nuevos';
                $result.="<div class='card col-xs-12 col-sm-12 col-md-6 col-lg-6'><div class='card-body'><table class='table'><thead><th colspan='2'>{$titulo}</th></thead><tbody>";
                $keys = array_keys($value);
                $values = array_values($value);
                for($i=0; $i<count($keys); $i++):
                    $result.= "<tr><td><b>{$keys[$i]}:</b></td><td>{$values[$i]}</td></tr>";
                endfor;    
                $result.="</body></table></div></div>";                            
                //$result[$key] =  array(['keys' => $keys, 'values' => $values]);
            endforeach;
        endif;

        return $result;
    }
}
