<?php

namespace App\Http\Controllers;

use App\User;
use App\Logros;
use App\Empresa;
use App\Idiomas;
use App\Elaboral;

use App\Estudios;
use App\Oestudios;
use App\Curriculum;
use Illuminate\Http\Request;
use App\Traits\StorageTraits;

//use App\Mail\confirmarRegistro;
use App\Traits\CollectionsTraits;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Notifications\confirmarRegistro;

use App,Auth,Validator,Crypt,Mail,DomPDF;

use Illuminate\Support\Facades\Notification;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Foundation\Auth\RegistersUsers;

class UsuariosController extends Controller
{
    use CollectionsTraits, StorageTraits;

    public $model = 'App\User';
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if (Auth::user()->inv_activa == "si") {
            return $this->validar(Crypt::encrypt(Auth::user()->id));
        }

        $usuarios = User::where('empresas_id',Auth::user()->empresas_id)->orderBy('empresas_id','ASC')->get();
        return view('usuario.index',compact('usuarios'));
    }

    public function usersApi()
    {
        return auth()->user()->empresa->usuarios->map(function($i){
            return [
                'id' => $i->id,
                'avatar_user' => $i->avatar_user,
                'name' => $i->name,
                'count_is_read' => $i->messages_is_read->count()
            ];
        });
    }

     /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function usuariosFiltrar()
    {
        $usuarios = User::where('empresas_id',Auth::user()->empresas_id)->orderBy('empresas_id','ASC')->get();
        return view('usuario.filtrar',compact('usuarios'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $roles = Role::All();
        $empresas = Empresa::get();

        return view('usuario.create',compact('roles','empresas'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'inv_activa' => ['required', 'string', 'max:4'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('usuarios.create'))->withErrors($validacion)->withInput();
        }

        $request->request->add(['empresas_id' => Auth::user()->empresas_id]);
        $usuario = user::create($request->all());

        $usuario->assignRole($request->roles);
        $this->enviar_invitacion($usuario->id);

        $this->activityCreate(
            ['attribute' => $usuario], 
            $this->model, 
            $usuario->id, 
            "se ha generado el usuario ${usuario['name']}."
        );

        flash('Se ha creado el usuario.')->success();
        return redirect(route('usuarios.index'));
    }

    public function registrarsePublic(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'inv_activa' => ['required', 'string', 'max:4'],
            'usuario' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return back()->withErrors($validacion)->withInput();
        }

        $inv_activa = $request->inv_activa;

        $usuario = new User();
        $usuario->name = $request->usuario;
        $usuario->email = $request->email;
        $usuario->roles_id = 4;

        $usuario->empresas_id = $request->empresa;

        $usuario->inv_activa = $inv_activa;
        $usuario->save();

        $this->enviar_invitacion($usuario->id);

        flash('Se ha creado el usuario satisfactoriamente.  Revisa el correo '.$usuario->email.' para proseguir con el registro')->success();
        return redirect('/');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function reenviar(Request $request, $id)
    {
        $usuario = User::find($id);
        return view('usuario.reenviar',compact('usuario'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function reenviar_update(Request $request,$id)
    {
        $validacion = Validator::make($request->all(), [
            'usuario' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('enviar_invitacion',$id))->withErrors($validacion)->withInput();
        }

        $usuario = User::findOrFail($id);
        $usuario->name = $request->usuario;
        $usuario->email = $request->email;
        $usuario->save();

        $this->enviar_invitacion($usuario->id);

        flash('Se ha enviado la invitación al usuario.')->success();
        return redirect(route('usuarios.index'));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);
        $roles = Role::all();

        return view('usuario.edit',compact('usuario', 'roles'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],

            'usuario' => ['required_if:inv_activa,no', 'nullable', 'string', 'max:255', 'unique:users,id,'.$id],
            'password' => ['required_if:inv_activa,no', 'nullable', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return back()->withErrors($validacion)->withInput();
        }


        $usuario = User::findOrFail($id);
        $old = User::findOrFail($id);
        $usuario->name = $request->nombre;
        $usuario->email = $request->email;
        $usuario->inv_activa = 'no';
        $usuario->usuario = $request->usuario;
        $usuario->password = Hash::make($request->password);
        $usuario->save();

        $usuario->syncRoles($request->roles);

        $this->activityUpdate(
            ['attribute' => $usuario, 'old' => $old], 
            $this->model, $usuario->id, 
            "el empleado(a) ${usuario['name']} ha sido actualizado."
        );

        flash('Se ha actualizado el empleado con exito.')->success();
        return redirect(route('usuarios.index'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function perfil()
    {
        $usuario = Auth::user();
        return view('usuario.perfil',compact('usuario'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update_perfil(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'usuario' => ['required', 'string', 'max:255', 'unique:users,id,'.$id],
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'password' => ['nullable','string', 'min:8', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('perfil'))->withErrors($validacion)->withInput();
        }

        $usuario = User::find(Auth::user()->id);
        $usuario->usuario = $request->usuario;
        $usuario->name = $request->nombre;
        $usuario->email = $request->email;

        if ($request->password) {
            $usuario->password = Hash::make($request->password);
        }

        $usuario->save();

        flash('Se ha actualizado tu perfil.')->success();
        return redirect(route('perfil'));
    }

    public function enviar_invitacion_url(Request $request,$id)
    {
        $this->enviar_invitacion($id);
        flash('Se enviado la invitación.')->success();
        return redirect(route('usuarios.index'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function enviar_invitacion($id)
    {
        $usuario = User::findOrFail($id);
        $usuario->notify(new confirmarRegistro(route('registro',Crypt::encrypt($usuario->id))));

        //Mail::to($usuario->email)->send(new confirmarRegistro(route('registro',Crypt::encrypt($usuario->id))));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function validar($id)
    {
        $usuario = User::findOrFail(Crypt::decrypt($id));

        if ($usuario->inv_activa == "no") {
            flash('Ha ocurrido un error, la cuenta ya esta activa.')->warning();
            return redirect(route('login'));
        }

        $key = $id;

        Auth::login($usuario);

        return view('usuario.continue',compact('usuario','key'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function validar_update(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'inv_activa' => ['required', 'string', 'max:4'],
            'usuario' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('registro',$request->_key))->withErrors($validacion)->withInput();
        }

        $usuario = User::findOrFail($id);
        $usuario->name = $request->usuario;
        $usuario->password =  Hash::make($request->password);
        $usuario->inv_activa = 'no';
        $usuario->save();

        flash('Se ha completado el registro con exito.')->success();
        return redirect()->route('inicio');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function estado(Request $request,$id)
    {
        if (Auth::user()->id == $id) {
            flash('Ha ocurrido un error, el estado de la cuenta no puede ser modificado.')->warning();
            return redirect(route('usuarios.index'));
        }

        $usuario = User::findOrFail($id);
        $old = User::findOrFail($id);
        $usuario->activo = $old->activo == 'si' ? 'no' : 'si';
        //dd($usuario);
        $usuario->save();
        $estado  = $usuario->activo == 'si' ? 'activado': 'desactivado';

        $this->activityUpdate(
            ['attribute' => $usuario, 'old' => $old], 
            $this->model, $usuario->id, 
            "el empleado(a) ${usuario['name']} ha sido {$estado}."
        );
        flash("el empleado(a) ${usuario['name']} ha sido {$estado}.")->success();
        return redirect(route('usuarios.index'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function getUsuarios(Request $request)
    {
        if($request->ajax()){

            $_usuarios = User::get();
            $usuarios = collect([]);

            foreach ($_usuarios as $usuario) {
                if ($usuario->empresas_id == Auth::user()->empresas_id && $usuario->rol->nombre == 'Usuario' && $usuario->activo == 'si') {
                    $usuarios->push(['id' =>$usuario->id, 'text' => $usuario->nombre]);
                }
            }

            return $usuarios;

        }

        return redirect(route('home'));
    }

    /**
    * Vista de hoja de vida
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function curriculum()
    {
        $usuario = Auth::user();
        $curriculum = Curriculum::firstOrNew(['usuario_id' => $usuario->id]);
        $idiomas = [];

        $paises = array("Colombia","Afganistán","Albania","Alemania","Andorra","Angola","Antigua y Barbuda","Arabia Saudita","Argelia","Argentina","Armenia","Australia","Austria","Azerbaiyán","Bahamas","Bangladés","Barbados","Baréin","Bélgica","Belice","Benín","Bielorrusia","Birmania","Bolivia","Bosnia y Herzegovina","Botsuana","Brasil","Brunéi","Bulgaria","Burkina Faso","Burundi","Bután","Cabo Verde","Camboya","Camerún","Canadá","Catar","Chad","Chile","China","Chipre","Ciudad del Vaticano","Comoras","Corea del Norte","Corea del Sur","Costa de Marfil","Costa Rica","Croacia","Cuba","Dinamarca","Dominica","Ecuador","Egipto","El Salvador","Emiratos Árabes Unidos","Eritrea","Eslovaquia","Eslovenia","España","Estados Unidos","Estonia","Etiopía","Filipinas","Finlandia","Fiyi","Francia","Gabón","Gambia","Georgia","Ghana","Granada","Grecia","Guatemala","Guyana","Guinea","Guinea ecuatorial","Guinea-Bisáu","Haití","Honduras","Hungría","India","Indonesia","Irak","Irán","Irlanda","Islandia","Islas Marshall","Islas Salomón","Israel","Italia","Jamaica","Japón","Jordania","Kazajistán","Kenia","Kirguistán","Kiribati","Kuwait","Laos","Lesoto","Letonia","Líbano","Liberia","Libia","Liechtenstein","Lituania","Luxemburgo","Madagascar","Malasia","Malaui","Maldivas","Malí","Malta","Marruecos","Mauricio","Mauritania","México","Micronesia","Moldavia","Mónaco","Mongolia","Montenegro","Mozambique","Namibia","Nauru","Nepal","Nicaragua","Níger","Nigeria","Noruega","Nueva Zelanda","Omán","Países Bajos","Pakistán","Palaos","Panamá","Papúa Nueva Guinea","Paraguay","Perú","Polonia","Portugal","Reino Unido","República Centroafricana","República Checa","República de Macedonia","República del Congo","República Democrática del Congo","República Dominicana","República Sudafricana","Ruanda","Rumanía","Rusia","Samoa","San Cristóbal y Nieves","San Marino","San Vicente y las Granadinas","Santa Lucía","Santo Tomé y Príncipe","Senegal","Serbia","Seychelles","Sierra Leona","Singapur","Siria","Somalia","Sri Lanka","Suazilandia","Sudán","Sudán del Sur","Suecia","Suiza","Surinam","Tailandia","Tanzania","Tayikistán","Timor Oriental","Togo","Tonga","Trinidad y Tobago","Túnez","Turkmenistán","Turquía","Tuvalu","Ucrania","Uganda","Uruguay","Uzbekistán","Vanuatu","Venezuela","Vietnam","Yemen","Yibuti","Zambia","Zimbabue");
        $depto_ciudades = $this->ciudadesxdepartamento();

        $genero = $curriculum->genero;
        $nacionalidad = $curriculum->nacionalidad;

        $nombres = $curriculum->nombres;
        $primer_apellido = $curriculum->p_apellido;
        $segundo_apellido = $curriculum->s_apellido;

        $pais_extranjero = $curriculum->pais_extranjero;

        $dni_nal_tipo = $curriculum->dni_nal_tipo;
        $dni_nal = $curriculum->dni_nal;

        $dni_ext_tipo = $curriculum->dni_ext_tipo;
        $dni_ext = $curriculum->dni_ext;

        $libreta = $curriculum->libreta;
        $numero_libreta = $curriculum->numero_libreta;

        $dm = $curriculum->dm;
        $imagen = $curriculum->imagen;

        $dni_ext_tipo = $curriculum->dni_ext_tipo;
        $dni_ext = $curriculum->dni_ext;

        $nacimiento_municipio = $curriculum->nacimiento_municipio;
        $nacimiento_departamento = $curriculum->nacimiento_departamento;
        $nacimiento_pais = $curriculum->nacimiento_pais;
        $telefonos = $curriculum->telefonos;

        $mun_nacimiento = $curriculum->nacimiento_municipio;
        $dep_nacimiento = $curriculum->nacimiento_departamento;
        $pais_nacimiento = $curriculum->nacimiento_pais;
        $fecha_nacimiento = $curriculum->fecha_nacimiento;
        $edad = $curriculum->edad;

        $grado = $curriculum->grado;
        $titulo = $curriculum->titulo;
        $escuela = $curriculum->escuela;
        $ciudad_escuela = $curriculum->ciudad_escuela;
        $fecha_fin = $curriculum->fecha_fin;
        $certificado_basico = $curriculum->certificado_estudios;

        return view('hojavida.index',compact(
            'depto_ciudades',
            'paises',

            'imagen',
            'nombres',
            'primer_apellido',
            'segundo_apellido',
            'telefonos',

            'mun_nacimiento',
            'dep_nacimiento',
            'pais_nacimiento',
            'fecha_nacimiento',
            'pais_nacimiento',

            'genero',
            'libreta',
            'numero_libreta',
            'dm',

            'nacionalidad',
            'dni_nal_tipo',
            'dni_nal',


            'grado',
            'titulo',
            'escuela',
            'ciudad_escuela',
            'fecha_fin',
            'certificado_basico',

            'dni_ext_tipo',
            'pais_extranjero',

            'logros_laborales',
            'idiomas',

            'estudios_superiores',
            'otros_estudios',

            'experiencias_laborales'
        ));
    }

    /**
    * Request de hoja de vida
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update_curriculum(Request $request)
    {
        $curriculum = Curriculum::where('usuario_id',Auth::id())->first();
        $old = Curriculum::where('usuario_id',Auth::id())->first();
        if (!$curriculum) {
            $curriculum = new Curriculum();
        }

        $curriculum->usuario_id = Auth::id();

        $ruta = $this->rutaUser('curriculum/avatar/');
        $curriculum->imagen = $request->hasFile('inp_imagen') ? $this->uploadFile($request->inp_image, $ruta) : $curriculum->imagen;

        $curriculum->nombres = $request->nombres;
        $curriculum->p_apellido = $request->primer_apellido;
        $curriculum->s_apellido = $request->segundo_apellido;
        $curriculum->genero = $request->genero;
        $curriculum->telefonos = $request->telefonos;

        $curriculum->nacimiento_municipio = $request->mun_nacimiento;
        $curriculum->nacimiento_departamento = $request->dep_nacimiento;
        $curriculum->nacimiento_pais = $request->pais_nacimiento;

        /* Libreta militar */
        $curriculum->libreta = $request->libreta;
        $curriculum->numero_libreta = $request->numero_libreta;
        $curriculum->dm = $request->dm; /* Distrito militar */
        /* Libreta militar */

        /* Nacionalidades */
        $curriculum->nacionalidad = $request->nacionalidad;

        /* Colombiano */
        $curriculum->dni_nal_tipo = $request->dni_nal_tipo;
        $curriculum->dni_nal = $request->dni_nal;
        /* Colombiano */

        /* Extranjero */
        $curriculum->pais_extranjero = $request->pais_extranjero;
        $curriculum->dni_ext_tipo = $request->dni_ext_tipo;
        $curriculum->dni_ext = $request->dni_ext;
        /* Extranjero */
        /* Nacionalidades */

        /* Ultimo estudio */
        $curriculum->grado = $request->grado;
        $curriculum->titulo = $request->titulo;
        $curriculum->escuela = $request->escuela;
        $curriculum->ciudad_escuela = $request->ciudad_escuela;
        $curriculum->fecha_fin = $request->fecha_fin;

        $ruta = $this->rutaUser('curriculum/certificado_basico/');
        $curriculum->certificado_estudios = $request->hasFile('certificado_basico') ? $this->uploadFile($request->certificado_basico, $ruta) : $curriculum->certificado_estudios;

        /* Ultimo estudio */

        $curriculum->save();

        $this->activityUpdate(
            ['attribute' => $curriculum, 'old' => $old], 
            $this->model, $curriculum->id, 
            "ha actualizado su curriculum."
        );

        $contador_estudios = $request->contador_estudios;
        Estudios::where('curriculum_id',$curriculum->id)->delete();
        for ($i=0; $i < $contador_estudios; $i++) { 
            $estudio = new Estudios();
            $estudio->curriculum_id = $curriculum->id;
            $estudio->modalidad = $request->modalidad_ac[$i];
            $estudio->estudio = $request->titulo_estudio[$i];
            $estudio->escuela = $request->est_superiores_escuela[$i];
            $estudio->finalizado = $request->graduado[$i];
            $estudio->semestres = $request->semestres[$i];
            $estudio->fecha_fin = $request->fecha_fin_estudio[$i];
            $estudio->tarjeta = $request->tarjeta_profesional[$i];

            if(isset($request->certificado[$i])){
                $certificado = $request->certificado[$i];
                $_destino = 'files/'.Auth::id().'/';
                $_renombre = date('YmdHis') . "_certificadoE." . $certificado->getClientOriginalExtension();
                $certificado->move($_destino, $_renombre);
                $estudio->certificado = $_destino.$_renombre;
            }

            $estudio->save();
        }


        $contador_otros_estudios = $request->contador_otros_estudios;
        Oestudios::where('curriculum_id',$curriculum->id)->delete();
        for ($i=0; $i < $contador_otros_estudios; $i++) { 
            $oestudio = new Oestudios();
            $oestudio->curriculum_id = $curriculum->id;

            $oestudio->estudio = $request->titulo_otro_estudio[$i];
            $oestudio->escuela = $request->otros_estudios_escuela[$i];
            $oestudio->horas = $request->otros_estudios_horas[$i];
            $oestudio->ano = $request->otros_estudios_ano[$i];

            if(isset($request->certificado_otros_est[$i])){
                $certificado = $request->certificado_otros_est[$i];
                $_destino = 'files/'.Auth::id().'/';
                $_renombre = date('YmdHis') . "_certificadoOE." . $certificado->getClientOriginalExtension();
                //dd($_destino.$_renombre);
                $certificado->move($_destino, $_renombre

                );
                $oestudio->certificado = $_destino.$_renombre;
            }

            $oestudio->save();
        }

        $contador_logros = $request->contador_logros;
        Logros::where('curriculum_id',$curriculum->id)->delete();
        for ($i = 0; $i < $contador_logros; $i++) { 
            $logro = new Logros();
            $logro->curriculum_id = $curriculum->id;

            $logro->logro = $request->logro[$i];

            if(isset($request->certificado_logro[$i])){
                $certificado = $request->certificado_logro[$i];
                $_destino = 'files/'.Auth::id().'/';
                $_renombre = date('YmdHis') . "_certificadoL." . $certificado->getClientOriginalExtension();
                $certificado->move($_destino, $_renombre);
                $logro->certificado = $_destino.$_renombre;
            }

            $logro->save();
        }

        $contador_idiomas = $request->contador_idiomas;
        Idiomas::where('curriculum_id',$curriculum->id)->delete();
        for ($i = 0; $i < $contador_idiomas; $i++) { 
            $idioma = new Idiomas();
            $idioma->curriculum_id = $curriculum->id;

            $idioma->idioma = $request->idioma[$i];
            $idioma->habla = $request->habla[$i];
            $idioma->lee = $request->lee[$i];
            $idioma->escribe = $request->escribe[$i];
            $idioma->observacion = $request->observacion[$i];

            if(isset($request->certificado_idioma[$i])){
                $certificado = $request->certificado_idioma[$i];
                $_destino = 'files/'.Auth::id().'/';
                $_renombre = date('YmdHis') . "_certificadoI." . $certificado->getClientOriginalExtension();
                $certificado->move($_destino, $_renombre);
                $idioma->certificado = $_destino.$_renombre;
            }
            $idioma->save();
        }

        $contador_experiencias = $request->contador_experiencias;
        Elaboral::where('curriculum_id',$curriculum->id)->delete();
        for ($i = 0; $i < $contador_experiencias; $i++) {
            $experiencia_laboral = new Elaboral();
            $experiencia_laboral->curriculum_id = $curriculum->id;

            $dedicacion = $request->explab_dedicacion[$i];
            if ($dedicacion == 'otra') {
                $dedicacion = $request->explab_od[$i];
            }

            $pais = $request->explab_pais[$i];
            $departamento = $request->explab_departamento[$i];
            $ciudad = $request->explab_ciudad[$i];

            if ($pais != 'Colombia') {
                $departamento = $request->explab_inpdepartamento[$i];
                $ciudad = $request->explab_inpciudad[$i];
            }

            $experiencia_laboral->empresa = $request->explab_entidad[$i];
            $experiencia_laboral->dedicacion = $dedicacion;
            $experiencia_laboral->tipo = $request->explab_tipo[$i];

            $experiencia_laboral->cargo = $request->explab_cargo[$i];
            $experiencia_laboral->dependencia = $request->explab_dependencia[$i];

            $experiencia_laboral->pais = $pais;
            $experiencia_laboral->departamento = $departamento; 
            $experiencia_laboral->ciudad = $ciudad;
            $experiencia_laboral->direccion = $request->explab_direccion[$i];
            $experiencia_laboral->laborando = $request->explab_actualmente[$i];

            $experiencia_laboral->telefonos = $request->explab_telefonos[$i];
            $experiencia_laboral->fingreso = $request->explab_fingreso[$i];
            $experiencia_laboral->fterminacion = $request->explab_fterminacion[$i];

            if(isset($request->explab_certificado[$i])){
                $certificado = $request->explab_certificado[$i];
                $_destino = 'files/'.Auth::id().'/';
                $_renombre = date('YmdHis') . "_certificadoEXP." . $certificado->getClientOriginalExtension();
                $certificado->move($_destino, $_renombre);
                $experiencia_laboral->certificado = $_destino.$_renombre;
            }
            $experiencia_laboral->save();
        }

        flash('Se ha actualizado la hoja de vida.')->success();
        return redirect(route('curriculum'));
    }

    /**
    * Vista de hoja de vida
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function curriculum_ver($user_id)
    {
        $usuario = User::findOrFail($user_id);
        $idiomas = [];
        $curriculum = Curriculum::where('usuario_id',$usuario->id)->first();
        if (!$curriculum) {
            flash('Este usuario no ha iniciado la creación de su hoja de vida.')->warning();
            return redirect(route('usuarios.index'));
        }

        $genero = $curriculum->genero;
        $nacionalidad = $curriculum->nacionalidad;

        $nombres = $curriculum->nombres;
        $primer_apellido = $curriculum->p_apellido;
        $segundo_apellido = $curriculum->s_apellido;

        $pais_extranjero = $curriculum->pais_extranjero;

        $dni_nal_tipo = $curriculum->dni_nal_tipo;
        $dni_nal = $curriculum->dni_nal;

        $dni_ext_tipo = $curriculum->dni_ext_tipo;
        $dni_ext = $curriculum->dni_ext;

        $libreta = $curriculum->libreta;
        $numero_libreta = $curriculum->numero_libreta;

        $dm = $curriculum->dm;
        $imagen = $curriculum->imagen;

        $nacimiento_municipio = $curriculum->nacimiento_municipio;
        $nacimiento_departamento = $curriculum->nacimiento_departamento;
        $nacimiento_pais = $curriculum->nacimiento_pais;
        $telefonos = $curriculum->telefonos;

        $mun_nacimiento = $curriculum->nacimiento_municipio;
        $dep_nacimiento = $curriculum->nacimiento_departamento;
        $pais_nacimiento = $curriculum->nacimiento_pais;
        $fecha_nacimiento = $curriculum->fecha_nacimiento;
        $edad = $curriculum->edad;

        $grado = $curriculum->grado;
        $titulo = $curriculum->titulo;
        $escuela = $curriculum->escuela;
        $ciudad_escuela = $curriculum->ciudad_escuela;
        $fecha_fin = $curriculum->fecha_fin;
        $certificado_basico = $curriculum->certificado_estudios;


         $estudios_superiores = [];
        if (isset($curriculum->id)) {
            $estudios_superiores = Estudios::where('curriculum_id',$curriculum->id)->orderBy('fecha_fin', 'Desc')->get();
        }

        //dd($estudios_superiores);
        $otros_estudios = [];
        if (isset($curriculum->id)) {
            $otros_estudios = Oestudios::where('curriculum_id',$curriculum->id)->orderBy('ano', 'Desc')->get();
        }

        $logros_laborales = [];
        if (isset($curriculum->id)) {
            $logros_laborales = Logros::where('curriculum_id',$curriculum->id)->get();
        }

        $ano = [];
        if (isset($curriculum->id)) {
            $idiomas = Idiomas::where('curriculum_id',$curriculum->id)->get();
        }

        $experiencias_laborales = [];
        if (isset($curriculum->id)) {
            $experiencias_laborales = Elaboral::where('curriculum_id',$curriculum->id)->orderBy('fterminacion', 'Desc')->get();
        }
        $depto_ciudades = $this->ciudadesxdepartamento();
        $paises = $this->paises();

        return view('hojavida.show',compact(
            'usuario', 'depto_ciudades', 'paises',
            'imagen','nombres','primer_apellido','segundo_apellido','telefonos',
            'mun_nacimiento', 'dep_nacimiento', 'pais_nacimiento','fecha_nacimiento', 'edad',
            'genero', 'libreta', 'numero_libreta', 'dm',
            'nacionalidad', 'dni_nal_tipo', 'dni_nal',
            'dni_ext_tipo', 'dni_ext',
            'grado', 'titulo', 'escuela', 'ciudad_escuela', 'fecha_fin', 'certificado_basico',
            'dni_ext_tipo', 'pais_extranjero',
            'logros_laborales', 'idiomas',
            'estudios_superiores', 'otros_estudios',
            'experiencias_laborales'
        ));
    }

    /**
    * Vista de hoja de vida
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function exportarhv($user_id)
    {
        $usuario = User::findOrFail($user_id);
        $curriculum = Curriculum::where('usuario_id',$usuario->id)->first();
        if (!$curriculum) {
            flash('Este usuario no ha iniciado la creación de su hoja de vida.')->warning();
            return redirect(route('usuarios.index'));
        }

        $genero = $curriculum->genero;
        $nacionalidad = $curriculum->nacionalidad;

        $nombres = $curriculum->nombres;
        $primer_apellido = $curriculum->p_apellido;
        $segundo_apellido = $curriculum->s_apellido;

        $pais_extranjero = $curriculum->pais_extranjero;

        $dni_nal_tipo = $curriculum->dni_nal_tipo;
        $dni_nal = $curriculum->dni_nal;

        $dni_ext_tipo = $curriculum->dni_ext_tipo;
        $dni_ext = $curriculum->dni_ext;

        $libreta = $curriculum->libreta;
        $numero_libreta = $curriculum->numero_libreta;

        $dm = $curriculum->dm;
        $imagen = $curriculum->imagen;

        if (strpos("123".$imagen, 'https://') != false || strpos("123".$imagen, 'http://') != false) {
            $ruta_img = explode('/', $imagen);
            unset($ruta_img[0]);
            unset($ruta_img[1]);
            unset($ruta_img[2]);
            $imagen = implode('/', $ruta_img);
            $imagen = public_path($imagen);
        }

        $imagen = $imagen ?: public_path('admin/images/usuario/usuario.jpg');

        $nacimiento_municipio = $curriculum->nacimiento_municipio;
        $nacimiento_departamento = $curriculum->nacimiento_departamento;
        $nacimiento_pais = $curriculum->nacimiento_pais;
        $telefonos = $curriculum->telefonos;

        $mun_nacimiento = $curriculum->nacimiento_municipio;
        $dep_nacimiento = $curriculum->nacimiento_departamento;
        $pais_nacimiento = $curriculum->nacimiento_pais;

        $grado = $curriculum->grado;
        $titulo = $curriculum->titulo;
        $escuela = $curriculum->escuela;
        $ciudad_escuela = $curriculum->ciudad_escuela;
        $fecha_fin = $curriculum->fecha_fin;
        $certificado_basico = $curriculum->certificado_estudios;

        $certificados = [];
        $certificados[] = $certificado_basico;

        $estudios_superiores = [];
        if (isset($curriculum->id)) {
            $estudios_superiores = Estudios::where('curriculum_id',$curriculum->id)->get();
        }

        $otros_estudios = [];
        if (isset($curriculum->id)) {
            $otros_estudios = Oestudios::where('curriculum_id',$curriculum->id)->get();
        }

        $logros_laborales = [];
        if (isset($curriculum->id)) {
            $logros_laborales = Logros::where('curriculum_id',$curriculum->id)->get();
        }

        $idiomas = [];
        if (isset($curriculum->id)) {
            $idiomas = Idiomas::where('curriculum_id',$curriculum->id)->get();
        }

        $experiencias_laborales = [];
        if (isset($curriculum->id)) {
            $experiencias_laborales = Elaboral::where('curriculum_id',$curriculum->id)->get();
        }

        /* CERTIFICADOS */
        $certificados_estudios_superiores = [];
        foreach ($estudios_superiores as $estudio_superior) {
            if ($estudio_superior->certificado) {
                $certificados_estudios_superiores[] = $estudio_superior->certificado;
            }
        }

        foreach ($otros_estudios as $otro_estudio) {
            if ($otro_estudio->certificado) {
                $certificados[] = $otro_estudio->certificado;
            }
        }

        foreach ($logros_laborales as $logro_laboral) {
            if ($logro_laboral->certificado) {
                $certificados[] = $logro_laboral->certificado;
            }
        }

        foreach ($idiomas as $idioma) {
            if ($idioma->certificado) {
                $certificados[] = $idioma->certificado;
            }
        }

        foreach ($experiencias_laborales as $experiencia_laboral) {
            if ($experiencia_laboral->certificado) {
                $certificados[] = $experiencia_laboral->certificado;
            }
        }

        $pdf = PDF::loadView('hojavida.export',compact(
            'usuario',
            'depto_ciudades',
            'paises',

            'imagen',
            'nombres',
            'primer_apellido',
            'segundo_apellido',
            'telefonos',

            'mun_nacimiento',
            'dep_nacimiento',
            'pais_nacimiento',

            'genero',
            'libreta',
            'numero_libreta',
            'dm',

            'nacionalidad',
            'dni_nal_tipo',
            'dni_nal',

            'dni_ext_tipo',
            'dni_ext',

            'grado',
            'titulo',
            'escuela',
            'ciudad_escuela',
            'fecha_fin',
            'certificado_basico',

            'dni_ext_tipo',
            'pais_extranjero',

            'logros_laborales',
            'idiomas',

            'estudios_superiores',
            'otros_estudios',

            'experiencias_laborales'
        ));

        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true)
            ->setPaper('a4')
            ->setOption('margin-top', 0)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-bottom', 0);

        return $pdf->stream();
    }
}
