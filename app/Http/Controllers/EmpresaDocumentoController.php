<?php

namespace App\Http\Controllers;

use App\EmpresaDocumento;
use App\Traits\StorageTraits;
use Auth;
use Illuminate\Http\Request;

class EmpresaDocumentoController extends Controller
{
    use StorageTraits;

    public $model = 'App\EmpresaDocumento';

/*
    function __construct()
    {
         $this->middleware('permission:empresa-documentos.index', ['only' => ['index']]);
         $this->middleware('permission:empresa-documentos.store', ['only' => ['store']]);
    }
*/
    public function index(){
        $empresa = Auth::user()->empresa;
        return view('empresa.documentos',compact('empresa'));
    }

    public function store(Request $request){

        $request->request->add([
            'ruta' => '',
            'empresa_id' => Auth::user()->empresa->id
        ]);
        $create = EmpresaDocumento::create($request->all());

        $ruta = $this->rutaEmpresa(Auth::user()->empresa->id.'/documentos/'.$create->id);
        $create->ruta = $this->uploadFile($request->documento, $ruta);
        $create->save();

        $this->activityCreate(
            ['attribute' => $create], 
            $this->model,
            $create->id,
            "un nuevo documento se ha adjuntado a la empresa."
        );

        return back();
    }

    public function show($id){

    }
}
