<?php

namespace App\Http\Controllers;

use App\EmpresaExperiencia;
use App\EmpresaExperienciaUnspsc;
use App\Http\Resources\EmpresaExperienciaResource;
use App\Traits\StorageTraits;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\EmpresaExperienciaRequest;

class EmpresaExperienciaController extends Controller
{
    use StorageTraits;
/*
    function __construct()
    {
         $this->middleware('permission:empresa-experiencia.index', ['only' => ['index']]);
         $this->middleware('permission:empresa-experiencia.create', ['only' => ['store']]);
    }
*/
    public $model = "App\EmpresaExperiencia";

    public function index(){    
        $empresa = Auth::user()->empresa;
        return view('empresa.experiencia',compact('empresa'));
    }

    public function store(EmpresaExperienciaRequest $request){
        $request->request->add(['empresa_id' => Auth::user()->empresa->id]);
        $create = EmpresaExperiencia::create($request->all());

        $ruta = $this->rutaEmpresa($create->empresa_id.'/certificados/'.$create->id);
        $create->url_contrato = $request->hasFile('file_contrato') ? $this->uploadFile($request->file_contrato, $ruta) : '';
        $create->url_certificado = $request->hasFile('file_certificado') ? $this->uploadFile($request->file_certificado, $ruta) : '';
        $create->save();

        $create->Codes()->sync($request->codigos_unspsc);

        $this->activityCreate(
            ['attribute' => $create], 
            $this->model, 
            $create->id, 
            "Se ha adjuntado una nueva experiencia a la empresa."
        );

        return back();
    }

    public function code_unspsc(Request $request){
        $codes = CodeUnspsc::where('code', 'like', '%' . $request->searchTerm . '%');
        return response()->json($codes);
    }

    public function show($id){
        return response()->json(new EmpresaExperienciaResource(EmpresaExperiencia::find($id)));
    }
}
