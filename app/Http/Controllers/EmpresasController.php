<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Mail\confirmarRegistro;
use App\Rol;
use App\Traits\StorageTraits;
use App\User;
use Auth,Validator,Crypt,Mail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EmpresasController extends Controller
{

    use StorageTraits;


    public function __construct()
    {
        //$this->middleware('permission:universal')->only('informacion');
        //$this->middleware('permission:empresa-experiencia.update', ['only' => ['updateInformacion']]);
    }
    
    public $model = 'App\Empresa';

    public function informacion()
    {
        
        //$user = Auth::user();
        //$role = Role::find(1);
        //$role->revokePermissionTo('universal');//remueve un permiso
        //$role->givePermissionTo([7,8,9,10,11]);//se agregan  array de permisos a un rol 
        //dd($user->can('universal'));//si tiene ese permiso dara un true
        //dd($user->getAllPermissions());//permisos de un user
        //dd($user->getPermissionsViaRoles());//permisos de un user
        //dd($user->getRoleNames());//roles de un user
        //dd($user->hasAllRoles(Role::all()));//roles de un user
        $empresa = Auth::user()->empresa;
        return view('empresa.informacion',compact('empresa'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::get();
        $usuarios = User::whereNotNull('empresas_id')->get();
        return view('empresa.index',compact('empresas','usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'nit' => ['required', 'string', 'max:255', 'unique:empresas'],
            'nombre' => ['required', 'string', 'max:255', 'unique:empresas'],
            
            'direccion' => ['required', 'string', 'max:255'],
            'telefono' => ['required', 'string', 'max:255'],

            'activo' => ['required', 'string', 'max:2'],
            'email' => ['nullable', 'string', 'email', 'max:255', 'unique:empresas'],

            'usuario' => ['required', 'string', 'max:255'],
            'email_usuario' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('empresas.create'))->withErrors($validacion)->withInput();
        }

        $empresa = new Empresa();
        $empresa->nit = $request->nit;
        $empresa->nombre = $request->nombre;
        $empresa->activo = $request->activo;
        $empresa->email = $request->email;
        $empresa->save();

        $usuario = $empresa->usersAll()->create([
            'name' => $request->usuario,
            'email' => $request->email_usuario,
            'roles_id' => Rol::where('nombre','Administrador')->first()->id,
            'inv_activa' => 'si',
        ]);

/*
        $usuario = new User();
        $usuario->name = $request->usuario;
        $usuario->email = $request->email_usuario;
        $usuario->roles_id = Rol::where('nombre','Administrador')->first()->id;
        $usuario->empresas_id = $empresa->id;
        $usuario->inv_activa = 'si';
        $usuario->save();
*/
        $this->enviar_invitacion($usuario->id);

        flash('Se ha creado la empresa y se ha definido el usuario administrador con exito.')->success();
        return redirect(route('empresas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajaxEmpresas(){
        $empresas = Empresa::all();
        return response()->json($empresas);
    }


    public function enviar_invitacion($id)
    {
        $usuario = User::findOrFail($id);

        Mail::to($usuario->email)->send(new confirmarRegistro(route('registro',Crypt::encrypt($usuario->id))));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::find($id);
        return view('empresa.informacion',compact('empresa'));
    }



    public function updateInformacion(Request $request, $id){
        //dd($request->all());
        $validacion = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255', Rule::unique('empresas')->ignore(Auth::user()->empresa->id)],
            'nit' => ['required', 'string', 'max:255', Rule::unique('empresas')->ignore(Auth::user()->empresa->id)],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('empresas')->ignore(Auth::user()->empresa->id)],
            
            'ciudad' => ['required', 'string', 'max:255'],
            'barrio' => ['required', 'string', 'max:255'],
            'direccion' => ['required', 'string', 'max:255'],
            'telefono' => ['required', 'integer'],
            'representante_legal' => ['required', 'string', 'max:255']
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return back()->withErrors($validacion)->withInput();
        }

        $newRuta = $this->rutaEmpresa($id);

        $update = Empresa::findOrFail($id);
        $old = Empresa::findOrFail($id);
        $update->fill($request->all());

        $update->url_logo_web = $request->hasFile('url_logo_web') ? $this->uploadFile($request->url_logo_web, $newRuta, $update->url_logo_web) : $update->url_logo_web;
        $update->url_logo_movil = $request->hasFile('url_logo_movil') ? $this->uploadFile($request->url_logo_movil, $newRuta, $update->url_logo_movil) : $update->url_logo_movil;
        $update->save();

         $this->activityUpdate(
                ['attribute' => $update, 'old' => $old], 
                $this->model, $update->id, 
                "se actualizaron los datos de la empresa."
        );


        flash('Se ha actualizado los datos de la empresa con exito.')->success();
        return back();

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::findOrFail($id);
        return view('empresa.edit',compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nit = $request->nit;
        $nombre = $request->nombre;
        $activo = $request->activo;
        $email = $request->email;

        $empresa = Empresa::findOrFail($id);
        $empresa->nit = $nit;
        $empresa->nombre = $nombre;
        $empresa->activo = $activo;
        $empresa->email = $email;

        $empresa->save();
        return redirect(route('empresas.index'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function estado(Request $request,$id)
    {
        $empresa = Empresa::findOrFail($id);
        $estado = 'si';
        
        if ($empresa->activo == 'si') {
            $estado = 'no';
        }

        $empresa->update(['activo' => $estado]);

        return redirect(route('empresas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usuarios($id)
    {
        $empresa = Empresa::findOrFail($id);
        $usuarios = User::where('empresas_id',$id)->get();

        return view('empresa.usuarios',compact('empresa','usuarios'));
    }
}
