<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CorrespondenciaController extends Controller
{
   public function index(){
       return view('correspondencia.index');
   }

   public function consulta(){
       return view('correspondencia.consulta');
   }

    // RADICACION
    public function getRadicacion(){
        return view('correspondencia.radicacion.index');    
    }

    public function getRadicacionEnviado(){
        return view('correspondencia.radicacion.enviado');    
    }
    
    public function getRadicacionRecibido(){
        return view('correspondencia.radicacion.recibido');    
    }

    public function getRadicacionInterno(){
        return view('correspondencia.radicacion.interno');    
    }

    public function getRadicacionSalida(){
        return view('correspondencia.radicacion.salida');    
    }

    // BANDEJA

    public function getBandejaEnviado(){
        return view('correspondencia.bandeja.enviado');    
    }
    
    public function getBandejaRecibido(){
        return view('correspondencia.bandeja.recibido');    
    }

    public function getBandejaInterno(){
        return view('correspondencia.bandeja.interno');    
    }

    public function getBandejaSalida(){
        return view('correspondencia.bandeja.salida');    
    }


}
