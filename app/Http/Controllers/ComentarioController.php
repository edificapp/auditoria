<?php

namespace App\Http\Controllers;

use App\Comentario;
use Illuminate\Http\Request;
use App\Events\ComentarioCreado;
use App\Http\Resources\ComentarioResource;

class ComentarioController extends Controller
{
    public function store(Request $request){
        $request->request->add(['user_id' => auth()->id()]);
        $comentario = Comentario::create($request->all());
        
        ComentarioCreado::dispatch(new ComentarioResource($comentario));
    }

    public function destroy(Comentario $comentario){
        $comentario->delete();
    }

    public function like(Comentario $comentario){
        $comentario->like();
    }

    public function unLike(Comentario $comentario){
        $comentario->unLike();
    }
}
