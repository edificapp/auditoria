<?php

namespace App\Http\Controllers;

use App\Team;
use App\User;
use App\Message;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use App\Events\PrivateMessageSent;
use App\Events\TeamMessageSent;
use App\Http\Resources\MessageResource;

class ChatMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function fetchMessages()
    {
        return Message::with('user')->get();
    }
   
    public function privateMessages(User $user)
    {
        if(Message::messageIsRead($user)->get()->count() > 0){
            Message::messageIsRead($user)->update(['is_read' => 1]);
        }
        
        return Message::with('user')->where(['user_id'=> auth()->id(), 'message_id'=> $user->id, 'message_type' => 'App\User'])
        ->orWhere(function($query) use($user){
            $query->where(['user_id' => $user->id, 'message_id' => auth()->id(), 'message_type' => 'App\User']);
        })
        ->get();
    }

    public function teamMessages(Team $team)
    {
        return Message::with('user')
        ->where(['message_id'=> $team->id, 'message_type' => 'App\Team'])
        ->whereDate('created_at', '>=', auth()->user()->team_user_created_at($team->id)->format('Y-m-d'))
        ->get();
    }

    public function sendPrivateMessage(Request $request,User $user)
    {
        //return $request->all();
        if(request()->has('file')){
            $message = $user->messages()->create([
                'name_file' => request('file')->getClientOriginalName(),
                'file' => request('file')->store("public/chat/private/{$user->id}"),
                'user_id' => auth()->id()
            ]);
        }else{
            $message = $user->messages()->create([
                'mensaje' => $request->message,
                'user_id' => auth()->id()
            ]);
        }

        broadcast(new PrivateMessageSent($message))->toOthers();
        
        return response(['status'=>'Message private sent successfully','message'=> new MessageResource($message)]);

    }

    public function sendTeamMessage(Request $request,Team $team)
    {
        if(request()->has('file')){
            $message = $team->messages()->create([
                'name_file' => request('file')->getClientOriginalName(),
                'file' => request('file')->store("public/chat/team/{$team->id}"),
                'user_id' => auth()->id()
            ]);
        }else{
            $message = $team->messages()->create([
                'mensaje' => $request->message,
                'user_id' => auth()->id()
            ]);
        }

        broadcast(new TeamMessageSent($message))->toOthers();
        
        return response(['status'=>'Message private sent successfully','message'=> new MessageResource($message)]);

    }
}
