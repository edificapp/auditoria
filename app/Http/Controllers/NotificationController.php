<?php

namespace App\Http\Controllers;

use Auth;
use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function list(){
        return view('notifications.index');
    }

    public function link(Notification $notificacion){
        $notificacion->markAsRead();
        return redirect($notificacion->url);
    }
}
