<?php

namespace App\Http\Controllers;

use App\PersonalProyecto;
use Illuminate\Http\Request;
use App\Notifications\NotificacionPersonalProyecto;

class PersonalProyectoController extends Controller
{
    private $model ="App\PersonalProyecto";

    public function store(Request $request){
        if(PersonalProyecto::where('proyecto_id', $request->proyecto_id)->where('user_id', $request->user_id)->get()->count() > 0){
            flash('Este usuario ya se encuentra relacionado al proyecto')->warning();
            return back();
        }
        //dd($request->all());
        $personal = PersonalProyecto::create($request->all() + ['activo' => TRUE]);

        $this->activityCreate(
            ['attribute' => $personal],
            $this->model,
            $personal->id,
            "se ha relacionado el usuario {$personal->user->name} del proyecto {$personal->proyecto->nombre}."
        );

        $personal->user->notify(new NotificacionPersonalProyecto($personal->proyecto->nombre, $personal->cargo)); 
    	return back();

    }

    public function edit(PersonalProyecto $personal_proyecto){
        return response()->json($personal_proyecto);
    }

    public function update(Request $request, PersonalProyecto $personal_proyecto){
        $old = $personal_proyecto;
        $personal_proyecto->update($request->all());

        $this->activityCreate(
            ['attribute' => $personal_proyecto, 'old' => $old],
            $this->model,
            $personal_proyecto->id,
            "se ha actualizado el usuario {$personal_proyecto->user->name} del proyecto {$personal_proyecto->proyecto->nombre}."
        );
        $personal_proyecto->user->notify(new NotificacionPersonalProyecto($personal_proyecto->proyecto->nombre, $personal_proyecto->cargo)); 
        return back();
    }

    public function destroy(PersonalProyecto $personal_proyecto){
    	$personal_proyecto->update(['activo' => !$personal_proyecto->activo]);

         $this->activityDelete(
            $this->model, 
            $personal_proyecto->id,
            "se ha desvinculado el usuario {$personal_proyecto->user->name} del proyecto {$personal_proyecto->proyecto->nombre}."
        );

    	return back();
    }
}
