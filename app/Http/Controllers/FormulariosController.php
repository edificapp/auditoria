<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Formulario;
use App\User;

use Auth,Validator;

class FormulariosController extends Controller
{

    private $rutaView = 'formulario';
    private $model = 'App\Formulario';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::has('ownerFormularios')->where('empresas_id', Auth::user()->empresas_id)->get();
        //dd($users);
        return view("{$this->rutaView}.index",compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("{$this->rutaView}.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255', 'unique:formularios'],
            'activo' => ['required', 'string', 'max:255'],
        ]);
        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();

            $ruta = route('formularios.create');
            if ($request->clonado) {
                $ruta = route('formularios.clone',$request->clonado);
            }
            return redirect($ruta)->withErrors($validacion)->withInput();
        }


        $formulario = new Formulario();
        $formulario->creador_id = Auth::user()->id;
        $formulario->nombre = $request->nombre;
        $formulario->activo = $request->activo;
        $formulario->formulario = $request->arreglos_campos;

        if ($request->clonado) {
            $formulario->clonado_id = $request->clonado;
        }

        $formulario->save();


        $message = $request->clonado ? 'Se ha clonado el formulario.' : 'Se ha creado un formulario.';
        $this->activityCreate(
            ['attribute' => $formulario], 
            $this->model, 
            $formulario->id, 
            $message
        );

        flash($message)->success();
        return redirect(route('formularios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formulario = Formulario::findOrFail($id);
        return view("{$this->rutaView}.show",compact("formulario"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Formulario $formulario)
    {
        if($formulario->grupo_formularios->count() > 0){
            flash('No se puede editar este formulario, debes desvincularlo del grupo')->warning();
            return back();
        }
        
        if ($formulario->creador_id != Auth::user()->id) {
            flash('No tienes permisos para modificar este formulario')->warning();
            return redirect(route('formularios.index'));
        }

        return view("{$this->rutaView}.edit",compact("formulario"));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clone($id)
    {
        $formulario = Formulario::findOrFail($id);
        return view("{$this->rutaView}.clone",compact("formulario"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function refresh(Formulario $formulario){
        $present = $formulario->old;
        $old = $formulario->formulario;
        $formulario->old = $old;
        $formulario->formulario = $present;
        $formulario->save();

        return view("{$this->rutaView}.show",compact("formulario"));
    } 


    public function update(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'activo' => ['required', 'string', 'max:255'],
        ]);

        $formulario = Formulario::findOrFail($id);
        $old = Formulario::findOrFail($id);

        if($formulario->grupo_formularios->count() > 0){
            flash('No se puede eliminar este formulario, debes desvincularlo del grupo')->warning();
            return back();
        }

        if ($formulario->creador_id != Auth::user()->id) {
            flash('No tienes permisos para modificar este formulario')->warning();
            return redirect(route('formularios.index'));
        }

        //dd($request->arreglos_campos);
        $formulario->nombre = $request->nombre;
        $formulario->activo = $request->estado;
        $formulario->old = $formulario->formulario;
        $formulario->formulario = $request->arreglos_campos;

        $formulario->save();

        $this->activityUpdate(
            ['attribute' => $formulario, 'old' => $old], 
            $this->model, $formulario->id, 
            "ha actualizado un Formulario."
        );

        flash('Se ha actualizado el formulario.')->success();
        return redirect(route('formularios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function estado(Request $request,$id)
    {

        $formulario = Formulario::findOrFail($id);
        $old = $formulario;

        if($formulario->grupo_formularios->count() > 0){
            flash('No se puede editar este formulario, debes desvincularlo del grupo')->warning();
            return back();
        }

        $estado = 'si';
        if ($formulario->activo == 'si') {
            $estado = 'no';
        }

        $formulario->update(['activo' => $estado]);

        $message = $formulario->activo == 'si' ? 'activado' : 'desactivado' ;

        $this->activityOtros(
            ['attribute' => $formulario, 'old' => $old], 
            $this->model, $formulario->id, 
            "el formulario se ha {$message}.",
            'activar'
        );

        flash('Se ha modificado el estado de este formulario.')->success();
        return redirect(route('formularios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getFormularios(Request $request)
    {
        if($request->ajax()){

            $_formularios = Formulario::get();
            $formulario = collect([]);

            foreach ($_formularios as $formulario) {
                $formulario->push(['id' =>$formulario->id, 'text' => $formulario->nombre]);
            }

            return $formulario;

        }

        return redirect(route('home'));
    }
}
