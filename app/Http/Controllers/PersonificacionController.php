<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PersonificacionController extends Controller
{
    private $personificacion_id =[];

    public function store(User $usuario){
        array_push($this->personificacion_id, auth()->id());
        session(['old_personificado_id' => $this->personificacion_id]);
        auth()->loginUsingId($usuario->id);
        $nombre = $usuario->curriculum ? $usuario->curriculum->full_name : $usuario->email;
        return back()->with('success', "Te has personificado como {$nombre}.");
    }

    public function destroy(){
        auth()->loginUsingId(session('old_personificado_id')[0]);
        session()->forget('old_personificado_id');
        return back()->with('success', "Te has logueado con tu cuenta.");
    }
}
