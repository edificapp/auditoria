<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\CollectionsTraits;
use App\Traits\StorageTraits;
use App\Traits\VerifyTraits;
use App\Curriculum;
use Auth;

class CurriculumController extends Controller
{
	use CollectionsTraits, StorageTraits, VerifyTraits;

    public function index(){
    	$curriculum = $this->verifyCurriculum();
    	$paises = $this->paises();
    	$depto_ciudades = $this->ciudadesxdepartamento();
    	//dd($curriculum->avatar());
    	return view('hojavida.curriculum.index', compact('curriculum', 'paises', 'depto_ciudades'));
    }

    public function create(){
    	
    }

    public function store(Request $request){
    	
    }

    public function show($id){
    	
    }

    public function edit($id){
    	
    }

    public function update(Request $request ,$id){
    	$curriculum =Curriculum::find($id);

        if($request->hasFile('avatar')){
        	$ruta = $this->rutaUser('avatar');
            $curriculum->imagen = $this->uploadFile($request->avatar, $ruta, $curriculum->imagen);
        }

        $curriculum->nombres = $request->nombres;
        $curriculum->p_apellido = $request->primer_apellido;
        $curriculum->s_apellido = $request->segundo_apellido;
        $curriculum->genero = $request->genero;
        $curriculum->telefonos = $request->telefonos;

        $curriculum->nacimiento_municipio = $request->mun_nacimiento;
        $curriculum->nacimiento_departamento = $request->dep_nacimiento;
        $curriculum->nacimiento_pais = $request->pais_nacimiento;
        $curriculum->fecha_nacimiento = $request->fecha_nacimiento;

        //dd($request->primer_apellido);
        /* Libreta militar */
        $curriculum->libreta = $request->libreta;
        $curriculum->numero_libreta = $request->numero_libreta;
        $curriculum->dm = $request->dm; /* Distrito militar */
        /* Libreta militar */

        /* Nacionalidades */
        $curriculum->nacionalidad = $request->nacionalidad;

        /* Colombiano */
        $curriculum->dni_nal_tipo = $request->dni_nal_tipo;
        $curriculum->dni_nal = $request->dni_nal;
        /* Colombiano */

        /* Extranjero */
        $curriculum->pais_extranjero = $request->pais_extranjero;
        $curriculum->dni_ext_tipo = $request->dni_ext_tipo;
        $curriculum->dni_ext = $request->dni_ext;
        /* Extranjero */
        /* Nacionalidades */

        /* Ultimo estudio */
        $curriculum->grado = $request->grado;
        $curriculum->titulo = $request->titulo;
        $curriculum->escuela = $request->escuela;
        $curriculum->ciudad_escuela = $request->ciudad_escuela;
        $curriculum->fecha_fin = $request->fecha_fin;

        if($request->hasFile('certificado_basico')){
        	$ruta = $this->rutaUser('certificado_basico');
            $curriculum->certificado_estudios = $this->uploadFile($request->certificado_basico, $ruta, $curriculum->certificado_estudios);
        }
        /* Ultimo estudio */

        $curriculum->save();

        flash('Se ha actualizado la Información General.')->success();
        return back();
    }

    public function destroy($id){
    	
    }
}
