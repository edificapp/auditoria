<?php

namespace App\Http\Controllers;

use App\DocumentoContratista;
use App\Radicado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\StorageTraits;

use Auth;

class DocumentoContratistaController extends Controller
{

    use StorageTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function projectos()
    {
        if(Auth::user()->rol->nombre == 'Contratista'){
            $directories = Auth::user()->contratistaProyecto;
        }else{
            $directories = Auth::user()->espectadorProyecto;
        }
        return view('contratista.dir', compact('directories'));
    }

    public function contratistas($proyecto_id){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function documentos()
    {
         return view('contratista.documentos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $radicado =  new Radicado;
        $radicado->contratista_proyecto_id = $id;
        $radicado->save();

        foreach($request->file('documentos') as $doc){
            $documento = new DocumentoContratista;
            $ruta = $this->rutaContratista($id, $radicado->id);
            $documento->ruta = $this->uploadFile($doc, $ruta);
            $documento->borrado = '0';
            $documento->radicado_id = $radicado->id;
            $documento->save();
        }

        return redirect()->route('radicado.export', ['contratista', Auth::user()->id, $radicado->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function borrado($id)
    {
        $documento = DocumentoContratista::findOrFail($id);
        $documento->borrado = '1';
        $documento->save();
        flash('El documento se borró satisfactoriamente')->warning();
        return redirect()->back();
    }
}
