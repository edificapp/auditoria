<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Traits\VerifyTraits;
use App\Traits\StorageTraits;
use App\Traits\CollectionsTraits;
use App\Elaboral;
use Auth;

class ElaboralController extends Controller
{
    use VerifyTraits, StorageTraits, CollectionsTraits;

    public $model = 'App\Elaboral';

    public function index(){
    	$curriculum = $this->verifyCurriculum();
    	return view('hojavida.laborales.index', compact('curriculum'));
    }

    public function create(){
    	$depto_ciudades = $this->ciudadesxdepartamento();
    	$paises = $this->paises();
    	return view('hojavida.laborales.create', compact('paises', 'depto_ciudades'));
    }

    public function store(Request $request){

        $experiencia_laboral = new ELaboral();
        $experiencia_laboral->curriculum_id = Auth::user()->curriculum->id;

        $experiencia_laboral->empresa = $request->entidad;
        $experiencia_laboral->dedicacion = $request->dedicacion == 'otra' ? $request->explab_od : $request->dedicacion;
        $experiencia_laboral->tipo = $request->tipo;

        $experiencia_laboral->cargo = $request->cargo;
        $experiencia_laboral->dependencia = $request->dependencia;

        $experiencia_laboral->pais = $request->pais;
        $experiencia_laboral->departamento = $request->pais != 'Colombia'? $request->inpdepartamento :$request->departamento;
        $experiencia_laboral->ciudad = $request->pais != 'Colombia'? $request->inpciudad :$request->ciudad;
        $experiencia_laboral->direccion = $request->direccion;
        $experiencia_laboral->laborando = $request->actualmente;

        $experiencia_laboral->telefonos = $request->telefono;
        $experiencia_laboral->fingreso = $request->fingreso;
        $experiencia_laboral->fterminacion = $request->fterminacion;

        $experiencia_laboral->funciones = $request->funciones;
        $experiencia_laboral->proyectos = $request->proyectos;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('laboral');
            $experiencia_laboral->certificado = $this->uploadFile($request->certificado, $ruta);
        }

        $experiencia_laboral->save();

        $this->activityCreate(
            ['attribute' => $experiencia_laboral], 
            $this->model, 
            $experiencia_laboral->id, 
            "ha creado una experiencia laboral."
        );

        flash('Se ha Creado una Nueva Experiencia Laboral.')->success();
        return redirect()->route('laboral.index');
    }

    public function show($id){
    }

    public function edit($id){
    	$depto_ciudades = $this->ciudadesxdepartamento();
    	$paises = $this->paises();
    	$item = Elaboral::find($id);
    	return view('hojavida.laborales.edit', compact('item', 'depto_ciudades', 'paises'));
    }

    public function update(Request $request, $id){
    	
        $experiencia_laboral = ELaboral::find($id);
        $old = ELaboral::find($id);

        $experiencia_laboral->empresa = $request->entidad;
        $experiencia_laboral->dedicacion = $request->dedicacion == 'otra' ? $request->explab_od : $request->dedicacion;
        $experiencia_laboral->tipo = $request->tipo;

        $experiencia_laboral->cargo = $request->cargo;
        $experiencia_laboral->dependencia = $request->dependencia;

        $experiencia_laboral->pais = $request->pais;
        $experiencia_laboral->departamento = $request->pais != 'Colombia' ? $request->inpdepartamento : $request->departamento;
        $experiencia_laboral->ciudad = $request->pais != 'Colombia' ? $request->inpciudad : $request->ciudad;
        $experiencia_laboral->direccion = $request->direccion;
        $experiencia_laboral->laborando = $request->actualmente;

        $experiencia_laboral->telefonos = $request->telefonos;
        $experiencia_laboral->fingreso = $request->fingreso;
        $experiencia_laboral->fterminacion = $request->fterminacion;

        $experiencia_laboral->funciones = $request->funciones;
        $experiencia_laboral->proyectos = $request->proyectos;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('laboral');
            $experiencia_laboral->certificado = $this->uploadFile($request->certificado, $ruta, $experiencia_laboral->certificado);
        }

        $experiencia_laboral->save();

        $this->activityUpdate(
            ['attribute' => $experiencia_laboral, 'old' => $old], 
            $this->model, $experiencia_laboral->id, 
            " ha actualizado una experiencia laboral."
        );

        flash('Se ha Edita la Experiencia Laboral.')->success();
        return redirect()->route('laboral.index');
    }

    public function destroy($id){
    	$item = Elaboral::find($id);
    	$this->deleteFile($item->certificado);
    	$item->delete();

        $this->activityDelete(
            $this->model, 
            $item->id,
            "La experiencia laboral  ha sido eliminado."
        );

    	flash('Se ha Borrado la experiencia Laboral con Exito.')->error();
        return redirect()->route('laboral.index');
    }
}
