<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EspectadorProyecto;

class EspectadorProyectoController extends Controller
{
    public function store(Request $request){
    	$item = EspectadorProyecto::where('user_id', $request->user)->where('proyecto_id', $request->proyecto)->get();
    	if(!$item->isEmpty()){
    		$update = $item->first();
    		$update->activo = $update->activo == '1' ? '0': '1';
    		$data = 'se actualizo';
    		$update->save();
    	}else{
    		$create = new EspectadorProyecto;
	        $create->proyecto_id = $request->proyecto;
	        $create->user_id = $request->user;
	        $create->activo = '1';
			$create->save();
    		$data = 'se creo';
    	}

    	return response()->json([
		    'action' => $data,
		    'state' => 'success'
		]);
    }
}
