<?php

namespace App\Http\Controllers\Teamwork;

use App\User;
use Illuminate\Http\Request;
use Mpociot\Teamwork\TeamInvite;
use Illuminate\Routing\Controller;
use App\Http\Resources\TeamResource;
use Mpociot\Teamwork\Facades\Teamwork;
use App\Notifications\TeamWorkEmailsInvite;

class TeamMemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the members of the given team.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teamModel = config('teamwork.team_model');
        $team = $teamModel::findOrFail($id);

        return [
            'team' => $team,
            'store' => route('teams.members.invite', $team),
            'invites' => $team->invites->map(function($item){ return $this->viewInvites($item);  }),
            'members' => $team->users->map(function($item) use($team){ return $this->viewUsers($item, $team);  }),
        ];
    }

    private function viewUsers($user, $team){
        $csrf_field = csrf_field();
        $result = "<tr><td>{$user->name}</td><td>";
                if(auth()->user()->isOwnerOfTeam($team)):
                    if(auth()->user()->getKey() !== $user->getKey()):
                        $url = route('teams.members.destroy', [$team->id, $user->id]);
                        $result .= "<form style='display: inline-block;' action='{$url}' method='post'>
                            {$csrf_field}
                            <input type='hidden' name='_method' value='DELETE' />
                            <button class='btn btn-danger btn-sm'><i class='fa fa-trash-o'></i></button>
                        </form>";
                    endif;
                endif;
        return $result .= "</td></tr>";
    }

    private function viewInvites($invite){
        return "<tr><td>{$invite->email}</td><td>
                    <a href='{route('teams.members.resend_invite', $invite)}' class='btn btn-sm btn-default'>
                        <i class='fa fa-envelope-o'></i> Re-enviar Invitación
                    </a>
                </td>
            </tr>";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $team_id
     * @param int $user_id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy($team_id, $user_id)
    {
        $teamModel = config('teamwork.team_model');
        $team = $teamModel::findOrFail($team_id);
        /*
        if (! auth()->user()->isOwnerOfTeam($team)) {
            abort(403);
        }
        */
        $userModel = config('teamwork.user_model');
        $user = $userModel::findOrFail($user_id);
        if ($user->getKey() === auth()->user()->getKey()) {
            abort(403);
        }

        $user->detachTeam($team);
        
        return new TeamResource($team);
    }

    /**
     * @param Request $request
     * @param int $team_id
     * @return $this
     */
    public function invite(Request $request, $team_id)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $teamModel = config('teamwork.team_model');
        $team = $teamModel::findOrFail($team_id);

        if (! Teamwork::hasPendingInvite($request->email, $team)) {
            Teamwork::inviteToTeam($request->email, $team, function ($invite) {
                
                $enviar = [
                    'email' => $invite->email, 
                    'teamName' => $invite->team->name, 
                    'teamId' => $invite->team,
                    'token' => $invite->accept_token
                ];
                // Send email to user
                $user = User::where('email', $invite->email)->first();
                $mensaje = $user->notify(new TeamWorkEmailsInvite($enviar));   
            });
        } else {
            return redirect()->back()->withErrors([
                'email' => 'La dirección de correo electrónico ya está invitada al equipo.',
            ]);
        }
        
        return new TeamResource($team);
    }

    /**
     * Resend an invitation mail.
     *
     * @param $invite_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resendInvite($invite_id)
    {
        $invite = TeamInvite::findOrFail($invite_id);
        $enviar = [
                    'email' => $invite->email, 
                    'teamName' => $invite->team->name, 
                    'teamId' => $invite->team,
                    'token' => $invite->accept_token
                ];

        $user = User::where('email', $invite->email)->first();
        $mensaje = $user->notify(new TeamWorkEmailsInvite($enviar)); 

        flash("se ha re-enviado la invitación")->success();
        return back();
    }

    public function userBelongsToTeam($user_id){
        $estado = false;
        foreach(auth()->user()->teams as $team):
            foreach($team->users->pluck('id') as $id):
                if($id == $user_id): 
                    $estado = true;
                endif; 
            endforeach; 
        endforeach;
        return response()->json($estado);
    }
}
