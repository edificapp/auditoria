<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use App\ClienteTercero;
use App\Contacto;
use Auth, Validator;
use App\Activity;


class ContactoController extends Controller
{
    public $model = 'App\Contacto';

    public function index(){
        $contactos = Contacto::where('activo', 1)->get();
        return view('directorio.index',compact('contactos'));
    }

    public function store(Request $request){
        $validacion =  Validator::make($request->all(), [
            'nombres' => ['required', 'string', 'max:255'],
            'apellidos' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'celular' => ['required', 'numeric', 'digits:10'],
            'cargo' => ['required']
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return back()->withErrors($validacion)->withInput();
        }

        $array_entidad = explode("-", $request->entidad);
        if($array_entidad[0] == 'cliente'){
            $entidad = ClienteTercero::find($array_entidad[1]);
        }else{
            $entidad = Proveedor::find($array_entidad[1]);
        }

        $new = $entidad->contactos()->create([
            'owner_id' => Auth::user()->id,
            'empresa_id' => Auth::user()->empresas_id,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'email' => $request->email,
            'celular' => $request->celular,
            'telefono' => $request->telefono,
            'ext' => $request->ext,
            'cargo' => $request->cargo,
        ]);

        $this->activityCreate(
            ['attribute' => $new], 
            $this->model, 
            $new->id, 
            "el Contacto ${new['nombres']} ${new['apellidos']} ha sido creado."
        );

        return back();
    }

    public function show($id){
        $data = Contacto::find($id);
        return response()->json($data);
    }

    public function update(Request $request,$id){
        $validacion =  Validator::make($request->all(), [
            'nombres' => ['required', 'string', 'max:255'],
            'apellidos' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'celular' => ['required', 'numeric', 'digits:10'],
            'cargo' => ['required']
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return back()->withErrors($validacion)->withInput();
        }

        $update = Contacto::findOrFail($id);
        $old = Contacto::findOrFail($id);
        $update->fill($request->all());
        if($update->isDirty()){
            $update->save();

            $this->activityUpdate(
                ['attribute' => $update, 'old' => $old], 
                $this->model, $update->id, 
                "el Contacto ${update['nombres']} ${update['apellidos']} ha sido actualizado."
            );
        }else{
            flash('No ahi ningun cambio para guardar.')->warning();
        }
       
        return back();

    }

    public function activo($id){
        $contacto = Contacto::find($id);
        $contacto->activo = !$contacto->activo;
        $contacto->save();

        $this->activityDelete(
            $this->model, 
            $contacto->id,
            "el Contacto ${contacto['nombres']} ${contacto['apellidos']} ha sido eliminado."
        );

        return back();
    }
}
