<?php

namespace App\Http\Controllers;

use App\Notifications\confirmarRegistro;
use App\Proveedor;
use App\Traits\StorageTraits;
use App\User;
use Auth,Validator,Crypt,Mail;
use Illuminate\Http\Request;


class ProveedorController extends Controller
{
    use StorageTraits;

    public function store(Request $request){
        if($request->tipo_persona == 'Natural'){
            $validacion = $this->store_proveedor_natural($request);
        }elseif($request->tipo_persona == 'Juridico'){
            $validacion = $this->store_proveedor_juridico($request);
        }

        if ($validacion) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('contactos.listar', 'proveedor'))->withErrors($validacion)->withInput();
        }

        return redirect()->route('contactos.listar', 'proveedor');
    }

     public function store_proveedor_juridico($request){
        $validacion =  Validator::make($request->all(), [
            'nombre_empresa' => ['required', 'string', 'max:255'],
            'numero_documento' => ['required', 'string', 'max:255']
        ]);

        if(!$validacion->fails()){
            $create = new Proveedor;
            $create->empresa_id = Auth::user()->empresas_id;
            $create->nombre_empresa = $request->nombre_empresa;
            $create->tipo_documento = $request->tipo_documento;
            $create->numero_documento = $request->numero_documento;
            $create->telefono_contacto = $request->telefono;
            $create->direccion = $request->direccion;
            $create->save();

            $ruta = $this->rutaEmpresa(Auth::user()->empresas_id).'/proveedor/'.$create->id;
            $create->certificado_bancarios = $request->hasFile('certificado_bancario') ? $this->uploadFile($request->certificado_bancario, $ruta) : '';
            $create->save();

            $this->activityCreate(
                ['attribute' => $create],
                'App\Proveedor',
                $create->id,
                "ha creado un proovedor."
            );

            return false;
        }

        return $validacion;
    }

    public function store_proveedor_natural($request){
        $validacion =  Validator::make($request->all(), [
            'nombres' => ['required', 'string', 'max:255'],
            'apellidos' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
        ]);

        if(!$validacion->fails()){
            $create = new Proveedor;
            $create->empresa_id = Auth::user()->empresas_id;
            $create->nombre_empresa = $request->nombres.' '.$request->apellidos;
            $create->tipo_documento = $request->tipo_documento;
            $create->numero_documento = $request->numero_documento;
            $create->telefono_contacto = $request->telefono;
            $create->direccion = $request->direccion;
            $create->save();

            $ruta = $this->rutaEmpresa(Auth::user()->empresas_id).'/proveedor/'.$create->id;
            $create->rut = $request->hasFile('rut') ? $this->uploadFile($request->rut, $ruta) : '';
            $create->certificado_bancarios = $request->hasFile('certificado_bancario') ? $this->uploadFile($request->certificado_bancario, $ruta) : '';
            $create->save();

            if($request->generar_usuario == 'on'){
                $usuario = new User();
                $usuario->name = $request->nombres.' '.$request->apellidos;
                $usuario->email = $request->email;
                $usuario->empresas_id = Auth::user()->empresas_id;
                $usuario->inv_activa = 'si';
                $usuario->save();

                $usuario->notify(new confirmarRegistro(route('registro',Crypt::encrypt($usuario->id))));
            }

            $this->activityCreate(
                ['attribute' => $create],
                'App\Proveedor',
                $create->id,
                "ha creado un proovedor."
            );

            return false;
        }


        return $validacion;
    }
}
