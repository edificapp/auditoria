<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\StorageTraits;
use App\DocumentoProyecto;

class DocumentoProyectoController extends Controller
{
    use StorageTraits;

    public function store(Request $request){
    	$ruta = $this->ruteProyecto($request->proyecto_id);
        $documento = new DocumentoProyecto;
        $documento->nombre = $request->nombre;
        $documento->description = $request->description;
        $documento->proyecto_id = $request->proyecto_id;
        $documento->tipo = $request->tipo;
        $documento->ruta = $this->uploadFile($request->file('documentos'), $ruta);
        $documento->save();
    	/*
    	foreach($request->file('documentos') as $doc){
            $ruta = $this->ruteProyecto($request->proyecto_id);
            $documento = new DocumentoProyecto;
            $documento->proyecto_id = $request->proyecto_id;
            $documento->tipo = $request->tipo;
            $documento->ruta = $this->uploadFile($doc, $ruta);
            $documento->save();
        }
		*/
        return back();
    }

    public function update(Request $request, $id){
    	$documento = DocumentoProyecto::find($id);
    	$documento->description = $request->description;
    	$documento->save();

    	return back();
    }
}
