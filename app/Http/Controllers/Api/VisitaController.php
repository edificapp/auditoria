<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use App\Visita;
use App\Proyecto;
use App\GFormulario;
use Illuminate\Http\Request;
use App\Traits\StorageTraits;
use App\Helpers\DataCollection;
use App\Http\Controllers\Controller;
use App\Http\Resources\proyecto\VisitaResource;
use App\Http\Resources\proyecto\VisitaFormularioResource;

class VisitaController extends Controller
{
    use StorageTraits;

    private $arrayItem = [];

    public function index(Proyecto $proyecto){
        return $proyecto->visitas->map(function($item){
            return new VisitaResource($item); 
        });
    }

    public function store(Proyecto $proyecto, Request $request){
       $formulario_g_formularios_ids = GFormulario::find($request->g_formulario_id)->formularios_pivot()->pluck('id');
       $visita =  $proyecto->visitas()->create($request->all() + ['creador_id' => auth()->id()]);
       $visita->formularios_g_formulario()->attach($formulario_g_formularios_ids);
       return new VisitaResource($visita);
    }

    public function update(Request $request, Visita $visita){
        $formulario_g_formularios_ids = GFormulario::find($request->g_formulario_id)->formularios_pivot()->pluck('id');
        $visita->formularios_g_formulario()->attach($formulario_g_formularios_ids);
        $visita =  $visita->update($request->all());
        return new VisitaResource($visita);
    }

    public function destroy(Visita $visita){
        if($visita->visita_formularios->count() > 0){
            $visita->visita_formularios()->delete();
        }
        
        $visita->delete();
        return response()->json(true);
    }

    public function storeFotos(Request $request, Visita $visita){
        $descriptions = json_decode($request->descriptions);
        foreach($request->photos as $key => $photo):
            $ruta = "visitas/{$visita->id}/adjuntar";
            $new = $visita->adjuntar()->create([
                'descripcion' => $descriptions[$key], 
                'ruta' => $this->uploadFile($photo, $ruta)
            ]);

            array_push($this->arrayItem, $new);
        endforeach;

        return response()->json($this->arrayItem);
    }

    public function getFotos(Visita $visita){
        return $visita->adjuntar->map(function($i){
            return [
                'id' => $i->id,
                'descripcion' => $i->descripcion, 
                'ruta' =>$i->url_ruta
            ];
        });
    }

    public function getVisitaFormularios(Visita $visita){
        return $visita->visita_formularios->map(function($e){
            return [
                'formulario_pivot_id' => $e->id, //es el id de la visita formulario
                'completo' => $e->completo,
                'nombre' => $e->formulario_g_formulario->formulario->nombre
            ];
        });
    }

    public function formato_muestra(Visita $visita){
        $g_formulario = $visita->visita_formularios[0]->formulario_g_formulario->grupo_formulario;
        $componentes = DataCollection::componentes_muestra()->where('tipo', $g_formulario->tipo_muestra)->first();
        return [
            'tipo' => $g_formulario->tipo_muestra,
            'rangos' => DataCollection::rango_edad(),
            'componentes' => $componentes['data'],
            'pesos_esperados' => is_null($g_formulario->peso_esperados) ? [] : $g_formulario->peso_esperados,
            'n_muestreos' => [$g_formulario->muestra_minima, $g_formulario->muestra_minima, 0],
            'obligatorio' => $g_formulario->muestra_obligatorio == 'Si' ? TRUE : FALSE
        ];
    }
}
