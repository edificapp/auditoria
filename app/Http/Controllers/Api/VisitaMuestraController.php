<?php

namespace App\Http\Controllers\Api;

use App\Visita;
use App\VisitaMuestra;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitaMuestraController extends Controller
{
    public function listar(Visita $visita){
        return $visita->muestras->map(function($e){
            return $this->show($e);
        });
    }

    public function store(Visita $visita, Request $request){
        $visita_muestra = $visita->muestras()->create($request->all());
        return $this->show($visita_muestra);
    }

    public function show(VisitaMuestra $visita_muestra){
        return $visita_muestra->toArray();
    }
}
