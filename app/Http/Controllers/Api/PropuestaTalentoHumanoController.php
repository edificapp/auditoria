<?php

namespace App\Http\Controllers\Api;

use App\Profesion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropuestaTalentoHumano;

class PropuestaTalentoHumanoController extends Controller
{
    public function competentes(PropuestaTalentoHumano $talento){
        $competentes = $talento->profesion->estudios;
        return $competentes->map(function($item){
            return [
                'user_id' => $item->curriculum->usuario_id,
                'nombre' => $item->curriculum->full_name
            ];
        });
    }

    public function update(Request $request, PropuestaTalentoHumano $talento){
        $talento->competente_id = $request->competente_id;
        $talento->proyectos = $request->proyectos;
        $talento->save();
        return back();
    }
}
