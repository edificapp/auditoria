<?php

namespace App\Http\Controllers\Api;

use App\Proyecto;
use App\TerceroProyecto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Tercero\TerceroProyectoResource;
use App\TerceroParametro;

class TerceroParametroController extends Controller
{

    public function store(Request $request, TerceroProyecto $tercero_proyecto){
        $new = $tercero_proyecto->parametros()->create($request->all());
        return response()->json($new);
    }

    public function update(Request $request, TerceroParametro $tercero_parametro){
        $tercero_parametro->update($request->all());
    }

    public function destroy(TerceroParametro $tercero_parametro){
        $tercero_parametro->delete();
    }
}
