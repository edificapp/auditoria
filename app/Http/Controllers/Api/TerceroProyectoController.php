<?php

namespace App\Http\Controllers\Api;

use App\Proyecto;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Tercero\TerceroProyectoResource;


class TerceroProyectoController extends Controller
{
    public function index(Proyecto $proyecto){
        return $proyecto->tercerosPivot->map(function($item){
            return new TerceroProyectoResource($item);
        });
    }

    public function agregarTerceroToProyecto(Request $request, Proyecto $proyecto){
        $new = $proyecto->tercerosPivot()->create([
            'cliente_tercero_id' => $request->cliente_tercero_id,
            'activo' => 1
        ]);
        return $proyecto->tercerosPivot->map(function($item){
            return new TerceroProyectoResource($item);
        });
    }
}
