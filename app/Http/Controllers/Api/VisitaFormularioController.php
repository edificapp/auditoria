<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use App\Visita;
use App\FormularioVisita;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\proyecto\VisitaFormularioResource;
use App\Traits\StorageTraits;
use Carbon\Carbon;

class VisitaFormularioController extends Controller
{
    use StorageTraits;
    private $arrayItem = [];

    public function dataInicial(FormularioVisita $formulario, Request $request){
        $formulario->ffhh_inicio = Carbon::now();
        $formulario->latitud_inicio = $request->latitud_inicio;
        $formulario->longitud_inicio = $request->longitud_inicio;
        $formulario->save();
        return response()->json($formulario);
    }

    public function dataFinal(FormularioVisita $formulario, Request $request){
        $formulario->ffhh_final = Carbon::now();
        $formulario->latitud_final = $request->latitud_final;
        $formulario->longitud_final = $request->longitud_final;
        $formulario->save();
        return response()->json($formulario);
    }

    public function storeFirmas(Request $request, Visita $visita){
        $ruta = "visitas/{$visita->id}/firmas/{$request->formulario_visita_id}";
        $new = $visita->firmas()->create($request->all()+['ruta' => '']);
        $new->update(['ruta' => $this->uploadFile($request->photo, $ruta)]);
        return response()->json($new); 
    }

    public function storeResData(Request $request, Visita $visita){
        foreach($request->respuestas as $res): 
            $new = $visita->respuestas()->create($res);
            array_push($this->arrayItem, $new);
        endforeach;

        return response()->json($this->arrayItem);
    }

    public function getFormulario(FormularioVisita $visita_formulario){
        return new VisitaFormularioResource($visita_formulario);
    }

  
}
