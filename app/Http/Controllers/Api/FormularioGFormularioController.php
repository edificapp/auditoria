<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\FormularioGFormulario;
use App\Http\Controllers\Controller;

class FormularioGFormularioController extends Controller
{
    public function respuestas(Request $request, FormularioGFormulario $formulario_g_formulario){
        foreach($request->name as $key => $name):
            $formulario_g_formulario->respuestas()->create([
                'name' => $name, 
                'respuesta' => $request['respuesta'][$key],
                'visita_id' => $request->visita_id
            ]);
        endforeach;
        return TRUE;
    }

    public function firmas(Request $request, FormularioGFormulario $formulario_g_formulario){
        $ruta = "visitas/{$request->visita_id}/firmas/{$formulario_g_formulario->id}";
        $formulario_g_formulario->firmas()->create([
            'visita_id' => $request->visita_id, 
            'cc' => $request->cc,
            'nombre' => $request->nombre,  
            'ruta' => $this->uploadFile($request->photo, $ruta)
        ]);
        return TRUE;
    }
}
