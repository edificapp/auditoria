<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\TipoProfesion;
use Illuminate\Http\Request;

class TipoProfesionController extends Controller
{
    public function show(TipoProfesion $tipo){
        return [
            'id' => $tipo->id,
            'nombre' => $tipo->nombre,
            'profesiones' => $tipo->profesiones->map(function($item){
                return [
                    'id' => $item->id,
                    'nombre' => $item->nombre,
                ];
            })
        ];
    }
}
