<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tercero\TerceroResource;

class TerceroController extends Controller
{
    public function index(){ 
        return  auth()->user()->empresa->terceros->map(function($item){
            return new TerceroResource($item);
        });
    } 
}
