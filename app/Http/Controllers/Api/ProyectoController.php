<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use App\Proyecto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\proyecto\VisitaResource;
use App\Http\Resources\proyecto\PersonalResource;

class ProyectoController extends Controller
{
    public function updateField(Request $request, Proyecto $proyecto, $field){
        $proyecto[$field] = $request->field;
        $proyecto->save();

        return [
            'proyecto' => $proyecto,
            'message' => "el proyecto {$proyecto->nombre} se ha actualizado el campo $field por {$proyecto[$field]} de manera exitosa."
        ];
    }

    public function personal(Proyecto $proyecto){
        $coordinadores = $proyecto->personal->filter(function($item){ return $item->cargo == 'Coordinador'; });
        $supervisores = $proyecto->personal->filter(function($item){ return $item->cargo == 'Supervisor'; });
        $inspectores = $proyecto->personal->filter(function($item){ return $item->cargo == 'Inspector'; });
        return [
            'coordinadores' => $coordinadores->map(function($item){ return new PersonalResource($item); }),
            'inspectores' => $inspectores->map(function($item){ return new PersonalResource($item); }),
            'supervisores' => $supervisores->map(function($item){ return new PersonalResource($item); }),
        ];
    }

    public function getProyectos(){
        $user = JWTAuth::parseToken()->authenticate();     
        $p_ids = $user->personal_proyectos->where('cargo', 'Inspector')->where('activo')->pluck('id'); 
        return $user->proyectos_ejecucion->map(function($i) use($p_ids){
            return  [
                'id' => $i->id,
                'nombre' => $i->nombre,
                'count_visitas' => $i->visitas->whereIn('personal_proyecto_id', $p_ids)->count()
            ];
        });
    }

    public function getVisitasProyecto(Proyecto $proyecto){
        $user = JWTAuth::parseToken()->authenticate();
        $p_ids = $user->personal_proyectos->where('cargo', 'Inspector')->where('activo')->pluck('id'); 
        return $proyecto->visitas->whereIn('personal_proyecto_id', $p_ids)->map(function($e){
           return new VisitaResource($e);
        });
    }
}
