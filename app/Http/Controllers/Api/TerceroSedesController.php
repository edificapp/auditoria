<?php

namespace App\Http\Controllers\Api;

use App\Ciudad;
use App\Proyecto;
use App\TerceroSede;
use App\TerceroProyecto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Tercero\TerceroSedesResource;
use App\Http\Resources\Tercero\TerceroProyectoResource;

class TerceroSedesController extends Controller
{

    public function list(TerceroProyecto $tercero_proyecto){
        return $tercero_proyecto->sedes->map(function($item){
            return new TerceroSedesResource($item);
        });
    }

    public function storeSedes(Request $request, TerceroProyecto $tercero_proyecto){
        $sedes = $request->sedes;
        array_pop($sedes);

        $validarCiudades = $this->validarCiudades($sedes);
        if(count($validarCiudades['vacios']) > 0): 
            return ['status' => FALSE, 'data' => $validarCiudades['vacios']];
        endif;
        
        foreach($sedes as $key => $sede):
            $inst = TerceroSede::where('nombre', $sede[5])->first();
            $tercero_proyecto->sedes()->create([
                'nombre' => $sede[0],
                'tipo' => 'sede',
                'ciudad_id' => $validarCiudades['encontrados'][$key],
                'barrio' => $sede[2],
                'direccion' => $sede[3],
                'telefono' => $sede[4],
                'instituto_id' => is_null($inst) ? NULL : $inst->id
            ]);
        endforeach;

        return ['status' => TRUE, 'data' => new TerceroProyectoResource($tercero_proyecto)];
    }

    public function storeInstitutos(Request $request, TerceroProyecto $tercero_proyecto){
        $sedes = $request->sedes;
        array_pop($sedes);

        $validarCiudades = $this->validarCiudades($sedes);
        if(count($validarCiudades['vacios']) > 0): 
            return ['status' => FALSE, 'data' => $validarCiudades['vacios']];
        endif;
        foreach($sedes as $key => $sede):
                $tercero_proyecto->sedes()->create([
                    'nombre' => $sede[0],
                    'tipo' => 'instituto',
                    'ciudad_id' => $validarCiudades['encontrados'][$key],
                    'barrio' => $sede[2],
                    'direccion' => $sede[3],
                    'telefono' => $sede[4],
                    'instituto_id' => NULL
                ]);
        endforeach;

        return ['status' => TRUE, 'data' => new TerceroProyectoResource($tercero_proyecto)];
    }

    public function validarCiudades($sedes){
        $encontrado = [];
        $vacio = [];
        foreach($sedes as $item):
            $ciudad = Ciudad::where('nombre', $item[1])->first();
            if(is_null($ciudad)):
                array_push($vacio, "<span> La ciudad {$item[1]} no existe</span></br>" );
            else: 
                array_push($encontrado, $ciudad->id);
            endif;
        endforeach;
        return ['vacios' => $vacio, 'encontrados' => $encontrado];
    }

    public function validarCiudadesUpdate($sedes){
        $encontrado = [];
        $vacio = [];
        foreach($sedes as $item):
            $ciudad = Ciudad::where('nombre', $item['ciudad'])->first();
            if(is_null($ciudad)):
                array_push($vacio, "<span> La ciudad {$item['ciudad']} no existe</span></br>" );
            else: 
                array_push($encontrado, $ciudad->id);
            endif;
        endforeach;
        return ['vacios' => $vacio, 'encontrados' => $encontrado];
    }


    public function show(TerceroSede $tercero_sede){
        return new TerceroSedesResource($tercero_sede);
    }

    public function updateSedes(Request $request, TerceroProyecto $tercero_proyecto){

        $validarCiudades = $this->validarCiudadesUpdate($request->sedes);
        if(count($validarCiudades['vacios']) > 0): 
            return ['status' => FALSE, 'data' => $validarCiudades['vacios']];
        endif;

        foreach($request->sedes as $key => $sede):
            $inst = TerceroSede::where('nombre', $sede['instituto'])->first();
            TerceroSede::find($sede['id'])->update([
                'nombre' => $sede['nombre'],
                'ciudad_id' => $validarCiudades['encontrados'][$key],
                'barrio' => $sede['barrio'],
                'direccion' => $sede['direccion'],
                'telefono' => $sede['telefono'],
                'instituto_id' => is_null($inst) ? NULL : $inst->id
            ]);
        endforeach;

        return ['status' => TRUE, 'data' => new TerceroProyectoResource($tercero_proyecto)];
    }

    public function updateInstituciones(Request $request, TerceroProyecto $tercero_proyecto){

        $validarCiudades = $this->validarCiudadesUpdate($request->sedes);
        if(count($validarCiudades['vacios']) > 0): 
            return ['status' => FALSE, 'data' => $validarCiudades['vacios']];
        endif;

        foreach($request->sedes as $key => $sede):
            TerceroSede::find($sede['id'])->update([
                'nombre' => $sede['nombre'],
                'ciudad_id' => $validarCiudades['encontrados'][$key],
                'barrio' => $sede['barrio'],
                'direccion' => $sede['direccion'],
                'telefono' => $sede['telefono'],
                'instituto_id' => NULL
            ]);
        endforeach;

        return ['status' => TRUE, 'data' => new TerceroProyectoResource($tercero_proyecto)];
    }
}
