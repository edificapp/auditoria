<?php

namespace App\Http\Controllers\Api;

use App\Proyecto;
use Illuminate\Http\Request;
use App\FormularioGFormulario;
use App\Http\Controllers\Controller;

class GFormularioController extends Controller
{
    public function index(Proyecto $proyecto){
        return response()->json($proyecto->grupo_formularios);
    }
}
