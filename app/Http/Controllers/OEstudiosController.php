<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\VerifyTraits;
use App\Traits\StorageTraits;
use App\Oestudios;
use Auth;

class OestudiosController extends Controller
{
    use VerifyTraits, StorageTraits;

    private $model = 'App\Oestudios';

    public function index(){
    	$curriculum = $this->verifyCurriculum();
    	return view('hojavida.oestudios.index', compact('curriculum'));
    }

    public function create(){
    	return view('hojavida.oestudios.create');
    }

    public function store(Request $request){
    	$oestudio = new Oestudios();
        $oestudio->curriculum_id = Auth::user()->curriculum->id;
        $oestudio->estudio = $request->estudio;
        $oestudio->escuela = $request->escuela;
        $oestudio->horas = $request->horas;
        $oestudio->ano = $request->ano;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('otros_estudios');
            $oestudio->certificado = $this->uploadFile($request->certificado, $ruta);
        }

        $oestudio->save();

        $this->activityCreate(
            ['attribute' => $oestudio],
            $this->model,
            $oestudio->id,
            "ha creado un otros estudios en la hoja de vida."
        );

        flash('Se ha Creado un Nuevo Otros Estudios.')->success();
        return redirect()->route('otros-estudios.index');
    }

    public function show($id){
    	return view('hojavida.estudios.create');
    }

    public function edit($id){
    	$estudio = Oestudios::find($id);
    	return view('hojavida.oestudios.edit', compact('estudio'));
    }

    public function update(Request $request, $id){
    	$oestudio = Oestudios::find($id);
        $old = Oestudios::find($id);
        $oestudio->curriculum_id = Auth::user()->curriculum->id;
        $oestudio->estudio = $request->estudio;
        $oestudio->escuela = $request->escuela;
        $oestudio->horas = $request->horas;
        $oestudio->ano = $request->ano;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('otros_estudios');
            $oestudio->certificado = $this->uploadFile($request->certificado, $ruta, $oestudio->certificado);
        }

        $oestudio->save();

        $this->activityUpdate(
            ['attribute' => $oestudio, 'old' => $old], 
            $this->model, $oestudio->id, 
            "ha actualizado uno de los otros estudios en la hoja de vida."
        );

        flash('Se ha Edita Otros Estudios con Exito.')->success();
        return redirect()->route('otros-estudios.index');
    }

    public function destroy($id){
    	$estudio = Oestudios::find($id);
    	$this->deleteFile($estudio->certificado);
    	$estudio->delete();

        $this->activityDelete(
            $this->model, 
            $estudio->id,
            "ha eliminado uno de los otros estudios de la hoja de vida."
        );

    	flash('Se ha Borrado Otros Estudios con Exito.')->error();
        return redirect()->route('otros-estudios.index');
    }
}
