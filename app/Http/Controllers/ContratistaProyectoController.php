<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContratistaProyecto;
use App\TerceroProyecto;

class ContratistaProyectoController extends Controller
{
    public function documentos($id){
        $directorio = ContratistaProyecto::find($id);
        return view('contratista.documentos', compact('directorio'));
    }

    public function documentosPublic($id, $tipo){
        if($tipo == 'contratista'){
            $directorio = ContratistaProyecto::find($id);            
        }else{
            $directorio = TerceroProyecto::find($id);            
        }
        return view('espectador.documentos', compact('directorio'));
    }

    public function store(Request $request){
    	$item = ContratistaProyecto::where('user_id', $request->user)->where('proyecto_id', $request->proyecto)->get();
    	if(!$item->isEmpty()){
    		$update = $item->first();
    		$update->activo = $update->activo == '1' ? '0': '1';
    		$update->save();
    		$data = 'se actualizo';
    	}else{
    		$create = new ContratistaProyecto;
	        $create->proyecto_id = $request->proyecto;
	        $create->user_id = $request->user;
	        $create->activo = '1';
			$create->save();
    		$data = 'se creo';
    	}

    	return response()->json([
		    'action' => $data,
		    'state' => 'success'
		]);
    }
}
