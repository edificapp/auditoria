<?php

namespace App\Http\Controllers;

use Auth;
use App\Estudios;
use App\TipoProfesion;
use App\Traits\VerifyTraits;
use Illuminate\Http\Request;
use App\Traits\StorageTraits;


class EstudiosController extends Controller
{
    use VerifyTraits, StorageTraits;
    private $model = 'App\Estudios';

    public function index(){
    	$curriculum = $this->verifyCurriculum();
    	return view('hojavida.estudios.index', compact('curriculum'));
    }

    public function create(){
        $tipoProfesiones = TipoProfesion::all();
    	return view('hojavida.estudios.create', compact('tipoProfesiones'));
    }

    public function store(Request $request){
        $ruta = $this->rutaUser('estudios_superiores');
        $estudio = auth()->user()->curriculum->estudios()->create(
            $request->all()
        );
        $estudio->update([
            'tarjeta' => isset($request->tarjeta) ? $request->tarjeta : NULL,
            'certificado' => $request->hasFile('certificado') ? $this->uploadFile($request->certificado, $ruta) : NULL
        ]);
        $this->activityCreate(
            ['attribute' => $estudio],
            $this->model,
            $estudio->id,
            "ha creado un nuevo estudio."
        );

        flash('Se ha Creado un Nuevo Estudio Superior.')->success();
        return redirect()->route('estudios.index');
    }

    public function show($id){
    	return view('hojavida.estudios.create');
    }

    public function edit($id){
        $tipoProfesiones = TipoProfesion::all();
    	$estudio = Estudios::find($id);
    	return view('hojavida.estudios.edit', compact('estudio', 'tipoProfesiones'));
    }

    public function update(Request $request, Estudios $estudio ){
        $old = $estudio;
        $ruta = $this->rutaUser('estudios_superiores');
        $estudio->update(
            $request->all()
        );
        $estudio->update([
            'tarjeta' => isset($request->tarjeta) ? $request->tarjeta : $estudio->tarjeta,
            'certificado' => $request->hasFile('certificado') ? $this->uploadFile($request->certificado, $ruta, $estudio->certificado) : $estudio->certificado
        ]);

        $this->activityUpdate(
            ['attribute' => $estudio, 'old' => $old], 
            $this->model, $estudio->id, 
            "el Usuario  ha actualizado un Estudio."
        );

        flash('Se ha Edita el Estudio Superior con Exito.')->success();
        return redirect()->route('estudios.index');
    }

    public function destroy($id){
    	$estudio = Estudios::find($id);
    	$this->deleteFile($estudio->certificado);
    	$estudio->delete();

        $this->activityDelete(
            $this->model,
            $estudio->id,
            "ha eliminado un estudio."
        );

    	flash('Se ha Borrado el Estudio Superior con Exito.')->error();
        return redirect()->route('estudios.index');
    }

}
