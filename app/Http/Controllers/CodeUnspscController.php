<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CodeUnspsc;

class CodeUnspscController extends Controller
{
    public function codes($code){
        $codes = CodeUnspsc::orWhere('code', 'like', '%' . $code . '%')->get();
        return response()->json($codes);
    }
}
