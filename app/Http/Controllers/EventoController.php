<?php

namespace App\Http\Controllers;

use App\Evento;
use App\EventoInvitado;
use App\User;
use App\Contacto;
use App\ClienteTercero;
use App\Proveedor;
use App\Jobs\EventsTaskJob;
use App\Jobs\EventsTaskContactoJob;
use Auth, Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Http\Requests\EventoRequest;

class EventoController extends Controller
{
    public $model = 'App\Evento';
    public function index(){
        $horas = $this->horas();
        $duracion = $this->duracion();
        return view('calendario.index', compact('horas', 'duracion'));
    }

    public function store(EventoRequest $request){
        $request->request->add(['owner_id' => Auth::user()->id]);
        $evento = Evento::create($request->all());

        $this->send_notifications($evento, $request->invitados_usuarios, $request->contactos);

        $evento->evento_invitados_user()->create([
            'user_id' => Auth::user()->id,
            'acepto' => 2
        ]);

        $this->activityCreate(
            ['attribute' => $evento], 
            $this->model, 
            $evento->id, 
            "el evento ${evento['title']} ha sido creado."
        );
        return back();
        //return $this->resource_data($evento);
    }

    public function update(EventoRequest $request, $id){
        $evento = evento::findOrFail($id);
        $old = evento::findOrFail($id);
        $evento->fill($request->all());
        $evento->save();
        //return ['attribute' => $evento, 'old' => $old];

        $this->activityUpdate(
            ['attribute' => $evento, 'old' => $old],
            $this->model, $evento->id,
            "el Evento ${evento['title']} ha sido actualizado."
        );
        $this->send_notifications($evento, $request->invitados_usuarios, $request->invitados_contactos);

        //return $this->resource_data($evento);
        return back();
    }

    public function send_notifications($new, $invitados_usuarios, $invitados_contactos){
        if($invitados_usuarios){
            //$new->invitados_user()->sync($invitados_usuarios);
            foreach ($invitados_usuarios as $value) {
                if(EventoInvitado::where('evento_id', $new->id)->where('user_id', $value)->get()->count() == 0){
                   // return 'aca';
                    $new->invitados_user()->attach($value);
                    EventsTaskJob::dispatchNow($value, route('eventos.notify.contacto', $new->id), Auth::user()->id);
                }
               // return 'aca';
            }
        }
        if($invitados_contactos){
           foreach ($invitados_contactos as $value) {
                if(!in_array($value, $new->contactos)){
                    EventsTaskContactoJob::dispatchNow($value, route('eventos.notify.contacto', $new->id));
                }
           }
        }
    }

    public function update_ajax(Request $request){
        $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
        $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
        $update = Evento::find($request->id);
        $old = Evento::find($request->id);
        $update->fecha = Carbon::parse($request->start)->format('Y-m-d');
        $update->hora = Carbon::parse($request->start)->format('H:i');
        $update->duracion = Carbon::parse($start)->diffInMinutes(Carbon::parse($end));
        $update->save();

        $this->activityUpdate(
            ['attribute' => $update, 'old' => $old], 
            $this->model, $update->id, 
            "el Evento ${update['title']} ha sido actualizado."
        );
/*
        $new->invitados()->sync($request->invitados);
        $new->invitados()->attach(Auth::user()->id);
*/
        return 'OK';
    }

    public function confirmacion($id,$resp){
        $item = EventoInvitado::where('evento_id', $id)->where('user_id', Auth::user()->id)->first();
        $item->acepto  = $resp;
        $item->save();
        $user_name = Auth::user()->name;
        $evento = $item->evento->title;

        $this->activityOtros(
            ['attribute' => $item],
            'App\EventoInvitado', 
            $item->id,
            "${user_name} ha aceptado asistir al evento ${evento}.",
            'confirmar evento'
        );

        return back();
    }

    public function destroy($id){
        $this->eliminar($id);
        return 'ok';
    }

    public function eliminar($id){
        $evento_delete = Evento::find($id);
        EventoInvitado::where('evento_id', $id)->delete();
        $evento_delete->delete();

        $this->activityDelete(
            $this->model,
            $evento_delete->id,
            "el Contacto ${evento_delete['title']} ha sido eliminado."
        );
    }

    public function show($id){
        $item = Evento::find($id);
        $array_invitados_user = collect();
        return [
            'id'=>$item->id,
            'title'=>$item->title,
            'fecha'=>$item->fecha,
            'hora'=>$item->hora,
            'duracion'=>$item->duracion,
            'lugar'=>$item->lugar,
            'start'=>$item->start,
            'end'=>$item->end,
            'color'=>$item->color,
            'textColor'=>'#FFFFFF',
            'description'=> $item->description,
            'contactos'=> $item->contactos,
            'invitados_user' => $item->invitados_user,
            'url_actualizar' => route('eventos.update', $item->id),
        ];
    }

    public function data(){
        return response()->json(Auth::user()->agenda_eventos->map(function($item){
            return $this->resource_data($item);
        }));
    }

    public function resource_data($item){
        return [
            'id'=>$item->id,
            'title'=>$item->title,
            'start'=>$item->start,
            'end'=>$item->end,
            'color'=>$item->color,
            'textColor'=>'#FFFFFF',
            'description'=> $item->description
        ];
    }

    public function eventos_users($id){
        $evento = Evento::find($id);
        $invitacion = EventoInvitado::where('evento_id', $id)->where('user_id', Auth::user()->id)->first();
        return view('calendario.ver_user', compact('evento', 'invitacion'));
    }

    public function eventos_entidades($id){
        $evento = Evento::find($id);
        return view('calendario.ver_contacto', compact('evento'));
    }

    public function horas(){
        $minutos = collect(['00', '15', '30', '45']);
        $result = collect();
        for ($i=6; $i < 20 ; $i++) {
            foreach ($minutos as $key => $minuto) {
                $hora = $i < 10 ? "0{$i}" : $i;
                $result->push("{$hora}:{$minuto}");
            }
        }
        return $result;
    }

    public function duracion(){
        $result = collect();
        for ($i=1; $i < 49; $i++) {
            $valor = $i*15;
            $horas = ($valor / 60) % 60;
            $minutos = fmod($valor, 60)  == 0 ? '00' : fmod($valor, 60);
            $data = collect(['value' => $valor, 'duracion' => $horas < 10 ? '0'.$horas.':'.$minutos : $horas.':'.$minutos]);
            $result->push($data);
        }

        return $result;
    }
}
