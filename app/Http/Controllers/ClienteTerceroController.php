<?php

namespace App\Http\Controllers;

use App\ClienteTercero;
use App\Proveedor;
use App\Proyecto;
use App\User;
use App\ContratistaProyecto;
use App\Traits\StorageTraits;
use Illuminate\Http\Request;
use Auth,Validator;
use App\Http\Requests\ClienteTerceroRequest;

class ClienteTerceroController extends Controller
{
    use StorageTraits;

    public function listar($tab_active){
        $proveedores = Proveedor::orderBy('id', 'desc')->get(); 
        $contactos = Auth::user()->empresa->contactos;
        $proyectos = Proyecto::where('estado', 'ejecucion')->orderBy('id', 'desc')->get(); 
        return view('entidades.index',compact('proveedores', 'contactos', 'tab_active', 'proyectos'));
    }

    public function store(ClienteTerceroRequest $request){
        //dd($request->all());
        $request->request->add([
            'empresa_id' => Auth::user()->empresas_id, 
            'telefono_contacto' => $request->numero_contacto
        ]);
        $create = ClienteTercero::create($request->all());

        $ruta = $this->rutaEmpresa(Auth::user()->empresas_id).'/cliente_tercero/'.$create->id;
        $create->certificado_bancario = $request->hasFile('certificado_bancario') ? $this->uploadFile($request->certificado_bancario, $ruta) : '';
        $create->save();


        if($request->proyectos){
            foreach ($request->proyectos as $value) {
                $proyecto = Proyecto::find($value);
                $proyecto->tercerosPivot()->create([
                    'cliente_tercero_id' => $create->id,
                    'activo' => 1
                ]);
            }
        }
        return redirect()->route('contactos.listar', $create->tipo_contacto);
    }

    public function update(ClienteTerceroRequest $request, $id){

        $update = ClienteTercero::find($id);
        $update->nombre = $request->nombre;
        $update->tipo_documento = $request->tipo_documento;
        $update->numero_documento = $request->numero_documento;
        $update->direccion = $request->direccion;
        $update->telefono_contacto = $request->numero_contacto;
        $update->tipo_contacto = $request->tipo_contacto;
        $update->email = $request->email;

         $ruta = $this->rutaEmpresa(Auth::user()->empresas_id).'/cliente_tercero/'.$update->id;
        $update->certificado_bancario = $request->hasFile('certificado_bancario') ? $this->uploadFile($request->certificado_bancario, $ruta) : '';
        $update->save();

        return redirect()->route('contactos.listar', $update->tipo_contacto);
    }

    public function show($id){
        return ClienteTercero::find($id);
    }
}
