<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\TipoProfesionImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function tipos($tipo){
    	return view('import', compact('tipo'));
    }

    public function tiposImport(Request $request, $tipo){
    	$request->validate([
	        'file' => 'required|max:100000|mimes:xlsx,xls',
	    ]);
    	$file = $request->file('file');
    	try {
    		if($tipo == 'tipo'){
    			Excel::import(new TipoProfesionImport(),$file);
	    	}
		} catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
		     $fallas = $e->failures();

		     foreach ($fallas as $falla) {
		         $falla->row(); // fila en la que ocurrió el error
		         $falla->attribute(); // el número de columna o la "llave" de la columna
		         $falla->errors(); // Errores de las validaciones de laravel
		         $falla->values(); // Valores de la fila en la que ocurrió el error.
		     }
		}

		return "termino";
    }
}
