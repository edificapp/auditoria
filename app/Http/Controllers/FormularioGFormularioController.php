<?php

namespace App\Http\Controllers;

use App\GFormulario;
use Illuminate\Http\Request;
use App\FormularioGFormulario;

class FormularioGFormularioController extends Controller
{
    public function store(Request $request, GFormulario $grupo_formularios){
        if($grupo_formularios->visita->count() > 0): 
            return ['status'=> false, 'message' => 'no se puede modificar este grupo de formularios, esta relacionado a una visita'];
        endif;

        $new = $grupo_formularios->formularios()->create($request->all());
        return ['status'=> true, 'data' => $new];
    }

    public function destroy(FormularioGFormulario $formulario_grupo_formulario){

        if($formulario_grupo_formulario->grupo_formulario->visita->count() > 0): 
            return ['status'=> false, 'message' => 'no se puede modificar este grupo de formularios, esta relacionado a una visita'];
        endif;

        $formulario_grupo_formulario->delete();
        return ['status'=> true, 'message' => 'se ha eliminado'];
    }
}
