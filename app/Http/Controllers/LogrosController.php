<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\VerifyTraits;
use App\Traits\StorageTraits;
use App\Logros;
use Auth;

class LogrosController extends Controller
{
    use VerifyTraits, StorageTraits;

    private $model = 'App\Logros';

    public function index(){
    	$curriculum = $this->verifyCurriculum();
    	return view('hojavida.logros.index', compact('curriculum'));
    }

    public function create(){
    	return view('hojavida.logros.create');
    }

    public function store(Request $request){
    	$logro = new Logros();
        $logro->curriculum_id = Auth::user()->curriculum->id;
        $logro->logro = $request->logro;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('logros');
            $logro->certificado = $this->uploadFile($request->certificado, $ruta);
        }

        $logro->save();

        $this->activityCreate(
            ['attribute' => $logro],
            $this->model,
            $logro->id,
            "ha relacionado un nuevo Logro a la hoja de vida."
        );

        flash('Se ha Creado un Nuevo Logro.')->success();
        return redirect()->route('logros.index');
    }

    public function show($id){
    }

    public function edit($id){
    	$logro = Logros::find($id);
    	return view('hojavida.logros.edit', compact('logro'));
    }

    public function update(Request $request, $id){
    	$logro = Logros::find($id);
        $old= Logros::find($id);
        $logro->logro = $request->logro;

        if($request->hasFile('certificado')){
        	$ruta = $this->rutaUser('logros');
            $logro->certificado = $this->uploadFile($request->certificado, $ruta, $logro->certificado);
        }

        $logro->save();

         $this->activityUpdate(
            ['attribute' => $logro, 'old' => $old], 
            $this->model, $logro->id, 
            "ha actualizado en logro en la hoja de vida."
        );

        flash('Se ha Edita el logro con Exito.')->success();
        return redirect()->route('logros.index');
    }

    public function destroy($id){
    	$logro = Logros::find($id);
    	$this->deleteFile($logro->certificado);
    	$logro->delete();

         $this->activityDelete(
            $this->model,
            $logro->id,
            "ha eliminado un logro de la hoja de vida."
        );

    	flash('Se ha Borrado el Logro con Exito.')->error();
        return redirect()->route('logros.index');
    }
}

