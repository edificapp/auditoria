<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReunionProyecto;
use App\ReunionProyectoDocumento;
use Auth;
use App\Traits\StorageTraits;

class ReunionProyectoController extends Controller
{
	use StorageTraits;

    public function store(Request $request){
    	$reunion = new ReunionProyecto;
    	$reunion->motivo = $request->motivo;
    	$reunion->fecha = $request->fecha;
    	$reunion->lugar = $request->lugar;
    	$reunion->proyecto_id = $request->proyecto_id;
    	$reunion->owner_id = Auth::user()->id;
    	$reunion->save();
    	foreach($request->file('documentos') as $doc){
            $ruta = $this->rutaReunion($request->proyecto_id, $reunion->id);
            $documento = new ReunionProyectoDocumento;
            $documento->reunion_proyecto_id = $reunion->id;
            $documento->ruta = $this->uploadFile($doc, $ruta);
            $documento->save();
        }

        $this->activityCreate(
            ['attribute' => $reunion],
            "App\Reunionproyecto",
            $reunion->id,
            "ha creado una reunion al proyecto {$reunion->proyecto->nombre}."
        );

        return back();
    }

    public function show($id){
    	$reunion = ReunionProyecto::find($id);
    	return [
            'id' => $reunion->id,
            'motivo' => $reunion->motivo,
            'fecha' => $reunion->fecha,
            'lugar' => $reunion->lugar,
            'documentos' => $reunion->documentos->map(function($item){
            	return [
            		'name' => $item->name,
            		'url_public' => $item->url_public,
            		'size' => $item->size
            	];
            })
        ];
    }

    public function destroy($id){
    	$reunion = ReunionProyecto::find($id);
    	$reunion->documentos->each(function($item){
    		ReunionProyectoDocumento::find($item->id)->delete();
    	});
    	$reunion->delete();

        $this->activityDelete(
            "App\Reunionproyecto",
            $reunion->id,
            "ha eliminado la reunion {$reunion->nombre} del proyecto {$reunion->proyecto->nombre}."
        );

    	return back();
    }
}
