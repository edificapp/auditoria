<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciudad;

class CiudadController extends Controller
{
    public function departamento_ciudades($id){
    	return Ciudad::where('departamento_id', $id)->get()
    	;
    }
}
