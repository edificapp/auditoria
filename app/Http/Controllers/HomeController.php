<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private $model = 'App\User';
    
    public function index()
    {
        if(Auth::guest()){
            return redirect()->route('login');
        }else{
            if(is_null(auth()->user()->empresas_id)):
                return redirect()->route('usuarios.index');
            else: 
                return view('inicio.index');
            endif;
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function salir()
    {
        $this->activityOtros(
            [],
            $this->model, Auth::user()->id, 
            "el usuario ha cerrado sesion.",
            'logout'
        );
        Auth::logout();
        return redirect(route('home'));
    }
/*
    public function index()
    {
        return view('home');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

}
