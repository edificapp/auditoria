<?php

namespace App\Http\Controllers;

use App\Profesion;
use App\Propuesta;
use App\CodeUnspsc;
use App\TipoProfesion;
use App\ContratoUnspscs;
use App\PropuestaUnspscs;
use App\ContratosPropuesta;
use Illuminate\Http\Request;
use App\ExperienciaPropuesta;
use App\PropuestaTalentoHumano;
use Illuminate\Support\Facades\Auth;

class PropuestasController extends Controller
{
    private $model = "App\Propuesta";
    public function index()
    {
        //dd(Auth::user()->empresa->contactos);
        $propuestas = Propuesta::all();
        $terceros = Auth::user()->empresa->clientes_terceros->filter(function ($item){return $item->tipo_contacto == 'cliente'; });
        $contactos = Auth::user()->empresa->contactos;
        return view('propuesta.index', compact('propuestas', 'terceros', 'contactos'));
    }
    public function store(Request $request)
    {   
        // dd($request->all());

        $propuestas = new Propuesta;
        $propuestas->nombre = $request->nombre;
        $propuestas->numero_proceso = $request->numero_proceso;
        $propuestas->entidad_contratante = $request->entidad_contratante;
        $propuestas->fecha_entrega = $request->fecha_entrega;
        $propuestas->lugar_entrega = $request->lugar_entrega;
        $propuestas->hora_entrega = $request->hora_entrega;
        $propuestas->contacto = $request->contacto;
        $propuestas->creador_id = Auth::user()->id;
        $propuestas->estado = 'borrador';
        $propuestas->save();

        $this->activityCreate(
            ['attribute' => $propuestas], 
            $this->model, 
            $propuestas->id, 
            "ha creado una propuesta."
        );

        flash('Se ha creado una propuesta correctamente.')->success();
        return redirect(route('propuestas.index'));
    }

    public function updateExperiencia(Request $request)
    {   
        
        $experiencia = ExperienciaPropuesta::find($request->experiencia_id);
        $old = ExperienciaPropuesta::find($request->experiencia_id);
        $experiencia->contrato_cantidad = $request->contratos_cantidad;
        $experiencia->tiempo_anos = $request->años;
        $experiencia->tiempo_meses = $request->meses;
        $experiencia->monto_smlv = $request->monto;
        $experiencia->propuesta_id = $request->propuesta_id;
        $experiencia->contractual_id = Auth::user()->id;
        $experiencia->save();

        $this->activityUpdate(
            ['attribute' => $experiencia, 'old' => $old], 
            "App\ExperienciaPropuesta", $experiencia->id, 
            "ha actualizado una experiencia de la propuesta {$experiencia->propuesta->nombre}."
        );

        flash('Se ha actualizado la experiencia correctamente.')->success();
        return back();
    }
    public function storeExperiencia(Request $request)
    {   
        $propuestas = new ExperienciaPropuesta;
        $propuestas->contrato_cantidad = $request->contratos_cantidad;
        $propuestas->tiempo_anos = $request->años;
        $propuestas->tiempo_meses = $request->meses;
        $propuestas->monto_smlv = $request->monto;
        $propuestas->propuesta_id = $request->propuesta_id;
        $propuestas->contractual_id = Auth::user()->id;
        $propuestas->save();

        $this->activityCreate(
            ['attribute' => $propuestas], 
            "App\ExperienciaPropuesta", 
            $propuestas->id, 
            "ha creado una experiencia a la propuesta {$propuestas->propuesta->nombre}."
        );

        flash('Se ha creado una experiencia correctamente.')->success();
        return back();
    }
    public function storeTalento(Request $request)
    {   
        // dd($request->all());
        $propuestas = new PropuestaTalentoHumano;
        $propuestas->cargo = $request->cargo;
        $propuestas->propuesta_id = $request->propuesta_id;
        $propuestas->tipo_id = $request->tipo;
        $propuestas->profesion_id = $request->profesion;
        $propuestas->dedicacion = $request->dedicacion;
        $propuestas->experiencia_especifica = $request->experiencia_especifica;
        $propuestas->cantidad_contratos = $request->cantidad_contratos;
        $propuestas->experiencia_general_años = $request->experiencia_años;
        $propuestas->cantidad = $request->cantidad;
        $propuestas->salud = $request->salud;
        $propuestas->save();

        $this->activityCreate(
            ['attribute' => $propuestas], 
            "App\PropuestaTalentoHumano", 
            $propuestas->id, 
            "ha relacionado un Talento humano a la propuesta {$propuestas->propuesta->nombre}."
        );

        flash('Se ha creado tanlento correctamente.')->success();
        return back();
    }


    public function getBorrador()
    {
        $propuestas = Propuesta::where('estado', 'borrador')->get();
        return view('propuesta.borrador',compact('propuestas'));
    }

    public function getPreparacion()
    {
        $propuestas = Propuesta::where('estado', 'preparacion')->get();
        return view('propuesta.preparacion',compact('propuestas'));
    }

    public function editPropuestaBorrador($id)
    {   
        $propuesta = Propuesta::findOrFail($id);
        $tipo_profesion = TipoProfesion::all();
        $profesiones = Profesion::all();
        return view('propuesta.borrador-edit',compact('propuesta','tipo_profesion','profesiones'));
    }
    public function editPropuestaPreparacion($id)
    {   
        $propuesta = Propuesta::findOrFail($id);
        $propuesta->estado = 'preparacion';
        $propuesta->save();

        //dd($propuesta->talento_propuesta);

        $tipo_profesion = TipoProfesion::all();
        $profesiones = Profesion::all();
        $cantidad_status = false;
        $tiempo_status = false;
        $monto_status = false;
        $code_status = false;

        foreach ($propuesta->experiencia as $experiencia){
            $borrador_cantidad = $experiencia->contrato_cantidad;
            $borrador_anos = $experiencia->tiempo_anos;
            $borrador_meses = $experiencia->tiempo_meses;
            $borrador_monto = $experiencia->tiempo_meses;
        }       
        foreach ($propuesta->code_uns as $code){
            $borrador_code = $code->code_uns->code;
        }

        $anos = 0;
        $meses = 0;
        $monto = 0;
        $code_contratos=[];
        foreach($propuesta->contratos as $documento){
            $anos = $anos + $documento->tiempo_anos; 
            $meses = $meses + $documento->monto_smlv; 
            $monto = $monto + $documento->monto;
            foreach($documento->code_uns as $code){
                array_push($code_contratos ,$code->code_uns->code);
            }
        }
        if(count($propuesta->contratos) >= $borrador_cantidad){
            $cantidad_status = true;
        }
        if($anos >= $borrador_anos){
            $tiempo_status = true;
        }
        if($monto >= $borrador_monto){
            $monto_status = true;
        }

        if(in_array($borrador_code, $code_contratos)){
            $code_status = true;
        }
        return view('propuesta.preparacion-edit',compact('propuesta','tipo_profesion','profesiones','cantidad_status', 'tiempo_status','monto_status','code_status'));
    }

    public function searchUnspsc($search)
    {   
        $codes = CodeUnspsc::where('code','like',"%$search%")->limit(5)->get();
        return response()->json($codes);
    }

    public function storeCode(Request $request)
    {   
        $propuesta_code = PropuestaUnspscs::where('propuesta_id', $request->propuesta)->first();

        if($propuesta_code){
            $propuesta_code->delete();
        }

        $code = CodeUnspsc::where('code', $request->foo)->first();
        $code_propuesta = new PropuestaUnspscs; 
        $code_propuesta->propuesta_id  = $request->propuesta;
        $code_propuesta->unspscs_id = $code->id;
        $code_propuesta->save();
       
        return back();
    }

    public function storePropuestaPreparacionContrato(Request $request){

        $codes = explode(',', $request->foo);
      
        $contrato_propuesta = new ContratosPropuesta;
        $contrato_propuesta->name = $request->name;
        $contrato_propuesta->tiempo_anos = $request->time_anos;
        $contrato_propuesta->tiempo_meses = $request->time_meses;
        $contrato_propuesta->monto = $request->monto;
        $contrato_propuesta->propuesta_id = $request->propuesta_id;
        $contrato_propuesta->save();

        foreach($codes as $code){
            // dd($code);
            $contrato_code = new ContratoUnspscs;
            $contrato_code->contrato_propuesta_id = $contrato_propuesta->id;
            $contrato_code->code_id = $code; 
            $contrato_code->save();
        }

        $this->activityCreate(
            ['attribute' => $contrato_propuesta], 
            "App\ContratosPropuesta", 
            $contrato_propuesta->id, 
            "ha relacionado un contrato a la propuesta {$propuestas->propuesta->nombre}."
        );
        
        return back();
    }

    
}
