<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrmController extends Controller
{
    public function index(){
        return view('crm.index');
    }

    public function getServicioCliente(){
        return view('crm.servicios.cliente');
    }

    public function getServicioDevolucion(){
        return view('crm.servicios.devoluciones');
    }

    public function getServicioTecnico(){
        return view('crm.servicios.servicio_tecnico');
    }

    public function ventas(){
        return view('crm.ventas.index');
    }

    public function getVentasGestion(){
        return view('crm.ventas.gestion');
    }

    public function getVentasCotizacion(){
        return view('crm.ventas.cotizacion');
    }

    public function getVentas(){
        return view('crm.ventas.ventas');
    }

    public function getVentasMes(){
        return view('crm.ventas.ventasMes');
    }

    public function getVentasAno(){
        return view('crm.ventas.ventasAno');
    }

    public function getVentasProyecto(){
        return view('crm.ventas.ventasProyectos');
    }
    
}
