<?php

namespace App\Http\Controllers;

use Auth;

use App\Rol;
use App\User;
use App\Visita;
use App\Proyecto;
use Carbon\Carbon;
use App\Formulario;
use App\Departamento;
use App\ClienteTercero;
use App\formulariosVisita;
use App\EspectadorProyecto;
use App\supervisorProyecto;
use App\ContratistaProyecto;
use Illuminate\Http\Request;
use App\Http\Resources\Tercero\TerceroProyectoResource;

class ProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = ClienteTercero::where('tipo_contacto', 'cliente')->get();
        $proyectos = Proyecto::where('estado', 'adjudicado')->get();
        return view('proyecto.index',compact('proyectos', 'contactos'));
    }

    public function ejecucion()
    {
        $proyectos = Proyecto::where('estado', 'ejecucion')->get();
        return view('proyecto.ejecucion.index',compact('proyectos'));
    }

    public function cerrados()
    {
        $proyectos = Proyecto::where('estado', 'cerrado')->get();
        return view('proyecto.cerrados',compact('proyectos'));
    }

    public function update_estado($estado, $proyecto_id){
        $proyecto = Proyecto::find($proyecto_id);
        $proyecto->estado = $estado;
        $proyecto->save();
        return redirect()->route('proyectos.ejecucion');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyecto = new Proyecto();
        $proyecto->nombre = $request->nombre;
        $proyecto->entidad_contratante = $request->entidad_contratante;
        $proyecto->n_proceso = $request->n_proceso;
        $proyecto->contacto_id = $request->contacto_id;
        $proyecto->creador_id = Auth::user()->id;
        $proyecto->save();

        $this->activityCreate(
            ['attribute' => $proyecto],
            'App\Proyecto',
            $proyecto->id,
            "ha creado un Proyecto."
        );
        
        flash('Se ha creado el proyecto correctamente.')->success();
        return redirect(route('proyectos.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $supervisores = supervisorProyecto::where('proyecto_id',$proyecto->id)->pluck('supervisor_id');
        return view('proyecto.edit',compact('proyecto','supervisores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $old = Proyecto::findOrFail($id);
        $proyecto->nombre = $request->nombre;
        $proyecto->activo = $request->estado;
        
        $proyecto->save();

        supervisorProyecto::where('proyecto_id',$proyecto->id)->delete();
        foreach ($request->supervisores as $clave => $supervisor_id) {
            $supervisor_v = new supervisorProyecto();
            $supervisor_v->proyecto_id = $proyecto->id;
            $supervisor_v->supervisor_id = $supervisor_id;
            $supervisor_v->save();
        }

        $this->activityUpdate(
            ['attribute' => $proyecto, 'old' => $old], 
            "App\Proyecto", $proyecto->id, 
            "ha actualizado el proyecto {$proyecto->nombre}."
        );

        flash('Se ha actualizado el proyecto correctamente.')->success();
        return redirect(route('proyectos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* ************************************ ***************************** ********************************* */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /*submenu proyectos ejecucion*/
    public function show($id) /* visitas.index */
    {
        $proyecto = Proyecto::findOrFail($id);
        return view('proyecto.ejecucion.documentos',compact('proyecto'));
    }

    public function sub_menu($proyecto_id, $vista){
        $proyecto = Proyecto::findOrFail($proyecto_id);
        $formularios = Formulario::all();
        $departamentos = Departamento::all();
        $terceros_proyecto = $proyecto->tercerosPivot->map(function($item){
            return new TerceroProyectoResource($item);
        });
        return view('proyecto.ejecucion.'.$vista ,compact('proyecto', 'formularios', 'departamentos', 'terceros_proyecto'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function visitas_crear($id_proyecto) /* visitas.create */
    {
        $formularios = Formulario::get();
        $proyecto = Proyecto::findOrFail($id_proyecto);
        return view('proyecto.visitas.create',compact('proyecto','formularios'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function visitas_crear_store(Request $request, $id_proyecto) /* visitas.create.store */
    {
        $_fecha = $request->fecha_visita . ' ' . $request->hora_visita;        
        $fecha = Carbon::parse($_fecha)->format('Y-m-d H:i:s');

        $visita = new Visita();
        $visita->proyecto_id = $id_proyecto;
        $visita->auditor_id = $request->auditor_id;
        $visita->creador_id = Auth::user()->id;
        $visita->nombre = $request->nombre;
        $visita->activo = $request->estado;
        $visita->fecha_visita = $fecha;

        $visita->save();

        foreach ($request->formularios as $formulario_id) {
            $formulariov = new formulariosVisita();
            $formulariov->visita_id = $visita->id;
            $formulariov->formulario_id = $formulario_id;
            $formulariov->save();
        }

        $this->activityCreate(
            ['attribute' => $visita],
            "App\Visita",
            $visita->id,
            "ha creado una visita al proyecto {$visita->proyecto->nombre}."
        );

        flash('Se ha creado la visita correctamente.')->success();
        return redirect(route('proyectos.show',$id_proyecto));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function visitas_editar($id_proyecto,$id_visita) /* visitas.edit */
    {
        $_usuarios = User::get();
        $usuarios = collect([]);

        foreach ($_usuarios as $usuario) {
            if ($usuario->empresas_id == Auth::user()->empresas_id && $usuario->rol->nombre == 'Usuario' && $usuario->activo == 'si') {
                $usuarios->push($usuario);
            }
        }

        $formularios_el = formulariosVisita::where('visita_id',$id_visita)->pluck('formulario_id');

        $formularios = Formulario::get();

        $proyecto = Proyecto::findOrFail($id_proyecto);
        $visita = Visita::findOrFail($id_visita);

        return view('proyecto.visitas.edit',compact('proyecto','visita','formularios','usuarios','formularios_el'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function visitas_editar_update(Request $request, $id_proyecto, $id_visita) /* visitas.editar.update */
    {
        $_fecha = $request->fecha_visita . ' ' . $request->hora_visita;

        $fecha = Carbon::createFromFormat('m/d/Y H:i:s',$_fecha)->format('Y-m-d H:i:s');

        $visita = Visita::findOrFail($id_visita);
        $old = Visita::findOrFail($id_visita);
        $visita->auditor_id = $request->auditor_id;
        $visita->nombre = $request->nombre;
        $visita->activo = $request->estado;
        $visita->fecha_visita = $fecha;

        $visita->save();

        formulariosVisita::where('visita_id',$visita->id)->delete();
        foreach ($request->formularios as $formulario_id) {
            $formulariov = new formulariosVisita();
            $formulariov->visita_id = $visita->id;
            $formulariov->formulario_id = $formulario_id;
            $formulariov->save();
        }

        $this->activityUpdate(
            ['attribute' => $visita, 'old' => $old], 
            "App\Visita", $visita->id, 
            "ha editado la visita {$visita->nombre} del proyecto {$visita->proyecto->nombre}."
        );

        flash('Se ha actualizado la visita correctamente.')->success();
        return redirect(route('proyectos.show',$id_proyecto));
    }

    public function contratistas($id){
        $proyecto = Proyecto::find($id);
        return view('espectador.contratistas', compact('proyecto'));
    }


    public function terceros(Proyecto $proyecto){
        $terceros = auth()->user()->empresa->terceros; 
        return [
            'checkedTerceros' => $proyecto->terceros->pluck('id'),
            'terceros' => $terceros->map(function($item){
                                return [
                                    'id' => $item->id,
                                    'name' => $item->nombre
                                ];
                            })
        ];
    }

    public function relacionarTerceros(Proyecto $proyecto, Request $request){
        $newArray = collect($request->terceros);
        $add = $newArray->diff($proyecto->terceros->pluck('id'));
        if(count($add) > 0):
            foreach($add as $item):
                $proyecto->terceros()->attach($newArray);
            endforeach;
        endif;
        $updateCollect = $proyecto->terceros->pluck('id')->diff($newArray);
        if(count($updateCollect) > 0):
            foreach($updateCollect as $item):
                $update = ClienteTercero::find($item);
                $update->activo = false;
                $update->save();
            endforeach;
        endif;
    }

}
