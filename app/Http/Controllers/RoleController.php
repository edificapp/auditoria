<?php

namespace App\Http\Controllers;

use App\Role;
use App\Traits\CollectionsTraits;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;


class RoleController extends Controller
{
    use CollectionsTraits;

    public function index(Request $request)
    {
        return view('roles.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modulos = $this->modulos();
        $permission = Permission::get();
        return view('roles.create', compact('permission', 'modulos'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'rol' => 'required',
            'permission' => 'required',
        ]);

        $new = new Role;
        $new->name = $request->rol;
        $new->guard_name = 'web';
        $new->empresa_id = auth()->user()->empresa->id;
        $new->save();

        $new->syncPermissions($request->permission);

        return redirect()->route('roles.index')
                        ->with('success','Rol creado satisfactoriamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();

        return view('roles.show',compact('role','rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modulos = $this->modulos();
        $permission = Permission::get();
        $rol = Role::find($id);
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
            
        return view('roles.edit',compact('rol','rolePermissions', 'modulos', 'permission'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'rol' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->update(['name' => $request->input('rol')]);
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')
                        ->with('success','Rol actualizado satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
                        ->with('error','Rol Borrado Satisfactoriamente');
    }
}
