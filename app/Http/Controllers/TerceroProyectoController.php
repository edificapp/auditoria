<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TerceroProyecto;

class TerceroProyectoController extends Controller
{
	/*
    public function documentos($id){
        $directorio = ContratistaProyecto::find($id);
        return view('contratista.documentos', compact('directorio'));
    }

    public function documentosPublic($id){
        $directorio = ContratistaProyecto::find($id);
        return view('espectador.documentos', compact('directorio'));
    }
    */

    public function store(Request $request){
    	$item = TerceroProyecto::where('tercero_id', $request->user)->where('proyecto_id', $request->proyecto)->get();
    	if(!$item->isEmpty()){
    		$update = $item->first();
    		$update->activo = $update->activo == '1' ? '0': '1';
    		$update->save();
    		$data = 'se actualizo';
    	}else{
    		$create = new TerceroProyecto;
	        $create->proyecto_id = $request->proyecto;
	        $create->tercero_id = $request->user;
	        $create->activo = '1';
			$create->save();
    		$data = 'se creo';
    	}

    	return response()->json([
		    'action' => $data,
		    'state' => 'success'
		]);
    }
}
