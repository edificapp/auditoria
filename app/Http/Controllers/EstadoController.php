<?php

namespace App\Http\Controllers;

use App\Estado;
use App\Events\EstadoCreado;
use App\Http\Resources\EstadoResource;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    private $status;

    public function index(){
        $this->status = collect();
        if(is_null(auth()->user()->currentTeam)): 
            foreach(auth()->user()->teams as $team): 
                $this->getEstados($team->users);
            endforeach;
        else: 
            $this->getEstados(auth()->user()->currentTeam->users);
        endif;
        $estados = $this->status->sortByDesc('id');
        return $estados->values()->all();
    }

    private function getEstados($users){
        foreach($users as $user):
            foreach($user->estados as $estado):
                $this->status->push(new EstadoResource($estado)); 
            endforeach;
        endforeach;
    }

    public function store(Request $request){
        $request->request->add(['owner_id' => auth()->id()]);
        $estado = Estado::create($request->all());
        EstadoCreado::dispatch($estado->id);
    }

    public function show(Estado $estado){
        return new EstadoResource($estado);
    }

    public function like(Estado $estado){
        $estado->like();
    }

    public function unLike(Estado $estado){
        $estado->unLike();
    }
}
