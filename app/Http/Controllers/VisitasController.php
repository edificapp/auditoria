<?php

namespace App\Http\Controllers;

use Auth;

use App\Visita;
use App\Proyecto;
use Carbon\Carbon;
use App\Formulario;
use App\GFormulario;
use App\AdjuntarVisita;

use App\RespuestasVisita;
use App\formulariosVisita;

use Illuminate\Http\Request;

use App\Imports\VisitaImport;
use Maatwebsite\Excel\Facades\Excel;

class VisitasController extends Controller
{

	
	public function index()
	{
		$visitas_ = Visita::get();
		$visitas = collect([]);
		foreach ($visitas_ as $index => $visita) {
			$proyecto = Proyecto::find($visita->proyecto_id);

			if ($visita->auditor_id != Auth::id() && $proyecto->supervisor_id != Auth::id()) {
				continue;
			}

			$visita->rol = $proyecto->supervisor_id != Auth::id() ? 'Tecnico' : 'Supervisor';    	
			$visitas->push($visita);

		}

		return view('visitas.index');
	}
	
	public function store(Proyecto $proyecto, Request $request){
		$formulario_g_formularios_ids = GFormulario::find($request->g_formulario_id)->formularios_pivot()->pluck('id');
		$new = $proyecto->visitas()->create($request->all() + ['creador_id' => auth()->id()]);
		$this->activityCreate(
			['attribute' => $new],
			"App\Visita",
			$new->id,
			"se ha creado una visita al proyecto {$new->proyecto->nombre}."
		);
		
		return back();
	}
	/*
    public function export() 
    {
        return Excel::download(new LugarProyectosExport, 'lugares_de_Proyecto.xlsx');
    }
	*/
    public function import($proyecto_id, $file) 
    {
    	try {
		    Excel::import(new VisitaImport($proyecto_id),$file);
		} catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
		     $fallas = $e->failures();

		     foreach ($fallas as $falla) {
		         $falla->row(); // fila en la que ocurrió el error
		         $falla->attribute(); // el número de columna o la "llave" de la columna
		         $falla->errors(); // Errores de las validaciones de laravel
		         $falla->values(); // Valores de la fila en la que ocurrió el error.
		     }
		}
        
        //Excel::queueImport(new VisitaImport($proyecto_id),$file);
    }

    public function asignar(Request $request){
    	if($request->tipo_asignacion == 'formulario'){
    		foreach ($request->visita_id as $value) {
    			Visita::find($value)->formularios()->sync($request->formulario);
    		}
    	}else{
    		foreach ($request->visita_id as $value) {
    			Visita::find($value)->inspectores()->sync($request->inspector);
    		}
    	}

    	return back();
    }

    ///***** peticiones ajax
    public function inspectores_ajax($id){
    	$visita = Visita::find($id);
    	return $visita->inspectores->map(function ($item){
            return [
            	'url' => route('curriculum_ver', $item->id),
            	'nombre' => $item->nombre
            ];
        });
    	
    }

    public function formularios_ajax($id){
    	$visita = Visita::find($id);
    	return $visita->formularios->map(function ($item){
            return [
            	'url' => route('formularios.show', $item->id),
            	'nombre' => $item->nombre
            ];
        });
    }

    //******fin peticiones ajax

	public function visita(Request $request,$id) {
		$visita = Visita::findOrFail($id);
		$formularios = formulariosVisita::where('visita_id',$visita->id)->pluck('formulario_id');

		foreach ($formularios as $pos => $formulario_id) {
			$formularios[$pos] = Formulario::findOrFail($formulario_id);
		}

		if (is_null($visita->inicio_visita)) {
			$visita->inicio_visita = Carbon::now('America/Bogota');
			$visita->estado = 'proceso';
			$visita->coordenadas_inicio = $request->pos;
			$visita->save();
		}

		$respvisita = RespuestasVisita::where('visita_id',$visita->id)->pluck('respuesta','campo');

		return view('visitas.formulario',compact('visita','formularios','respvisita'));
	}

	public function visitaSave(Request $request,$id)
	{
		$visita = Visita::findOrFail($id);
		$campos = $request->all();
		$cantidad_campos = count($campos) - 1;
		$cantidad_campos_llenos = 0;

		RespuestasVisita::where('visita_id',$visita->id)->delete();

		foreach ($campos as $campo => $valor) {
			if ($campo == '_token') {
				continue;
			}

			$respvisita = new RespuestasVisita();
			

			if (!is_null($valor)) {
				$cantidad_campos_llenos++;
				$respvisita->campo = $campo;
				$respvisita->respuesta = $valor;
				$respvisita->visita_id = $visita->id;
				$respvisita->save();
			}

		}

		if ($cantidad_campos_llenos < $cantidad_campos) {
			flash("Se ha guardado el formulario pero no se completaron todos los campos.")->success();
			return redirect(route('visitas'));
		} else {
			$visita->fin_visita = Carbon::now('America/Bogota');
			$visita->save();

			flash("Se ha guardado el formulario.")->success();
			return redirect(route('firmar',$visita->id));
		}
	}

	public function firmar($id) {
		$visita = Visita::findOrFail($id);
		$formularios = formulariosVisita::where('visita_id',$visita->id)->pluck('formulario_id');

		foreach ($formularios as $pos => $formulario_id) {
			$formularios[$pos] = Formulario::findOrFail($formulario_id);
		}

		if (is_null($visita->inicio_visita)) {
			$visita->inicio_visita = Carbon::now('America/Bogota');
			$visita->estado = 'proceso';
			$visita->save();
		}

		return view('visitas.firmar',compact('visita','formularios'));
	}

	public function firmadoguardar(Request $request,$id) {
		$visita = Visita::findOrFail($id);
		$visita->imagen = base64_encode($request->dataFirma);
		$visita->estado = 'fin';
		$visita->coordenadas_fin = $request->pos;
		$visita->fin_visita = Carbon::now('America/Bogota');
		$visita->save();

		flash("Se ha finalizado la visita correctamente.")->success();
		return redirect(route('visitas'));
	}

	public function ver(Visita $visita){
		//dd($visita->visita_formularios[0]);
		/*
		dd([
			'N_formularios' => $visita->visita_formularios->count(),
			'N_respuestas_1' => $visita->visita_formularios[0]->respuestas->count(),
			'N_preguntas_1' => count(json_decode($visita->visita_formularios[0]->formulario_g_formulario->formulario->formulario)),
			'form' => json_decode($visita->visita_formularios[0]->formulario_g_formulario->formulario->formulario),
			'resp' => $visita->visita_formularios[0]->respuestas

		]);
		*/
		
		$proyecto = $visita->proyecto;
		return view('proyecto.ejecucion.ver-visita', compact('visita', 'proyecto'));
	}

	public function ver2(Visita $visita) {
		$visita = Visita::findOrFail($id);

		$visita->coordenadas_inicio = str_replace('@', '', $visita->coordenadas_inicio);
		$visita->coordenadas_inicio = str_replace(',18z', '', $visita->coordenadas_inicio);

		$visita->coordenadas_fin = str_replace('@', '', $visita->coordenadas_fin);
		$visita->coordenadas_fin = str_replace(',18z', '', $visita->coordenadas_fin);
		
		$formularios = formulariosVisita::where('visita_id',$visita->id)->pluck('formulario_id');
		foreach ($formularios as $pos => $formulario_id) {
			$formularios[$pos] = Formulario::findOrFail($formulario_id);
		}
			//dd($formularios);

		$respvisita = RespuestasVisita::where('visita_id',$visita->id)->pluck('respuesta','campo');
		
		if ($visita->estado != 'fin') {
			flash("No tiene permitida esta acción.")->error();
			return redirect(route('visitas'));
		}

		return view('visitas.show',compact('respvisita','visita','formularios'));
	}



	public function adjuntar($id){
		return view('visitas.adjuntar',['visita' => Visita::find($id)]);
	}

	public function upload(Request $request){
		//dd($request->all());
		$AdjuntarVisita =  new AdjuntarVisita;
		foreach ($request->photo as $file) {
			$AdjuntarVisita->upload($file, $request->visita_id);
		}
		flash("los archivos se adjuntaron con exito.")->success();
		return back();
	}

	public function updateUpload(Request $request,$id){
		$AdjuntarVisita =  new AdjuntarVisita;
		$adjuntar = AdjuntarVisita::find($id);
		if($request->hasFile('photo')){
			$adjuntar->ruta = $AdjuntarVisita->uploadUpdate($request->photo, $adjuntar);
		}
		$adjuntar->descripcion = $request->descripcion;
		$adjuntar->save();

		flash("el archivo adjunto fue editado con exito.")->success();
		return back();
	}

	public function deleteUpload($id){
		$upload = AdjuntarVisita::find($id)->delete();
		return back();
	}
}
