<?php

namespace App\Http\Controllers;

use App\Proyecto;
use App\Formulario;
use Illuminate\Http\Request;

class ProyectoInformeController extends Controller
{
    public function formularios_informe(Proyecto $proyecto){
        $formularios_id = collect();
        if($proyecto->visitas->count() == 0){
            flash("el proyecto {$proyecto->nombre} no tiene visitas")->warning();
            return back();
        }

        if($proyecto->visitas->filter(function($e){ return $e->completo; })->count() == 0){
            flash("el proyecto {$proyecto->nombre} no tiene visitas completas")->warning();
            return back();
        }

        foreach($proyecto->visitas->filter(function($e){ return $e->completo; }) as $visita):
            foreach($visita->visita_formularios as $vf):
                $formularios_id->push($vf->formulario_g_formulario->formulario->id);
            endforeach; 
        endforeach;

        $formularios = Formulario::whereIn('id', $formularios_id->unique())->get(['id', 'nombre']);

        if($formularios->count() > 1){
            return view('proyecto.ejecucion.informe.seleccionar-formulario', compact('formularios', 'proyecto'));
        }else{
            return redirect()->route('proyecto.informe', [$formularios[0]->id, $proyecto->id]);
        }
    }

    public function informe(Formulario $formulario, Proyecto $proyecto){
        $visitas_completas = $proyecto->visitas->filter(function($e){ return $e->completo; });
        $vr = $visitas_completas->map(function($e){
            return $e->visita_formularios->pluck('formulario_g_formulario.formulario_id');
        });
        $visitas = $visitas_completas->filter(function($e) use($formulario){
            return in_array($formulario->id, $e->visita_formularios->pluck('formulario_g_formulario.formulario_id')->toArray());
        });

        //dd([$formulario->id, $vr, $visitas]);
        $preguntas = $this->filtrarPreguntas(json_decode($formulario->formulario));
        //dd(json_decode($formulario->formulario));
        //dd($preguntas);
        return view('proyecto.ejecucion.informe.informe', compact('proyecto', 'visitas', 'preguntas', 'formulario'));
    }
    
    public function filtrarPreguntas($campos){
        $labels = collect();
        foreach($campos as $campo):
            if($campo->type != 'header'){
                $labels->push($campo->label);
            }
        endforeach;
        return $labels;
    }
}
