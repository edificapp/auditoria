<?php

namespace App\Http\Controllers;
use Auth;
use App\DocumentoPropuesta;

use Illuminate\Http\Request;
use App\Traits\StorageTraits;

class PropuestaDocumentoController extends Controller
{
    use StorageTraits;

    private $model = 'App\DocumentoPropuesta';

    public function store(Request $request){
        
        $create = new DocumentoPropuesta;
        $create->nombre = $request->name;
        // $create->fecha_generacion = $request->fecha_generacion;
        // $create->fecha_vencimiento = $request->fecha_vencimiento;
        $create->ruta = '';
        $create->propuesta_id = $request->propuesta_id;
        $create->save();

        $ruta = $this->rutaPropuesta($request->propuesta_id.'/documentos/'.$create->id);
        $create->ruta = $this->uploadFile($request->documento, $ruta);
        $create->save();

        $this->activityCreate(
            ['attribute' => $create], 
            $this->model, 
            $create->id, 
            "Se ha cargado un nuevo documento a la propuesta {$create->propuesta->nombre}."
        );

        return back();
    }

}
