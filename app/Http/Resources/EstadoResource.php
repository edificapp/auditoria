<?php

namespace App\Http\Resources;

use App\Http\Resources\ComentarioResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EstadoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->id,
                'body' => $this->body,
                'date' => $this->created_at->diffForHumans()
            ],
            'likes'=>[
                'is_liked' => $this->isLiked(),
                'count' => $this->likesCount()
            ],
            'owner' => [
                'id' => $this->owner->id,
                'name' => $this->owner->name,
                'avatar' => $this->owner->avatar_user
            ],
            'comentarios' => $this->comentarios->map(function($item){ return new ComentarioResource($item); }),
        ];
    }
}
