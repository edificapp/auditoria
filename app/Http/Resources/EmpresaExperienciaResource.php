<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmpresaExperienciaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre_contrato' => $this->nombre_contrato, 
            'numero_contrato' => $this->numero_contrato, 
            'valor' => $this->valor_del_contrato, 
            'certificado' => $this->url_certificado == '' ? $this->url_certificado : $this->certificado, 
            'contrato' => $this->url_contrato == '' ? $this->url_contrato : $this->contrato, 
            'empresa_id' => $this->empresa, 
            'entidad_contratante' => $this->entidad_cliente->nombre, 
            'fecha_final' => $this->fecha_final, 
            'fecha_inicial' => $this->fecha_inicial, 
            'codes' => $this->codes 
        ];
    }
}
