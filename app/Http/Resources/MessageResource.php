<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'mensaje' => $this->mensaje,
            'file' => $this->file,
            'archivo' => $this->archivo,
            'user_id' => $this->user_id,
            'message_id' => $this->message_id,
            'user' => $this->user,
            'created_at' => $this->created_at
        ];
    }
}
