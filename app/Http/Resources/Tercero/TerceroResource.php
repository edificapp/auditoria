<?php

namespace App\Http\Resources\Tercero;

use Illuminate\Http\Resources\Json\JsonResource;

class TerceroResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre
        ];
    }
}
