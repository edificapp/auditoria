<?php

namespace App\Http\Resources\Tercero;
use Illuminate\Http\Resources\Json\JsonResource;

class TerceroSedesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'departamento' => $this->ciudad->departamento->nombre,
            'ciudad' => $this->ciudad->nombre,
            'ciudad_id' => $this->ciudad->id,
            'direccion' => $this->direccion,
            'telefono' => $this->telefono,
            'barrio' => $this->barrio,
            'tipo' => $this->tipo,
            'instituto' => !$this->instituto ? 'sin institucion educativa' : $this->instituto->nombre,
            'instituto_id' => !$this->instituto ?  NULL : $this->instituto->id,
            'sedes' => $this->sedes() ? $this->sedes()->count() : 0
        ];
    }
}
