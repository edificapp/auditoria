<?php

namespace App\Http\Resources\Tercero;

use App\Http\Resources\Tercero\TerceroSedesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TerceroProyectoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'cliente_tercero_id' => $this->cliente_tercero_id,
            'nombre' => $this->cliente_tercero->nombre,
            'sedes' =>$this->sedes->map(function($item){ return new TerceroSedesResource($item); }),
            'parametros' => $this->parametros,
            'contarSedes' => $this->sedes->count()
        ];
    }
}
