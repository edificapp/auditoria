<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComentarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->id,
                'body' => $this->body,
                'date' => $this->created_at->diffForHumans()
            ],
            'likes'=>[
                'is_liked' => $this->isLiked(),
                'count' => $this->likesCount()
            ],
            'owner' => [
                'id' => $this->user->id,
                'name' => $this->user->name,
                'avatar' => $this->user->avatar_user
            ],
        ];
    }
}
