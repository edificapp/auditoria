<?php

namespace App\Http\Resources\proyecto;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Tercero\TerceroSedesResource;

class VisitaPersonalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'completo' => $this->completo,
            'fecha' => $this->fecha_visita,
            'sede' => new TerceroSedesResource($this->sede),
            'formularios' => $this->visita_formularios->map(function($i){
                return [
                    'formulario_pivot_id' => $i->id,
                    'completo' => $i->completo,
                    'nombre' => $i->formulario_g_formulario ? $i->formulario_g_formulario->formulario->nombre : 'no tiene',
                   // 'campos' => $i->formulario,
                    'fechas' => [
                        'inicio' => $i->ffhh_inicio,
                        'final' => $i->ffhh_final
                    ],
                    'latitud' => [
                        'inicio' => $i->latitud_inicio,
                        'final' => $i->latitud_final
                    ],
                    'longitud' => [
                        'inicio' => $i->longitud_inicio,
                        'final' => $i->longitd_final
                    ]
                 ];
            }),
        ];
    }
}
