<?php

namespace App\Http\Resources\proyecto;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Tercero\TerceroSedesResource;

class VisitaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fecha_visita' => $this->fecha_visita,
            'inspector' => $this->personal_proyecto->user->name,
            'inspector_id' => $this->personal_proyecto->id,
            'tercero' => $this->sede->tercero_proyecto->cliente_tercero->nombre,
            'tercero_id' => $this->sede->tercero_proyecto->id,
            'completo' => $this->completo,
            'g_formulario' => $this->visita_formularios->count() > 0 ? $this->visita_formularios[0]->formulario_g_formulario->g_formulario_id: NULL,
            'nucleo' => $this->sede->instituto ? $this->sede->instituto->nombre : "Sin {$this->proyecto->tipo_nucleo}",
            'sede' => new TerceroSedesResource($this->sede),
            'sede_nombre' => $this->sede->nombre,
            'departamento' => $this->sede->ciudad->departamento->nombre,
            'municipio' => $this->sede->ciudad->nombre,
        ];
    }
}
