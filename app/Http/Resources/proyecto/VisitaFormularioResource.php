<?php

namespace App\Http\Resources\proyecto;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Tercero\TerceroSedesResource;

class VisitaFormularioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'formulario_pivot_id' => $this->id,
            'completo' => $this->completo,
            'nombre' => $this->formulario_g_formulario->formulario->nombre,
            'campos' => $this->formulario_movil,
            'fechas' => [
                'inicio' => $this->ffhh_inicio,
                'final' => $this->ffhh_final
            ],
            'latitud' => [
                'inicio' => $this->latitud_inicio,
                'final' => $this->latitud_final
            ],
            'longitud' => [
                'inicio' => $this->longitud_inicio,
                'final' => $this->longitd_final
            ]
        ];
    }
}
