<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropuestaUnspscs extends Model
{
    protected $table = "propuesta_unspscs";

    public function code_uns(){
    	return $this->belongsTo('App\CodeUnspsc', 'unspscs_id');
    }

}