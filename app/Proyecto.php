<?php

namespace App;

use App\Scopes\EmpresaScope;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
	protected $table = "proyectos";

	protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new EmpresaScope);
    }

	public function owner(){
		return $this->belongsTo('App\User', 'creador_id');
	}

	 public function contacto(){
    	return $this->belongsTo('App\ClienteTercero', 'contacto_id');
    }

	public function grupo_formularios(){
        return $this->hasMany(GFormulario::class);
    }

	public function visitas(){
		return $this->hasMany(Visita::class)->orderBy('id','ASC');
	}

	public function contratistas(){
		return $this->hasMany('App\ContratistaProyecto');
	}

	public function espectadores(){
		return $this->hasMany('App\EspectadorProyecto');
	}

	/*
	public function terceros(){
		return $this->hasMany('App\TerceroProyecto');
	}
	*/

	public function terceros(){
        return $this->belongsToMany(ClienteTercero::class, 'tercero_proyectos')
        		->where('cliente_terceros.tipo_contacto', 'tercero')
        		->where('tercero_proyectos.activo', true)
				->withPivot('id')
        		->orderBy('id', 'desc');
	}

	public function tercerosPivot(){
		return $this->hasMany(TerceroProyecto::class);
	}

	public function supervisores(){
		return $this->hasMany('App\supervisorProyecto', 'proyecto_id');
	}

	public function documentos_contractuales(){
		return $this->hasMany('App\DocumentoProyecto', 'proyecto_id')->where('tipo', 'contractual')->orderBy('id','desc');
	}

	public function documentos_tecnicos(){
		return $this->hasMany('App\DocumentoProyecto', 'proyecto_id')->where('tipo', 'tecnico')->orderBy('id','desc');
	}

	public function todos_documentos(){
		return $this->hasMany('App\DocumentoProyecto', 'proyecto_id')->orderBy('id','desc');
	}

	public function personal(){
		return $this->hasMany('App\PersonalProyecto', 'proyecto_id')->orderBy('id','desc');
	}

	public function reuniones(){
		return $this->hasMany('App\ReunionProyecto', 'proyecto_id')->orderBy('id','desc');
	}

	public function validar_personal($personal){
		$coordinador = 0;
		$supervisor = 0;
		$inspector = 0;
		foreach ($personal as $persona) {
			$persona->cargo != 'Coordinador' ? :$coordinador++;
			$persona->cargo != 'Supervisor' ? :$supervisor++;
			$persona->cargo != 'Inspector' ? :$inspector++;
		}

		return $coordinador > 0 && $supervisor > 0 && $inspector > 0 ? true : false;
	}
}
