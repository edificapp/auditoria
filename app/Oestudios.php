<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oestudios extends Model
{
    protected $table = "oestudios";

    //relations
    public function curriculum(){
    	return $this->belongsTo('App\Curriculum');
    }

     //mutations
    public function getCertificadoFileAttribute(){
    	if($this->certificado != NULL){
        	return \Storage::url($this->certificado);
    	}else{
    		return $this->certificado;
    	}
    }
}
