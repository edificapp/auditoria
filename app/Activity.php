<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity as ActivityVendor;

class Activity extends ActivityVendor
{
    
    protected $table = 'activity_log';
    protected $dates = [
        'created_at'
    ];

    public function getTimeAttribute(){
        return $this->created_at->addHour(24) <= \Carbon\Carbon::today() ? "{$this->created_at->format('Y-m-d  | g:i A')}" : "Hace ".\Carbon\Carbon::now()->diffForHumans($this->updated_at, true, false, 1); 
    }

    public function oldProperties($attribute){
        $val =  $this->properties[$attribute];
        $keys = array_keys($val);
        $value = array_values($val);
        $result = collect();
        for($i=0; $i<count($keys); $i++):
            if(!is_array($value[$i])):
                $result->push("$keys[$i] : $value[$i]");
            endif;
        endfor;

        return $result;
    }
}


