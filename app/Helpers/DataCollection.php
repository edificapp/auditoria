<?php

namespace App\Helpers;
use Carbon\Carbon;

class DataCollection {
    static function componentes_muestra(){  
    	return collect([
            [
                'tipo' => 'Complementario',
                'data' => ['Lácteo', 'Proteíco', 'Cereal acompañante', 'Fruta']
            ],
            [
                'tipo' => 'Almuerzo',
                'data' => ['Proteíco', 'Cereal (arroz)', 'Tubérculos, raíces, plátanos', 'verdura fria o caliente', 'Fruta', 'Lacteo']
            ],
            [
                'tipo' => 'Industrial',
                'data' => ['Lácteo', 'Derivados del cereal', 'Frutas', 'Azúcares y dulces', 'Quoso']
            ],
            [
                'tipo' => 'Libre',
                'data' => []
            ]
        ]);
    }


    static function rango_edad(){
    	return ['4 - 8 años y 11 meses', '9 - 13 años 11 meses', '14 - 17 años 7 meses'];
    }
}
