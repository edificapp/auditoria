<?php

namespace App\Helpers;

class Helper {
    static function timeOfAgeMonths(string $months)
    {
        if($months > 12){
            $age =  intval($months/12);
            $months = $months%12;
            return "$age años y $months meses";
        }else{
            return "$months meses";
        }
    }

    static function button ($tipo){
        $buttons =  [
            'btn-edit-form' => 'btn btn-sm btn-inverse-info btn-rounded',
            'btn-create-form' => 'btn btn-primary',

        ];
        return $buttons[$tipo];
    }

    static function icon_button($tipo){
        $d =[
            'btn-edit-form' => '<i class="mdi mdi-pencil-box-outline"></i>',
            'btn-create-form' => '<i class="mdi mdi-flask mdi-note-plus"></i>'
        ];

        return $d[$tipo];
    }
}
