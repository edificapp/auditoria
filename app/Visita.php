<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $table = "visitas";
    protected $fillable = ['observacion', 'fecha_visita', 'sede_id', 'proyecto_id', 'creador_id', 'personal_proyecto_id'];

    public function adjuntar(){
    	return $this->hasMany('App\AdjuntarVisita');
    }

    public function proyecto(){
        return $this->belongsTo(Proyecto::class, 'proyecto_id');
    }

    /*
    public function formularios(){
    	return $this->belongsToMany('App\Formulario', 'formularios_visita');
    }
    */
/*
    public function grupo_formularios(){
        return $this->belongsTo(GFormulario::class, 'g_formulario_id');
    }
*/
    public function personal_proyecto(){
        return $this->belongsTo(PersonalProyecto::class, 'personal_proyecto_id');
    }

    public function sede(){
        return $this->belongsTo(TerceroSede::class, 'sede_id');
    }

    public function respuestas(){
        return $this->hasMany(FormularioRespuestas::class);
    }

    public function firmas(){
        return $this->hasMany(FormularioFirmas::class);
    }

    public function formularios_g_formulario(){
        return $this->belongsToMany(FormularioGFormulario::class, 'formulario_visitas')->withPivot('latitud', 'longitud', 'ffhh_inicio', 'ffhh_final');
    }

    public function visita_formularios(){
        return $this->hasMany(FormularioVisita::class, 'visita_id');
    }

    public function muestras(){
        return $this->hasMany(VisitaMuestra::class);
    }

    //functions
    public function visita_formulario_select_respuestas($arrays_id){
        $data = collect();
        $formulario_visita = $this->visita_formularios->filter(function($e) use($arrays_id){
            return in_array($e->formulario_g_formulario_id,$arrays_id);
        })->first();
        foreach($formulario_visita->formulario as $input):
            if(!empty($input->name)):
                if(property_exists($input, 'value')): 
                    $data->push($input->value);
                else:
                    $array_res = collect();
                    foreach($input->values as $value):
                        if($value->selected): 
                            $array_res->push($value->value);
                        endif; 
                    endforeach;
                    $data->push($array_res);
                endif;
            endif;
        endforeach;
        //dd($data);
        return $data;
    }


    //muttations
    public function getCompletoAttribute(){
        $formularios = $this->visita_formularios()->count();
        $completados = $this->visita_formularios->filter(function($i){ return $i->completo == TRUE; })->count();
        return $formularios == $completados ? TRUE : FALSE;
     }

}
