<?php

namespace App;

use App\Traits\HasLikes;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use HasLikes;

    protected $fillable= ['body', 'owner_id'];

    public function owner(){
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function comentarios(){
        return $this->hasMany(Comentario::class)->orderBy('id', 'desc');
    }
}
