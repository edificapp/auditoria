<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Curriculum extends Model
{
    protected $table = 'curriculum';
    protected $fillable = ['usuario_id', 'imagen', 'certificado_estudios', 'nombres'];


    //relations
    public function user(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function estudios(){
    	return $this->hasMany('App\Estudios')->orderBy('fecha_fin', 'Desc');
    }

    public function otrosEstudios(){
    	return $this->hasMany('App\Oestudios')->orderBy('ano', 'Desc');
    }

    public function idiomas(){
    	return $this->hasMany('App\Idiomas');
    }

    public function logros(){
    	return $this->hasMany('App\Logros');
    }

    public function laborales(){
        //return $this->hasMany('App\Elaboral')->orderBy('fterminacion', 'Desc');
        return $this->hasMany('App\Elaboral')->orderBy('laborando', 'Asc')->orderBy('fterminacion', 'Desc');
    }


    //mutations
    public function getCertificadoFileAttribute(){
        if($this->certificado_estudios != NULL)
        {
            return \Storage::url($this->certificado_estudios);
        }else{
            return NULL;
        }
    }

    public function getAvatarAttribute()
    {
        if($this->imagen != NULL)
        {
            return \Storage::url($this->imagen);
        }else{
            $avatar_ui = "https://ui-avatars.com/api/?name={$this->nombres}+{$this->p_apellido}&background=random&rounded=true&format=svg";
            return $this->nombres == NULL ? asset('admin/images/usuario/usuario.jpg') : $avatar_ui;
        }
    }

    public function getFullNameAttribute(){
        return is_null($this->nombres) ? $this->user->name : $this->nombres.' '.$this->p_apellido.' '.$this->s_apellido;
    }

    public function getFullGeneroAttribute(){
        return $this->genero == 'F' ? 'Femenino' : 'Masculino';
    }

    function getEdadAttribute(){
      if(is_null($this->fecha_nacimiento)):
        return 'no tiene fecha de nacimiento';
      else: 
        list($ano,$mes,$dia) = explode("-",$this->fecha_nacimiento);
          $ano_diferencia  = date("Y") - $ano;
          $mes_diferencia = date("m") - $mes;
          $dia_diferencia   = date("d") - $dia;
          if ($dia_diferencia < 0 || $mes_diferencia < 0)
            $ano_diferencia--;
          return "{$ano_diferencia} años.";
      endif;  
    }

}
