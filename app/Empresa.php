<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
	protected $fillable = ['nombre', 'siglas', 'email', 'nit', 'ciudad', 'barrio', 'direccion', 'telefono', 'representante_legal'];
	protected $table = "empresas";
	public $timestamps = false;

	/**
     * Se encarga de retornar el rol del usuario    
     */
    public function usersAll()
    {
        return $this->hasMany('App\User','empresas_id');
    }


    public function usuarios()
    {
        return $this->hasMany('App\User','empresas_id','id')->where('inv_activa', 'no')->where('activo', 'si')->orderBy('name', 'asc');
    }

    public function roles(){
        return $this->hasMany(Role::class);
    }

    public function clientes_terceros(){
        return $this->hasMany('App\ClienteTercero', 'empresa_id');
    }

    public function terceros(){
        return $this->clientes_terceros()->where('tipo_contacto', 'tercero');
    }

    public function contactos(){
        return $this->hasMany('App\Contacto', 'empresa_id');
    }

    public function proveedores(){
        return $this->hasMany('App\Proveedor', 'empresa_id');
    }

/* 6/01/2021
    public function tecnicos()
    {
        return $this->hasMany('App\User','empresas_id','id')->where('roles_id', 4)->where('activo', 'si');
    }

    public function contratistas()
    {
        return $this->hasMany('App\User','empresas_id','id')->where('roles_id', 5)->where('activo', 'si');
    }
*/

    public function experiencias(){
        return $this->hasMany('App\EmpresaExperiencia', 'empresa_id');
    }

    public function documentos(){
        return $this->hasMany('App\EmpresaDocumento', 'empresa_id');
    }

    public function getLogoWebAttribute(){
        if($this->url_logo_web){
            return \Storage::url($this->url_logo_web);
        }else{
            return asset('img/logo_default/default-logo.png');
        }
    }

    public function getLogoMovilAttribute(){
        if($this->url_logo_movil){
            return \Storage::url($this->url_logo_movil);
        }else{
            return asset('img/logo_default/logo_default_movil.png');
        }
    }


    /*****funciones***///

    public function entidades(){
        $clientes = ClienteTercero::all();
        $proveedores = Proveedor::all()->map(function($item) use ($clientes){
           return $clientes->push($item);
        });
        return $clientes->map(function($item){
            return  [
                'select' => $item->select,
                'nombre' => $item->nombre
            ];
        });
    }

}
