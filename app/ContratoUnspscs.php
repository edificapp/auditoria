<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContratoUnspscs extends Model
{
    protected $table = "contrato_unspscs";

    public function code_uns(){
    	return $this->belongsTo('App\CodeUnspsc', 'code_id');
    }
}
