<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contacto extends Model
{
    use Notifiable;

	protected $fillable = ['owner_id', 'nombres', 'apellidos', 'email', 'celular', 'telefono', 'ext', 'cargo', 'empresa_id'];

    public function contacto(){
        return $this->morphTo();
    }

    public function getAvatarAttribute() {
      return "https://ui-avatars.com/api/?name={$this->nombres}+{$this->apellidos}&background=random&rounded=true&format=svg";
    }

    public function getFullNameAttribute(){
    	return $this->nombres.' '.$this->apellidos;
    }
}
