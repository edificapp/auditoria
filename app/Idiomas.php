<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idiomas extends Model
{
    protected $table = "idiomas";

     //relations
    public function curriculum(){
    	return $this->belongsTo('App\Curriculum');
    }

    //mutations
    public function getCertificadoFileAttribute(){
    	if($this->certificado != NULL){
        	return \Storage::url($this->certificado);
    	}else{
    		return $this->certificado;
    	}
    }

    //function
    public function calificacion($valor){
    	if($valor == 'R'){
    		return 'Regular';
    	}else if($valor == 'B'){
    		return 'Bien';
    	}else if($valor == 'MB'){
    		return 'Muy Bien';
    	}else{
    		return '';
    	}
    }

}
