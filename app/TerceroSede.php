<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerceroSede extends Model
{
    protected $fillable = ['nombre', 'ciudad_id', 'barrio', 'direccion', 'telefono', 
                            'ciudad_id', 'tercero_proyecto_id', 'tipo', 'instituto_id'];

    public function tercero_proyecto(){
        return $this->belongsTo(TerceroProyecto::class, 'tercero_proyecto_id');
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class, 'ciudad_id');
    }

    public function instituto(){
        return $this->belongsTo(TerceroSede::class, 'instituto_id');
    }

    public function sedes(){
        return $this->hasMany(TerceroSede::class, 'instituto_id');
    }
}
