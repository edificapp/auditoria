<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitaMuestra extends Model
{
    protected $casts = [
        'pesos' => 'array',
    ];

    public $timestamps = false;

    protected $fillable = ['visita_id', 'tipo_rango', 'pesos'];
    
}
