<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class TipoProfesionImport implements WithMultipleSheets 
{

    public function  __construct()
    {
    }
   
    public function sheets(): array
    {
        return [
            new FirstSheetImportProfesion()
        ];
    }
}
