<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class VisitaImport implements WithMultipleSheets 
{
    protected $proyecto_id;

    public function  __construct($proyecto_id)
    {
        $this->proyecto_id = $proyecto_id;
    }
   
    public function sheets(): array
    {
        return [
            new FirstSheetImport($this->proyecto_id)
        ];
    }
}
