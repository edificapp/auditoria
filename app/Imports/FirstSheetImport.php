<?php

namespace App\Imports;

use App\Visita;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Traits\CiudadTraits;
use Auth;

class FirstSheetImport implements ToModel, WithHeadingRow
{
    use CiudadTraits;

    protected $proyecto_id;

    public function  __construct($proyecto_id)
    {
        $this->proyecto_id = $proyecto_id;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dd($row);
        
        return new Visita([
            'nombre' => $row['nombre'],
            'direccion' => $row['direccion'],
            'ciudad' => $this->departamento_ciudades($row['ciudad'], $row['departamento']),
            'barrio' => $row['barrio'],
            'observacion' => $row['observaciones'],
            'fecha_visita' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha'])->format('Y-m-d'),
            'proyecto_id' => $this->proyecto_id,
            'creador_id' => Auth::user()->id
        ]);
    }



}
