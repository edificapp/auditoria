<?php

namespace App\Imports;

use App\TipoProfesion;
use App\Profesion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;

class FirstSheetImportProfesion implements ToModel, WithHeadingRow
{

    public function  __construct()
    {

    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Profesion([
            'nombre' => $row['nombre'],
            'tipo_profesion_id' => $row['tipo']
        ]);
    }



}
