<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = "ciudades";
    protected $fillable = ['nombre', 'departamento_id'];

    public function departamento(){
        return $this->belongsTo(Departamento::class, 'departamento_id');
    }

    public function getNombreMinusculaAttribute(){
        return strtolower($this->nombre);
    }
}
