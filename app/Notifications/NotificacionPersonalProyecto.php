<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotificacionPersonalProyecto extends Notification implements ShouldQueue
{
    use Queueable;
    protected $nombre_proyecto;
    protected $cargo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($nombre_proyecto, $cargo)
    {
        $this->nombre_proyecto = $nombre_proyecto;
        $this->cargo = $cargo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from("noreply@gapfergon.com", auth()->user()->empresa->nombre)
            ->greeting("Proyecto {$this->nombre_proyecto}")
            ->subject("Te han relacionado al proyecto {$this->nombre_proyecto} como {$this->cargo}")
            ->line("Te han relacionado al proyecto {$this->nombre_proyecto} como {$this->cargo}")
            ->line('Gestor Administrativo y de Proyectos');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'tipo' => "Te han relacionado al proyecto {$this->nombre_proyecto} como {$this->cargo}",
            'receptor_id' => $notifiable->id,
            'url' =>  route('notificaciones'),
            'remitente' => auth()->id(),
        ];
    }
}
