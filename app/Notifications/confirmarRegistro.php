<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class confirmarRegistro extends Notification
{
    use Queueable;
    protected $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

       return (new MailMessage)
            ->from('noreply@gapfergon.com','GAP FERGON')
            ->greeting('Registro en GAP')
            ->subject('Validación de cuenta')
            ->line('Ha iniciado el proceso de tu registro en la plataforma GAP de FERGON Outsourcing S.A.S., da click en el boton')
            ->action('CONTINUAR MI REGISTRO', url($this->url))
            ->line('Gestor Administrativo y de Proyectos');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'tipo' => 'validacion de cuenta',
            'receptor_id' => $notifiable->id,
            'url' => url($this->url)
        ];
    }
}

    