<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EventsTaskContacto extends Notification
{
    use Queueable;
    public $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreply@gapfergon.com','GAP FERGON')
            ->greeting('Registro de Reunión')
            ->subject('Informe de Reunión')
            ->line('Te han invitado a una reunion en la plataforma GAP de FERGON Outsourcing S.A.S., da click en el boton')
            ->action('Ver Invitación a Reunión', $this->url)
            ->line('Gestor Administrativo y de Proyectos');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'tipo' => 'Nuevo evento Agenda',
            'receptor_id' => $notifiable->id,
            'url' => $this->url
        ];
    }
}
