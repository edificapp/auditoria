<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TeamWorkEmailsInvite extends Notification implements ShouldQueue
{
    use Queueable;
    protected $invite;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($invite)
    {
        $this->invite = $invite;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreply@gapfergon.com','GAP FERGON')
            ->greeting('Grupos')
            ->subject('Invitación al grupo'.$this->invite['teamName'])
            ->action('Haga clic aquí para unirse al grupo'.$this->invite['teamName'], route('teams.accept_invite', $this->invite['token']))
            ->line('Gestor Administrativo y de Proyectos');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

                \Log::debug('datatable'); 
        return [
            'tipo' => 'Invitación al grupo'.$this->invite['teamName'],
            'receptor_id' => $notifiable->id,
            'url' =>  route('teams.accept_invite', $this->invite['token']),
            'remitente' => auth()->id(),
        ];
    }
}
