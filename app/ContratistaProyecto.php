<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContratistaProyecto extends Model
{
    public function proyecto(){
    	return $this->belongsTo('App\Proyecto', 'proyecto_id');
    }

    public function radicados(){
    	return $this->hasMany('App\Radicado', 'contratista_proyecto_id');
    }

    public function contratista(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
