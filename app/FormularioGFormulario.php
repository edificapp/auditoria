<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioGFormulario extends Model
{
    protected $fillable = ['g_formulario_id', 'formulario_id'];

    //relaciones

    public function formulario(){
        return $this->belongsTo(Formulario::class);
    }

    public function grupo_formulario(){
        return $this->belongsTo(GFormulario::class, 'g_formulario_id');
    }

    public function visita_formularios(){
        return $this->hasMany(FormularioVisita::class, 'formulario_g_formulario_id');
    }

}
