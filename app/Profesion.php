<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    protected $table = "profesion";
    protected $fillable = ['nombre', 'tipo_profesion_id'];

    public function tipo(){
        return $this->belongsTo(TipoProfesion::class, 'tipo_profesion_id');
    }

    public function estudios(){ 
        return $this->hasMany(Estudios::class, 'profesion_id');
    }
}
