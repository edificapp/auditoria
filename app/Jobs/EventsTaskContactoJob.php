<?php

namespace App\Jobs;

use App\Contacto;
use App\Notifications\EventsTaskContacto;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EventsTaskContactoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recibe_id;
    protected $url;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recibe_id, $url)
    {
        $this->recibe_id = $recibe_id;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Contacto::find($this->recibe_id)->notify(new EventsTaskContacto($this->url));
    }
}
