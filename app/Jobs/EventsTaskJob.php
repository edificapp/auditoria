<?php

namespace App\Jobs;

use App\User;
use App\Notifications\EventsTask;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EventsTaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recibe_id;
    protected $url;
    protected $envia_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recibe_id, $url, $envia_id)
    {
        $this->recibe_id = $recibe_id;
        $this->url = $url;
        $this->envia_id = $envia_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        User::find($this->recibe_id)->notify(new EventsTask($this->envia_id, $this->url));
    }
}
