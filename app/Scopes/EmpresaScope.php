<?php
namespace App\Scopes;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use JWTAuth;

class EmpresaScope implements Scope
{
	public function apply(Builder $builder, Model $model)
	{
		$builder->whereIn(
			'creador_id', 
			auth()->user() 
				? auth()->user()->empresa->usersAll->pluck('id')
				: JWTAuth::parseToken()->authenticate()->empresa->usersAll->pluck('id')
		);
	}
}