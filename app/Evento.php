<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon, Auth;


class Evento extends Model
{
    protected $fillable = ['title','description', 'color', 'lugar', 'owner_id', 'duracion', 'hora', 'fecha','contactos'];
    protected $casts = [
        'contactos' => 'array',
    ];

    public function invitados_user(){
        return $this->belongsToMany('App\User', 'evento_invitados')->where('user_id','!=', Auth::user()->id)->withPivot('acepto');
    }

    public function owner(){
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function evento_invitados_user(){
        return $this->hasMany('App\EventoInvitado', 'evento_id');
    }

    public function getStartAttribute(){
        return $this->fecha.' '.$this->hora.':00';
    }

    public function getEndAttribute(){
        return Carbon::parse($this->fecha.' '.$this->hora.':00')->addMinutes($this->duracion)->format('Y-m-d H:i:s');
    }

    public function getDuracionRealAttribute(){
         $horas = ($this->duracion / 60) % 60;
         $minutos = fmod($this->duracion, 60)  == 0 ? '00' : fmod($this->duracion, 60);
         return $horas < 10 ? '0'.$horas.':'.$minutos : $horas.':'.$minutos;
    }

    public function getOwnerEventAttribute(){
        return $this->owner_id = Auth::user()->id ? true : false;
    }
}
