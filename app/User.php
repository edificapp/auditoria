<?php

namespace App;

use App\Traits\Notifiable;
use App\Notifications\EventsTask;
use Illuminate\Support\Facades\Auth;
use App\Notifications\MyResetPassword;
use Spatie\Permission\Traits\HasRoles;
use Mpociot\Teamwork\Traits\UserHasTeams;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, UserHasTeams;


    protected $guard_name = 'web';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'empresas_id','inv_activa'
    ];

    protected $appends = ['avatar_user'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }    

    /**
     * Se encarga de retornar el rol del usuario
     */

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }

    public function actividades_log()
    {
        return $this->morphMany('App\Activity', 'causer')->where('causer_id', auth()->id())->OrderBy('created_at', 'DESC');
    }

    /*
    public function mensajes(){
        return $this->hasMany('App\Message', 'from');
    }
*/
    //relacion con una empresa
    public function empresa()
    {
        return $this->hasOne('App\Empresa','id','empresas_id');
    }

    public function agenda_eventos(){
        return $this->belongsToMany('App\Evento', 'evento_invitados')->where('acepto', 2);
    }

    public function personal_proyectos(){
        return $this->hasMany(PersonalProyecto::class);
    }

    public function proyectos(){
        return $this->belongsToMany(Proyecto::class, 'personal_proyectos');
    }

    public function proyectos_ejecucion(){
        return $this->belongsToMany(Proyecto::class, 'personal_proyectos')->where('estado', 'ejecucion');
    }

    //relacion creador proyectos
    public function ownerProyectos(){
        return $this->hasMany('App\Proyecto', 'creador_id');
    }

    //relacion supervisor proyectos
    public function supervisorProyectos(){
        return $this->hasMany('App\supervisorProyecto', 'supervisor_id');
    }

    public function espectadorProyecto(){
        return $this->hasMany('App\EspectadorProyecto', 'user_id')->where('activo', 1);
    }

    public function contratistaProyecto(){
        return $this->hasMany('App\ContratistaProyecto', 'user_id')->where('activo', 1);
    }

    //relaciones hoja de vida
    public function curriculum(){
        return $this->hasOne('App\Curriculum', 'usuario_id');
    }

    //relacion formularios
    public function ownerFormularios(){
        return $this->hasMany('App\Formulario', 'creador_id');
    }

    //relacion visitante
    public function auditorVisitas(){
        return $this->hasMany('App\Visita', 'auditor_id');
    }

    public function evento(){
        return $this->belongsTo('App\Evento', 'evento_id');
    }

    //
    public function documentos(){
        return $this->hasMany('App\DocumentoContratista', 'user_id')->where('borrado','0')->OrderBy('created_at','DESC');
    }

    //attribues
    public function getNombreAttribute(){
        return $this->curiculum ? $this->curriculum->full_name : $this->name;
    }

    public function setNombreAttribute($value){
        $this->attributes['name'] = $value;
    }

    public function getAvatarUserAttribute()
    {
        return $this->curriculum ? $this->curriculum->avatar : "https://ui-avatars.com/api/?name={$this->name}&background=random&&rounded=true&format=svg";
    }


    //funciones
    public function validarEspectadorProyecto($proyecto, $user){
        return  EspectadorProyecto::where('proyecto_id', $proyecto)->where('user_id', $user)->where('activo', 1)->first();
    }

    public function validarContratistaProyecto($proyecto, $user){
        return ContratistaProyecto::where('proyecto_id', $proyecto)->where('user_id', $user)->where('activo', 1)->first();
    }


    public function validarSupervisor($proyecto_id){
        $relacion = supervisorProyecto::where('supervisor_id', $this->id)->where('proyecto_id', $proyecto_id)->first();
        if($relacion != null){
            return true;
        }else{
            return false;
        }
    }

    public function estados(){
        return $this->hasMany(Estado::class, 'owner_id');
    }

    public function messages()
    {
        return $this->morphMany(Message::class, 'message');
    }

    public function messages_is_read(){
        return $this->hasMany(Message::class, 'user_id')->where('message_type', 'App\User')->where('message_id', auth()->id())->whereNull('is_read');
    }

    public function team_user(){
        return $this->hasMany(TeamUser::class);
    }

    public function team_user_created_at($team_id){
        return $this->team_user->where('team_id', $team_id)->first()->created_at;
    }
}
