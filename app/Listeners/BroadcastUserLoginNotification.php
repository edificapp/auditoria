<?php

namespace App\Listeners;

use App\Events\UserSessionChanged;
use Illuminate\Auth\Events\Login;
use Spatie\Activitylog\Models\Activity;

class BroadcastUserLoginNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IlluminateAuthEventsLogin  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->subject = 'login';
        $event->description = 'Login Exitoso';

        flash('Hola ' . $event->user->nombre. ', Bienvenido!')->success();
        activity($event->subject)
            ->by($event->user)
            ->log($event->description);

        broadcast(new UserSessionChanged("{$event->user->name} Se ha Conectado a Gap", 'success'));
    }
}
