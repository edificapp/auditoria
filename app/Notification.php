<?php

namespace App;

use Illuminate\Notifications\DatabaseNotification;

class Notification extends DatabaseNotification
{
	 public function receptor(){
        return $this->belongsTo('App\User', 'notifiable_id');
    }

    public function getRemitenteAttribute(){
        return User::find($this->data['remitente']);
    }

    public function getUrlAttribute(){
        return $this->data['url'];
    }

    public function getFechaHumanAttribute(){ 
        return $this->created_at->diffForHumans(); 
    }

    public function getTipoAttribute(){ 
        return $this->data['tipo']; 
    }

}
