<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerceroParametro extends Model
{
    protected $fillable = ['obligacion', 'cantidad', 'cliente_tercero_id'];
}
