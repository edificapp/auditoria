<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoContratista extends Model
{
    protected $table = "documentos_contratistas";
    protected $fillable = [
        'ruta', 'radicado_id'
    ];

    public function radicado(){
    	return $this->belongsTo('App\Radicado');
    }

    public function getNameDocumentAttribute(){
    	$arr = explode('/', $this->ruta);
    	$arrDoc = explode('.', $arr[5]);
    	return collect(['name' => $arrDoc[0], 'type' => $arrDoc[1]]);
    }

    public function getRutaStorageAttribute(){
    	return \Storage::url($this->ruta);
    }

    public function tipo($tipo){
    	if($tipo == 'docx'){
    		return 'mdi-file-word text-primary';
    	}elseif($tipo == 'ppsx'){
    		return 'mdi-file-powerpoint text-danger';
    	}elseif($tipo == 'pdf'){
    		return 'mdi-file-pdf text-danger';
    	}elseif($tipo == 'png' || $tipo == 'jpg'){
    		return 'mdi-file-image text-warning';
    	}elseif($tipo == 'xls'){
    		return 'mdi-file-excel text-success';
    	}else{
    		return 'mdi-file-document text-info';
    	}
    }
}
