<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienciaPropuesta extends Model
{
    protected $table = "experiencia_propuesta";

    public function propuesta(){
    	return $this->belongsTo(ExperienciaPropuesta::class, 'propuesta_id');
    }
}
