<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeUnspsc extends Model
{
    public function EmpresaExperiencia(){
        return $this->belongsToMany('App\EmpresaExperiencia', 'empresa_experiencia_unspscs', 'code_unspsc_id', 'empresa_experiencia_id');
    }
}
