<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspectadorProyecto extends Model
{
    public function proyecto(){
    	return $this->belongsTo('App\Proyecto', 'proyecto_id');
    }
}
