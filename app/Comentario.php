<?php

namespace App;

use App\Traits\HasLikes;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    use HasLikes;

    protected $fillable = ['user_id', 'body', 'estado_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
