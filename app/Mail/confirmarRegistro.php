<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class confirmarRegistro extends Mailable
{
    use Queueable, SerializesModels;

    /* Esta variable recibe la url con el token para continuar el registro*/
    protected $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $titulo = "Registro en www.gapfergon.com";
        $descripcion = "Se te ha registrado en el aplicativo GAP, para continuar con tu registro simplemente debes presionar click sobre el botón que dice 'Continuar mi registro' y completar la información.";
        $texto_boton = "Continuar mi registro";
        $accion = $this->url;

        //return $this->from('')->subject('Verificación de Correo en el Registro de la plataforma GAP')->view('layouts.email',compact('titulo','descripcion','accion','texto_boton'));
        //return $this->subject('Verificación de Correo en el Registro de la plataforma GAP')->view('layouts.email',compact('titulo','descripcion','accion','texto_boton'));
        return $this->view('layouts.email',compact('titulo','descripcion','accion','texto_boton'));
    }
}
