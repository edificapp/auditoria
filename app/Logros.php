<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logros extends Model
{
    protected $table = "logros";

    //relations
    public function curriculum(){
    	return $this->belongsTo('App\Curriculum');
    }

    //mutations
    public function getCertificadoFileAttribute(){
    	if($this->certificado != NULL){
        	return \Storage::url($this->certificado);
    	}else{
    		return $this->certificado;
    	}
    }
}
