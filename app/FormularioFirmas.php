<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioFirmas extends Model
{
    protected $fillable = ['cc', 'tipo_dc', 'nombre', 'ruta', 'formulario_visita_id', 'visita_id'];

    public function formulario_g_formulario(){
        return $this->belongsTo(FormularioGFormulario::class, 'formulario_g_formulario_id');
    }

    public function visita(){
        return $this->belongsTo(Visita::class);
    }

    public function getUrlAttribute(){
        return \Storage::url($this->ruta);
    }
}
