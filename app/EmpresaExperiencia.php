<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Storage;

class EmpresaExperiencia extends Model
{
    protected $fillable = [
        'nombre_contrato', 'numero_contrato','entidad_contratante', 'fecha_inicial', 'fecha_final', 'valor_del_contrato', 'empresa_id'
    ];

    public function getCertificadoAttribute(){
        return Storage::url($this->url_certificado);
    }

    public function getContratoAttribute(){
        return Storage::url($this->url_contrato);
    }

    public function Codes(){
        return $this->belongsToMany('App\CodeUnspsc', 'empresa_experiencia_unspscs', 'empresa_experiencia_id', 'code_unspsc_id');
    }

    public function empresa(){
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }

    public function entidad_cliente(){
        return $this->belongsTo('App\ClienteTercero', 'entidad_contratante');
    }
    
}
