<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Elaboral extends Model
{
    protected $table = 'elaboral';
    //relations
    public function curriculum(){
    	return $this->belongsTo('App\Curriculum');
    }

    //mutations
    public function getCertificadoFileAttribute(){
        return $this->certificado != NULL ? \Storage::url($this->certificado) : $this->certificado;
    }

    public function getTiempoMesesAttribute(){
        $f_final = $this->laborando == 'si' ? Carbon::now() : Carbon::parse($this->fterminacion)->startOfDay();
        $f_inicial = Carbon::parse($this->fingreso)->startOfDay();
        //$age = $f_inicial->diffInYears($f_final);
        return $f_inicial->diffInMonths($f_final);
    }

    public function getDedicacionesAttribute(){
    	if($this->dedicacion == "tc"){
    		return 'Tiempo completo';
    	}else if($this->dedicacion == "mt"){
    		return 'Medio tiempo';
        }else if($this->dedicacion == "tp"){
        	return 'Tiempo parcial';
        }else if($this->dedicacion == "otra"){
        	return $this->dedicacion;
        }
    }

    public function getLaboralOdAttribute(){
    	if($this->dedicacion == 'tc' || $this->dedicacion == 'mt' || $this->dedicacion == 'tp'){
    		return false;
    	}else{
    		return true;
    	}
    }

     public function getVerificarPaisAttribute(){
        return $this->pais == 'Colombia' ? true : false;
    }
}

