<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propuesta extends Model
{
    protected $table = "propuestas";

    public function documentos_propuestas(){
        return $this->hasMany('App\DocumentoPropuesta', 'propuesta_id');
	}
    public function talento_propuesta(){
        return $this->hasMany('App\PropuestaTalentoHumano', 'propuesta_id');
	}

    public function talentos_finalizados(){
        return $this->hasMany(PropuestaTalentoHumano::class, 'propuesta_id')->whereNotNull('competente_id');
    }

    public function code_uns(){
        return $this->hasMany('App\PropuestaUnspscs', 'propuesta_id');
	}
    public function entidad(){
        return $this->belongsTo('App\ClienteTercero', 'entidad_contratante');
	}
    public function contact (){
        return $this->belongsTo('App\Contacto', 'contacto');
	}
    public function experiencia (){
        return $this->hasMany('App\ExperienciaPropuesta', 'propuesta_id');
    }
    
    public function contratos (){
        return $this->hasMany('App\ContratosPropuesta', 'propuesta_id');
    }
    
    public static function checkStatus($id){
        $relacion = 0;
        $propuesta = Propuesta::find($id);

        if(count($propuesta->documentos_propuestas) > 0){
            $relacion = $relacion + 1;
        }
    
        if(count($propuesta->talento_propuesta) > 0){
            $relacion = $relacion + 1;
        }

        if(count($propuesta->code_uns) > 0){
            $relacion = $relacion + 1;
        }
        if(count($propuesta->experiencia) > 0){
            $relacion = $relacion + 1;
        }

        if($relacion == 4){

            // if($propuesta->estado != 'preparacion'){
            //     $propuesta->estado = "preparacion";
            //     $propuesta->save();
            // }
            return true;  
        }
        else{
            return false;
        }

	}

}
