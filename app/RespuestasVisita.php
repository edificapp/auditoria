<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestasVisita extends Model
{
    protected $table = 'visita_respuestas';
}
