<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalProyecto extends Model
{
    protected $fillable=['user_id', 'proyecto_id', 'cargo', 'activo'];

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function proyecto(){
    	return $this->belongsTo('App\Proyecto', 'proyecto_id');
    }

    public function visitas(){
        return $this->hasMany(Visita::class);
    }
}
