<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    protected $table = "lugares";
    protected $fillable = ['name', 'direccion', 'barrio', 'telefono', 'ciudad', 'lugar_id', 'empresa_id', 'tercero_proyecto_id'];

    public function sedes(){
        return $this->hasMany(Lugar::class);
    }
}
