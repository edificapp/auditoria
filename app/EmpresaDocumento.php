<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Storage;

class EmpresaDocumento extends Model
{

	protected $fillable = [
        'nombre', 'fecha_generacion', 'fecha_vencimiento','ruta', 'empresa_id'
    ];


    public function getEstadoAttribute(){

        return  $this->fecha_vencimiento >= Carbon::now()->format('Y-m-d') ? 'Vigente': 'Vencido';
    }

    public function getRutaDocumentoAttribute(){
       return Storage::url($this->ruta);
    }
}
