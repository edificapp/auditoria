<?php
namespace App\Traits;
use Spatie\Activitylog\Contracts\Activity;
use Auth;


trait ActivityTraits
{

   public function activityCreate($properties, $model, $model_id, $description){
    activity()
       ->causedBy(Auth::user()->id)
        ->tap(function(Activity $activity) use($description, $model_id, $model) {
          $activity->subject_id = $model_id;
          $activity->subject_type = $model;
          $activity->description = $description;
          $activity->log_name = 'creado';
       })
       ->withProperties($properties)
       ->log('updated');
   }

   public function activityUpdate($properties, $model, $model_id, $description){
     activity()
       ->causedBy(Auth::user()->id)
       ->tap(function(Activity $activity) use($description, $model_id, $model) {
          $activity->subject_id = $model_id;
          $activity->subject_type = $model;
          $activity->description = $description;
          $activity->log_name = 'actualizado';
       })
       ->withProperties($properties)
       ->log('created');
   }

   public function activityDelete($model, $model_id, $description){
    activity()
       ->causedBy(Auth::user()->id)
        ->tap(function(Activity $activity) use($description, $model_id, $model) {
          $activity->subject_id = $model_id;
          $activity->subject_type = $model;
          $activity->description = $description;
          $activity->log_name = 'eliminado';
       })->log('deleted');
   }

   public function activityOtros($properties, $model, $model_id, $description, $tipo){
    activity()
       ->causedBy(Auth::user()->id)
        ->tap(function(Activity $activity) use($description, $model_id, $model, $tipo) {
          $activity->subject_id = $model_id;
          $activity->subject_type = $model;
          $activity->description = $description;
          $activity->log_name = $tipo;
       })
       ->withProperties($properties)
       ->log('otros');
   }
}
