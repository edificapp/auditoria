<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;


trait StorageTraits
{
    public function uploadFile($file, $newRuta, $oldRuta = ''){
        if($this->existsFile($oldRuta)){
            $this->deleteFile($oldRuta);
        }
        return $file->storeAs('public/'.$newRuta, $file->getclientoriginalname() );
    }

    public function existsFile($ruta){
        return Storage::exists($ruta);
    }

    public function deleteFile($ruta){
        if($this->existsFile($ruta))
            Storage::delete($ruta);
    }

    public function deleteDir(){
        Storage::deleteDirectory($directory);
    }

    public function rutaUser($carpeta){
        return 'users/'.\Auth::user()->id.'/'.$carpeta;
    }

    public function rutaContratista($proyecto, $radicado){
        return 'contratistas/'.\Auth::user()->id.'/'.$proyecto.'/'.$radicado;
    }

    public function rutaTercero($proyecto, $radicado, $tercero){
        return 'Terceros/'.$tercero.'/'.$proyecto.'/'.$radicado;
    }

    public function rutaEmpresa($empresa_id){
        return 'Empresas/'.$empresa_id;
    }

    public function rutaPropuesta($propuesta_od){
        return 'Propuestas/'.$propuesta_od;
    }

    public function ruteProyecto($proyecto_id){
        return 'documentos_proyectos/'.$proyecto_id;
    }

    public function rutaReunion($proyecto_id, $reunion_id){
        return 'documentos_proyectos/'.$proyecto_id.'/'.$reunion_id;
    }
}
