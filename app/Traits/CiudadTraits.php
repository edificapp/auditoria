<?php
namespace App\Traits;

Use App\Departamento;


trait CiudadTraits
{
    public function departamento_ciudades($ciudad_name, $departamento_name){

        $departamento = Departamento::where('nombre', $departamento_name)->first();
        if($departamento){
            $ciudad = $departamento->ciudades->first(function ($value, $key) use ($ciudad_name){
                    return $value->nombre == $ciudad_name;
                });
            return $ciudad->id;
        }
        return 0;
    }
}
