<?php
namespace App\Traits;
use App\Resource;
//App\Traits\ResourceTraits

Class ResourceTraits
{
    public function resource($documents, $carpeta){
         $ruta = $documents->store($carpeta);
         $file = new Resource; 
         $file->ruta = $ruta;
         $file->save();
         return $file->id;
    }

    static function uploadEmail($documents, $carpeta){
         $path = $documents->storeAs(
            $carpeta, $documents->getClientOriginalName()
         );    

         return collect(['path' => $path, 'name' => $documents->getClientOriginalName(), 'type' => $documents->getClientOriginalExtension()]);
    }
    
    static function uploadProcess($documents, $carpeta){
        return $documents->storeAs(
            $carpeta, $documents->getClientOriginalName()
         );    
   }
}
