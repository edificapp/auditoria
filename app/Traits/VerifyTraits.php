<?php
namespace App\Traits;

use App\curriculum;
use Auth;

trait VerifyTraits
{
    //functions
    public function verifyCurriculum(){
        $curriculum = Auth::user()->curriculum;
        if($curriculum == null ){
            $curriculum = new curriculum;
            $curriculum->usuario_id = Auth::user()->id;
            $curriculum->save();
        }
        return $curriculum;
    }
}
