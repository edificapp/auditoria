<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerceroProyecto extends Model
{
    protected $fillable = ['cliente_tercero_id', 'proyecto_id', 'activo'];

    public function cliente_tercero(){
        return $this->belongsTo(ClienteTercero::class, 'cliente_tercero_id');
    } 

    public function proyecto(){
    	return $this->belongsTo('App\Proyecto', 'proyecto_id');
    }

    public function sedes(){
        return $this->hasMany(TerceroSede::class);
    }

    public function parametros(){
        return $this->hasMany(TerceroParametro::class, 'tercero_proyecto_id');
    }

    public function radicados(){
    	return $this->hasMany('App\Radicado', 'tercero_proyecto_id');
    }
    
}
