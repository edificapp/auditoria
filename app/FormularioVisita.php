<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioVisita extends Model
{
    protected $filable = ['visita_id', 'formulario_g_formulario_id', 'ffhh_inicio', 'latitud_inicio', 'longitud_inicio', 'ffhh_final', 'latitud_final', 'longitud_final'];

    public function respuestas(){
        return $this->hasMany(FormularioRespuestas::class, 'formulario_visita_id');
    }

    public function visita(){
        return $this->belongsTo(Visita::class);
    }

    public function firmas(){
        return $this->hasMany(FormularioFirmas::class, 'formulario_visita_id');
    }

    public function formulario_g_formulario(){
        return $this->belongsTo(FormularioGFormulario::class, 'formulario_g_formulario_id');
    }

    //mutations
    public function getCompletoAttribute(){
        return $this->firmas()->count() > 0 ? TRUE:FALSE; 
    }

    public function getFormularioAttribute(){
        $formulario = json_decode($this->formulario_g_formulario->formulario->formulario);
        if($this->respuestas->count() > 0):
            $value_simple = ['text', 'textarea','time', 'number'];
            $value_multiple = ['radio-group', 'checkbox-group', 'select'];
            $respuestas = $this->respuestas->pluck('res', 'name')->toArray();
            foreach($formulario as  $input):
                if(!empty($input->name)):
                    $res = $respuestas[$input->name];
                    if($input->type == 'date'): 
                        $input->value = date('Y-m-d', strtotime($res[0]));
                    elseif(in_array($input->type, $value_simple)):
                        $input->value = $res[0];
                    elseif(in_array($input->type, $value_multiple)):
                        foreach($input->values as $i => $value):
                            $value->selected = $res[$i] > 0 ? TRUE: FALSE;
                        endforeach;
                    endif;
                endif;
            endforeach;
        endif;
        return $formulario;
    }

    public function getFormularioMovilAttribute(){
        $formulario = json_decode($this->formulario_g_formulario->formulario->formulario);
        foreach($formulario as  $input):
            $input->label = strip_tags($input->label);
        endforeach;
        if($this->respuestas->count() > 0 && $this->completo):
            $value_simple = ['text', 'textarea','time', 'number'];
            $value_multiple = ['radio-group', 'checkbox-group', 'select'];
            $respuestas = $this->respuestas->pluck('res', 'name')->toArray();
            foreach($formulario as  $input):
                $input->label = strip_tags($input->label);
                if(!empty($input->name)):
                    $res = $respuestas[$input->name];
                    if($input->type == 'date'): 
                        $input->value = date('Y-m-d', strtotime($res[0]));
                    elseif(in_array($input->type, $value_simple)):
                        $input->value = $res[0];
                    elseif(in_array($input->type, $value_multiple)):
                        foreach($input->values as $i => $value):
                            $value->selected = $res[$i] > 0 ? TRUE: FALSE;
                        endforeach;
                    endif;
                endif;
            endforeach;
        endif;
        return $formulario;
    }
}
