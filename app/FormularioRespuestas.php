<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioRespuestas extends Model
{
    protected $fillable = ['name', 'formulario_visita_id', 'res', 'visita_id'];
    protected $casts = ['res' => 'array'];

    public function formulario_g_formulario(){
        return $this->belongsTo(FormularioGFormulario::class, 'formulario_g_formulario_id');
    }

    public function visita(){
        return $this->belongsTo(Visita::class);
    }

    public function setResAttribute($value){
        $this->attributes['res'] = json_encode($value);
    }

}
