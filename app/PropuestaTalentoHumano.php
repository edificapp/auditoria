<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropuestaTalentoHumano extends Model
{
    protected $table = "propuesta_talento_humano";

    protected $casts = [
        'proyectos' => 'array'
    ]; 

    public function propuesta(){
        return $this->belongsTo(Propuesta::class, 'propuesta_id');
    }

    public function profesion(){
        return $this->belongsTo(Profesion::class, 'profesion_id');
    }

    public function competente(){
        return $this->belongsTo(User::class, 'competente_id');
    }
}
