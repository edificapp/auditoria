<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['user_id', 'message_id', 'file', 'mensaje', 'name_file', 'message_type'];
    protected $appends = ['archivo'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    /*
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    */

    public function message()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function setFileAttribute($value){
        $this->attributes['file'] = \Storage::url($value);
    }

    public function getArchivoAttribute(){
        $array_f = explode(".", $this->name_file);
        return count($array_f) > 1 ? $array_f: [$this->name_file, '???'];
    }

    public function scopeMessageIsRead($query, $user)
    {
        return $query->where('user_id', $user->id)
                     ->where('message_type', 'App\User')
                     ->where('message_id', auth()->id());
    }
}
