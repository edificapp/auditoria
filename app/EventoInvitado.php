<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventoInvitado extends Model
{
	protected $fillable = ['user_id', 'evento-id', 'acepto'];
     protected $appends = ['estado'];

    public function invitado(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function getEstadoAttribute(){
        return $this->activo == 1 ? 'bg-success' : 'bg-danger';
    }

}
