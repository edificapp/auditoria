<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProfesion extends Model
{
    protected $table = "tipo_profesion";
    protected $fillable = ['nombre'];

    public function profesiones(){
        return $this->hasmany(Profesion::class, 'tipo_profesion_id');
    }
}
