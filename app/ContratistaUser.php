<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratistaUser extends Model
{
    //
    protected $table = "contratista_users";

    protected $fillable = [
        'contratista_id', 'user_id'
    ];
}
