<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GFormulario extends Model
{
    protected $fillable = ['name', 'proyecto_id', 'tipo_muestra', 'muestra_minima', 'muestra_maxima', 'muestra_obligatorio', 'peso_esperados'];

    protected $casts = [
        'peso_esperados' => 'array',
    ];

    public function formularios(){
        return $this->belongsToMany(Formulario::class, 'formulario_g_formularios');
    }
    
    public function formularios_pivot(){
        return $this->hasMany(FormularioGFormulario::class, 'g_formulario_id');
    }

    public function proyecto(){
        return $this->belongsTo(Proyecto::class);
    }
}
