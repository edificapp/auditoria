<?php

namespace App\Exports;

use App\LugarProyecto;
use Maatwebsite\Excel\Concerns\FromCollection;

class LugarProyectosExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return LugarProyecto::all();
    }
}
