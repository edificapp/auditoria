<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tercero extends Model
{
    public function validarTerceroP($proyecto, $tercero){
        return TerceroProyecto::where('proyecto_id', $proyecto)->where('tercero_id', $tercero)->where('activo', 1)->first();
    }

    public function empresa(){
    	return $this->belongsTo('App\Empresa', 'empresa_id');
    }
}
