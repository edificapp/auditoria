<?php

namespace App;

use Mpociot\Teamwork\TeamworkTeam;

class Team extends TeamworkTeam
{
    protected $appends = ['avatar'];

    public function getAvatarAttribute()
    {
        return "https://ui-avatars.com/api/?name={$this->name}&background=random&&rounded=true&format=svg";
    }

    public function messages()
    {
        return $this->morphMany(Message::class, 'message');
    }

    public function messages_is_read(){
        return $this->morphMany(Message::class, 'message')->whereNull('is_read');
    }

}
