<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteTercero extends Model
{
    protected $fillable = ['nombre','contacto', 'tipo_documento', 'numero_documento',
                            'direccion', 'telefono_contacto', 'tipo_contacto','email',
                            'empresa_id', 'activo'];

    public function contacto(){
        return $this->belongsTo('App\Contacto', 'contacto_id');
    }
    public function getCertificadoBancarioPublicAttribute(){
    	return \Storage::url($this->certificado_bancarios);
    }

    public function contactos()
    {
        return $this->morphMany('App\Contacto', 'contacto');
    }

    public function getSelectAttribute(){
        return 'cliente-'.$this->id;
    }

    public function proyectos(){
        return $this->belongsToMany(Proyecto::class, 'tercero_proyectos');
    }

    public function proyectosId($proyecto_id){
        $array_proyectos = $this->proyectos()->pluck('id');
        return in_array($proyecto_id, $array_proyectos);
    }

}
