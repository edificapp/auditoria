<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
        });

        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('nit')->unique();
            $table->string('nombre')->unique();
            $table->string('email')->nullable();
            $table->enum('activo', ['si', 'no'])->default('si');
        });

        if (Schema::hasTable('roles') && Schema::hasTable('empresas')) {
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                
                $table->unsignedBigInteger('roles_id');
                $table->unsignedBigInteger('empresas_id')->nullable();

                $table->string('usuario')->unique()->nullable();
                $table->string('nombre');
                $table->string('email')->unique();

                $table->enum('activo', ['si', 'no'])->default('si');
                $table->enum('inv_activa', ['si', 'no'])->default('no');

                $table->string('password')->nullable();

                $table->rememberToken();
                $table->timestamps();

                $table->foreign('roles_id')->references('id')->on('roles');
                $table->foreign('empresas_id')->references('id')->on('empresas');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('empresas');
    }
}
