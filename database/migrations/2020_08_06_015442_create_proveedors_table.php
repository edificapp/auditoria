<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('nombre_empresa')->nullable();
            $table->enum('tipo_documento', ['Cedula de Ciudadania', 'Cedula de Extranjeria', 'Nit'])->nullable();
            $table->string('numero_documento')->nullable();
            $table->string('rut')->nullable();
            $table->string('certificado_bancarios')->nullable();
            $table->boolean('activo')->default(1);
            $table->integer('empresa_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedors');
    }
}
