<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienciaPropuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencia_propuesta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contrato_cantidad');
            $table->integer('propuesta_id');
            $table->integer('contractual_id');
            $table->string('tiempo_anos');
            $table->string('tiempo_meses');
            $table->double('monto_smlv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencia_propuesta');
    }
}
