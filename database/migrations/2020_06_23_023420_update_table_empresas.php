<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->text('direccion')->after('email')->nullable();
            $table->text('barrio')->after('direccion')->nullable();
            $table->text('ciudad')->after('barrio')->nullable();
            $table->integer('telefono')->after('ciudad')->nullable();
            $table->text('representante_legal')->after('telefono')->nullable();
            $table->text('url_logo_web')->after('telefono')->nullable();
            $table->text('url_logo_movil')->after('telefono')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('direccion');
            $table->dropColumn('barrio');
            $table->dropColumn('ciudad');
            $table->dropColumn('telefono');
            $table->dropColumn('representante_legal');
            $table->dropColumn('url_logo_pie');
            $table->dropColumn('url_logo_web');
            $table->dropColumn('url_logo_movil');
        });
    }
}
