<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorreccionAuditorVisita extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('supervisores_proyecto');
        Schema::dropIfExists('formularios_visita');

        Schema::create('supervisores_proyecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('proyecto_id')->nullable();
            $table->unsignedBigInteger('supervisor_id')->nullable();

            $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade');
            $table->foreign('supervisor_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('formularios_visita', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('visita_id')->nullable();
            $table->unsignedBigInteger('formulario_id')->nullable();

            $table->foreign('visita_id')->references('id')->on('visitas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supervisores_proyecto');
        Schema::dropIfExists('formularios_visita');
    }
}
