<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableVisitaAndProyecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proyectos', function (Blueprint $table) {
            $table->string('tipo_terceros')->default('Operador')->after('fecha_creacion_propuesta');
            $table->string('tipo_lugar')->default('Sede')->after('tipo_terceros');
            $table->boolean('tipo_nucleo')->default(false)->after('tipo_lugar');
        });

        Schema::table('visitas', function (Blueprint $table) {
            $table->dropColumn('auditor_id');
            $table->dropColumn('ciudad');
            $table->dropColumn('barrio');
            $table->dropColumn('direccion');
            $table->integer('sede_id')->unsigned()->after('observacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proyectos', function (Blueprint $table) {
            $table->dropColumn('tipo_terceros');
            $table->dropColumn(' tipo_lugar');
            $table->dropColumn('tipo_nucleo');
        });

        Schema::table('visitas', function (Blueprint $table) {
            $table->dropColumn('tercero_id');
            $table->string('barrio')->nullable()->after('fecha_visita');
            $table->string('direccion')->nullable()->after('fecha_visita');
            $table->integer('ciudad')->nullable()->after('fecha_visita');
            $table->integer('lugar_id')->unsigned();
        });
    }
}
