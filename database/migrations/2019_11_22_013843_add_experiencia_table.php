<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExperienciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elaboral', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('curriculum_id');

            $table->string('empresa')->nullable();
            $table->string('dedicacion')->nullable();
            $table->string('tipo')->nullable();
            $table->string('cargo')->nullable();
            $table->string('dependencia')->nullable();
            $table->string('pais')->nullable();
            $table->string('departamento')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefonos')->nullable();
            $table->date('fingreso')->nullable();
            $table->date('fterminacion')->nullable();
            $table->string('certificado')->nullable();
            
            $table->timestamps();

            $table->foreign('curriculum_id')->references('id')->on('curriculum')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elaboral');
    }
}
