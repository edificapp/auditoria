<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableVisitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visitas', function (Blueprint $table) {
            $table->integer('g_formulario_id')->unsigned()->after('proyecto_id');
            $table->integer('personal_proyecto_id')->unsigned()->after('proyecto_id');
             $table->dropColumn('nombre');
        });

        Schema::table('g_formularios', function (Blueprint $table) {
            $table->boolean('activo')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visitas', function (Blueprint $table) {
            $table->dropColumn('g_formulario_id');
            $table->dropColumn('personal_proyecto_id');
             $table->string('g_formulario_id')->after('proyecto_id')->nullable();
        });

        Schema::table('g_formularios', function (Blueprint $table) {
            $table->dropColumn('activo');
        });
    }
}
