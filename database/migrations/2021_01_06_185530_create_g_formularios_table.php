<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_formularios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('empresa_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('formulario_g_formularios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('g_formulario_id');
            $table->integer('formulario_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_formularios');
        Schema::dropIfExists('formulario_g_formularios');
    }
}
