<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('email');
            $table->bigInteger('celular');
            $table->integer('telefono')->nullable();
            $table->string('ext')->nullable();
            $table->string('cargo');
            $table->boolean('activo')->default(1);
            $table->integer('owner_id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            $table->integer('contacto_id')->unsigned();
            $table->string('contacto_type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
