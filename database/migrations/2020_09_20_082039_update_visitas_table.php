<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visitas', function (Blueprint $table) {
            $table->string('barrio')->nullable()->after('fecha_visita');
            $table->string('direccion')->nullable()->after('fecha_visita');
            $table->integer('ciudad')->nullable()->after('fecha_visita');
            $table->text('observacion')->nullable()->after('fecha_visita');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visitas', function (Blueprint $table) {
            $table->dropColumn('direccion');
            $table->dropColumn('ciudad');
            $table->dropColumn('observacion');
        });
    }
}
