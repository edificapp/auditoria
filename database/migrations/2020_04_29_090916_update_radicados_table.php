<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRadicadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('radicados', function (Blueprint $table) {
            $table->Integer('tercero_proyecto_id')->nullable()->after('contratista_proyecto_id');
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('radicados', function (Blueprint $table) {
            $table->dropColumn('tercero_proyecto_id');
        });
    }
}
