<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurriculum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculum', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usuario_id');

            $table->string('imagen')->nullable();

            $table->enum('genero', ['F', 'M'])->nullable();
            $table->string('nombres')->nullable();
            $table->string('p_apellido')->nullable();
            $table->string('s_apellido')->nullable();
            $table->enum('nacionalidad', ['COL', 'DOB', 'EXT'])->nullable();
            $table->string('pais_extranjero')->nullable();
            $table->enum('dni_nal_tipo',['CC','NIT','OTRO'])->nullable();  // tipo documento de identificacion nacional
            $table->string('dni_nal')->nullable();  // documento de identificacion nacional
            $table->enum('libreta', ['1A', '2A'])->nullable();
            $table->string('numero_libreta')->nullable();
            $table->string('dm')->nullable(); // distrito militar
            $table->string('dni_ext_tipo')->nullable();  // tipo documento de identificacion extranjero
            $table->string('dni_ext')->nullable();  // documento de identificacion extranjero
            $table->string('nacimiento_municipio')->nullable();
            $table->string('nacimiento_departamento')->nullable();
            $table->string('nacimiento_pais')->nullable();
            $table->string('telefonos')->nullable();

            $table->timestamps();

            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculum');
    }
}
