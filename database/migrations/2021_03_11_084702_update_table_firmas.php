<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableFirmas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('formulario_respuestas', function (Blueprint $table) {
            $table->dropColumn('formulario_g_formulario_id');
            $table->integer('formulario_visita_id')->unsigned()->after('id');
        });

        Schema::table('formulario_firmas', function (Blueprint $table) {
            $table->dropColumn('formulario_g_formulario_id');
            $table->integer('formulario_visita_id')->unsigned()->after('id');
        });

        Schema::table('visitas', function (Blueprint $table) {
            $table->dropColumn('g_formulario_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formulario_respuestas', function (Blueprint $table) {
            $table->dropColumn('formulario_visita_id');
            $table->integer('formulario_g_formulario_id')->unsigned()->after('id');
        });

        Schema::table('formulario_firmas', function (Blueprint $table) {
            $table->dropColumn('formulario_visita_id');
            $table->integer('formulario_g_formulario_id')->unsigned()->after('id');
        });

        Schema::table('visitas', function (Blueprint $table) {
            $table->integer('g_formulario_id')->unsigned()->after('id');
        });
    }
}
