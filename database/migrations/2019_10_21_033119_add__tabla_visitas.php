<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablaVisitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('creador_id');
            $table->unsignedBigInteger('proyecto_id')->nullable();

            $table->string('nombre');
            $table->dateTime('fecha_visita');
            $table->enum('activo', ['si', 'no'])->default('si');

            $table->index(['id', 'creador_id']);

            $table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('auditores_visita', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('visita_id')->nullable();
            $table->unsignedBigInteger('auditor_id')->nullable();

            $table->foreign('visita_id')->references('id')->on('visitas')->onDelete('cascade');
            $table->foreign('auditor_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('formularios_visita', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('visita_id')->nullable();
            $table->unsignedBigInteger('formulario_id')->nullable();

            $table->foreign('visita_id')->references('id')->on('visitas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
