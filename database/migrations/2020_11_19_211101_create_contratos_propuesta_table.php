<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratosPropuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos_propuesta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('tiempo_anos');
            $table->string('tiempo_meses');
            $table->double('monto');
            $table->integer('propuesta_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos_propuesta');
    }
}
