<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableEstudios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('curriculum_id');

            $table->enum('modalidad', ['TC','TL','TE','UN','ES','MG','DC'])->nullable();
            $table->string('estudio')->nullable();
            $table->string('escuela')->nullable();
            $table->enum('finalizado', ['si','no'])->nullable();
            $table->string('semestres')->nullable();
            $table->string('fecha_fin')->nullable();
            $table->string('tarjeta')->nullable(); /* Tarjeta profesional */
            $table->string('certificado')->nullable();

            $table->timestamps();

            $table->foreign('curriculum_id')->references('id')->on('curriculum')->onDelete('cascade');
        });

        Schema::create('oestudios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('curriculum_id');

            $table->string('estudio')->nullable();
            $table->string('escuela')->nullable();
            $table->string('ano')->nullable();
            $table->string('horas')->nullable();
            $table->string('certificado')->nullable();
            
            $table->timestamps();

            $table->foreign('curriculum_id')->references('id')->on('curriculum')->onDelete('cascade');
        });

        Schema::create('logros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('curriculum_id');

            $table->string('logro')->nullable();
            $table->string('certificado')->nullable();
            
            $table->timestamps();

            $table->foreign('curriculum_id')->references('id')->on('curriculum')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
        Schema::dropIfExists('oestudios');
        Schema::dropIfExists('logros');
    }
}
