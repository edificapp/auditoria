<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsCurriculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('curriculum', function (Blueprint $table) {
            $table->string('correspondencia_municipio')->nullable();
            $table->string('correspondencia_departamento')->nullable();
            $table->string('correspondencia_pais')->nullable();
            $table->string('grado')->nullable();
            $table->string('titulo')->nullable();
            $table->string('escuela')->nullable();
            $table->string('ciudad_escuela')->nullable();
            $table->string('fecha_fin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curriculum', function (Blueprint $table) {
            $table->dropColumn('correspondencia_municipio');
            $table->dropColumn('correspondencia_departamento');
            $table->dropColumn('correspondencia_pais');
            $table->dropColumn('grado');
            $table->dropColumn('titulo');
            $table->dropColumn('escuela');
            $table->dropColumn('ciudad_escuela');
            $table->dropColumn('fecha_fin');
        });
    }
}
