<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormularioVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario_visitas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('visita_id')->unsigned();
            $table->integer('formulario_g_formulario_id')->unsigned();
            $table->string('latitud_inicio')->nullable();
            $table->string('longitud_inicio')->nullable();
            $table->dateTime('ffhh_inicio')->nullable();
            $table->string('latitud_final')->nullable();
            $table->string('longitud_final')->nullable();
            $table->dateTime('ffhh_final')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulario_visitas');
    }
}
