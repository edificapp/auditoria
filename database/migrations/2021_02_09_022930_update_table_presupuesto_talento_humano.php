<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTablePresupuestoTalentoHumano extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propuesta_talento_humano', function (Blueprint $table) {
            $table->integer('competente_id')->unsigned()->nullable()->after('salud');
            $table->json('proyectos')->nullable()->after('competente_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('propuesta_talento_humano', function (Blueprint $table) {
            $table->dropColumn('competente_id');
            $table->dropColumn('proyectos');
        });
    }
}
