<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableEvento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventos', function (Blueprint $table) {
            $table->date('fecha')->after('color');
            $table->string('hora')->after('color');
            $table->integer('duracion')->after('color');
            $table->integer('owner_id')->after('color');
            $table->string('lugar')->after('color');
        });

        Schema::table('evento_invitados', function (Blueprint $table) {
            $table->boolean('acepto')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventos', function (Blueprint $table) {
            $table->dropColumn('owner_id');
            $table->dropColumn('lugar');
            $table->dropColumn('fecha');
            $table->dropColumn('hora');
            $table->dropColumn('duracion');
        });

        Schema::table('evento_invitados', function (Blueprint $table) {
            $table->dropColumn('acepto');
        });
    }
}
