<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableFormularioRespuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('formulario_respuestas', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->integer('formulario_g_formulario_id')->after('name');
            $table->integer('visita_id')->after('formulario_g_formulario_id');
            $table->dropColumn('campo_id');
        });

        Schema::table('formulario_firmas', function (Blueprint $table) {
            $table->string('cc')->after('id');
            $table->string('nombre')->after('cc');
            $table->integer('visita_id')->after('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formulario_respuestas', function (Blueprint $table) {
            $table->integer('campo_id')->after('id');
            $table->dropColumn('formulario_g_formulario_id');
            $table->dropColumn('visita_id');
            $table->dropColumn('name');
        });

        Schema::table('formulario_firmas', function (Blueprint $table) {
            $table->dropColumn('cc');
            $table->dropColumn('nombre');
            $table->dropColumn('visita_id');

        });
    }
}
