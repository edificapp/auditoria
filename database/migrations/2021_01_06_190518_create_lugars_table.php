<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLugarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('ciudad_id')->unsigned();
            $table->string('direccion')->nullable();
            $table->string('barrio')->nullable();
            $table->string('telefono')->nullable();
            $table->integer('lugar_id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            $table->integer('tercero_proyecto_id')->unsigned();
            $table->boolean('estado')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lugares');
    }
}
