<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableElaboral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('elaboral', function (Blueprint $table) {
            $table->string('funciones')->nullable()->after('laborando');
            $table->string('proyectos')->nullable()->after('funciones');
        });

        Schema::table('curriculum', function (Blueprint $table) {
            $table->date('fecha_nacimiento')->nullable()->after('nacimiento_pais');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elaboral', function (Blueprint $table) {
            $table->dropColumn('funciones');
            $table->dropColumn('proyectos');
        });

        Schema::table('curriculum', function (Blueprint $table) {
            $table->dropColumn('fecha_nacimiento');
        });
    }
}
