<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableEstudio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estudios', function (Blueprint $table) {
            $table->dropColumn('modalidad');
            $table->dropColumn('estudio');
            $table->integer('profesion_id')->unsigned()->after('curriculum_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estudios', function (Blueprint $table) {
            $table->text('estudio')->after('curriculum_id');
            $table->text('modalidad')->after('estudio');
            $table->dropColumn('profesion_id');
        });
    }
}
