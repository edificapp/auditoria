<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTercerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_terceros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->enum('tipo_documento', ['Cedula de Ciudadania', 'Cedula de Extranjeria', 'Nit']);
            $table->string('numero_documento');
            $table->string('direccion')->nullable();
            $table->string('certificado_bancario')->nullable();
            $table->string('telefono_contacto')->nullable();
            $table->enum('tipo_contacto', ['cliente', 'tercero']);
            $table->boolean('activo')->default(1);
            $table->integer('empresa_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_terceros');
    }
}
