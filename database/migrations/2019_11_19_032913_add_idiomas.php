<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdiomas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idiomas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('curriculum_id');

            $table->string('idioma')->nullable();
            $table->enum('habla', ['R','B','MB'])->nullable();
            $table->enum('lee', ['R','B','MB'])->nullable();
            $table->enum('escribe', ['R','B','MB'])->nullable();
            $table->string('observacion')->nullable();
            $table->string('certificado')->nullable();
            
            $table->timestamps();

            $table->foreign('curriculum_id')->references('id')->on('curriculum')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idiomas');
    }
}
