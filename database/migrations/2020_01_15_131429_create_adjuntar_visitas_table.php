<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjuntarVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjuntar_visitas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ruta');
            $table->text('descripcion')->nullable();
            $table->unsignedBigInteger('visita_id');
            $table->foreign('visita_id')->references('id')->on('visitas')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjuntar_visitas');
    }
}
