<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropuestaTalentoHumanoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propuesta_talento_humano', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cargo');
            $table->integer('propuesta_id');
            $table->integer('tipo_id');
            $table->integer('profesion_id');
            $table->string('dedicacion');
            $table->string('experiencia_especifica');
            $table->integer('cantidad_contratos');
            $table->string('experiencia_general_años');
            $table->string('cantidad');
            $table->enum('salud', ['si','no'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propuesta_talento_humano');
    }
}
