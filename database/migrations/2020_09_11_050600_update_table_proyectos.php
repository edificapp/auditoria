<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableProyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proyectos', function (Blueprint $table) {
            $table->enum('estado', ['adjudicado', 'ejecucion', 'cerrado'])->default('adjudicado')->after('activo');
            $table->string('n_proceso')->nullable()->after('estado');
            $table->date('fecha_creacion_propuesta')->nullable()->after('n_proceso');
            $table->date('fecha_inicial_entrega')->nullable()->after('n_proceso');
            $table->string('entidad_contratante')->nullable()->after('n_proceso');
            $table->string('contacto_id')->nullable()->after('n_proceso');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proyectos', function (Blueprint $table) {
            $table->dropColumn('estado');
            $table->dropColumn('n_proceso');
            $table->dropColumn('fecha_creacion_propuesta');
            $table->dropColumn('fecha_inicial_entrega');
            $table->dropColumn('entidad_contratante');
            $table->dropColumn('contacto_id');
        });
    }
}
