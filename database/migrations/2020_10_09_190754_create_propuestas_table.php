<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propuestas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->integer('numero_proceso');
            $table->integer('creador_id');
            $table->integer('entidad_contratante');
            $table->date('fecha_entrega');
            $table->text('lugar_entrega');
            $table->string('hora_entrega');
            $table->text('contacto');
            $table->enum('estado', ['borrador', 'preparacion', 'presentacion']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propuestas');
    }
}
