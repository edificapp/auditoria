<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReunionProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reunion_proyectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('motivo');
            $table->date('fecha');
            $table->string('lugar');
            $table->integer('proyecto_id');
            $table->integer('owner_id');
            $table->timestamps();
        });

        Schema::create('reunion_proyecto_documentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ruta');
            $table->integer('reunion_proyecto_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reunion_proyectos');
        Schema::dropIfExists('reunion_proyecto_documentos');
    }
}
