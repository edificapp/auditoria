<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaExperienciaUnspscsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_experiencia_unspscs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('empresa_experiencia_id');
            $table->integer('code_unspsc_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_experiencia_unspscs');
    }
}
