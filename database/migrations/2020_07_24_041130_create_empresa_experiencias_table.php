<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaExperienciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_experiencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_contrato');
            $table->string('numero_contrato');
            $table->integer('entidad_contratante');
            $table->date('fecha_inicial');
            $table->date('fecha_final');
            $table->json('codigos_unspsc');
            $table->integer('valor_del_contrato');
            $table->string('url_contrato')->nullable();
            $table->string('url_certificado')->nullable();
            $table->integer('empresa_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_experiencias');
    }
}
