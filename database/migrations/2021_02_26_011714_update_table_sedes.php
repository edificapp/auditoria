<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableSedes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tercero_sedes', function (Blueprint $table) {
            $table->enum('tipo', ['sede', 'instituto'])->after('id');
            $table->integer('instituto')->nullable()->after('tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tercero_sedes', function (Blueprint $table) {
            $table->dropColumn('tipo');
            $table->dropColumn('instituto');
        });
    }
}
