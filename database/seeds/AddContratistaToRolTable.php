<?php

use Illuminate\Database\Seeder;

class AddContratistaToRolTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table('roles')->insert([
    		'nombre' => 'Contratista'
    	]);
    }
}
