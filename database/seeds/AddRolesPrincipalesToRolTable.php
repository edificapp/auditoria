<?php

use Illuminate\Database\Seeder;

class AddRolesPrincipalesToRolTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
    		'nombre' => 'Super Administrador',
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Administrador'
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Coordinador'
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Usuario'
        ]);
    }
}
