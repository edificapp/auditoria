<?php

use Illuminate\Database\Seeder;

class AddUsuarioInicialToUsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
    		'roles_id' => 1,
    		'usuario' => 'p_usuario',
    		'nombre' => 'Primer Usuario',
    		'email' => 'admin@admin.com',
    		'password' => Hash::make('123456')
    	]);
    }
}
