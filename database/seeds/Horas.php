<?php

use App\Oestudios;
use Illuminate\Database\Seeder;

class Horas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        $estudios = Oestudios::all();
        foreach($estudios as $estudio):
             $estudio->hora = $estudio->horas;
             $estudio->save();
        endforeach;
    }
}
