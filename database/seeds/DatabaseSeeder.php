<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([

        // AddUsuarioInicialToUsersTable::class,
        // AddRolesPrincipalesToRolTable::class,
        //AddContratistaToRolTable::class,
        //AddCodeUnspscTable::class,
        Horas::class,

        ]);
    }
}
