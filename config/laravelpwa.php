<?php

return [
    'name' => 'auditoriaPWA',
    'manifest' => [
        'name' => env('APP_NAME', 'Auditoria'),
        'short_name' => 'PWA',
        'start_url' => '/',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation'=> 'any',
        'icons' => [
            '72x72' => '/images/favicon.ico',
            '96x96' => '/images/favicon.ico',
            '128x128' => '/images/favicon.ico',
            '144x144' => '/images/favicon.ico',
            '152x152' => '/images/favicon.ico',
            '192x192' => '/images/favicon.ico',
            '384x384' => '/images/favicon.ico',
            '512x512' => '/images/favicon.ico'
        ],
        'splash' => [
            '640x1136' => '/images/favicon.ico',
            '750x1334' => '/images/favicon.ico',
            '828x1792' => '/images/favicon.ico',
            '1125x2436' => '/images/favicon.ico',
            '1242x2208' => '/images/favicon.ico',
            '1242x2688' => '/images/favicon.ico',
            '1536x2048' => '/images/favicon.ico',
            '1668x2224' => '/images/favicon.ico',
            '1668x2388' => '/images/favicon.ico',
            '2048x2732' => '/images/favicon.ico'
        ],
        'custom' => [
            // '/admin/images/favicon.png'
        ]
    ]
];
